package id.sit.pos.restclient.controller;

import java.lang.reflect.Type;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import id.sit.pos.util.HttpUtil;

public class RestClient<T>{

	private String url;
	private List<Entry<String, Object>> bodyParams = new ArrayList<Entry<String, Object>>();
	private String charset="UTF-8";
	private Class<T> responseClass;
	private Type type;

	public RestClient(String url, Class<T> responseClass) {
		super();
		this.url = url;
		this.responseClass = responseClass;
	}

	public RestClient(String url, Type type) {
		this.url = url;
		this.type = type;
	}

	public T sendGet() throws Exception{
    	String strResponse = "";
		strResponse = HttpUtil.sendGet(url, charset, bodyParams.toArray(new Entry[bodyParams.size()]));

		GsonBuilder builder = new GsonBuilder();
	    builder.excludeFieldsWithoutExposeAnnotation();
	    Gson gson = builder.create();
	    if (responseClass != null)
	    	return gson.fromJson(strResponse, responseClass);
	    else
	    	return gson.fromJson(strResponse, type);
    }

    public T sendPost() throws Exception {
    	String strResponse = "";

		strResponse = HttpUtil.sendPost(url, charset, (Entry<String, Object>[]) bodyParams.toArray());

		GsonBuilder builder = new GsonBuilder();
	    builder.excludeFieldsWithoutExposeAnnotation();
	    Gson gson = builder.create();
	    if (responseClass != null)
	    	return gson.fromJson(strResponse, responseClass);
	    else
	    	return gson.fromJson(strResponse, type);
    }

    public void putBodyParam (String param, Object value) {
    	bodyParams.add(new SimpleEntry<String, Object>(param, value));
    }

    public void removeBodyParam (String param) {
    	bodyParams.remove(param);
    }

    public void clearBodyParam(){
    	bodyParams.clear();
    }

	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getCharset() {
		return charset;
	}


	public void setCharset(String charset) {
		this.charset = charset;
	}


	public Class<T> getResponseClass() {
		return responseClass;
	}


	public void setResponseClass(Class<T> responseClass) {
		this.responseClass = responseClass;
	}
}