package id.sit.pos.user.model;

import java.util.Date;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.master.model.Warehouse;
import id.sit.pos.transaction.model.SodEod;

@DatabaseTable(tableName = "sessions")
public class Session extends RModel{
	public static final String _STORE_ID = "office_id";
	public static final String _MACHINE_ID = "client_id";
	public static final String _USER_ID = "user_id";
	public static final String _SHIFT_NO = "shift_no";
	public static final String _START = "start_time";
	public static final String _END = "end_time";
	public static final String _SODEOD_ID = "sodeod_id";

	@DatabaseField(columnName = _STORE_ID, foreign = true)
	private Warehouse store;

	@DatabaseField(columnName = _MACHINE_ID)
	private String machineId;

	@DatabaseField(columnName = _SHIFT_NO)
	private Integer shiftNo;

	@DatabaseField(columnName = _USER_ID, foreign = true)
	private User user;

	@DatabaseField(columnName = _START)
	private Date start;

	@DatabaseField(columnName = _END)
	private Date end;

	@DatabaseField(columnName = _SODEOD_ID, foreign = true)
	private SodEod sodEod;

	public Session(){

	}

	public Warehouse getStore() {
		return store;
	}

	public void setStore(Warehouse store) {
		this.store = store;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public Integer getShiftNo() {
		return shiftNo;
	}

	public void setShiftNo(Integer i) {
		this.shiftNo = i;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User userLogin) {
		this.user = userLogin;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public SodEod getSodEod() {
		return sodEod;
	}

	public void setSodEod(SodEod sodEod) {
		this.sodEod = sodEod;
	}
}
