package id.sit.pos.transaction.model;

import java.math.BigDecimal;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.customer.model.Member;
import id.sit.pos.master.model.Warehouse;

@DatabaseTable(tableName = "account_receivables")
public class AccountReceivable extends RModel {
	public static final String _AR_NUMBER = "ar_number";
	public static final String _TRANSACTION_ID = "sale_id";
	public static final String _TRANSACTION_NO = "transaction_number";
	public static final String _MEMBER_ID = "member_id";
	public static final String _TOTAL_AMOUNT = "total_amount";
	public static final String _TOTAL_PAID = "total_paid";
	public static final String _OUTSTANDING = "outstanding";
	public static final String _STORE_ID = "branch_id";
	public static final String _PAYMENT_DISC = "payment_discount";
	public static final String _BILL_TO_NAME = "bill_to_name";
	public static final String _BILL_TO_EMAIL = "bill_to_email";
	public static final String _BILL_TO_ADDRESS = "bill_to_address";
	public static final String _BILL_TO_PHONE = "bill_to_phone";

	@DatabaseField(columnName = _AR_NUMBER)
	private String ArNumber;

	@DatabaseField(columnName = _TRANSACTION_ID, foreign = true)
	private Transaction transaction;

	@DatabaseField(columnName = _TRANSACTION_NO)
	private String transactionNo;

	@DatabaseField(columnName = _MEMBER_ID, foreign = true)
	private Member member;

	@DatabaseField(columnName = _TOTAL_AMOUNT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalAmt;

	@DatabaseField(columnName = _TOTAL_PAID, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalPaid;

	@DatabaseField(columnName = _OUTSTANDING, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal outstanding;

	@DatabaseField(columnName = _STORE_ID, foreign = true)
	private Warehouse branch;

	@DatabaseField(columnName = _PAYMENT_DISC)
	private String paymentDisc;

	@DatabaseField(columnName = _BILL_TO_NAME)
	private String billToName;

	@DatabaseField(columnName = _BILL_TO_EMAIL)
	private String billToEmail;

	@DatabaseField(columnName = _BILL_TO_ADDRESS)
	private String billToAddress;

	@DatabaseField(columnName = _BILL_TO_PHONE)
	private String billToPhone;


	public String getArNumber() {
		return ArNumber;
	}

	public void setArNumber(String arNumber) {
		ArNumber = arNumber;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member customer) {
		this.member = customer;
	}

	public BigDecimal getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}

	public BigDecimal getTotalPaid() {
		return totalPaid;
	}

	public void setTotalPaid(BigDecimal totalPaid) {
		this.totalPaid = totalPaid;
	}

	public BigDecimal getOutstanding() {
		return outstanding;
	}

	public void setOutstanding(BigDecimal outstanding) {
		this.outstanding = outstanding;
	}

	public Warehouse getBranch() {
		return branch;
	}

	public void setBranch(Warehouse branch) {
		this.branch = branch;
	}

	public String getPaymentDisc() {
		return paymentDisc;
	}

	public void setPaymentDisc(String paymentDisc) {
		this.paymentDisc = paymentDisc;
	}

	public String getBillToName() {
		return billToName;
	}

	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}

	public String getBillToEmail() {
		return billToEmail;
	}

	public void setBillToEmail(String billToEmail) {
		this.billToEmail = billToEmail;
	}

	public String getBillToAddress() {
		return billToAddress;
	}

	public void setBillToAddress(String billToAddress) {
		this.billToAddress = billToAddress;
	}

	public String getBillToPhone() {
		return billToPhone;
	}

	public void setBillToPhone(String billToPhone) {
		this.billToPhone = billToPhone;
	}
}
