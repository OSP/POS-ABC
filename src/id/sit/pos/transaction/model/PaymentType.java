package id.sit.pos.transaction.model;

public enum PaymentType {
	CASH,
	CREDIT
}
