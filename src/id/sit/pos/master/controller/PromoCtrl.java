package id.sit.pos.master.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.master.model.Product;
import id.sit.pos.master.model.Promo;
import id.sit.pos.master.model.PromoItem;
import id.sit.pos.master.model.PromoPrizeItem;
import id.sit.pos.util.DateUtil;

public class PromoCtrl {
	private static Logger logger = Logger.getLogger(PromoCtrl.class.getName());

	private List<PromoItem> promoItems = new ArrayList<PromoItem>();

	public List<PromoPrizeItem> getItemPrize(Product item) throws SQLException{
		Dao<PromoPrizeItem, Integer> ppiDao = DaoManagerImpl.getPromoPrizeItemDao();
		String query =
				"SELECT pl."+PromoPrizeItem._ID+", pl."+PromoPrizeItem._PRODUCT_ID+", pl."+PromoPrizeItem._MAX_QTY+", pl."+PromoPrizeItem._PROMO_ID +
				" FROM "+PromoPrizeItem.class.getAnnotation(DatabaseTable.class).tableName()+" pl "+
				" INNER JOIN "+Promo.class.getAnnotation(DatabaseTable.class).tableName()+" p ON pl."+PromoPrizeItem._PROMO_ID+" = p.id "+
				" INNER JOIN "+PromoItem.class.getAnnotation(DatabaseTable.class).tableName()+" pil ON pil."+PromoItem._PROMO_ID+" = p.id "+
				" WHERE NOW() BETWEEN p."+Promo._VALID_FROM+" AND p."+Promo._VALID_UNTIL+" AND pil."+PromoItem._PRODUCT_ID+" = "+ item.getId();
		GenericRawResults<PromoPrizeItem> rawResults = ppiDao.queryRaw(query,new RawRowMapper<PromoPrizeItem>(){
			@Override
			public PromoPrizeItem mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				PromoPrizeItem ppi = new PromoPrizeItem();
				int i = 0;
				while (i<columnNames.length) {
					String value = resultColumns[i];
					if (value != null) {
						switch (columnNames[i]) {
						case PromoPrizeItem._ID:
							ppi.setId(new Integer(value));
							break;
						case PromoPrizeItem._PRODUCT_ID:
							ppi.setProduct(new Product(new Integer(value)));
							break;
						case PromoPrizeItem._MAX_QTY:
							ppi.setMaxQty(new Integer(value));
							break;
						case PromoPrizeItem._PROMO_ID:
							ppi.setPromo(new Promo(new Integer(value)));
							break;
						default:
							break;
						}
					}
					i++;
				}
				return ppi;
			}
		});

		List<PromoPrizeItem> ppiResult = null;

		try {
			ppiResult = rawResults.getResults();
			rawResults.close();
		} catch (Exception e) {
			logger.error(e.toString(), e);
		}

		return ppiResult;
	}

//	public PromoItem searchPromoItem(Product product, int qty, BigDecimal minTransactionAmt) throws SQLException {
	public PromoItem searchPromoItem(Product product) throws SQLException {
		Date currentDate = new Date();

		PromoItem promoItem = null;
		Dao<PromoItem, Integer> promoItemDao = DaoManagerImpl.getPromoItemDao();
		String query = "SELECT promo_item_lists.*"
				+ " FROM promo_item_lists"
				+ " INNER JOIN promotions ON promo_item_lists.promotion_id = promotions.id"
				+ " WHERE (promo_item_lists.product_id = " + product.getId()
				+ " AND promotions.from <= '" + DateUtil.format(currentDate, "yyyy-M-dd") + "'"
				+ " AND promotions.to >= '" + DateUtil.format(currentDate, "yyyy-M-dd") + "');";
//				+ " AND (promotions.min_amount <= " + minTransactionAmt + " AND promotions.min_qty <= " + qty + "));";

		GenericRawResults<PromoItem> rawResults = promoItemDao.queryRaw(query, new RawRowMapper<PromoItem>(){

			@Override
			public PromoItem mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				PromoItem promoItem = new PromoItem();
				int i = 0;
				while (i<columnNames.length) {
					String value = resultColumns[i];
					if (value != null) {
					switch (columnNames[i]) {
					case PromoItem._ID:
						promoItem.setId(new Integer(value));
						break;
					case PromoItem._PROMO_ID:
						promoItem.setPromo(new Promo(new Integer(value)));
						break;
					case PromoItem._PRODUCT_ID:
						promoItem.setProduct(new Product(new Integer(value)));
						break;
					case PromoItem._MIN_QTY:
						promoItem.setMinQty(new Integer(value));
						break;
					case PromoItem._REFERENCE_PRODUCT_ID:
						if(value != null){
							promoItem.setReferenceProduct(new Product(new Integer(value)));
						}
						break;
					case RModel._CREATED_AT:
						try {
							promoItem.setCreatedAt(DateUtil.parseDbDate(value));
						} catch (ParseException e) {
							logger.error(e.toString(), e);
						}
						break;
					case RModel._UPDATED_AT:
						try {
							promoItem.setUpdatedAt(DateUtil.parseDbDate(value));
						} catch (ParseException e) {
							logger.error(e.toString(), e);
						}
						break;
					default:
						break;
					}
					}
					i++;
				}
				return promoItem;
			}
		});

		try {
			promoItem = rawResults.getFirstResult();
			if (promoItem != null) {
				promoItems.add(promoItem);
				DaoManagerImpl.getProductDao().refresh(promoItem.getProduct());
				DaoManagerImpl.getPromoDao().refresh(promoItem.getPromo());
				if(promoItem.getReferenceProduct() != null){
					DaoManagerImpl.getProductDao()
						.refresh(promoItem.getReferenceProduct());
				}
				if(promoItem.getReferenceProduct() != null){
					DaoManagerImpl.getProductDao()
						.refresh(promoItem.getReferenceProduct());
				}
			}
		} catch (Exception e) {
			logger.error(e.toString(), e);
		} finally {
			rawResults.close();
		}

		return promoItem;
	}

	public PromoItem searchPromoItemByReferenceProduct(Product product) throws SQLException {
		Date currentDate = new Date();

		PromoItem promoItem = null;
		Dao<PromoItem, Integer> promoItemDao = DaoManagerImpl.getPromoItemDao();
		String query = "SELECT promo_item_lists.*"
				+ " FROM promo_item_lists"
				+ " INNER JOIN promotions ON promo_item_lists.promotion_id = promotions.id"
				+ " WHERE (promo_item_lists.reference_product_id = " + product.getId()
				+ " AND promotions.from <= '" + DateUtil.format(currentDate, "yyyy-M-dd") + "'"
				+ " AND promotions.to >= '" + DateUtil.format(currentDate, "yyyy-M-dd") + "');";
//				+ " AND (promotions.min_amount <= " + minTransactionAmt + " AND promotions.min_qty <= " + qty + "));";

		GenericRawResults<PromoItem> rawResults = promoItemDao.queryRaw(query, new RawRowMapper<PromoItem>(){

			@Override
			public PromoItem mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				PromoItem promoItem = new PromoItem();
				int i = 0;
				while (i<columnNames.length) {
					String value = resultColumns[i];
					if (value != null) {
					switch (columnNames[i]) {
					case PromoItem._ID:
						promoItem.setId(new Integer(value));
						break;
					case PromoItem._PROMO_ID:
						promoItem.setPromo(new Promo(new Integer(value)));
						break;
					case PromoItem._PRODUCT_ID:
						promoItem.setProduct(new Product(new Integer(value)));
						break;
					case PromoItem._MIN_QTY:
						promoItem.setMinQty(new Integer(value));
						break;
					case PromoItem._REFERENCE_PRODUCT_ID:
						if(value != null){
							promoItem.setReferenceProduct(new Product(new Integer(value)));
						}
						break;
					case RModel._CREATED_AT:
						try {
							promoItem.setCreatedAt(DateUtil.parseDbDate(value));
						} catch (ParseException e) {
							logger.error(e.toString(), e);
						}
						break;
					case RModel._UPDATED_AT:
						try {
							promoItem.setUpdatedAt(DateUtil.parseDbDate(value));
						} catch (ParseException e) {
							logger.error(e.toString(), e);
						}
						break;
					default:
						break;
					}
					}
					i++;
				}
				return promoItem;
			}
		});

		try {
			promoItem = rawResults.getFirstResult();
			if (promoItem != null) {
				promoItems.add(promoItem);
				DaoManagerImpl.getProductDao().refresh(promoItem.getProduct());
				DaoManagerImpl.getPromoDao().refresh(promoItem.getPromo());
				if(promoItem.getReferenceProduct() != null){
					DaoManagerImpl.getProductDao()
						.refresh(promoItem.getReferenceProduct());
				}
			}
		} catch (Exception e) {
			logger.error(e.toString(), e);
		} finally {
			rawResults.close();
		}

		return promoItem;
	}

	public PromoPrizeItem getMatchingPrize(Product item) {
		if (promoItems == null && !promoItems.isEmpty())
			return null;
		for (PromoItem promoItem : promoItems) {
			Promo promo = promoItem.getPromo();
			for (PromoPrizeItem pItem : promo.getPrizeItems()) {
				if (pItem.getProduct().getId() == item.getId()) {
					pItem.setPromoItem(promoItem);
					return pItem;
				}
			}
		}
		return null;
	}



	public List<PromoItem> getPromoItems() {
		return promoItems;
	}
}
