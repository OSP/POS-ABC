package id.sit.pos.config;

public class InvalidSettingException extends Exception {
	/**
	 *
	 */
	private static final long serialVersionUID = 6222971421364367940L;
	String message;
	public InvalidSettingException(String message) {
		this.message = message;
	}

	public String getMessage(){
		return message;
	}
}
