package id.r.engine.view;

import java.sql.SQLException;

import id.r.engine.controller.RCallback;
import id.r.engine.model.RModel;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public abstract class RGRidForm<T extends RModel> extends RForm<T> {

	@FXML
	protected TableView<T> table;

	@FXML
	protected Button addBtn;

	@FXML
	protected Button deleteBtn;

	private T selected;

	protected ObservableList<T> tableData = FXCollections.observableArrayList();

	@FXML
	public void initialize() {
		super.initialize();

		table.setRowFactory(tv -> {
			TableRow<T> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				if (getMode() != RForm.Mode.VIEW)
					return;

				T rowData = row.getItem();

				if (event.getClickCount() == 1 && (!row.isEmpty())) {
					select(rowData);
					updateForm(rowData);
					rowMouseEvent(event, row);
				} else {
					rowMouseEvent(event, row);
				}
			});

			return row;
		});

		table.addEventFilter(KeyEvent.ANY, event->{
			T item = table.getSelectionModel().getSelectedItem();
			rowKeyEvent(event, item);
		});

		table.setItems(tableData);

		addBtn.defaultButtonProperty().bind(addBtn.focusedProperty());
	}

	public void rowMouseEvent(MouseEvent event, TableRow<T> row){

	}


	public void rowKeyEvent(KeyEvent event, T item){

	}

	@Override
	public void setMode(Mode mode) {
		super.setMode(mode);
		if (mode == RForm.Mode.VIEW) {
			table.setMouseTransparent(false);
		} else if (mode == RForm.Mode.EDIT) {
			table.setMouseTransparent(true);
		}

	}

	@FXML
	public void add() {
		if (getMode() == RForm.Mode.EDIT) {
			MainApp.showAlertConfirm(Config.getMessage("common_edit_to_add"), new RCallback() {

				@Override
				public void onEvent(String event, Object... value) {
					if ((Boolean) value[0]) {
						save();
					}
				}
			});

		}
		resetForm();
		setMode(RForm.Mode.EDIT);
	}

	@FXML
	public void delete() {
		try {
			rDao.delete(getSelected());
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			showMessage(e.toString(), MessageType.ERROR);
		}

		tableData.remove(getSelected());
	}

	@Override
	public void afterSave() {
		super.afterSave();
		resetForm();
	}


	public void select(T selection) {
		selected = selection;
	}

	public T getSelected() {
		return (T) selected;
	}
}
