package id.sit.pos.user.model;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;

import id.r.engine.model.RModel;

public class AuditTrail extends RModel {

	@DatabaseField(columnName = "user_id", foreign = true)
	private User user;

	@DatabaseField(columnName = "screen_name")
	private String screenName;

	@DatabaseField(columnName = "start_access")
	private Date startAccess;

	@DatabaseField(columnName = "end_access")
	private Date endAccess;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public Date getStartAccess() {
		return startAccess;
	}

	public void setStartAccess(Date startAccess) {
		this.startAccess = startAccess;
	}

	public Date getEndAccess() {
		return endAccess;
	}

	public void setEndAccess(Date endAccess) {
		this.endAccess = endAccess;
	}
}
