package id.sit.pos.transaction.controller;

import java.sql.SQLException;
import java.util.Date;

import com.j256.ormlite.dao.CloseableIterator;

import id.r.engine.controller.RDao;
import id.r.engine.model.SearchParam;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.master.model.Voucher;
import id.sit.pos.master.model.VoucherDetail;
import id.sit.pos.master.model.Warehouse;

public class VoucherController {

	private RDao<VoucherDetail> voucherDetailDao = new RDao<VoucherDetail>(VoucherDetail.class);

	public VoucherDetail getVoucherDetail(String voucherCode) throws SQLException {
		CloseableIterator<VoucherDetail> iterator = voucherDetailDao.search(new SearchParam[]{
				new SearchParam(VoucherDetail._CODE, voucherCode)
		});
		if (iterator.hasNext())
			return iterator.nextThrow();
		else
			return null;
	}

	public boolean isValid(Voucher voucher) throws SQLException {
		Integer branchId = SettingCtrl.getSetting().getStore().getId();
		return isValid(voucher, branchId);
	}

	public boolean isValid(Voucher voucher, Integer branchId) throws SQLException {
		Date currentDate = new Date();
		Warehouse validBranch = voucher.getValidBranch();
		if (validBranch != null) {
			boolean isValidBranch = false;
			if (voucher.getValidBranch().getHeadOffice() != null) {
				if (voucher.getValidBranch().getId() == branchId)
					isValidBranch = true;
			} else {
				CloseableIterator<?> iterator = validBranch.getBranches().closeableIterator();
				while (iterator.hasNext()) {
					Warehouse branch = (Warehouse) iterator.nextThrow();
					if (branch.getId()==branchId) {
						isValidBranch = true;
						break;
					}
				}
				iterator.close();
				if (!isValidBranch)
					return false;
			}
		}

		if (voucher.getValidFrom()!=null && currentDate.compareTo(voucher.getValidFrom())<0) {
			return false;
		} else if (voucher.getValidUntil()!=null && currentDate.compareTo(voucher.getValidUntil())>0){
			return false;
		}

		return true;
	}
}
