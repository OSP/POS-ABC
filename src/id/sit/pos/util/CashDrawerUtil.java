package id.sit.pos.util;

import org.apache.log4j.Logger;

import jpos.CashDrawer;
import jpos.JposConst;
import jpos.JposException;
import jpos.events.DirectIOEvent;
import jpos.events.DirectIOListener;
import jpos.events.StatusUpdateEvent;
import jpos.events.StatusUpdateListener;

public class CashDrawerUtil implements StatusUpdateListener, DirectIOListener, JposConst {
	private static Logger logger = Logger.getLogger(MSRUtil.class.getName());
    private CashDrawer cd = null;

    public void open() {
        try {
            cd.openDrawer();
        } catch (JposException e) {
        	logger.error(e.toString(), e);
        }
    }

    public void startDevice() {
        try {
            cd = new CashDrawer();
            cd.open("DRWST50");
            cd.addStatusUpdateListener(this);
            cd.claim(100);
            cd.setDeviceEnabled(true);

            logger.info("CashDrawer: Device Started");
        } catch(JposException e){
        	logger.error(e.toString(), e);
        	stopDevice();
        }
    }

    public void stopDevice() {
        try {
            cd.removeStatusUpdateListener(this);
            cd.removeDirectIOListener(this);
            cd.close();

            logger.info("CashDrawer: Device Stop");
        } catch (JposException e) {
        	logger.error(e.toString(), e);
        }
    }

    public void statusUpdateOccurred(StatusUpdateEvent sue) {
        try {
            if (cd.getDrawerOpened() == true)
                System.out.println("CashDrawer Status: OPENED");
            else
                System.out.println("CashDrawer Status: CLOSED");
        } catch (JposException e) {
        	logger.error(e.toString(), e);
        }
    }

    public void directIOOccurred(DirectIOEvent dioe) {
    }

}
