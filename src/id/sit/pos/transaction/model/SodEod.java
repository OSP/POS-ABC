package id.sit.pos.transaction.model;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.config.Config;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.master.model.Warehouse;
import id.sit.pos.user.model.User;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.DateUtil;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@DatabaseTable(tableName = "sod_eods")
public class SodEod extends RModel {
	public enum Type {
		SHIFT, DAY
	}
	public static final String _TYPE = "session_type";
	public static final String _DATE = "sod_eod_date";
	public static final String _STORE_ID = "office_id";
	public static final String _MACHINE_ID = "machine_id";
	public static final String _SHIFT_NO = "session";
	public static final String _USER_ID = "user_id";
	public static final String _START = "start_time";
	public static final String _START_100K = "start_100k";
	public static final String _START_50K = "start_50k";
	public static final String _START_20K = "start_20k";
	public static final String _START_10K = "start_10k";
	public static final String _START_5K = "start_5k";
	public static final String _START_2K = "start_2k";
	public static final String _START_1K = "start_1k";
	public static final String _START_500 = "start_500";
	public static final String _START_200 = "start_200";
	public static final String _START_100 = "start_100";
	public static final String _START_50 = "start_50";
	public static final String _END = "end_time";
	public static final String _END_100K = "end_100k";
	public static final String _END_50K = "end_50k";
	public static final String _END_20K = "end_20k";
	public static final String _END_10K = "end_10k";
	public static final String _END_5K = "end_5k";
	public static final String _END_2K = "end_2k";
	public static final String _END_1K = "end_1k";
	public static final String _END_500 = "end_500";
	public static final String _END_200 = "end_200";
	public static final String _END_100 = "end_100";
	public static final String _END_50 = "end_50";
	public static final String _MODAL = "start_amount";
	public static final String _PETTY_CASH = "petty_cash";
	public static final String _SPENDING = "total_spending";
	public static final String _CASH_INCOME = "end_amount";
	public static final String _SALES = "total_sales";
	public static final String _CASH_SALES = "cash";
	public static final String _DEBIT = "debit";
	public static final String _CREDIT = "credit";
	public static final String _TRANSFER = "transfer";
	public static final String _VOUCHER = "voucher";
	public static final String _PPN = "ppn";
	public static final String _NET_RETURN = "retur";
	public static final String _DISC = "discount";
	public static final String _SPECIAL_DISC = "special_discount";
	public static final String _ACTUAL = "actual_end_amount";
	public static final String _NET_SALES = "total_cash_sales";
	public static final String _DIFFERENCE = "difference";
	public static final String _PAID_DIFFERENCE = "paid_difference";
	public static final String _RECEIPT_COUNT = "receipt_count";
	public static final String _NOTE = "note";
	public static final BigDecimal V100K = new BigDecimal(100000);
	public static final BigDecimal V50K = new BigDecimal(50000);
	public static final BigDecimal V20K = new BigDecimal(20000);
	public static final BigDecimal V10K = new BigDecimal(10000);
	public static final BigDecimal V5K = new BigDecimal(5000);
	public static final BigDecimal V2K = new BigDecimal(2000);
	public static final BigDecimal V1K = new BigDecimal(1000);
	public static final BigDecimal V500 = new BigDecimal(500);
	public static final BigDecimal V200 = new BigDecimal(200);
	public static final BigDecimal V100 = new BigDecimal(100);
	public static final BigDecimal V50 = new BigDecimal(50);

	@DatabaseField(columnName = _TYPE)
	private Type type;

	@DatabaseField(columnName = _DATE)
	private Date date;

	@DatabaseField(columnName = _STORE_ID, foreign = true)
	private Warehouse store;

	@DatabaseField(columnName = _MACHINE_ID)
	private String machineId;

	@DatabaseField(columnName = _SHIFT_NO)
	private Integer shiftNo;

	@DatabaseField(columnName = _USER_ID, foreign = true, foreignAutoRefresh=true)
	private User user;

	@DatabaseField(columnName = _START, dataType = DataType.DATE, format = "yyyy-MM-dd")
	private Date start;

	@DatabaseField(columnName = _START_100K)
	public Integer start100K;

	@DatabaseField(columnName = _START_50K)
	public Integer start50K;

	@DatabaseField(columnName = _START_20K)
	public Integer start20K;

	@DatabaseField(columnName = _START_10K)
	public Integer start10K;

	@DatabaseField(columnName = _START_5K)
	public Integer start5K;

	@DatabaseField(columnName = _START_2K)
	public Integer start2K;

	@DatabaseField(columnName = _START_1K)
	public Integer start1K;

	@DatabaseField(columnName = _START_500)
	public Integer start500;

	@DatabaseField(columnName = _START_200)
	public Integer start200;

	@DatabaseField(columnName = _START_100)
	public Integer start100;

	@DatabaseField(columnName = _START_50)
	public Integer start50;

	@DatabaseField(columnName = _END)
	private Date end;

	@DatabaseField(columnName = _END_100K)
	public Integer end100K;

	@DatabaseField(columnName = _END_50K)
	public Integer end50K;

	@DatabaseField(columnName = _END_20K)
	public Integer end20K;

	@DatabaseField(columnName = _END_10K)
	public Integer end10K;

	@DatabaseField(columnName = _END_5K)
	public Integer end5K;

	@DatabaseField(columnName = _END_2K)
	public Integer end2K;

	@DatabaseField(columnName = _END_1K)
	public Integer end1K;

	@DatabaseField(columnName = _END_500)
	public Integer end500;

	@DatabaseField(columnName = _END_200)
	public Integer end200;

	@DatabaseField(columnName = _END_100)
	public Integer end100;

	@DatabaseField(columnName = _END_50)
	public Integer end50;

	@DatabaseField(columnName = _MODAL, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal modal = BigDecimal.ZERO;

	@DatabaseField(columnName = _PETTY_CASH, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal pettyCash = BigDecimal.ZERO;

	@DatabaseField(columnName = _SPENDING, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal spending = BigDecimal.ZERO;

	@DatabaseField(columnName = _SALES, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal sales = BigDecimal.ZERO;

	@DatabaseField(columnName = _CASH_SALES, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal cashSales = BigDecimal.ZERO;

	@DatabaseField(columnName = _CASH_INCOME, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal cashIncome = BigDecimal.ZERO;

	@DatabaseField(columnName = _DEBIT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal debit = BigDecimal.ZERO;

	@DatabaseField(columnName = _CREDIT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal credit = BigDecimal.ZERO;

	@DatabaseField(columnName = _TRANSFER, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal transfer = BigDecimal.ZERO;

	@DatabaseField(columnName = _VOUCHER, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal voucher = BigDecimal.ZERO;

	@DatabaseField(columnName = _NET_RETURN, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal netReturn = BigDecimal.ZERO;

	@DatabaseField(columnName = _PPN, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal ppn = BigDecimal.ZERO;

	@DatabaseField(columnName = _DISC, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal disc = BigDecimal.ZERO;

	@DatabaseField(columnName = _SPECIAL_DISC, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal specialDisc = BigDecimal.ZERO;

	@DatabaseField(columnName = _ACTUAL, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal actual = BigDecimal.ZERO;

	@DatabaseField(columnName = _NET_SALES, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal netSales = BigDecimal.ZERO;

	@DatabaseField(columnName = _DIFFERENCE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal difference = BigDecimal.ZERO;

	@DatabaseField(columnName = _PAID_DIFFERENCE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal paidDifference = BigDecimal.ZERO;

	@DatabaseField(columnName = _RECEIPT_COUNT)
	private int receiptCount;

	@DatabaseField(columnName = _NOTE)
	private String note;

	private StringProperty dateProperty;
	private StringProperty typeProperty;
	private IntegerProperty shiftProperty;
	private StringProperty userNameProperty;
	private StringProperty salesAmountProperty;
	private StringProperty actualSalesProperty;
	private StringProperty differenceProperty;

	public SodEod() {
		dateProperty 		= new SimpleStringProperty();
		typeProperty		= new SimpleStringProperty();
		shiftProperty 		= new SimpleIntegerProperty();
		userNameProperty 	= new SimpleStringProperty();
		salesAmountProperty	= new SimpleStringProperty();
		actualSalesProperty = new SimpleStringProperty();
		differenceProperty 	= new SimpleStringProperty();
	}

	public SodEod(Integer id){
		this.id = id;
	}

	public SodEod(Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
		typeProperty();
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
//		dateProperty();
	}

	public Warehouse getStore() {
		return store;
	}

	public void setStore(Warehouse branch) {
		this.store = branch;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public Integer getShiftNo() {
		return shiftNo;
	}

	public void setShiftNo(Integer shiftNo) {
		this.shiftNo = shiftNo;
//		shiftProperty();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
//		try {
//			userNameProperty();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public BigDecimal getModal() {
		return modal;
	}

	public void setModal(BigDecimal modal) {
		this.modal = modal;
	}

	public BigDecimal getSales() {
		return sales;
	}

	public void setSales(BigDecimal sales) {
		this.sales = sales;
	}

	public BigDecimal getCashIncome() {
		return cashIncome;
	}

	public void setCashIncome(BigDecimal cash) {
		this.cashIncome = cash;
	}

	public BigDecimal getDebit() {
		return debit;
	}

	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}

	public BigDecimal getCredit() {
		return credit;
	}

	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

	public BigDecimal getTransfer() {
		return transfer;
	}

	public void setTransfer(BigDecimal transfer) {
		this.transfer = transfer;
	}

	public BigDecimal getVoucher() {
		return voucher;
	}

	public void setVoucher(BigDecimal voucher) {
		this.voucher = voucher;
	}

	public BigDecimal getNetReturn() {
		return netReturn;
	}

	public void setNetReturn(BigDecimal retur) {
		this.netReturn = retur;
	}

	public BigDecimal getPpn() {
		return ppn;
	}

	public void setPpn(BigDecimal ppn) {
		this.ppn = ppn;
	}

	public BigDecimal getDisc() {
		return disc;
	}

	public void setDisc(BigDecimal disc) {
		this.disc = disc;
	}

	public BigDecimal getSpecialDisc() {
		return specialDisc;
	}

	public void setSpecialDisc(BigDecimal specialDisc) {
		this.specialDisc = specialDisc;
	}

	public BigDecimal getActual() {
		return actual;
	}

	public void setActual(BigDecimal actual) {
		this.actual = actual;
	}

	public BigDecimal getNetSales() {
		return netSales;
	}

	public void setNetSales(BigDecimal netSales) {
		this.netSales = netSales;
	}

	public Integer getStart100K() {
		return start100K;
	}

	public void setStart100K(Integer start100k) {
		start100K = start100k;
	}

	public Integer getStart50K() {
		return start50K;
	}

	public void setStart50K(Integer start50k) {
		start50K = start50k;
	}

	public Integer getStart20K() {
		return start20K;
	}

	public void setStart20K(Integer start20k) {
		start20K = start20k;
	}

	public Integer getStart10K() {
		return start10K;
	}

	public void setStart10K(Integer start10k) {
		start10K = start10k;
	}

	public Integer getStart5K() {
		return start5K;
	}

	public void setStart5K(Integer start5k) {
		start5K = start5k;
	}

	public Integer getStart2K() {
		return start2K;
	}

	public void setStart2K(Integer start2k) {
		start2K = start2k;
	}

	public Integer getStart1K() {
		return start1K;
	}

	public void setStart1K(Integer start1k) {
		start1K = start1k;
	}

	public Integer getStart500() {
		return start500;
	}

	public void setStart500(Integer start500) {
		this.start500 = start500;
	}

	public Integer getEnd100K() {
		return end100K;
	}

	public void setEnd100K(Integer end100k) {
		end100K = end100k;
	}

	public Integer getEnd50K() {
		return end50K;
	}

	public void setEnd50K(Integer end50k) {
		end50K = end50k;
	}

	public Integer getEnd20K() {
		return end20K;
	}

	public void setEnd20K(Integer end20k) {
		end20K = end20k;
	}

	public Integer getEnd10K() {
		return end10K;
	}

	public void setEnd10K(Integer end10k) {
		end10K = end10k;
	}

	public Integer getEnd5K() {
		return end5K;
	}

	public void setEnd5K(Integer end5k) {
		end5K = end5k;
	}

	public Integer getEnd2K() {
		return end2K;
	}

	public void setEnd2K(Integer end2k) {
		end2K = end2k;
	}

	public Integer getEnd1K() {
		return end1K;
	}

	public void setEnd1K(Integer end1k) {
		end1K = end1k;
	}

	public Integer getEnd500() {
		return end500;
	}

	public void setEnd500(Integer end500) {
		this.end500 = end500;
	}

	public BigDecimal getDifference() {
		return difference;
	}

	public void setDifference(BigDecimal diff) {
		this.difference = diff;
	}

	public BigDecimal getPettyCash() {
		return pettyCash;
	}

	public void setPettyCash(BigDecimal pettyCash) {
		this.pettyCash = pettyCash;
	}

	public BigDecimal getSpending() {
		return spending;
	}

	public void setSpending(BigDecimal spending) {
		this.spending = spending;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public BigDecimal getCashSales() {
		return cashSales;
	}

	public void setCashSales(BigDecimal cashSales) {
		this.cashSales = cashSales;
	}

	public Integer getStart200() {
		return start200;
	}

	public void setStart200(Integer start200) {
		this.start200 = start200;
	}

	public Integer getStart100() {
		return start100;
	}

	public void setStart100(Integer start100) {
		this.start100 = start100;
	}

	public Integer getStart50() {
		return start50;
	}

	public void setStart50(Integer start50) {
		this.start50 = start50;
	}

	public Integer getEnd200() {
		return end200;
	}

	public void setEnd200(Integer end200) {
		this.end200 = end200;
	}

	public Integer getEnd100() {
		return end100;
	}

	public void setEnd100(Integer end100) {
		this.end100 = end100;
	}

	public Integer getEnd50() {
		return end50;
	}

	public void setEnd50(Integer end50) {
		this.end50 = end50;
	}

	public BigDecimal getPaidDifference() {
		return paidDifference;
	}

	public void setPaidDifference(BigDecimal paidDifference) {
		this.paidDifference = paidDifference;
	}

	public int getReceiptCount() {
		return receiptCount;
	}

	public void setReceiptCount(int receiptCount) {
		this.receiptCount = receiptCount;
	}

	public StringProperty dateProperty() {
		if(getDate() != null){
			dateProperty.set(DateUtil.format(getDate(), Config.getDateFormat()));
		} else {
			dateProperty.set("");
		}
		return dateProperty;
	}

	public StringProperty typeProperty() {
		if(getType() != null)
			typeProperty.set(type.toString());
		else
			typeProperty.set("");
		return typeProperty;
	}

	public IntegerProperty shiftProperty() {
		if(getShiftNo() != null)
			shiftProperty.set(shiftNo);
		else
			shiftProperty.set(0);

		return shiftProperty;
	}

	public StringProperty userNameProperty() throws SQLException {
		if(user != null){
			DaoManagerImpl.getUserDao().refresh(getUser());
			userNameProperty.set(getUser().getFirstName()+ " "+getUser().getLastName());
		} else {
			userNameProperty.set("");
		}

		return userNameProperty;
	}

	public StringProperty salesAmountProperty() {
		if(sales != null)
			salesAmountProperty.set(CurrencyUtil.format(sales,false));
		else
			salesAmountProperty.set(BigDecimal.ZERO.toString());
		return salesAmountProperty;
	}

	public StringProperty actualSalesProperty() {
		if(getActual() != null)
			actualSalesProperty.set(CurrencyUtil.format(actual,false));
		else
			actualSalesProperty.set(BigDecimal.ZERO.toString());

		return actualSalesProperty;
	}

	public StringProperty differenceProperty() {
		if(getDifference() != null)
			differenceProperty.set(CurrencyUtil.format(difference,false));
		else
			differenceProperty.set(BigDecimal.ZERO.toString());
		return differenceProperty;
	}

}
