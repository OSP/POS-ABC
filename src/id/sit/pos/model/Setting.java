package id.sit.pos.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.master.model.Warehouse;

@DatabaseTable(tableName = "Setting")
public class Setting extends RModel {
	@DatabaseField
	private String machineId;

	@DatabaseField
	private String machineName;

	@DatabaseField
	private Integer branchId;

	private Warehouse branch;

	private BigDecimal customerPointToAmount = new BigDecimal(100000);
	private BigDecimal transactionRounding = new BigDecimal(500);
	List<Entry<Integer, String>> transactionExtraCharge = new ArrayList<Map.Entry<Integer,String>>();

	public Setting(){

	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public String getMachineName() {
		return machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public Integer getStoreId() {
		return branchId;
	}

	public void setStoreId(Integer branchId) {
		this.branchId = branchId;
	}

	public Warehouse getStore() {
		return branch;
	}

	public void setBranch(Warehouse branch) {
		this.branchId = branch.getId();
		this.branch = branch;
	}

	public BigDecimal getCustomerPointToAmount() {
		return customerPointToAmount;
	}

	public void setCustomerPointToAmount(BigDecimal customerPointToAmount) {
		this.customerPointToAmount = customerPointToAmount;
	}

	public BigDecimal getTransactionRounding() {
		return transactionRounding;
	}

	public void setTransactionRounding(BigDecimal transactionRounding) {
		this.transactionRounding = transactionRounding;
	}

	public List<Entry<Integer, String>> getTransactionsExtraCharges() {
		return transactionExtraCharge;
	}

	public void setTransactionsExtraCharges(List<Entry<Integer, String>> extraCharge) {
		this.transactionExtraCharge = extraCharge;
	}
}
