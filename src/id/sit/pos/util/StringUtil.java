package id.sit.pos.util;

public class StringUtil {

    public static String center(String s, int size) {
        return center(s, size, " ");
    }

    public static String center(String s, int size, String pad) {
        if (pad == null)
            throw new NullPointerException("pad cannot be null");
        if (pad.length() <= 0)
            throw new IllegalArgumentException("pad cannot be empty");
        if (s == null || size <= s.length())
            return s;

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < (size - s.length()) / 2; i++) {
            sb.append(pad);
        }
        sb.append(s);
        while (sb.length() < size) {
            sb.append(pad);
        }
        return sb.toString();
    }

    public static boolean isEmpty(String string) {
    	return (string == null || string.trim().isEmpty());
    }

    public static boolean isAllEmpty(String... strings) {
    	for (String s: strings) {
    		if (s != null && !s.trim().isEmpty())
    			return false;
    	}
    	return true;
    }

    public static String setdoubleQuote(String aString) {
        String quoteText = "";
        if (!aString.isEmpty()) {
            quoteText = "\"" + aString + "\"";
        }
        return quoteText;
    }
}