package id.sit.pos.transaction.view;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;

import javax.print.PrintException;

import id.r.engine.controller.RDao;
import id.r.engine.view.RForm;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.transaction.controller.ReceiptPrinterCtrl;
import id.sit.pos.transaction.controller.SodEodCtrl;
import id.sit.pos.transaction.model.SodEod;
import id.sit.pos.transaction.model.SodEod.Type;
import id.sit.pos.user.controller.SessionCtrl;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.DateUtil;
import id.sit.pos.util.PrintUtil;
import id.sit.pos.util.StringUtil;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;

public class EndShiftForm extends RForm<SodEod> {
	@FXML
	public Label store;

	@FXML
	public Label machine;

	@FXML
	public Label username;

	@FXML
	public Label shiftLbl;

	@FXML
	public Label shift;

	@FXML
	public Label startDate;

	@FXML
	public Label startTime;

	@FXML
	public Label endDate;

	@FXML
	public Label endTime;

	@FXML
	private GridPane moneyFraction;

	@FXML
	public TextField end100K;

	@FXML
	public TextField end50K;

	@FXML
	public TextField end20K;

	@FXML
	public TextField end10K;

	@FXML
	public TextField end5K;

	@FXML
	public TextField end2K;

	@FXML
	public TextField end1K;

	@FXML
	public TextField end500;

	@FXML
	public TextField end200;

	@FXML
	public TextField end100;

	@FXML
	public TextField cashIncome;

	@FXML
	public Label netSales;

	@FXML
	public Label netReturn;

	@FXML
	public Label ppn;

	@FXML
	public Label netIncome;

	@FXML
	public Label cash;

	@FXML
	public Label cashIncomeLbl;

	@FXML
	public Label debit;

	@FXML
	public Label credit;

	@FXML
	public Label transfer;

	@FXML
	public Label transferLbl;


	@FXML
	public Label voucher;

	@FXML
	public Label discount;

	@FXML
	public Label specialDiscount;

	@FXML
	public Label totalActual;

	@FXML
	public Label difference;

	@FXML
	public TextField paidDifference;

	@FXML
	public Label receiptCount;

	@FXML
	private Button printBtn;

	@FXML
	public Button endBtn;

	@FXML
	public Button printEODDetailBtn;

	@FXML
	public void printEodReport() {
		if (!printEodService.isRunning()) {
			printEodService.reset();
			printEodService.start();
		}
	}

	protected Service<Void> printEodService;

	public SodEodCtrl ctrl;

	private ReceiptPrinterCtrl receiptPrinterCtrl = new ReceiptPrinterCtrl();

	private boolean isEod = false;

	public EndShiftForm() {
		super();
		setTitle(Config.getMessage("eod_title"));
		getIcons().add(new Image(Config.getLogoPath()));
		ctrl = new SodEodCtrl(getRDao());

		printEodService = new Service<Void>() {

			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() throws Exception {
						startProgressBar();
						String eodReport = ctrl.generateEoDReportDetail();
//						InputStream is = new ByteArrayInputStream(
//								Charset.forName("UTF-8").encode(eodReport).array());
						PrintUtil.printText(eodReport);
						endProgressBar();
						return null;
					}
				};
			}
		};
	}



	@Override
	public void initialize() {
		model = SessionCtrl.getSession().getSodEod();
		super.initialize();

		getStage().initModality(Modality.APPLICATION_MODAL);

		if (Config.isEnableSodEodFraction()) {
			cashIncome.setDisable(true);
			setAsFormFields(end100K, end50K, end20K, end10K, end5K, end2K, end1K, end500, end200, end100);
		} else {
			moneyFraction.setVisible(false);
			moneyFraction.setManaged(false);
			setAsCurrencyFields(cashIncome, paidDifference);
			setAsNumericFields(cashIncome);
			setAsFormFields(cashIncome);
		}

		printBtn.defaultButtonProperty().bind(printBtn.focusedProperty());
		endBtn.defaultButtonProperty().bind(endBtn.focusedProperty());

		if (!Config.isAbc()) {
			printEODDetailBtn.setVisible(false);
			printEODDetailBtn.defaultButtonProperty().bind(printEODDetailBtn.focusedProperty());
		}
		store.setText(SettingCtrl.getSetting().getStore().getName());
		machine.setText(SettingCtrl.getSetting().getMachineName());
		username.setText(SessionCtrl.getSession().getUser().getUsername());

		if (isEod) {
			shift.setVisible(false);
			shiftLbl.setVisible(false);
			SodEodCtrl.setCloseDay(isEod);
			cashIncome.setDisable(true);
			paidDifference.setDisable(true);

			try {
				ctrl.endDay(model);
				updateForm(model);
				printBtn.setDisable(false);
				printEODDetailBtn.setDisable(false);
			} catch (SQLException e) {
				logger.error(e.toString(), e);
				MainApp.showAlertDbError();
			}
			setMode(Mode.VIEW);
		} else {
			shift.setText(model.getShiftNo().toString());
			setMode(Mode.EDIT);
			paidDifference.setDisable(true);
		}

		if (!Config.isEnableTransfer())
		{
			transferLbl.setVisible(false);
			transfer.setVisible(false);
		}

		stage.setOnCloseRequest(event-> {
	        end();
		});
	}

	@Override
	public RDao<SodEod> initRDao() {
		return new RDao<SodEod>(SodEod.class);
	}

	@Override
	public SodEod initModel() {
		SodEod sodEod;
		if (isEod)
			sodEod = new SodEod(Type.DAY);
		else
			sodEod = new SodEod(Type.SHIFT);

		sodEod.setDate(new Date());
		sodEod.setStore(SettingCtrl.getSetting().getStore());
		sodEod.setUser(SessionCtrl.getSession().getUser());
		sodEod.setShiftNo(SessionCtrl.getSession().getShiftNo());
		sodEod.setMachineId(SettingCtrl.getSetting().getMachineId());
		return sodEod;
	}

	@Override
	public void updateForm(SodEod model) {
		this.model = model;
		if (model.getShiftNo() != null)
			shift.setText(model.getShiftNo().toString());
		if (model.getStart() != null) {
			if (startDate != null)
			startDate.setText(DateUtil.format(model.getStart()));
			if (startTime != null)
			startTime.setText(DateUtil.formatTime(model.getStart()));
		}
		if (model.getEnd() != null) {
			if (endDate != null)
			endDate.setText(DateUtil.format(model.getEnd()));
			if (endTime != null)
			endTime.setText(DateUtil.formatTime(model.getEnd()));
		}
		if (model.getEnd100K() != null)
			end100K.setText(model.getEnd100K().toString());
		if (model.getEnd50K() != null)
			end50K.setText(model.getEnd50K().toString());
		if (model.getEnd20K() != null)
			end20K.setText(model.getEnd20K().toString());
		if (model.getEnd10K() != null)
			end10K.setText(model.getEnd10K().toString());
		if (model.getEnd5K() != null)
			end5K.setText(model.getEnd5K().toString());
		if (model.getEnd2K() != null)
			end2K.setText(model.getEnd2K().toString());
		if (model.getEnd1K() != null)
			end1K.setText(model.getEnd1K().toString());
		if (model.getEnd500() != null)
			end500.setText(model.getEnd500().toString());

		cashIncome.setText(CurrencyUtil.format(model.getCashIncome(), false));
		netSales.setText(CurrencyUtil.format(model.getNetSales(), false));
		netReturn.setText(CurrencyUtil.format(model.getNetReturn(), false));
		ppn.setText(CurrencyUtil.format(model.getPpn(), false));
		netIncome.setText(CurrencyUtil.format(model.getSales(), false));
		cash.setText(CurrencyUtil.format(model.getCashSales(), false));
		cashIncomeLbl.setText(CurrencyUtil.format(model.getCashIncome(), false));
		debit.setText(CurrencyUtil.format(model.getDebit(), false));
		credit.setText(CurrencyUtil.format(model.getCredit(), false));
		transfer.setText(CurrencyUtil.format(model.getTransfer(), false));
		voucher.setText(CurrencyUtil.format(model.getVoucher(), false));
		discount.setText(CurrencyUtil.format(model.getDisc(), false));
		specialDiscount.setText(CurrencyUtil.format(model.getSpecialDisc(), false));
		totalActual.setText(CurrencyUtil.format(model.getActual(), false));
		difference.setText(CurrencyUtil.format(model.getDifference(), false));
		paidDifference.setText(CurrencyUtil.format(model.getPaidDifference(), false));
		receiptCount.setText(String.valueOf(model.getReceiptCount()));
	}

	@Override
	public SodEod fetchFormData() {

		if (Config.isEnableSodEodFraction()) {
			if (StringUtil.isEmpty(end100K.getText()))
				end100K.setText("0");
			model.setEnd100K(new Integer(end100K.getText()));

			if (StringUtil.isEmpty(end50K.getText()))
				end50K.setText("0");
			model.setEnd50K(new Integer(end50K.getText()));

			if (StringUtil.isEmpty(end20K.getText()))
				end20K.setText("0");
			model.setEnd20K(new Integer(end20K.getText()));

			if (StringUtil.isEmpty(end10K.getText()))
				end10K.setText("0");
			model.setEnd10K(new Integer(end10K.getText()));

			if (StringUtil.isEmpty(end5K.getText()))
				end5K.setText("0");
			model.setEnd5K(new Integer(end5K.getText()));

			if (StringUtil.isEmpty(end2K.getText()))
				end2K.setText("0");
			model.setEnd2K(new Integer(end2K.getText()));

			if (StringUtil.isEmpty(end1K.getText()))
				end1K.setText("0");
			model.setEnd1K(new Integer(end1K.getText()));

			if (StringUtil.isEmpty(end500.getText()))
				end500.setText("0");
			model.setEnd500(new Integer(end500.getText()));

			if (StringUtil.isEmpty(end200.getText()))
				end200.setText("0");
			model.setEnd500(new Integer(end200.getText()));

			if (StringUtil.isEmpty(end100.getText()))
				end100.setText("0");
			model.setEnd500(new Integer(end100.getText()));

		} else {
			model.setCashIncome(CurrencyUtil.parse(cashIncome.getText()));
		}

		return model;
	}

	@Override
	public void beforeSave() {
		super.beforeSave();
		model.setEnd(new Date());
		if (Config.isEnableSodEodFraction()) {
			model.setCashIncome(SodEod.V100K.multiply(new BigDecimal(model.getEnd100K()))
				.add(SodEod.V50K.multiply(new BigDecimal(model.getEnd50K())))
				.add(SodEod.V20K.multiply(new BigDecimal(model.getEnd20K())))
				.add(SodEod.V10K.multiply(new BigDecimal(model.getEnd10K())))
				.add(SodEod.V5K.multiply(new BigDecimal(model.getEnd5K())))
				.add(SodEod.V2K.multiply(new BigDecimal(model.getEnd2K())))
				.add(SodEod.V1K.multiply(new BigDecimal(model.getEnd1K())))
				.add(SodEod.V500.multiply(new BigDecimal(model.getEnd500()))));
		}
	}

	public void afterSave() {
		try {
			super.afterSave();

			ctrl.endShift(model);

			this.endTime.setText(DateUtil.formatDateTime(new Date()));
			if (model.getDifference().compareTo(BigDecimal.ZERO) >= 0){
				paidDifference.setDisable(true);
			} else {
				paidDifference.setDisable(false);
			}

			updateForm(model);
			printBtn.setDisable(false);

			if(Config.isAbc())
			printEODDetailBtn.setDefaultButton(false);

		} catch (SQLException e1) {
			MainApp.showAlertDbError();
			logger.error(e1.toString(), e1);
			setMode(Mode.EDIT);
		}
	}

	@FXML
	public void end() {
		try {
			rDao.save(model);
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			MainApp.showAlertDbError();
		}
		getStage().close();
		MainApp.getInstance().logout();
	}

	@FXML
	public void payDifference() {
		generateEoDReportSummary();
	}

	@FXML
	public void generateEoDReportSummary() {
		if (!StringUtil.isEmpty(paidDifference.getText()))
			model.setPaidDifference(CurrencyUtil.parse(paidDifference.getText()));

		if (model.getDifference().compareTo(BigDecimal.ZERO) == -1 ){
			if ((model.getPaidDifference().add(model.getDifference())).compareTo(BigDecimal.ZERO)== -1) {
				showAlertError("sodeod_unpaid_difference");
				return;
			}
		}

		StringBuilder receipt = new StringBuilder();
		String xSpace = ReceiptPrinterCtrl.getAdditionalSpace();

		if (isEod)
			receipt.append(StringUtil.center("TUTUP HARIAN", Config.getPrinterMaxCharPerLine()));
		else
			receipt.append(StringUtil.center("TUTUP SHIFT", Config.getPrinterMaxCharPerLine()));
		receipt.append(String.format("%n"));

		if (!isEod){
			receipt.append(String.format("Shift       : %-15d%n", model.getShiftNo()));
			receipt.append(String.format("Kasir       : %-15s%n", model.getUser().getUsername()));
		}

		receipt.append(String.format("Date        : %-15s%n%n", DateUtil.format(model.getStart())));

		receipt.append(String.format(receiptPrinterCtrl.generateLine()));
		receipt.append(String.format("%s%n", "SALES"));
		receipt.append(String.format(receiptPrinterCtrl.generateSmallLine()));

		receipt.append(String.format(xSpace+"%26s  %12s%n", "Cash Income", CurrencyUtil.format(model.getCashIncome(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Net Sales", CurrencyUtil.format(model.getNetSales(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Net Return", CurrencyUtil.format(model.getNetReturn(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "PPN", CurrencyUtil.format(model.getPpn(), false)));

		receipt.append(String.format(receiptPrinterCtrl.generateSmallLine()));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Total Sales", CurrencyUtil.format(model.getSales(), false)));

		receipt.append(String.format(receiptPrinterCtrl.generateLine()));
		receipt.append(String.format("%n"));
		receipt.append(String.format(receiptPrinterCtrl.generateLine()));
		receipt.append(String.format("%s%n", "ACTUAL"));
		receipt.append(String.format(receiptPrinterCtrl.generateSmallLine()));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Cash", CurrencyUtil.format(model.getCashSales(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Cash Income", CurrencyUtil.format(model.getCashIncome(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Debit", CurrencyUtil.format(model.getDebit(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Credit", CurrencyUtil.format(model.getCredit(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Voucher", CurrencyUtil.format(model.getVoucher(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Discount", CurrencyUtil.format(model.getDisc(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Special Discount", CurrencyUtil.format(model.getSpecialDisc(), false)));
		receipt.append(String.format(receiptPrinterCtrl.generateSmallLine()));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Total Actual", CurrencyUtil.format(model.getActual(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Variance", CurrencyUtil.format(model.getDifference(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Variance Paid", CurrencyUtil.format(model.getPaidDifference(), false)));
		receipt.append(String.format(xSpace+"%26s  %12s%n", "Receipt Count", model.getReceiptCount()));
		receipt.append(String.format(receiptPrinterCtrl.generateLine()));
		receipt.append(Config.getEodFooter());
		receipt.append(String.format("%n"));
		receipt.append(String.format(receiptPrinterCtrl.generateSmallLine()));
		receipt.append(String.format("       Print Date : %20s%n", DateUtil.formatDateTime(new Date())));
		receipt.append(String.format("%n%n%n%n%n"));
		receipt.append(String.format("%n%n%n."));
		if (Config.isEnablePrinterTypeLX310()){
			receipt.append(String.format("%n%n%n%n%n"));
		}

		System.out.println(receipt.toString());
		try {
			if (isEod)
				receiptPrinterCtrl.print("TUTUP HARIAN "+DateUtil.format(model.getEnd()), receipt.toString());
			else
				receiptPrinterCtrl.print("TUTUP SHIFT "+model.getShiftNo()+" "+DateUtil.format(model.getEnd()), receipt.toString());
			printBtn.setDisable(true);
			printEODDetailBtn.setDisable(true);
		} catch (PrintException | IOException e) {
			logger.error(e.toString(), e);
			MainApp.showAlertDbError();
		}
		endBtn.setDisable(false);
		endBtn.requestFocus();
	}

	public ReceiptPrinterCtrl getReceiptPrinterCtrl() {
		return receiptPrinterCtrl;
	}

	public void setReceiptPrinterCtrl(ReceiptPrinterCtrl receiptPrinterCtrl) {
		this.receiptPrinterCtrl = receiptPrinterCtrl;
	}

	public boolean isEod() {
		return isEod;
	}

	public void setEod(boolean isEod) {
		this.isEod = isEod;
	}
}
