package id.r.engine.view;

import id.sit.pos.config.Config;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class RBrowser extends RUi{
	@FXML
	protected WebView webview;

	protected WebEngine webEngine;

	protected String url;

    public RBrowser() {
    	getIcons().add(new Image(Config.getLogoPath()));

    }

    @FXML
    public void initialize() {
    	webEngine = webview.getEngine();
    	if (url != null)
    		load();
    }

    public void load() {
    	webEngine.load(url);
    }

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
