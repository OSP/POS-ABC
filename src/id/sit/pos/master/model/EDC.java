package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName="edc_machines")
public class EDC extends RModel {
	public static final String _BANK_NAME = "bank_name";
	public static final String _ACCOUNT_NO = "account_number";
	public static final String _CREDIT_CHARGE = "credit_charge";


	@DatabaseField(columnName = _BANK_NAME)
	private String bankName;

	@DatabaseField(columnName = _ACCOUNT_NO)
	private String accountNo;

	@DatabaseField(columnName = _CREDIT_CHARGE)
	private Float creditCharge;

	public EDC() {
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public Float getCreditCharge() {
		return creditCharge;
	}

	public void setCreditCharge(Float creditCharge) {
		this.creditCharge = creditCharge;
	}
	
	

}
