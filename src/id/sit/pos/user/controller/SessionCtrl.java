package id.sit.pos.user.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import id.r.engine.controller.RDao;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.user.model.Role;
import id.sit.pos.user.model.Session;
import id.sit.pos.user.model.User;

public class SessionCtrl {
	private static Logger logger = Logger.getLogger(SessionCtrl.class.getName());
	private static Session session;
	private static SessionCtrl instance;
	private RDao<Session> sessionDao = new RDao<Session>(Session.class);

	public User authenticate (String username, String password) throws SQLException{
		List<User> users = new ArrayList<User>();
		QueryBuilder<User, Integer> userQb = DaoManagerImpl.getUserDao().queryBuilder();
		Where<User, Integer> uw = userQb.where();
		uw.eq(User._USERNAME, username)
		  .and()
		  .eq(User._PASSWORD_HASH, doHash(password))
		  .and()
		  .eq(User._STORE_ID, SettingCtrl.getBranchId());
		QueryBuilder<Role, Integer> roleQb = DaoManagerImpl.getRoleDao().queryBuilder();
		Where<Role, Integer> rw = roleQb.where();
		rw.eq(Role._NAME, Role.CASHIER)
					  .or()
					  .eq(Role._NAME, Role.CASHIER_HEAD)
					  .or()
					  .eq(Role._NAME, Role.STORE_MANAGER)
					  .or()
					  .eq(Role._NAME, Role.CEO)
					  .or().eq(Role._NAME, Role.SUPER_ADMIN)
					  .or().eq(Role._NAME, Role.SUPER_USER);

		userQb.join(roleQb);
		users = DaoManagerImpl.getUserDao().query(userQb.prepare());

		if (!users.isEmpty()) {
			return users.get(0);
		} else {
			return null;
		}
	}

	public User authenticate (String username, String password, String roleName) throws SQLException{
		List<User> users = new ArrayList<User>();
		QueryBuilder<User, Integer> userQb = DaoManagerImpl.getUserDao().queryBuilder();
		Where<User, Integer> uw = userQb.where();
		uw.eq(User._USERNAME, username)
		  .and()
		  .eq(User._PASSWORD_HASH, doHash(password));
		QueryBuilder<Role, Integer> roleQb = DaoManagerImpl.getRoleDao().queryBuilder();
		Where<Role, Integer> rw = roleQb.where();
		rw.eq(Role._NAME, roleName);

		userQb.join(roleQb);
		users = DaoManagerImpl.getUserDao().query(userQb.prepare());

		if (!users.isEmpty()) {
			return users.get(0);
		} else {
			return null;
		}
	}

	public User authenticate (String username, String password, String rolename, String logType) throws SQLException{
		List<User> users = new ArrayList<User>();
		QueryBuilder<User, Integer> userQb = DaoManagerImpl.getUserDao().queryBuilder();
		Where<User, Integer> uw = userQb.where();
		if(logType != null){
			uw.eq(User._USERNAME, username)
			  .and()
			  .eq(User._PASSWORD_HASH, doHash(password));
		} else {
			uw.eq(User._USERNAME, username)
			  .and()
			  .eq(User._PASSWORD_HASH, doHash(password))
			  .and()
			  .eq(User._STORE_ID, SettingCtrl.getBranchId());
		}
		QueryBuilder<Role, Integer> roleQb = DaoManagerImpl.getRoleDao().queryBuilder();
		Where<Role, Integer> rw = roleQb.where();
		rw.eq(Role._NAME, Role.CASHIER)
					  .or()
					  .eq(Role._NAME, Role.CASHIER_HEAD)
					  .or()
					  .eq(Role._NAME, Role.STORE_MANAGER)
					  .or()
					  .eq(Role._NAME, Role.CEO)
					  .or().eq(Role._NAME, Role.SUPER_ADMIN)
					  .or().eq(Role._NAME, Role.SUPER_USER);

		userQb.join(roleQb);
		users = DaoManagerImpl.getUserDao().query(userQb.prepare());

		if (!users.isEmpty()) {
			return users.get(0);
		} else {
			return null;
		}
	}

	public Session findLastOpenShift() throws SQLException {
		String query = "SELECT s.*"
			+ " FROM sessions s"
			+ " WHERE s.end_time is null "
			+ "   AND s.office_id = " + SettingCtrl.getSetting().getStoreId()
			+ "   AND s.client_id = '" + SettingCtrl.getSetting().getMachineId() + "'"
			+ " ORDER BY s.start_time DESC";

		GenericRawResults<Session> rawResults = sessionDao.getDao().queryRaw(query, sessionDao.getDao().getRawRowMapper());
		Session session = rawResults.getFirstResult();
		rawResults.close();
		return session;
	}

	public Session doLogin (String username, String password) throws SQLException {

		User user = authenticate(username, password);
		return doLogin(user);
	}

	public Session doLogin (User user) throws SQLException {

		if (user!=null) {
			session = new Session();
			session.setStore(SettingCtrl.getSetting().getStore());
			session.setMachineId(SettingCtrl.getSetting().getMachineId());
			session.setUser(user);
			session.setStart(Calendar.getInstance().getTime());
			sessionDao.save(session);
			return session;
		}
		return null;
	}

	public void doLogout() throws SQLException {
		if (session != null) {
			session.setEnd(new Date());
			sessionDao.save(session);
			session = null;
		}
	}

	public String doHash(String password) {
		 try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());

	        byte[] mdbytes = md.digest();

	        //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < mdbytes.length; i++) {
	          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			logger.log(Level.SEVERE, e.toString(), e);
		}
		 return null;
	}

	public static Session getSession() {
		return session;
	}

	public static SessionCtrl getInstance() {
		if (instance == null) {
			instance = new SessionCtrl();
		}
		return instance;
	}

	public static void update() throws SQLException {
		instance.getSessionDao().save(session);
	}

	public RDao<Session> getSessionDao() {
		return sessionDao;
	}

	public void setSessionDao(RDao<Session> sessionDao) {
		this.sessionDao = sessionDao;
	}

}
