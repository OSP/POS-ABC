package id.sit.pos.master.model;

import java.math.BigDecimal;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "branch_prices")
public class ProductBranchSetting extends RModel {
	public static final String _OFFICE_ID = "percentage";
	public static final String _MARGIN_AMT = "nominal";
	public static final String _MIN_STOCK = "additional_min_stock";
	public static final String _PRODUCT_ID = "product_id";
	public static final String _STORE_ID = "branch_id";

	@DatabaseField(foreign = true, columnName = _PRODUCT_ID)
	private Product product;

	@DatabaseField(foreign = true, columnName = _STORE_ID)
	private Warehouse store;

	@DatabaseField(columnName = _OFFICE_ID)
	private Float marginPct;

	@DatabaseField(columnName = _MARGIN_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal marginAmt;

	@DatabaseField(columnName = _MIN_STOCK)
	private double stockMin;


	public ProductBranchSetting() {
	}

	public ProductBranchSetting(Product product, Warehouse branch) {
		super();
		this.product = product;
		this.store = branch;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Warehouse getStore() {
		return store;
	}

	public void setStore(Warehouse branch) {
		this.store = branch;
	}

	public Float getMarginPct() {
		return marginPct;
	}

	public void setMarginPct(float marginPct) {
		this.marginPct = marginPct;
	}

	public BigDecimal getMarginAmt() {
		return marginAmt;
	}

	public void setMarginAmt(BigDecimal marginAmt) {
		this.marginAmt = marginAmt;
	}

	public double getStockMin() {
		return stockMin;
	}

	public void setStockMin(double stockMin) {
		this.stockMin = stockMin;
	}
}
