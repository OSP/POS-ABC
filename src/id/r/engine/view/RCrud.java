package id.r.engine.view;

import java.sql.SQLException;

import com.j256.ormlite.dao.CloseableIterator;

import id.r.engine.model.RModel;
import id.r.engine.model.SearchParam;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Control;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public abstract class RCrud<T extends RModel> extends RGRidForm<T> {

	protected Service<ObservableList<T>> searchService;
	private EventHandler<KeyEvent> searchCriteriaEh;

	public RCrud() {
		searchService = new Service<ObservableList<T>>() {

			@Override
			protected Task<ObservableList<T>> createTask() {
				return new Task<ObservableList<T>>() {
					@Override
					protected ObservableList<T> call() throws InterruptedException {
						updateMessage(Config.getMessage("common_searching"));
						startProgressBar();
						ObservableList<T> result = search(getSearchParams());
						if (result.isEmpty())
							updateMessage("No data match with search criteria.");
						else
							updateMessage("");
						endProgressBar();
						return result;
					}
				};
			}

			@Override
			public boolean cancel() {
				if (super.cancel()) {
					endProgressBar();
					return true;
				}
				return false;
			}
		};

		searchCriteriaEh = new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				if (e.getCode() == KeyCode.DOWN) {
					table.requestFocus();
			        table.getSelectionModel().select(0);
			        table.getFocusModel().focus(0);
				}
				if (e.getCode() == KeyCode.ESCAPE) {
					getStage().close();
				}
			}
		};

	}

	@FXML
	public void initialize() {
		super.initialize();
		message.textProperty().bind(searchService.messageProperty());

		table.itemsProperty().bind(searchService.valueProperty());

		table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
		    if (newSelection != null) {
		        updateModel(newSelection);
		    }
		});
	};

	public abstract SearchParam[] getSearchParams();

	public ObservableList<T> search(SearchParam[] params) {
		CloseableIterator<T> iterator = null;
		try {
			ObservableList<T> ol = FXCollections.observableArrayList();

			iterator = getRDao().search(params, true);
			while (iterator.hasNext()) {
				try {
					ol.add(iterator.next());
				} catch (RuntimeException e) {
					logger.error(e.toString(), e);
				}

			}
			return ol;
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			MainApp.showAlertDbError();
		} finally {
			try {
				if (iterator != null)
					iterator.close();
			} catch (SQLException e) {
				logger.error(e.toString(), e);
			}
		}
		return null;
	}

	@FXML
	public void search(){
		if (!searchService.isRunning()) {
			searchService.reset();
			searchService.start();
		} else {
			if (searchService.cancel()) {
				searchService.reset();
				searchService.start();
			}
		}
	}

	public void setAsSearchCriteriaFields (Control... fields) {
		for (Control field : fields) {
			if (field != null)
				field.addEventFilter(KeyEvent.KEY_PRESSED, searchCriteriaEh);
		}
	}

	@Override
	public void edit() {
		super.edit();
		if (addBtn != null)
			addBtn.setDisable(true);
	}

	public void afterSave() {
		super.afterSave();
		if (addBtn != null)
			addBtn.setDisable(false);
		search();
	}
}
