package id.r.engine.controller;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import id.r.engine.model.RModel;
import id.r.engine.model.SearchParam;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.exception.UndefinedDaoException;

public class RDao<T extends RModel> {
	private static Logger logger;

	protected Dao<T, Integer> dao;

	protected Class<T> model;

	protected Map<String, Dao<?, Integer>> daoMap = new HashMap<String, Dao<?, Integer>>();

	@SuppressWarnings("unchecked")
	public RDao(Class<T> model) {
		logger = Logger.getLogger(this.getClass().getName());
		this.model = model;
		try {
			dao = (Dao<T, Integer>) loadDao(model);
		} catch (UndefinedDaoException e) {
			logger.error(e.toString(), e);
		}
	}

	@SuppressWarnings("unchecked")
	protected Dao<? extends RModel, Integer> loadDao(Class<?> model) throws UndefinedDaoException {
		try {
			String simpleName = model.getSimpleName();

			return (Dao<T, Integer>) DaoManagerImpl.class.getDeclaredMethod("get" + simpleName + "Dao", null)
					.invoke(null, null);
		} catch (NoSuchMethodException e) {
			logger.error(e.toString(), e);
			throw new UndefinedDaoException();
		} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			logger.error(e.toString(), e);
		}
		return null;
	}

	public CloseableIterator<T> searchOr(SearchParam[] params) throws SQLException {
		return search(params, false, true);
	}

	public CloseableIterator<T> search(SearchParam[] params) throws SQLException {
		return search(params, false, false);
	}

	public CloseableIterator<T> search(SearchParam[] params, boolean distinct) throws SQLException {
		return search(params, distinct, false);
	}

	public CloseableIterator<T> search(SearchParam[] params, boolean distinct, boolean useOr) throws SQLException {
		if (params != null) {

			QueryBuilder<T, Integer> qb = dao.queryBuilder();

			if (distinct)
				qb.distinct();

			Where<T, Integer> w = null;
			Map<String, QueryBuilder<? extends RModel, Integer>> qbMap = new HashMap<String, QueryBuilder<? extends RModel, Integer>>();
			Map<String, Where<? extends RModel, Integer>> wMap = new HashMap<String, Where<? extends RModel, Integer>>();
			Map<String, Integer> cCounter = new HashMap<String, Integer>();

			int i = 0;
			for (SearchParam param : params) {
				if (param.getValue() != null) {
					Object value = param.getValue();
					String strValue = null;
					if (param.getValue() instanceof String) {
						strValue = ((String) param.getValue()).trim();
						if (strValue.isEmpty()) {
							continue;
						}
					}

					Where<?, Integer> wTemp = null;
					if (param.getModel() == null || param.getModel() == model) {
						if (!(value instanceof SearchParam.Order)) {
							if (w == null)
								w = qb.where();
							wTemp = w;
							i++;
						}
					} else {
						String key = param.getModel().getCanonicalName();
						try {

							if (!qbMap.containsKey(key)) {
								QueryBuilder<? extends RModel, Integer> qbForeign = loadDao(param.getModel())
										.queryBuilder();
								qbMap.put(key, qbForeign);

								Class<?> bridges[] = param.getBridges();
								if (bridges != null && bridges.length > 0) {

									if (bridges.length == 1 && qbMap.containsKey(bridges[0].getCanonicalName())) {
										qbMap.get(bridges[0].getCanonicalName()).join(qbForeign);
									}
									for (int j = 0; j < bridges.length; j++) {
										String bridgeKey = bridges[j].getCanonicalName();
										if (!qbMap.containsKey(bridgeKey)) {
											QueryBuilder<? extends RModel, Integer> qbBridge = loadDao(bridges[j])
													.queryBuilder();
											qbMap.put(bridgeKey, qbBridge);

											if (j == 0) {
												qbBridge.join(qbForeign);
												if (bridges.length==1)
													qb.join(qbBridge);
											} else if (j == (bridges.length - 1))
												qb.join(qbBridge);
											else
												qbBridge.join(qbMap.get(bridges[j - 1].getCanonicalName()));
										}
									}
								} else {
									qb.join(qbForeign);
								}
							}
						} catch (UndefinedDaoException e) {
							logger.error(e.toString(), e);
						}

						if (!(value instanceof SearchParam.Order)) {
							if (!wMap.containsKey(key))
								wMap.put(key, qbMap.get(key).where());

							wTemp = wMap.get(key);

							if (!cCounter.containsKey(key))
								cCounter.put(key, 0);

							cCounter.replace(key, cCounter.get(key) + 1);
						}
					}

					if (value instanceof String)
						if (param.getOperator() == SearchParam.Operator.S)
							wTemp.like(param.getColumnName(), "%" + strValue + "%");
						else if (param.getOperator() == SearchParam.Operator.E)
							wTemp.eq(param.getColumnName(), strValue);
						else if (strValue.startsWith(">"))
							wTemp.gt(param.getColumnName(), (strValue.replace(">", "").trim()));
						else if (strValue.startsWith(">="))
							wTemp.ge(param.getColumnName(), (strValue.replace(">=", "").trim()));
						else if (strValue.startsWith("<"))
							wTemp.lt(param.getColumnName(), (strValue.replace("<", "").trim()));
						else if (strValue.startsWith("<="))
							wTemp.le(param.getColumnName(), (strValue.replace("<=", "").trim()));
						else if (strValue.startsWith("<>"))
							wTemp.ne(param.getColumnName(), (strValue.replace("<>", "").trim()));
						else if (strValue.startsWith("="))
							wTemp.eq(param.getColumnName(), (strValue.replace("=", "").trim()));
						else if (strValue.startsWith(",")) {
							Iterable<String> strings = Arrays.asList(strValue.split(","));
							strings.forEach(v -> {
								v.trim();
							});
							wTemp.in(param.getColumnName(), strings.iterator());
						} else
							wTemp.like(param.getColumnName(), "%" + value + "%");

					else if (value instanceof SearchParam.Order)
						if ((SearchParam.Order) value == SearchParam.Order.ASC)
							qb.orderBy(param.getColumnName(), true);
						else
							qb.orderBy(param.getColumnName(), false);
					else {
						if (param.getOperator() == null)
							wTemp.eq(param.getColumnName(), value);
						else
							switch (param.getOperator()) {
							case LT:
								wTemp.lt(param.getColumnName(), value);
								break;
							case LTE:
								wTemp.le(param.getColumnName(), value);
								break;
							case E:
								wTemp.eq(param.getColumnName(), value);
								break;
							case GTE:
								wTemp.ge(param.getColumnName(), value);
								break;
							case GT:
								wTemp.gt(param.getColumnName(), value);
								break;
							case N:
								wTemp.ne(param.getColumnName(), value);
								break;
							case I:
								// TODO not supported yet
								logger.error("in operation is not supported yet");
							default:
								break;
							}
					}
				}
			}

			if (w != null && i > 1) {
				w.and(i);
			}

			if (useOr)
				for (Entry<String, Where<? extends RModel, Integer>> entry : wMap.entrySet()) {
					int c = cCounter.get(entry.getKey());
					if (c > 1) {
						entry.getValue().or(c);
					}
				}
			else
				for (Entry<String, Where<? extends RModel, Integer>> entry : wMap.entrySet()) {
					int c = cCounter.get(entry.getKey());
					if (c > 1) {
						entry.getValue().and(c);
					}
				}

			PreparedQuery<T> pq = qb.prepare();
			logger.info(String.format("Executing Query: %s", pq.getStatement()));
			return dao.iterator(pq);
		} else {
			return dao.iterator();
		}
	}

	public T queryFirst(SearchParam[] params) throws SQLException {
		return queryFirst(params, false, false);
	}

	public T queryFirst(SearchParam[] params, boolean distinct, boolean useOr) throws SQLException {
		CloseableIterator<T> results = search(params, distinct, useOr);
		T result = null;
		if (results.hasNext()) {
			result = results.next();
			results.closeQuietly();
		}
		return result;
	}

	public void beforeSave(RModel model) {
		if (model.getId() == null)
			model.setCreatedAt(new Date());

		if (model.getId() != null)
			model.setUpdatedAt(new Date());
	}

	public CreateOrUpdateStatus save(T model) throws SQLException {
		beforeSave(model);
		CreateOrUpdateStatus status = dao.createOrUpdate(model);
		afterSave(model);
		return status;
	}

	public void afterSave(RModel model) {

	}

	public int delete(T model) throws SQLException {
		return dao.delete(model);
	}

	public Dao<T, Integer> getDao() {
		return dao;
	}

	public static Logger getLogger() {
		return logger;
	}
}
