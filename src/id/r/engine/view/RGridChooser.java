package id.r.engine.view;

import id.r.engine.model.RModel;
import id.sit.pos.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.TableRow;
import javafx.scene.input.KeyCode;

public abstract class RGridChooser<T extends RModel> extends RGridSearch<T> implements RChooser {

	protected T selected;

	public RGridChooser() {
		super();
	}

	@FXML
	public void initialize() {
		super.initialize();

		table.setRowFactory(tv -> {
			TableRow<T> row = new TableRow<>();

			row.setOnMouseClicked(event -> {
				selected = row.getItem();

				if (event.getClickCount() == 1 && (!row.isEmpty())) {
				}
				if (event.getClickCount() == 2 && (!row.isEmpty())) {
					choose();
				}
			});

			return row;
		});

		table.setOnKeyPressed( keyEvent-> {
				final T selectedItem = table.getSelectionModel().getSelectedItem();

				if (selectedItem != null) {
					if (keyEvent.getCode().equals(KeyCode.ENTER)) {
						selected = selectedItem;
						choose();
					} else if (keyEvent.getCode().equals(KeyCode.ESCAPE))
						if (searchCriterias.size()>0)
							searchCriterias.get(0).requestFocus();
				}
			}
		);
	}

	@FXML
	@Override
	public void choose() {
		if (selected != null)
			getStage().close();
		else
			MainApp.showAlertError("browser_no_selection");
	}

	@FXML
	@Override
	public void cancel() {
		selected = null;
		getStage().close();
	}

	public T getSelected() {
		return selected;
	}
}
