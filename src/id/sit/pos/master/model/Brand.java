package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "brands")
public class Brand extends RModel{
	public static final String _NAME = "name";

	@DatabaseField
	private String code;

	@DatabaseField
	private String name;

	@DatabaseField(columnName = "discount_percent1")
	private float discountPct1;

	@DatabaseField(columnName = "discount_percent2")
	private float discountPct2;

	@DatabaseField(columnName = "discount_percent3")
	private float discountPct3;

	@DatabaseField(columnName = "discount_percent4")
	private float discountPc4;

	public Brand() {
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getDiscountPct1() {
		return discountPct1;
	}

	public void setDiscountPct1(float discountPct1) {
		this.discountPct1 = discountPct1;
	}

	public float getDiscountPct2() {
		return discountPct2;
	}

	public void setDiscountPct2(float discountPct2) {
		this.discountPct2 = discountPct2;
	}

	public float getDiscountPct3() {
		return discountPct3;
	}

	public void setDiscountPct3(float discountPct3) {
		this.discountPct3 = discountPct3;
	}

	public float getDiscountPc4() {
		return discountPc4;
	}

	public void setDiscountPc4(float discountPc4) {
		this.discountPc4 = discountPc4;
	}

}