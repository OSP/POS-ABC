package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "pos_machines")
public class PosMachine extends RModel{

	public static final String _OFFICE_ID = "office_id";
	public static final String _UUID = "uuid";
	public static final String _NAME= "name";

	/* hot key setting */
	public static final String _HK_CUSTOMER= "hotkey_customer";
	public static final String _HK_BROWSE_PRODUCT = "hotkey_browse_product";
	public static final String _HK_CHECK_ALL_STOCK = "hotkey_check_all_stock";
	public static final String _HK_RETURN_TRANS = "hotkey_return_trans";
	public static final String _HK_PENDING_TRANS = "hotkey_pending_trans";
	public static final String _HK_DEBIT = "hotkey_debit";
	public static final String _HK_CREDIT = "hotkey_credit";
	public static final String _HK_TRANSFER = "hotkey_transfer";
	public static final String _HK_CASH = "hotkey_cash";
	public static final String _HK_SAVE_TRANS = "hotkey_save_trans";
	public static final String _HK_PRINT_TRANS = "hotkey_print_trans";
	public static final String _HK_DELETE_ITEM = "hotkey_delete_item";
	public static final String _HK_DISCOUNT = "hotkey_discount";
	public static final String _HK_SELECT_ITEM = "hotkey_select_item";
	public static final String _HK_BARCODE = "hotkey_barcode";
	public static final String _HK_VOID_TRANS = "hotkey_void_trans";
	public static final String _HK_VOUCHER = "hotkey_voucher";

	public static final String _HIDE_EMPTY_STOCK = "hide_empty_stock";
	public static final String _ENABLE_PPN = "enable_ppn";
	public static final String _ENABLE_MINUS_STOCK = "enable_minus_stock";
	public static final String _ENABLE_LOSS_PRICE = "enable_loss_price";

	public static final String _ENABLE_NAME = "enable_name";
	public static final String _ENABLE_COLOUR = "enable_colour";
	public static final String _ENABLE_PRICE_LEVEL = "enable_price_level";
	public static final String _ENABLE_SIZE = "enable_size";
	public static final String _ENABLE_RETURN_CASH = "enable_return_cash";
	public static final String _ENABLE_DISCOUNT = "enable_discount";
	public static final String _ENABLE_SUPER_KEY = "enable_super_key";


	public static final String _RECEIPT_FOOTER = "receipt_footer";
	public static final String _EOD_FOOTER = "eod_footer";


	@DatabaseField(columnName = _OFFICE_ID)
	private String store;

	@DatabaseField(columnName = _UUID)
	private String uuid;

	@DatabaseField(columnName = _NAME)
	private String name;

	@DatabaseField(columnName = _HK_CUSTOMER)
	private String hkCustomer;

	@DatabaseField(columnName = _HK_BROWSE_PRODUCT)
	private String hkBrowseProduct;

	@DatabaseField(columnName = _HK_CHECK_ALL_STOCK)
	private String hkCheckAllStock;

	@DatabaseField(columnName = _HK_RETURN_TRANS)
	private String hkReturnTrans;

	@DatabaseField(columnName = _HK_PENDING_TRANS)
	private String hkPendingTrans;

	@DatabaseField(columnName = _HK_DEBIT)
	private String hkDebit;

	@DatabaseField(columnName = _HK_CREDIT)
	private String hkCredit;

	@DatabaseField(columnName = _HK_TRANSFER)
	private String hkTransfer;

	@DatabaseField(columnName = _HK_CASH)
	private String hkCash;

	@DatabaseField(columnName = _HK_SAVE_TRANS)
	private String hkSaveTrans;

	@DatabaseField(columnName = _HK_PRINT_TRANS)
	private String hkPrintTrans;

	@DatabaseField(columnName = _HK_DELETE_ITEM)
	private String hkDeleteItem;

	@DatabaseField(columnName = _HK_DISCOUNT)
	private String hkDiscount;

	@DatabaseField(columnName = _HK_SELECT_ITEM)
	private String hkSelectItem;

	@DatabaseField(columnName = _HK_BARCODE)
	private String hkBarcode;

	@DatabaseField(columnName = _HK_VOID_TRANS)
	private String hkVoidTrans;

	@DatabaseField(columnName = _HK_VOUCHER)
	private String hkVoucher;

	@DatabaseField(columnName = _HIDE_EMPTY_STOCK)
	private Boolean hideEmptyStock;

	@DatabaseField(columnName = _ENABLE_PPN)
	private Boolean enablePPN;

	@DatabaseField(columnName = _ENABLE_MINUS_STOCK)
	private Boolean enableMinusStock;

	@DatabaseField(columnName = _ENABLE_LOSS_PRICE)
	private Boolean enableLossPrice;

	@DatabaseField(columnName = _ENABLE_NAME)
	private Boolean enableName;

	@DatabaseField(columnName = _ENABLE_COLOUR)
	private Boolean enableColour;

	@DatabaseField(columnName = _ENABLE_PRICE_LEVEL)
	private Boolean enablePriceLevel;

	@DatabaseField(columnName = _ENABLE_SIZE)
	private Boolean enableSize;

	@DatabaseField(columnName = _ENABLE_RETURN_CASH)
	private Boolean enableReturnCash;

	@DatabaseField(columnName = _ENABLE_DISCOUNT)
	private Boolean enableDiscount;

	@DatabaseField(columnName = _ENABLE_SUPER_KEY)
	private Boolean enableSuperKey;

	@DatabaseField(columnName = _RECEIPT_FOOTER)
	private String receiptFooter;

	@DatabaseField(columnName = _EOD_FOOTER)
	private String eodFooter;

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHkCustomer() {
		return hkCustomer;
	}

	public String getHkBrowseProduct() {
		return hkBrowseProduct;
	}

	public String getHkCheckAllStock() {
		return hkCheckAllStock;
	}

	public String getHkReturnTrans() {
		return hkReturnTrans;
	}

	public String getHkPendingTrans() {
		return hkPendingTrans;
	}

	public String getHkDebit() {
		return hkDebit;
	}

	public String getHkTransfer() {
		return hkTransfer;
	}

	public String getHkCash() {
		return hkCash;
	}

	public String getHkSaveTrans() {
		return hkSaveTrans;
	}

	public String getHkPrintTrans() {
		return hkPrintTrans;
	}

	public String getHkDeleteItem() {
		return hkDeleteItem;
	}

	public String getHkDiscount() {
		return hkDiscount;
	}

	public String getHkSelectItem() {
		return hkSelectItem;
	}

	public String getHkBarcode() {
		return hkBarcode;
	}

	public String getHkVoidTrans() {
		return hkVoidTrans;
	}

	public String getHkVoucher() {
		return hkVoucher;
	}

	public String getHkCredit() {
		return hkCredit;
	}

	public Boolean isHideEmptyStock() {
		return hideEmptyStock;
	}

	public Boolean isEnablePPN() {
		return enablePPN;
	}

	public Boolean isEnableMinusStock() {
		return enableMinusStock;
	}

	public Boolean isEnableLossPrice() {
		return enableLossPrice;
	}

	public String getReceiptFooter() {
		return receiptFooter;
	}

	public String getEodFooter() {
		return eodFooter;
	}

	public Boolean isEnableName() {
		return enableName;
	}

	public Boolean isEnableColour() {
		return enableColour;
	}

	public Boolean isEnablePriceLevel() {
		return enablePriceLevel;
	}

	public Boolean isEnableSize() {
		return enableSize;
	}

	public Boolean isEnableReturnCash() {
		return enableReturnCash;
	}

	public Boolean isEnableDiscount() {
		return enableDiscount;
	}

	public Boolean isEnableSuperKey() {
		return enableSuperKey;
	}

}
