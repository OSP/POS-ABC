package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "units")
public class Unit extends RModel{
	public static final String _NAME = "name";
	public static final String _SHORT_NAME = "short_name";

	@DatabaseField(columnName=_NAME)
	private String name;

	@DatabaseField(columnName=_SHORT_NAME)
	private String shortName;

	public Unit(){

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}


}
