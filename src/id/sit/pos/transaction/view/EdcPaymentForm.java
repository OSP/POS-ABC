package id.sit.pos.transaction.view;

import java.math.BigDecimal;
import java.sql.SQLException;

import id.r.engine.controller.RDao;
import id.r.engine.view.RForm;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.master.model.EDC;
import id.sit.pos.transaction.model.BankPayment;
import id.sit.pos.transaction.model.EdcPayment;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.DeviceUtil;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import jpos.JposException;


import org.comtel2000.keyboard.control.VkProperties;

public class EdcPaymentForm extends RForm<EdcPayment> implements VkProperties {
	@FXML
	private ComboBox<EDC> bankName;

	@FXML
	private TextField cardNumber;

	@FXML
	private Label remainingToBePaid;

	@FXML
	private Label additionalCharge;

	@FXML
	private TextField paymentAmt;

	private EdcPayment.Type type;

	private BigDecimal toBePaidAmt;

	private BigDecimal chargeAmt;

	private Service<Void> msrService;

	public EdcPaymentForm() {
		super();
		if (type == EdcPayment.Type.CREDIT)
			setTitle(Config.getMessage("trans_credit_title"));
		else if (type == EdcPayment.Type.DEBIT)
			setTitle(Config.getMessage("trans_debit_title"));
		else if (type == EdcPayment.Type.TRANSFER)
			setTitle(Config.getMessage("trans_transfer_title"));

		msrService = new Service<Void>() {

			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() throws InterruptedException {
						try {
							if (Config.isEnableMsr())
								DeviceUtil.getMsrDevice().readOnce(data->{
									Platform.runLater(()->{
										cardNumber.setText(data.getAccountNumber());
									});
								});
						} catch (JposException e) {
							logger.error( e.toString(), e);
						}
						return null;
					}
				};
			}
		};
	}

	@FXML
	public void initialize() {
		super.initialize();

		bankName.setCellFactory((comboBox) -> {
		    return new ListCell<EDC>() {
		        @Override
		        protected void updateItem(EDC item, boolean empty) {
		            super.updateItem(item, empty);

		            if (item == null || empty) {
		                setText(null);
		            } else {
		                setText(item.getBankName());
		            }
		        }
		    };
		});

		// Define rendering of selected value shown in ComboBox.
		bankName.setConverter(
				new StringConverter<EDC>() {
		    @Override
		    public String toString(EDC edc) {
		        if (edc == null) {
		            return null;
		        } else {
		            return edc.getBankName();
		        }
		    }

		    @Override
		    public EDC fromString(String personString) {
		        return null; // No conversion fromString needed.
		    }
		});

		try {
			bankName.getItems().addAll(DaoManagerImpl.getEdcDao().queryForAll());
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			MainApp.showAlertDbError();
		}

		bankName.requestFocus();



		setAsMandatoryFields(bankName, cardNumber, paymentAmt);
		setAsNumericFields(cardNumber);
		setAsCurrencyFields(paymentAmt);
		setAsFormFields(bankName, cardNumber, additionalCharge, paymentAmt);
//		remainingToBePaid.setText(CurrencyUtil.format(toBePaidAmt));

		if (Config.isEnableOnScreenKeyboard()){
			cardNumber.getProperties().put(VK_TYPE, VK_TYPE_NUMERIC);
			paymentAmt.getProperties().put(VK_TYPE, VK_TYPE_NUMERIC);
		}

		msrService.start();
	}

	@FXML
	private void bankNameOnAction(){
		if(this.getType().equals(BankPayment.Type.CREDIT)) {
			if (bankName.getSelectionModel().getSelectedItem()!=null) {
				EDC edc = bankName.getSelectionModel().getSelectedItem();
				model.setBankName(edc.getBankName());

				Float addChargePct = edc.getCreditCharge();
				if (addChargePct == null)
					addChargePct = 0f;

				BigDecimal addChargeAmnt= new BigDecimal((addChargePct/100) * toBePaidAmt.floatValue());
				additionalCharge.setText(addChargeAmnt.toString());
				remainingToBePaid.setText(CurrencyUtil.format(toBePaidAmt.add(addChargeAmnt)));
				model.setAdditionalCharge(addChargeAmnt);
			}
		}
	}


	@Override
	public RDao<EdcPayment> initRDao() {
		return new RDao<EdcPayment>(EdcPayment.class);
	}

	@Override
	public EdcPayment initModel() {
		return new EdcPayment();
	}

	@Override
	public void updateForm(EdcPayment model) {
		bankName.getSelectionModel().select(model.getEdc());
		cardNumber.setText(model.getCardNumber());
		if (model.getAdditionalCharge() != null)
			additionalCharge.setText(model.getAdditionalCharge().toString());

		if (model.getToBePaidAmt() != null)
			remainingToBePaid.setText(CurrencyUtil.format(model.getToBePaidAmt()));

		if (model.getPaymentAmt() != null)
			paymentAmt.setText(model.getPaymentAmt().toString());
	}

	@Override
	public EdcPayment fetchFormData() {
		model.setType(type);
		model.setEdc(bankName.getSelectionModel().getSelectedItem());
		model.setCardNumber(cardNumber.getText());
		model.setToBePaidAmt(toBePaidAmt);
		model.setPaymentAmt(CurrencyUtil.parse(paymentAmt.getText()));

		return model;
	}

	@Override
	public void afterSave() {
		super.afterSave();
		getStage().close();
	}

	// Generated code
	public EdcPayment.Type getType() {
		return type;
	}

	public void setType(EdcPayment.Type type) {
		this.type = type;
	}

	public BigDecimal getToBePaidAmt() {
		return toBePaidAmt;
	}

	public void setToBePaidAmt(BigDecimal toBePaidAmt) {
		this.toBePaidAmt = toBePaidAmt;
	}

	public BigDecimal getChargeAmt() {
		return chargeAmt;
	}

	public void setChargeAmt(BigDecimal chargeAmt) {
		this.chargeAmt = chargeAmt;
	}


}
