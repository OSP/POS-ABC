package id.sit.pos.master.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "offices")
public class Warehouse extends RModel {
	public enum Type {
		Branch, HeadOffice
	}

	public static final String _HEAD_OFFICE_ID = "head_office_id";
	public static final String _TYPE = "type";
	public static final String _NAME = "office_name";
	public static final String _DESCRIPTION = "description";

	@DatabaseField(columnName = _HEAD_OFFICE_ID, foreign = true, foreignAutoRefresh = true)
	private Warehouse headOffice;

	@DatabaseField(columnName = _TYPE)
	private Type type;

	@DatabaseField(columnName = _NAME)
	private String name;

	@DatabaseField(columnName = _DESCRIPTION)
	private String description;

	@DatabaseField(columnName = "warehouse")
	private String warehouse;

	@DatabaseField(columnName = "phone_number")
	private String phoneNumber;

	@DatabaseField(columnName = "mobile_phone")
	private String mPhoneNumber;

	@DatabaseField
	private String address;

	@DatabaseField
	private String city;

	@ForeignCollectionField
	private ForeignCollection<Warehouse> branches;

	public Warehouse() {

	}

	public Warehouse(Integer id) {
		this.id = id;
	}

	public Warehouse getHeadOffice() {
		return headOffice;
	}

	public void setHeadOffice(Warehouse headOffice) {
		this.headOffice = headOffice;
	}

	@Override
	public String toString(){
		return getName();
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getmPhoneNumber() {
		return mPhoneNumber;
	}

	public void setmPhoneNumber(String mPhoneNumber) {
		this.mPhoneNumber = mPhoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public ForeignCollection<Warehouse> getBranches() {
		return branches;
	}

	public void setBranches(ForeignCollection<Warehouse> branches) {
		this.branches = branches;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
