package id.sit.pos.transaction.view;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.comtel2000.keyboard.control.VkProperties;

import id.r.engine.controller.RDao;
import id.r.engine.view.RForm;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.transaction.controller.SodEodCtrl;
import id.sit.pos.transaction.model.SodEod;
import id.sit.pos.transaction.model.SodEod.Type;
import id.sit.pos.user.controller.SessionCtrl;
import id.sit.pos.user.model.Role;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.DateUtil;
import id.sit.pos.util.DeviceUtil;
import id.sit.pos.util.PrintUtil;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class StartShiftForm extends RForm<SodEod> implements VkProperties{
	@FXML
	public Label store;

	@FXML
	public Label machine;

	@FXML
	public Label username;

	@FXML
	public Label startTime;

	@FXML
	public ComboBox<Integer> shift;

	@FXML
	private GridPane moneyFraction;

	@FXML
	public TextField start100K;

	@FXML
	public TextField start50K;

	@FXML
	public TextField start20K;

	@FXML
	public TextField start10K;

	@FXML
	public TextField start5K;

	@FXML
	public TextField start2K;

	@FXML
	public TextField start1K;

	@FXML
	public TextField start500;

	@FXML
	public TextField totalModal;

	@FXML
	public Button endDayBtn;

	@FXML
	public Button editBtn;

	@FXML
	public Button verifyBtn;

	protected Service<Void> printInisialService;
	private SodEodCtrl ctrl;

	public StartShiftForm() {
		super();
		setTitle(Config.getMessage("sod_title"));
		getIcons().add(new Image(Config.getLogoPath()));

		ctrl = new SodEodCtrl(getRDao());

		printInisialService = new Service<Void>() {
			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() throws Exception {
						startProgressBar();
						String inisialReport = ctrl.generateInisialReport();

						if (Config.isEnableAdvancedPrinting()){
							DeviceUtil.getPosPrintDevice().print(inisialReport);
						} else {
							PrintUtil.printText(inisialReport);
						}

						if(Config.isEnableCashDrawer()){
							DeviceUtil.getCashDrawerDevice().open();
						};

						endProgressBar();
						return null;
					}
				};
			}
		};
	}

	@FXML
	public void initialize(){
		super.initialize();

		stage.setOnCloseRequest(event-> {
			Platform.exit();
		});

		if (Config.isEnableSodEodFraction()) {
			totalModal.setDisable(true);
			setAsFormFields(start100K, start50K, start20K, start10K, start5K, start2K, start1K, start500);
		} else {
			moneyFraction.setVisible(false);
			moneyFraction.setManaged(false);
			setAsFormFields(totalModal);
			setAsCurrencyFields(totalModal);
		}
		setAsNumericFields(start100K, start50K, start20K, start10K, start5K, start2K, start1K, start500);

		if (SessionCtrl.getSession().getSodEod() != null) {
			shift.getSelectionModel().select(SessionCtrl.getSession().getSodEod().getShiftNo());
			shift.setDisable(true);
			loadShift(SessionCtrl.getSession().getSodEod());
			setMode(Mode.VIEW);
			editBtn.setDisable(true);
			endDayBtn.setDisable(true);
		} else {
			try {
				int lastShiftNo = ctrl.getLastShiftNo();
				shift.getItems().addAll(lastShiftNo+1);
				shift.getSelectionModel().select(0);
			} catch (SQLException e) {
				logger.error(e.toString(), e);
				MainApp.showAlertDbError();
			}
			shift.getSelectionModel().select(0);
			setMode(Mode.EDIT);
		}

		String userRole = SessionCtrl.getSession().getUser().getRole().getName();
		if (!userRole.equals(Role.CASHIER_HEAD) && !userRole.equals(Role.STORE_MANAGER)) {
			endDayBtn.setVisible(false);
		}

		endDayBtn.defaultButtonProperty().bind(endDayBtn.focusedProperty());
		verifyBtn.defaultButtonProperty().bind(verifyBtn.focusedProperty());

		store.setText(SettingCtrl.getSetting().getStore().getName());
		machine.setText(SettingCtrl.getSetting().getMachineName());
		username.setText(SessionCtrl.getSession().getUser().getUsername());
		totalModal.getProperties().put(VK_TYPE, VK_TYPE_NUMERIC);
	}

	@Override
	public RDao<SodEod> initRDao() {
		return new RDao<>(SodEod.class);
	}

	@Override
	public SodEod initModel() {
		SodEod sodEod = new SodEod(Type.SHIFT);
		sodEod.setStore(SettingCtrl.getSetting().getStore());
		sodEod.setMachineId(SettingCtrl.getSetting().getMachineId());
		sodEod.setUser(SessionCtrl.getSession().getUser());
		Calendar cal = Calendar.getInstance();
	    cal.set(Calendar.HOUR_OF_DAY, 0);
	    cal.set(Calendar.MINUTE, 0);
	    cal.set(Calendar.SECOND, 0);
	    cal.set(Calendar.MILLISECOND, 0);
		sodEod.setDate(cal.getTime());
		return sodEod;
	}

	@Override
	public void setMode(Mode mode) {
		super.setMode(mode);
		if (mode == Mode.VIEW) {
			verifyBtn.setDisable(false);
		} else {
			verifyBtn.setDisable(true);
		}
	}

	@Override
	public void beforeSave() {
		super.beforeSave();

		model.setStart(new Date());

		if (Config.isEnableSodEodFraction()) {
			model.setModal(SodEod.V100K.multiply(new BigDecimal(model.getStart100K()))
					.add(SodEod.V50K.multiply(new BigDecimal(model.getStart50K())))
					.add(SodEod.V20K.multiply(new BigDecimal(model.getStart20K())))
					.add(SodEod.V10K.multiply(new BigDecimal(model.getStart10K())))
					.add(SodEod.V5K.multiply(new BigDecimal(model.getStart5K())))
					.add(SodEod.V2K.multiply(new BigDecimal(model.getStart2K())))
					.add(SodEod.V1K.multiply(new BigDecimal(model.getStart1K())))
					.add(SodEod.V500.multiply(new BigDecimal(model.getStart500()))));
		}

		if (model.getModal() == null)
			model.setModal(BigDecimal.ZERO);
	}

	@Override
	public void afterSave() {
		SessionCtrl.getSession().setSodEod(model);
		SessionCtrl.getSession().setShiftNo(model.getShiftNo());
		try {
			SessionCtrl.update();
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			MainApp.showAlertDbError();
		}
		super.afterSave();
		shift.setDisable(true);
		setFormDisable(true);
		editBtn.setDisable(false);
		editBtn.requestFocus();
		verifyBtn.setDisable(false);
		saveBtn.setDisable(true);
		endDayBtn.setDisable(true);
	}

	public void totalModalOnAction() {
		saveBtn.fire();
	}

	public void verify() {
		SessionCtrl.getSession().setSodEod(model);
		/* bug fix inisial not printing
		 * 2016/05/27 - fzr
		*/
		if (!printInisialService.isRunning()){
			printInisialService.reset();
			printInisialService.start();
		}
		getStage().close();
	}

	@Override
	public void updateForm(SodEod model) {
		shift.getSelectionModel().select(model.getShiftNo());
		startTime.setText(DateUtil.formatDateTime(model.getStart()));

		if (model.getStart100K() != null)
			start100K.setText(model.getStart100K().toString());

		if (model.getStart50K() != null)
			start50K.setText(model.getStart50K().toString());

		if (model.getStart20K() != null)
			start20K.setText(model.getStart20K().toString());

		if (model.getStart10K() != null)
			start10K.setText(model.getStart10K().toString());
		else
			start10K.setText(null);

		if (model.getStart5K() != null)
			start5K.setText(model.getStart5K().toString());

		if (model.getStart2K() != null)
			start2K.setText(model.getStart2K().toString());

		if (model.getStart1K() != null)
			start1K.setText(model.getStart1K().toString());

		if (model.getStart500() != null)
			start500.setText(model.getStart500().toString());

		totalModal.setText(CurrencyUtil.format(model.getModal(), false));
	}

	@Override
	public SodEod fetchFormData() {
		model.setShiftNo(shift.getSelectionModel().getSelectedItem());
		if (Config.isEnableSodEodFraction()) {
			if (start100K.getText() == null)
				start100K.setText("0");
			model.setStart100K(new Integer(start100K.getText()));

			if (start50K.getText() == null)
				start50K.setText("0");
			model.setStart50K(new Integer(start50K.getText()));

			if (start20K.getText() == null)
				start20K.setText("0");
			model.setStart20K(new Integer(start20K.getText()));

			if (start10K.getText() == null)
				start10K.setText("0");
			model.setStart10K(new Integer(start10K.getText()));

			if (start5K.getText() == null)
				start5K.setText("0");
			model.setStart5K(new Integer(start5K.getText()));

			if (start2K.getText() == null)
				start2K.setText("0");
			model.setStart2K(new Integer(start2K.getText()));

			if (start1K.getText() == null)
				start1K.setText("0");
			model.setStart1K(new Integer(start1K.getText()));

			if (start500.getText() == null)
				start500.setText("0");
			model.setStart500(new Integer(start500.getText()));
		} else {
			model.setModal(CurrencyUtil.parse(totalModal.getText()));
		}

		return model;
	}

	@FXML
	public void endDay() {
		MainApp.getInstance().showEodPopup(true);
		getStage().close();
	}

	public void loadShift(SodEod sodEod) {
		model = sodEod;
		setMode(RForm.Mode.VIEW);
		editBtn.setDisable(true);
		updateForm(model);
	}
}
