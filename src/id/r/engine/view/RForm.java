package id.r.engine.view;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import id.r.engine.controller.RDao;
import id.r.engine.model.RModel;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.util.StringUtil;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public abstract class RForm<T extends RModel> extends RUi {
	public enum MessageType {
		INFO, ERROR
	}

	public enum Mode {
		VIEW, EDIT
	}

	protected static Logger logger;

	private Mode mode;

	@FXML
	protected Button editBtn;

	@FXML
	protected Button saveBtn;

	@FXML
	protected Button cancelBtn;

	protected T model;

	protected RDao<T> rDao;

	protected Service<T> saveService;

	protected List<Control> formFields = new ArrayList<Control>();

	protected List<Control> mandatoryFields = new ArrayList<Control>();

	private EventHandler<KeyEvent> formFieldEh;

	public RForm() {
		logger = Logger.getLogger(this.getClass().getName());
		rDao = initRDao();

		saveService = new Service<T>() {

			@Override
			protected Task<T> createTask() {
				return new Task<T>() {
					@Override
					protected T call() throws InterruptedException, SQLException {
						updateMessage(Config.getMessage("common_saving"));
						startProgressBar();

						try {
							rDao.save(model);
							return model;
						} catch (SQLException e) {
							logger.error(e.toString(), e);
							throw e;
						} finally {
							endProgressBar();
						}
					}
				};
			}
		};

		saveService.setOnSucceeded(t -> {
			afterSave();
		});

		saveService.setOnFailed(t -> {
			Throwable e = t.getSource().getException();
			logger.error(e.toString(), e);
			MainApp.showAlertDbError(getStage());
			saveBtn.setDisable(false);
		});

		formFieldEh = new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				if (e.getCode() == KeyCode.ESCAPE) {
					if (getMode() == Mode.EDIT) {
						cancel();
					}
				}
			}
		};

	}

	@FXML
	public void initialize() {
		if (message != null)
			message.textProperty().bind(saveService.messageProperty());

		if (saveBtn != null) {
			saveBtn.defaultButtonProperty().bind(saveBtn.focusedProperty());
		}
		if (cancelBtn != null) {
			cancelBtn.defaultButtonProperty().bind(cancelBtn.focusedProperty());
			cancelBtn.disableProperty().bind(saveBtn.disabledProperty());
		}

		if (editBtn != null) {
			editBtn.defaultButtonProperty().bind(editBtn.focusedProperty());
		}

		if (model == null)
			model = initModel();
		updateForm();

		if (editBtn == null)
			setMode(Mode.EDIT);
		else
			setMode(Mode.VIEW);
	}

	protected abstract RDao<T> initRDao();

	protected void setAsFormFields(Control... fields) {
		int i = 0;
		for (Control field : fields) {
			final int idx = i+1;

			field.addEventFilter(KeyEvent.ANY, formFieldEh);
			if (field instanceof TextArea) {
				field.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
					if (e.getCode() == KeyCode.TAB) {
						if (formFields.size()>(idx)) {
							formFields.get(idx).requestFocus();
						}
					}
				});
			}
			formFields.add(field);
			i++;
		}
	}

	protected void setAsMandatoryFields(Control... fields) {
		for (int i=0; i<fields.length; i++)
			mandatoryFields.add(fields[i]);
	}

	protected abstract T initModel();

	public void updateModel(T model) {
		this.model = model;
		resetForm();
		if (model != null)
			updateForm();
	}

	public void updateForm() {
		updateForm(model);
	}

	protected abstract void updateForm(T model);

	protected abstract T fetchFormData();

	public void setMode(Mode mode) {
		this.mode = mode;
		switch (mode) {
		case VIEW:
			setFormDisable(true);
			if (saveBtn != null) {
				saveBtn.setDisable(true);
			}
			if (editBtn != null) {
				editBtn.setDisable(false);
				editBtn.addEventFilter(KeyEvent.ANY, e->{
					if (e.getCode() == KeyCode.ESCAPE) {
						getStage().close();
					}
				});
			}
			break;
		case EDIT:
			setFormDisable(false);
			if (formFields.size()>0)
				formFields.get(0).requestFocus();

			if (saveBtn != null) {
				saveBtn.setDisable(false);
			}

			if (editBtn != null)
				editBtn.setDisable(true);
			break;
		}
	}

	protected void setFormDisable(boolean isDisabled) {
		formFields.forEach(field -> {
			if (field instanceof TextInputControl || field instanceof ComboBox<?> || field instanceof DatePicker)
				field.setDisable(isDisabled);
		});
	}

	public void resetModel() {
		model = initModel();
		resetForm();
		updateForm();
	}

	public void resetForm() {
		for (Control formField : formFields) {
			if (formField instanceof TextInputControl)
				((TextInputControl) formField).setText(null);
			else if (formField instanceof ComboBox<?>)
				((ComboBox<?>) formField).valueProperty().set(null);
			else if (formField instanceof DatePicker)
				((DatePicker) formField).setValue(null);
		}
	}

	@FXML
	public void edit() {
		setMode(Mode.EDIT);
		formFields.get(0).requestFocus();
	}

	public void beforeSave() {
		clearMessage();
		model = fetchFormData();
		if (saveBtn != null)
			saveBtn.setDisable(true);
	}

	public boolean validate() {
		Object mandatory = null;
		for (Control control : mandatoryFields) {
			if (control instanceof TextInputControl)
				mandatory = ((TextInputControl) control).getText();
			else if (control instanceof ComboBox<?>)
				mandatory = ((ComboBox<?>) control).getSelectionModel().getSelectedItem();
			else if (control instanceof DatePicker)
				mandatory = ((DatePicker) control).getValue();

			if (mandatoryFields.size()>0 && (mandatory == null || (mandatory instanceof String && StringUtil.isEmpty((String)mandatory)))) {
				showAlertError("common_error_mandatory");
				return false;
			}
		}
		return true;
	}

	public void save() {
		if (model == null)
			model = initModel();

		if (!validate())
			return;

		beforeSave();
		if (!saveService.isRunning()) {
			saveService.reset();
			saveService.start();
		}
	}

	public void afterSave() {
		resetForm();
		updateForm();
		showMessage(Config.getMessage("common_save_success"), MessageType.INFO);
		setMode(Mode.VIEW);
	}

	public void cancel() {
		if (editBtn == null) {
			getStage().close();
		} else {
			setMode(Mode.VIEW);
			resetForm();
			updateForm();
		}
	}

	@FXML
	public void delete() {
		try {
			if (this.model != null && this.model.getId() != null)
				rDao.delete(this.model);
			this.model = initModel();
			updateForm();
		} catch (SQLException e) {
			MainApp.showAlertError("common_error_db_content");
		}
	}

	public void showMessage(String message, MessageType type) {
		if (this.message != null) {
			if (type == MessageType.INFO)
				this.message.setText(message);
			else {// ERROR
				this.message.setText(message);
			}
		}
	}

	public void clearMessage() {
		if (message != null) {
			message.textProperty().unbind();
			this.message.setText("");
		}
	}

	public T getModel() {
		return model;
	}

	public void setModel(T model) {
		this.model = model;
	}

	public RDao<T> getRDao() {
		return rDao;
	}

	public void setRDao(RDao<T> dao) {
		this.rDao = dao;
	}

	public Mode getMode() {
		return mode;
	}

}
