package id.sit.pos.config;

import java.io.File;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.ini4j.Wini;

import com.j256.ormlite.stmt.QueryBuilder;

import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.MainApp;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.master.model.PosMachine;
import id.sit.pos.model.Setting;
import id.sit.pos.util.StringUtil;

public class Config {
	public static final String settingPath = "setting.ini";
	public static final String deviceSetting = "device.ini";
	public static final String dbSetting = "database.ini";

	private static String logoPath = MainApp.class.getResource("../../../assets/images/logo.png").toString();
	private static String localDbUrl = "jdbc:sqlite:local.db";
	private static String remoteDbUrl = "jdbc:postgresql://192.168.0.44/abc_development2";
	private static String remoteDbUsername = "wina1";
	private static String remoteDbPassword = "92018114923"; // 92018114923
	private static Locale langLocale = new Locale("en", "US");
	private static Locale currecyLocale = new Locale("in", "ID");
	private static ResourceBundle messages = ResourceBundle.getBundle("resources.MessagesBundle", langLocale);

	private static String dateFormat = "dd-M-yyyy";
	private static String timeFormat = "HH:mm:ss";
	private static String dbDateFormat = "yyyy-M-dd";
	private static String dbDateTimeFormat = dbDateFormat + " " + timeFormat;

	private static String domainUrl = "http://192.168.43.193:3000";
	private static String productImageUrl = domainUrl + "/uploads/product/image/";
	private static String defaultProductImagePath = MainApp.class
			.getResource("../../../assets/images/default_product.png").toString();
	private static String apiUrlBase = domainUrl + "/api/desktop_api/";
	private static String customerHotKey = "F1";
	private static String browseProductHotKey = "F2";
	private static String checkAllStockHotKey = "F3";
	private static String returnHotKey = "F4";
	private static String pendingHotKey = "F5";
	private static String debitHotKey = "F6";
	private static String creditHotKey = "F7";
	private static String voucherHotKey = "F8";
	private static String transferHotKey = "F9";
	private static String cashHotKey = "F10";
	private static String barcodeHotKey = "F11";
	private static String saveHotKey = "F12";
	private static String printHotKey = "F12";
	private static String deleteHotKey = "-";
	private static String discountHotKey = "D";
	private static String selectItemHotKey = "I";
	private static String voidHotKey = "Delete";

	private static int printerMaxCharPerLine = 40;

	private static String receiptFooter =
			  "========================================\n"
			+ "      TERIMA KASIH TELAH BERBELANJA     \n"
			+ "    TOKO ABC PILIHAN TERBAIK KELUARGA   \n"
			+ "       SHOES - CATALOGUE - FASHION      \n";

	private static String eodFooter =
			  "========================================\n"
			+ "               Mengetahui Manager Toko  \n"
			+ "\n\n\n"
			+ "              (                       ) \n";

	private static String webviewUrlCheckStock = apiUrlBase + "product_stock"; // brand_id
	private static String webviewUrlTransactionHistory = domainUrl + "transaction_histories"; // member_id

	private static boolean enableSize = true;
	private static boolean enableTransfer = false;
	private static boolean enableProductName = true;
	private static boolean enableColour = false;
	private static boolean enablePriceLevel = true;
	private static boolean enablePpn = true;
	private static boolean hideEmptyStock = true;
	private static boolean enableSodEodFraction = false;
	private static boolean abc = false;
	private static boolean enableCashDrawer = true;
	private static boolean enableMsr = true;
	private static boolean enableOnScreenKeyboard = false;
	private static boolean enableAdvancedPrinting = true;
	private static boolean enableMinusStock = false;
	private static boolean enableReturnCash = false;
	private static boolean enableLossPrice = false;
	private static boolean enableLineDisplay = false;

	private static boolean enableSuperKey = false;
	private static boolean enablePrinterType48 = false;
	private static boolean enableDisc = true;
	private static boolean enableDoubleReceipt = false;
 	private static String defaultTextTop = "";
 	private static String defaultTextBottom = "";
 	private static boolean defaultTextTopMarquee = false;
 	private static boolean defaultTextBottomMarquee = false;
 	private static int defaultTimeout = 5000;
 	private static String byeTextTop = "";
 	private static String byeTextBottom = "";
 	private static boolean byeTextTopMarquee = false;
 	private static boolean byeTextBottomMarquee = false;
 	private static int byeTimeout = 5000;
 	private static int marqueeSpeed = 100;
 	private static int marqueeReturnSpeed = 400;
	private static String purchaseText = "";
	private static boolean enablePrinterTypeLX310 = false;
	private static String advancedPrinterType = "";

	public void loadSetting() throws Exception {
		Wini db = new Wini(new File(dbSetting));
		Wini device = new Wini(new File(deviceSetting));
		Wini app = new Wini(new File(settingPath));

		/* load konfigurasi database */
		localDbUrl = db.get("DB", "local_db_url");
		remoteDbUrl = db.get("DB", "remote_db_url");
		remoteDbUsername = db.get("DB", "remote_db_username");
		remoteDbPassword = db.get("DB", "remote_db_password");

		/* load konfigurasi device setting */
		enablePrinterType48 = new Boolean(device.get("GENERAL", "enable_printer_type48"));
		enablePrinterTypeLX310 = new Boolean(device.get("GENERAL", "enable_printer_typelx310"));
		enableLineDisplay = new Boolean(device.get("GENERAL", "enable_line_display"));
		if(enableLineDisplay){
	 		defaultTextTop = device.get("LINEDISPLAY", "default_text_top");
	 		defaultTextBottom = device.get("LINEDISPLAY", "default_text_bottom");
	 		defaultTextTopMarquee = new Boolean(device.get("LINEDISPLAY", "default_text_top_marquee"));
	 		defaultTextBottomMarquee = new Boolean(device.get("LINEDISPLAY", "default_text_bottom_marquee"));
	 		defaultTimeout = new Integer(device.get("LINEDISPLAY", "default_timeout"));
	 		byeTextTop = device.get("LINEDISPLAY", "bye_text_top");
	 		byeTextBottom = device.get("LINEDISPLAY", "bye_text_bottom");
	 		byeTextTopMarquee = new Boolean(device.get("LINEDISPLAY", "bye_text_top_marquee"));
	 		byeTextBottomMarquee = new Boolean(device.get("LINEDISPLAY", "bye_text_bottom_marquee"));
	 		byeTimeout = new Integer(device.get("LINEDISPLAY", "bye_timeout"));
	 		marqueeSpeed = new Integer(device.get("LINEDISPLAY", "marquee_speed"));
	 		marqueeReturnSpeed = new Integer(device.get("LINEDISPLAY", "marquee_return_speed"));
			purchaseText = device.get("LINEDISPLAY", "purchase_text");
		}

		enableDoubleReceipt = new Boolean(device.get("GENERAL", "enable_double_struk"));
		enableMsr = new Boolean(device.get("GENERAL", "enable_msr"));
		enableCashDrawer = new Boolean(device.get("GENERAL", "enable_cash_drawer"));
		enableOnScreenKeyboard = new Boolean(device.get("GENERAL", "enable_onscreen_keyboard"));
		enableAdvancedPrinting = new Boolean(device.get("GENERAL", "enable_advanced_printing"));
		advancedPrinterType = device.get("GENERAL", "advance_printer_type");

		/* load konfigurasi app setting */
		domainUrl = app.get("SERVER", "base_url");
		dateFormat = app.get("FORMAT", "date_format");
		timeFormat = app.get("FORMAT", "time_format");

		String languageCode = app.get("LANG", "language_code");
		String countryCode = app.get("LANG", "country_code");
		changeLanguage(languageCode, countryCode);

		abc = new Boolean(app.get("GENERAL", "abc"));
		enableTransfer = new Boolean(app.get("PAYMENT", "enable_transfer"));
	}

	// TODO
	private PosMachine getAppSettingFromDB() throws SQLException{
		SettingCtrl.getInstance().load();

		QueryBuilder<PosMachine, Integer> qb;
		qb = DaoManagerImpl.getPosMachineDao().queryBuilder();
		qb.where().eq(PosMachine._OFFICE_ID,SettingCtrl.getSetting().getStoreId());

		return qb.queryForFirst();
	}

	public void LoadSettingFromDB(){
		PosMachine posMachine;
		try {
			posMachine = getAppSettingFromDB();
			if(posMachine != null){
				customerHotKey = posMachine.getHkCustomer();
				browseProductHotKey = posMachine.getHkBrowseProduct();
				checkAllStockHotKey = posMachine.getHkCheckAllStock();
				returnHotKey = posMachine.getHkReturnTrans();
				pendingHotKey = posMachine.getHkPendingTrans();
				debitHotKey = posMachine.getHkDebit();
				creditHotKey = posMachine.getHkCredit();
				voucherHotKey = posMachine.getHkVoucher();
				transferHotKey = posMachine.getHkTransfer();
				cashHotKey = posMachine.getHkCash();
				saveHotKey = posMachine.getHkSaveTrans();
				printHotKey = posMachine.getHkPrintTrans();
				deleteHotKey = posMachine.getHkDeleteItem();
				discountHotKey = posMachine.getHkDiscount();
				selectItemHotKey = posMachine.getHkSelectItem();
				barcodeHotKey = posMachine.getHkBarcode();
				voidHotKey = posMachine.getHkVoidTrans();

				hideEmptyStock = new Boolean(posMachine.isHideEmptyStock());
				enablePpn = new Boolean(posMachine.isEnablePPN());
				enableMinusStock = new Boolean(posMachine.isEnableMinusStock());
				enableLossPrice =  new Boolean(posMachine.isEnableLossPrice());
				enableReturnCash = new Boolean(posMachine.isEnableReturnCash());
				enableSuperKey = new Boolean(posMachine.isEnableSuperKey());
				enableDisc = new Boolean(posMachine.isEnableDiscount());
				enableSize = new Boolean(posMachine.isEnableSize());
				enableProductName = new Boolean(posMachine.isEnableName());
				enableColour = new Boolean(posMachine.isEnableColour());
				enablePriceLevel = new Boolean(posMachine.isEnablePriceLevel());

				receiptFooter = fetchMultiLine(posMachine.getReceiptFooter());
				eodFooter = fetchMultiLine(posMachine.getEodFooter());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private String fetchMultiLine(String string) {
		StringBuilder result = new StringBuilder();
		for(String str: string.split("#")){
			result.append(StringUtil.center(str,getPrinterMaxCharPerLine()));
			result.append(String.format("%n"));
		}
		return result.toString();
	}

	private String fetchMultiLine(Wini ini, String sectionName, String optionName) {
		StringBuilder result = new StringBuilder();
		int i = 1;
		while (i>0) {
			String line = ini.get(sectionName, optionName+"_"+i);
			if (line != null) {
				if (line.startsWith("\"") && line.endsWith("\""))
					if (line.length() > 2)
						line = line.substring(1, line.length()-2);
					else
						line = "";
				result.append(StringUtil.center(line,getPrinterMaxCharPerLine()));
				result.append(String.format("%n"));
				i++;
			} else {
				i = -1;
			}
		}
		return result.toString();
	}

	public static void changeLanguage(String languageCode, String countryCode) {
		langLocale = new Locale(languageCode, countryCode);
		messages = ResourceBundle.getBundle("resources/MessagesBundle", langLocale);
	}

	public static String getLogoPath() {
		return logoPath;
	}

	public static String getMessage(String key) {
		return messages.getString(key);
	}

	public static String getLocalDbUrl() {
		return localDbUrl;
	}

	public static void setLocalDbUrl(String localDbUrl) {
		Config.localDbUrl = localDbUrl;
	}

	public static String getRemoteDbUrl() {
		return remoteDbUrl;
	}

	public static void setRemoteDbUrl(String remoteDbUrl) {
		Config.remoteDbUrl = remoteDbUrl;
	}

	public static String getRemoteDbUsername() {
		return remoteDbUsername;
	}

	public static String getRemoteDbPassword() {
		return remoteDbPassword;
	}

	public static Locale getLangLocale() {
		return langLocale;
	}

	public static Locale getCurrencyLocale() {
		return currecyLocale;
	}

	public static String getDateFormat() {
		return dateFormat;
	}

	public static String getTimeFormat() {
		return timeFormat;
	}

	public static String getDbDateFormat() {
		return dbDateFormat;
	}

	public static String getDbDateTimeFormat() {
		return dbDateTimeFormat;
	}

	public static String getDomainUrl() {
		return domainUrl;
	}

	public static String getProductImageUrl(int productId) {
		return productImageUrl + productId + "/";
	}

	public static String getWebviewUrlCheckStock() {
		return webviewUrlCheckStock;
	}

	public static String getWebviewUrlTransactionHistory() {
		return webviewUrlTransactionHistory;
	}

	public static String getDefaultProductImagePath() {
		return defaultProductImagePath;
	}

	public static String getSettingpath() {
		return settingPath;
	}

	public static String getDevicesetting() {
		return deviceSetting;
	}

	public static String getDbsetting() {
		return dbSetting;
	}

	public static int getPrinterMaxCharPerLine() {
		if(isEnablePrinterType48()){
			setPrinterMaxCharPerLine(48);
		}
 		if(isEnablePrinterTypeLX310()){
 			setPrinterMaxCharPerLine(40);
		}
		return printerMaxCharPerLine;
	}

	public static String getReceiptFooter() {
		return receiptFooter;
	}

	public static String getEodFooter() {
		return eodFooter;
	}

	public static String getCustomerHotKey() {
		return customerHotKey;
	}

	public static String getBrowseProductHotKey() {
		return browseProductHotKey;
	}

	public static String getCheckAllStockHotKey() {
		return checkAllStockHotKey;
	}

	public static String getReturnHotKey() {
		return returnHotKey;
	}

	public static String getPendingHotKey() {
		return pendingHotKey;
	}

	public static String getDebitHotKey() {
		return debitHotKey;
	}

	public static String getCreditHotKey() {
		return creditHotKey;
	}

	public static String getVoucherHotKey() {
		return voucherHotKey;
	}

	public static String getTransferHotKey() {
		return transferHotKey;
	}

	public static String getCashHotKey() {
		return cashHotKey;
	}

	public static String getSaveHotKey() {
		return saveHotKey;
	}

	public static String getPrintHotKey() {
		return printHotKey;
	}

	public static String getDeleteHotKey() {
		return deleteHotKey;
	}

	public static String getDiscountHotKey() {
		return discountHotKey;
	}

	public static String getBarcodeHotKey() {
		return barcodeHotKey;
	}

	public static String getSelectItemHotKey() {
		return selectItemHotKey;
	}

	public static String getVoidHotKey() {
		return voidHotKey;
	}

	public static boolean isEnableSize() {
		return enableSize;
	}

	public static boolean isEnableTransfer() {
		return enableTransfer;
	}

	public static boolean isEnableProductName() {
		return enableProductName;
	}

	public static boolean isEnableColour() {
		return enableColour;
	}

	public static boolean isEnablePriceLevel() {
		return enablePriceLevel;
	}

	public static boolean isHideEmptyStock() {
		return hideEmptyStock;
	}

	public static boolean isEnableSodEodFraction() {
		return enableSodEodFraction;
	}

	public static boolean isEnablePpn() {
		return enablePpn;
	}

	public static boolean isAbc() {
		return abc;
	}

	public static boolean isEnableCashDrawer() {
		return enableCashDrawer;
	}

	public static boolean isEnableMsr() {
		return enableMsr;
	}

	public static boolean isEnableOnScreenKeyboard() {
		return enableOnScreenKeyboard;
	}

	public static boolean isEnableAdvancedPrinting() {
		return enableAdvancedPrinting;
	}

	public static boolean isEnableMinusStock() {
		return enableMinusStock;
	}

	public static boolean isEnableReturnCash() {
		return enableReturnCash;
	}

	public static boolean isEnableLossPrice() {
		return enableLossPrice;
	}
	public static boolean isEnableLineDisplay() {
		return enableLineDisplay;
	}

	public static boolean isEnableSuperKey() {
		return enableSuperKey;
	}

	public static boolean isEnablePrinterType48() {
		return enablePrinterType48;
	}

	public static void setPrinterMaxCharPerLine(int printerMaxCharPerLine) {
		Config.printerMaxCharPerLine = printerMaxCharPerLine;
	}

	public static boolean isEnableDisc() {
		return enableDisc;
	}

	public static boolean isEnableDoubleReceipt() {
		return enableDoubleReceipt;
	}

	public static String getDefaultTextTop() {
 		return defaultTextTop;
 	}

 	public static String getDefaultTextBottom() {
 		return defaultTextBottom;
 	}

 	public static boolean getDefaultTextTopMarquee() {
 		return defaultTextTopMarquee;
 	}

 	public static boolean getDefaultTextBottomMarquee() {
 		return defaultTextBottomMarquee;
 	}

 	public static boolean getByeTextTopMarquee() {
 		return byeTextTopMarquee;
 	}

 	public static boolean getByeTextBottomMarquee() {
 		return byeTextBottomMarquee;
 	}

 	public static String getByeTextTop() {
 		return byeTextTop;
 	}

 	public static String getByeTextBottom() {
 		return byeTextBottom;
 	}

 	public static int getDefaultTimeout() {
 		return defaultTimeout;
 	}

 	public static int getByeTimeout() {
 		return byeTimeout;
 	}

 	public static String getPurchaseText() {
 		return purchaseText;
 	}

 	public static int getMarqueeSpeed() {
 		return marqueeSpeed;
 	}

 	public static int getMarqueeReturnSpeed() {
 		return marqueeReturnSpeed;
	}

	public static boolean isEnablePrinterTypeLX310() {
		return enablePrinterTypeLX310;
	}

	public static String getAdvancedPrinterType() {
		return advancedPrinterType;
	}
}
