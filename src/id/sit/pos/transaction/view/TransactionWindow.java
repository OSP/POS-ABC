package id.sit.pos.transaction.view;


import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import javax.print.PrintException;

import org.apache.log4j.Logger;
import org.comtel2000.keyboard.control.VkProperties;

import id.r.engine.view.RBrowser;
import id.r.engine.view.RUi;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.customer.model.Member;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.master.model.Product;
import id.sit.pos.master.model.PromoItem;
import id.sit.pos.master.model.PromoPrizeItem;
import id.sit.pos.master.model.VoucherDetail;
import id.sit.pos.master.view.ProductBrowser;
import id.sit.pos.transaction.controller.EmptyCartException;
import id.sit.pos.transaction.controller.NoCustomerException;
import id.sit.pos.transaction.controller.OverCreditLimitException;
import id.sit.pos.transaction.controller.ReceiptPrinterCtrl;
import id.sit.pos.transaction.controller.SodEodCtrl;
import id.sit.pos.transaction.controller.TransactionCtrl;
import id.sit.pos.transaction.controller.UndefinedCostPriceException;
import id.sit.pos.transaction.controller.UndefinedPriceException;
import id.sit.pos.transaction.controller.UnpaidException;
import id.sit.pos.transaction.controller.UnselectedProduct;
import id.sit.pos.transaction.controller.UnselectedSize;
import id.sit.pos.transaction.controller.UnsufficientStockException;
import id.sit.pos.transaction.model.BankPayment;
import id.sit.pos.transaction.model.EdcPayment;
import id.sit.pos.transaction.model.SodEod;
import id.sit.pos.transaction.model.SodEod.Type;
import id.sit.pos.transaction.model.Transaction;
import id.sit.pos.transaction.model.Transaction.Status;
import id.sit.pos.transaction.model.TransactionItem;
import id.sit.pos.user.controller.SessionCtrl;
import id.sit.pos.user.model.Role;
import id.sit.pos.user.model.User;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.DeviceUtil;
import id.sit.pos.util.PrintUtil;
import id.sit.pos.util.StringUtil;
import id.sit.pos.util.LineDisplayDriver;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.animation.Animation;
import java.time.format.DateTimeFormatter;
import jpos.LineDisplayConst;


public class TransactionWindow extends RUi implements VkProperties  {
	private static Logger logger = Logger.getLogger(TransactionWindow.class.getName());
	private static Object locker = new Object();

	@FXML
	private Label shiftInfo;

	@FXML
	private Label userInfo;

	@FXML
	private Label customerInfo;

	@FXML
	private Label customerLevel;

	@FXML
	private Label customerPoint;

	@FXML
	private TextField purchasePrice;

	@FXML
	private TextField transactionNo;

	@FXML
	private TextField barcode;

	@FXML
	private Label article;

	@FXML
	private TextField size;

	@FXML
	private TextField qty;

	@FXML
	private Label availableQty;

	@FXML
	private ToggleButton returBtn;

	@FXML
	private ImageView productImage;

	@FXML
	private TableView<TransactionItem> table;

	@FXML
	private TableColumn<TransactionItem, Integer> itemNoClm;

	@FXML
	private TableColumn<TransactionItem, String> barcodeClm;

	@FXML
	private TableColumn<TransactionItem, String> articleClm;

	@FXML
	private TableColumn<TransactionItem, String> productName;

	@FXML
	private TableColumn<TransactionItem, String> sizeClm;

	@FXML
	private TableColumn<TransactionItem, BigDecimal> priceClm;

	@FXML
	private TableColumn<TransactionItem, Integer> qtyClm;

	@FXML
	private TableColumn<TransactionItem, Float> discPctClm;

	@FXML
	private TableColumn<TransactionItem, BigDecimal> discAmtClm;

	@FXML
	private TableColumn<TransactionItem, BigDecimal> discPriceClm;

	@FXML
	private TableColumn<TransactionItem, BigDecimal> totalClm;

	@FXML
	private Label subtotal;

	@FXML
	private Label totalDisc;

	@FXML
	private Label totalPrice;

	@FXML
	private Label voucherAmt;

	@FXML
	private Label totalPriceAfterVoucher;

	@FXML
	private TextField cash;

	@FXML
	private TextField change;

	@FXML
	private Label debitAmt;

	@FXML
	private Label creditCardAmt;

	@FXML
	private Label transferLbl;

	@FXML
	private Label transferAmt;

	@FXML
	private Button settingBtn;

	@FXML
	private Button customerBtn;

	@FXML
	private Button browseProductBtn;

	@FXML
	private Button voucherBtn;

	@FXML
	private Button debitBtn;

	@FXML
	private Button creditBtn;

	@FXML
	private Button transferBtn;

	@FXML
	private Button printBtn;

	@FXML
	private Button voidBtn;

	@FXML
	private Button pendingBtn;

	@FXML
	private Button checkStockBtn;

	@FXML
	private ProgressBar progressBar;

	@FXML
	private Label hotKey1;

	@FXML
	private Label descrHotKey1;

	@FXML
	private Label hotKey2;

	@FXML
	private Label descrHotKey2;

	@FXML
	private Label hotKey3;

	@FXML
	private Label descrHotKey3;

	@FXML
	private Label hotKey4;

	@FXML
	private Label descrHotKey4;

	@FXML
	private Label hotKey5;

	@FXML
	private Label descrHotKey5;

	@FXML
	private Label hotKey6;

	@FXML
	private Label descrHotKey6;

	@FXML
	private Label hotKey7;

	@FXML
	private Label descrHotKey7;

	@FXML
	private Label hotKey8;

	@FXML
	private Label descrHotKey8;

	@FXML
	private Label hotKey9;

	@FXML
	private Label descrHotKey9;

	@FXML
	private Label hotKey10;

	@FXML
	private Label descrHotKey10;

	@FXML
	private Label hotKey11;

	@FXML
	private Label descrHotKey11;

	@FXML
	private Label hotKey12;

	@FXML
	private Label descrHotKey12;

	@FXML
	private Label hotKey13;

	@FXML
	private Label descrHotKey13;

	@FXML
	private Label hotKey14;

	@FXML
	private Label descrHotKey14;

	@FXML
	private Label hotKey15;

	@FXML
	private Label descrHotKey15;

	@FXML
	private Label hotKey16;

	@FXML
	private Label descrHotKey16;

	@FXML
	private TextField refTransClm;

	@FXML
	private Button cashdrawBtn;

	@FXML
	private Label lblClock;

	private TransactionCtrl controller;
	private ReceiptPrinterCtrl receiptPrinterCtrl = new ReceiptPrinterCtrl();

	private ObservableList<TransactionItem> transactionRows = FXCollections.observableArrayList();

	protected Service<Boolean> selectProductService;

	protected Service<Image> productImageService;

	protected Service<Void> printService;

	protected Service<Void> printEodService;

	protected Service<Void> saveService;

	Screen secondaryScreen = null;

	Stage secondaryStage = null;

	User authorizedUser = null;

	public TransactionWindow() {
		selectProductService = new Service<Boolean>() {

			@Override
			protected Task<Boolean> createTask() {
				return new Task<Boolean>() {
					@Override
					protected Boolean call() throws InterruptedException {
						startProgressBar();
						Boolean result = false;

						try {
							result = controller.selectProduct(barcode.getText());
						} catch (SQLException e) {
							logger.error(e.toString(), e);
							MainApp.showAlertError("common_error_db_content");
						} catch (Exception e) {
							logger.error(e.toString(), e);
						} finally {
							endProgressBar();
						}
						return result;
					}
				};
			}
		};

		selectProductService.setOnSucceeded(t -> {
			if ((boolean) t.getSource().getValue()) {
				afterSelect();
			} else {
				MainApp.showAlertError("product_not_found");
				barcode.selectAll();
			}
		});

		productImageService = new Service<Image>() {

			@Override
			protected Task<Image> createTask() {
				return new Task<Image>() {
					@Override
					protected Image call() throws InterruptedException {
						Image image = new Image(Config.getProductImageUrl(controller.getSelectedItem().getId())
								+ controller.getSelectedItem().getImageUrl());
						return image;
					}
				};
			}
		};

		productImageService.setOnSucceeded(t -> {
			Image image = (Image) t.getSource().getValue();
			if (image != null) {
				if (image != null)
					productImage.setImage(image);
			}
		});

		printService = new Service<Void>() {

			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() throws Exception {
						startProgressBar();
						try {
							controller.checkout();
							// print double struk
							if (Config.isEnableDoubleReceipt()){
								for (int a = 0; a < 2; a++){
									receiptPrinterCtrl.print(controller.getCurrent().getTransactionNo(), controller.generateReceipt());
								}

							} else {
								receiptPrinterCtrl.print(controller.getCurrent().getTransactionNo(), controller.generateReceipt());
							}

							if (Config.isEnableCashDrawer())
								DeviceUtil.getCashDrawerDevice().open();
							// flag untuk reprint
							controller.setPrintedCount();
						} finally {
							endProgressBar();
						}
						return null;
					}
				};
			}
		};

		printService.setOnSucceeded(t -> {
//			if (controller.getCurrent().getStatus() == Status.FINISHED &&
//					controller.getCurrent().getReceiptPrintedCount().compareTo(BigDecimal.ZERO) > 0 ){
			if (!controller.isReprintMode()){
				BigDecimal custChange = controller.getCurrent().getCashChange();
				if ((custChange != BigDecimal.ZERO) && (custChange != null ) && controller.getCurrent().getMember() == null)
					MainApp.showInformation(CurrencyUtil.format(custChange, true));
			}

			resetAll();
			setDisableBtn(false);
			setFormAddDisable(false);
			barcode.requestFocus();
		});

		printService.setOnFailed(t -> {
			Throwable e = t.getSource().getException();
			if (e instanceof SQLException) {
				logger.error( e.toString(), e);
				MainApp.showAlertError("common_error_db_content");
			} else if (e instanceof NoCustomerException) {
				MainApp.showAlertError("trans_need_member");
				customerBtn.fire();
			} else if (e instanceof UnpaidException) {
				MainApp.showAlertError("trans_unpaid_checkout");
				cash.requestFocus();
			} else if (e instanceof EmptyCartException) {
				MainApp.showAlertError("trans_empty_checkout");
				barcode.requestFocus();
			} else if (e instanceof OverCreditLimitException) {
//				MainApp.showAlertError("trans_over_credit_limit_error");
				MainApp.showAlertCreditLimitError(controller.getCurrent().getMember());
				barcode.requestFocus();
			} else if (e instanceof PrintException ) {
				logger.error( e.toString(), e);
				MainApp.showAlertError("trans_print_error");
			} else if (e instanceof IOException) {
				logger.error( e.toString(), e);
				MainApp.showAlertError("common_error_content");
			} else {
				logger.error(e.toString(), e);
			}
			setPaymentDisabled(false);
			setDisableBtn(false);
		});

		saveService = new Service<Void>() {

			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() throws InterruptedException {
						startProgressBar();
						try {
							controller.saveAll();
						} catch (SQLException e) {
							logger.error( e.toString(), e);
							MainApp.showAlertError("common_error_db_content");
						}
						endProgressBar();
						return null;
					}
				};
			}
		};

		printEodService = new Service<Void>() {

			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() throws Exception {
						startProgressBar();
						String eodReport = controller.generateEoDReportDetail();
//						InputStream is = new ByteArrayInputStream(
//								Charset.forName("UTF-8").encode(eodReport).array());
						PrintUtil.printText(eodReport);
						endProgressBar();
						return null;
					}
				};
			}
		};

	}

	public void showClock(){
		Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0),
                event -> lblClock.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")))),
					new KeyFrame(Duration.seconds(1)));

		timeline.setCycleCount(Animation.INDEFINITE);
		timeline.play();
	}

	@FXML
	public void initialize() {

		controller = new TransactionCtrl();
		try {
			controller.createNew();
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			MainApp.showAlertError("common_error_db_content");
		}

		stage.setOnCloseRequest(event-> {
	        secondaryStage.close();
		});

		itemNoClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, Integer>("itemNo"));

		barcodeClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, String>("barcode"));

		articleClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, String>("article"));

		if (Config.isEnableProductName())
			productName.setCellValueFactory(new PropertyValueFactory<TransactionItem, String>("productName"));
		else
			productName.setVisible(false);

		if (Config.isEnableSize()){
			sizeClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, String>("size"));
		} else {
			sizeClm.setVisible(false);
		}

		priceClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, BigDecimal>("price"));

		qtyClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, Integer>("qty"));

		discPctClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, Float>("discPct"));

		discAmtClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, BigDecimal>("discAmt"));

		discPriceClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, BigDecimal>("discPrice"));

		totalClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, BigDecimal>("total"));

		table.setItems(transactionRows);

		table.setOnKeyPressed( keyEvent-> {
			TransactionItem selectedItem = table.getSelectionModel().getSelectedItem();

			if (selectedItem != null) {
				String code = keyEvent.getText();
				if (code.equals(Config.getDeleteHotKey())){
					deleteItem(selectedItem);
				} else if (code.equals(Config.getDiscountHotKey())){
					doSpecialDisc(selectedItem);
				}
			}
		});

		// Set validation
		setAsAlphanumericFields(size);
		setAsNumericFields(qty);
		setAsCurrencyFields(purchasePrice, cash);
		setAsUppercaseFields(size);
		// end of validation


		User userLogin = SessionCtrl.getSession().getUser();
		shiftInfo.setText("Shift "+SessionCtrl.getSession().getShiftNo()+" | "+userLogin.getRole().getName()+" @ "+SettingCtrl.getSetting().getStore().getName());
		userInfo.setText(userLogin.getFirstName()+" "+userLogin.getLastName());

		transactionNo.setManaged(false);
		transactionNo.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
			if (!newPropertyValue) { //on lost focus
				controller.setTransactionNo(transactionNo.getText());
			}
		});

		barcode.setOnKeyPressed(e->{
			if (e.getCode() == KeyCode.ENTER && StringUtil.isEmpty(barcode.getText())) {
				cash.requestFocus();
			} else if (e.getCode() == KeyCode.DOWN && transactionRows.size() > 0) {
				table.requestFocus();
		        table.getSelectionModel().select(0);
		        table.getFocusModel().focus(0);
			}
		});

		if (!Config.isEnableSize()) {
			size.setVisible(false);
			size.setManaged(false);
		}

		cash.setOnKeyPressed((e)->{
			if (e.getCode() == KeyCode.ENTER) {
				payByCash();
			}
		});

		if (!Config.isAbc())
			purchasePrice.setManaged(false);
		//end of setup fields

		customerBtn.defaultButtonProperty().bind(customerBtn.focusedProperty());
		browseProductBtn.defaultButtonProperty().bind(browseProductBtn.focusedProperty());
		if (!Config.isAbc()) {
			checkStockBtn.setVisible(false);
			checkStockBtn.setManaged(false);
		}
		voucherBtn.defaultButtonProperty().bind(voucherBtn.focusedProperty());
		debitBtn.defaultButtonProperty().bind(debitBtn.focusedProperty());
		creditBtn.defaultButtonProperty().bind(creditBtn.focusedProperty());
		if (Config.isEnableTransfer())
			transferBtn.defaultButtonProperty().bind(transferBtn.focusedProperty());
		else {
			transferLbl.setVisible(false);
			transferAmt.setVisible(false);
			transferBtn.setVisible(false);
		}
		printBtn.defaultButtonProperty().bind(printBtn.focusedProperty());
		voidBtn.defaultButtonProperty().bind(voidBtn.focusedProperty());
		pendingBtn.defaultButtonProperty().bind(pendingBtn.focusedProperty());
		if (Config.isEnableCashDrawer())
			cashdrawBtn.defaultButtonProperty().bind(cashdrawBtn.focusedProperty());
		else
			cashdrawBtn.setVisible(false);

		if (Config.isEnableOnScreenKeyboard()){
			qty.getProperties().put(VK_TYPE, VK_TYPE_NUMERIC);
			barcode.getProperties().put(VK_TYPE, VK_TYPE_NUMERIC);
			cash.getProperties().put(VK_TYPE, VK_TYPE_NUMERIC);
			transactionNo.getProperties().put(VK_TYPE, VK_TYPE_NUMERIC);
		}

		setCellAlignment();
		showContextMenu();

		displayTransactionInfo();
		registerAllHotKeys();
		displayHotKeyInfo();
		setPaymentDisabled(true);
		setDisableBtn(false);
		showClock();
		barcode.requestFocus();
	}

	private void setCellAlignment(){
		priceClm.setStyle("-fx-alignment: top-right;");
		qtyClm.setStyle("-fx-alignment: top-right;");
		discPctClm.setStyle("-fx-alignment: top-right;");
		discAmtClm.setStyle("-fx-alignment: top-right;");
		discPriceClm.setStyle("-fx-alignment: top-right;");
		totalClm.setStyle("-fx-alignment: top-right;");
	}

	public void setDisableBtn(boolean isDisabled )
	{
		voidBtn.setDisable(isDisabled);
		pendingBtn.setDisable(isDisabled);
		cashdrawBtn.setDisable(isDisabled);
	}

	public void showContextMenu(){
		table.setRowFactory(tv -> {
			TableRow<TransactionItem> row = new TableRow<>();

			ContextMenu rowMenu = new ContextMenu();
			MenuItem removeItem = new MenuItem("Cancel");
			removeItem.setOnAction((event) -> {
				deleteItem(row.getItem());
			});

			MenuItem specialDisc = new MenuItem("Special Discount");
			specialDisc.setOnAction((event) -> {
				doSpecialDisc(row.getItem());
			});

			rowMenu.getItems().addAll(removeItem, specialDisc);
			// only display context menu for non-null items:
			row.contextMenuProperty().bind(
					Bindings.when(Bindings.isNotNull(row.itemProperty())).then(rowMenu).otherwise((ContextMenu) null));

			/*add double click event to show context menu
			 * 2016-05-01			 *
			 * */
			row.setOnMouseClicked(event ->{
				if (event.getClickCount()== 2 && (!row.isEmpty()) ){
					rowMenu.show(getStage(), event.getSceneX(), event.getSceneY());
				}
			});
			return row;
		});

	}

	public void registerAllHotKeys() {
		getStage().getScene().addEventFilter(KeyEvent.KEY_PRESSED, event -> {
			// Key Combination 31/05/2016 - fzr
			if(Config.isEnableSuperKey()){
				KeyCombination superKey1 = new KeyCodeCombination(KeyCode.DELETE,
						KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);
				if (superKey1.match(event)){
					try {
						clearTransaction();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			String code = event.getCode().getName();
			if (code.equals(Config.getCustomerHotKey())) {
				customerBtnOnAction();
			} else if (code.equals(Config.getBarcodeHotKey())){
				event.consume();
				barcode.requestFocus();
			} else if (code.equals(Config.getBrowseProductHotKey())){
				browseProduct();
			} else if (code.equals(Config.getCheckAllStockHotKey())){
				if (Config.isAbc()){
					checkStockAllBranch();
				}
			} else if (code.equals(Config.getReturnHotKey())){
				returBtn.fire();
				System.out.println(returBtn.isSelected());
				if(returBtn.isSelected())
					returBtn.setSelected(true);
			} else if (code.equals(Config.getVoucherHotKey())){
				voucherBtn.fire();
			} else if (code.equals(Config.getDebitHotKey())){
				debitBtn.fire();
			} else if (code.equals(Config.getCreditHotKey())){
				creditBtn.fire();
			} else if (code.equals(Config.getTransferHotKey())){
				transferBtn.fire();
			} else if (code.equals(Config.getCashHotKey())){
				cash.requestFocus();
				event.consume();
			} else if (code.equals(Config.getPrintHotKey())){
				checkoutAndPrint();
			} else if (code.equals(Config.getVoidHotKey())){
				voidBtn.fire();
			} else if (code.equals(Config.getPendingHotKey())){
				pendingBtnOnAction();
			}else if (code.equals(Config.getSelectItemHotKey())){
				table.requestFocus();
		        table.getSelectionModel().select(0);
		        table.getFocusModel().focus(0);
			}
		});
	}

	@FXML
	public void printEodReport() {
		if (!printEodService.isRunning()) {
			printEodService.reset();
			printEodService.start();
		}
	}

	@FXML
	public void endShift() {
		SodEod sodEod = MainApp.getInstance().showEodPopup(false);
		if (sodEod == null)
			return;
	}

	@FXML
	public void endDay() {
//		MainApp.getInstance().showEodPopup(true);
//		MainApp.getInstance().logout();
	}

	@FXML
	public void settingMnuOnAction() {
		MainApp.getInstance().showSettingPopup();
	}

	@FXML
	public void customerBtnOnAction() {
		Member customer = MainApp.getInstance().showCustomerWindow();
		if (customer != null){
			if(customer.getLevel() != null) {
				controller.getCurrent().setMember(customer);
				controller.processAll();
				if (!saveService.isRunning()) {
					saveService.reset();
					saveService.start();
				}
				updateAll();
			}else {
				MainApp.showAlertError("trans_member_unprocessed");
			}
		}
	}

	@FXML
	public void transactionNoOnAction() {
		barcode.requestFocus();
	}

	@FXML
	public void barcodeOnAction() {
		if (barcode.getText() == null || barcode.getText().trim().isEmpty())
			return;

		if (selectProductService.isRunning())
			return;
		selectProductService.reset();
		selectProductService.start();
	}

	@FXML
	public void browseProduct() {
		Product product = showProductBrowser();
		if (product != null) {
			if(Config.isAbc()){
				try {
					DaoManagerImpl.getProductDao().refresh(product);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				barcode.setText(product.getBarcode());
				size.setText(product.getProductSize().getSize().getSizeNumber());
			} else {
				barcode.setText(product.getSku().getBarcode());
			}

			try {
				if (controller.selectProduct(product)) {
					controller.selectStock(product.getProductStock());
					afterSelect();
				} else {
					MainApp.showAlertError("product_not_found");
				}
			} catch (SQLException e) {
				logger.error(e.toString(), e);
			}
		}
	}

	@FXML
	public void addItem() {
		try {
			String barcodeOnQty = null;
			if (StringUtil.isEmpty(qty.getText())) {
				controller.setSelectedQty(1);
			} else {
				if (qty.getText().length() >= 12) {
					barcodeOnQty = qty.getText();
					controller.setSelectedQty(1);
				} else {
					controller.setSelectedQty(Integer.valueOf(qty.getText()));
				}
			}

			if (!returBtn.isSelected()) {
				TransactionItem addedItem = controller.addSelectedItem();
				if (transactionRows.indexOf(addedItem) < 0) {
					transactionRows.add(addedItem);
				} else {
					updateItem(addedItem);
				}

				controller.processPayment();

				if(Config.isEnableLineDisplay()){
					LineDisplayDriver ldd = DeviceUtil.getLineDisplayDevice();
					synchronized (locker) {
			 			ldd.setLocked(true);
					}

					ldd.clear();
					ldd.setText(0,addedItem.getProduct().getShortName() );
					ldd.display(0);
					ldd.setText(1,"<"+(new Transaction()).getToBePaidAmt() + "> " + addedItem.getInitPrice());
					ldd.display(1);
				}


				updateSummaryInfo();
				updatePaymentInfo();
				resetFormAdd();
				setPaymentDisabled(false);
				pendingBtn.setText("Pending");
				startGetPromoTask();
			} else {
				controller.setTransactionNo(transactionNo.getText());
				controller.setPurchasePrice(CurrencyUtil.parse(purchasePrice.getText()));
				TransactionItem addedItem = controller.addSelectedItem();
				if (transactionRows.indexOf(addedItem) < 0) {
					transactionRows.add(addedItem);
				} else {
					updateItem(addedItem);
				}

				updateSummaryInfo();
				updatePaymentInfo();
				resetFormAdd();
			}

			if (barcodeOnQty != null) {
				barcode.setText(barcodeOnQty);
				barcodeOnAction();
			}
		} catch (UnselectedProduct e) {
			MainApp.showAlertError("trans_no_selection");
		} catch (UnselectedSize e) {
			MainApp.showAlertError("trans_no_selection");
		} catch (UnsufficientStockException e) {
			MainApp.showAlertError("trans_out_of_stock");
		} catch (UndefinedPriceException e) {
			MainApp.showAlertError("trans_price_not_set");
		} catch (UndefinedCostPriceException e){
			MainApp.showAlertError("trans_cost_price_not_set");
		} catch (SQLException e) {
			MainApp.showAlertDbError();
			logger.error( e.toString(), e);
		}
	}

	@FXML
	public void enterSize() {
		try {
			if (!controller.selectSize(size.getText())){
				MainApp.showAlertError("trans_invalid_size");
				return;
			}

			if (!returBtn.isSelected()) {
				availableQty.setText(String.valueOf(controller.getAvailableQty()));
				if (controller.getAvailableQty() != 0) {
					qty.setDisable(false);
					qty.requestFocus();
				} else {
					MainApp.showAlertError("trans_empty_stock");
					size.requestFocus();
					size.selectAll();
				}
			} else {
				availableQty.setText("0");
				qty.requestFocus();
			}
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			availableQty.setText("0");
			MainApp.showAlertError("common_error_db_content");
		}
	}

//	Key Combination 31/05/2016 - fzr
	public void clearTransaction() throws SQLException{
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("CLEAR TRANSACTION");
		alert.setContentText("Are you sure for clearing all transaction? ");

		ButtonType okBt = new ButtonType("Yes");
		ButtonType cancelBt = new ButtonType("No", ButtonData.CANCEL_CLOSE);

		alert.getButtonTypes().setAll(okBt, cancelBt);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == okBt){
			User user = MainApp.getInstance().showLoginPopup(false, false, Role.STORE_MANAGER);
			if (user != null){
				controller.deleteAllTransaction();
			}
		}
	}

	public void deleteItem(TransactionItem item) {
		try {
			if (controller.getCurrent().getStatus() == Transaction.Status.FINISHED)
				return;

//			User user = MainApp.getInstance().showLoginPopup(false, false, Role.STORE_MANAGER);
//			if(user != null)
			User user = MainApp.getInstance().showLoginPopupExt(false, false);
			if(user != null)
				if (user.getRole().getName().equals(Role.STORE_MANAGER) ||user.getRole().getName().equals(Role.SUPER_ADMIN) ||
					user.getRole().getName().equals(Role.SUPER_USER) ){
					controller.deleteItem(item);
					controller.processPayment();
					table.getItems().remove(item);
					updateSummaryInfo();
					updatePaymentInfo();
					if (table.getItems().isEmpty())
						setPaymentDisabled(true);
				}
		} catch (SQLException e) {
			logger.error(e.toString(), e);
		}
	}

	@FXML
	public void voidBtnOnAction() {
		if (controller.isOnProgress()) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle(Config.getMessage("trans_void_dialog_title"));
			alert.setContentText(Config.getMessage("trans_new_w_unfinished"));

			ButtonType okBt = new ButtonType("Yes");
			ButtonType cancelBt = new ButtonType("No", ButtonData.CANCEL_CLOSE);

			alert.getButtonTypes().setAll(okBt, cancelBt);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == okBt){
				/* User user = MainApp.getInstance().showLoginPopup(false, false, Role.STORE_MANAGER);*/
				User user = MainApp.getInstance().showLoginPopupExt(false, false);
				if(user != null)
					if (user.getRole().getName().equals(Role.STORE_MANAGER) || user.getRole().getName().equals(Role.SUPER_ADMIN) ||
						user.getRole().getName().equals(Role.SUPER_USER) ){
						cancelTransaction();
					}
			}
		} else {
			try {
				controller.createNew();
			} catch (SQLException e) {
				logger.error(e.toString(), e);
				MainApp.showAlertError("common_error_db_content");
			}
			resetAll();

		}
	}

	public void cancelTransaction() {
		try {
			controller.delete();
			controller.createNew();
			resetAll();
			updateAll();
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			MainApp.showAlertError(Config.getMessage("common_error_db_content"));
		}
	}

	@FXML
	public void checkStockAllBranch() {
		RBrowser browser = new RBrowser();
		browser.setUrl(Config.getWebviewUrlCheckStock() + "?product_id=" + controller.getSelectedItem().getId()+"&user_id="+SessionCtrl.getSession().getUser().getId());
		MainApp.getInstance().createModalWindow(browser).showAndWait();
	}

	@FXML
	public void retur() {
		if (returBtn.isSelected()) {
			controller.setReturMode(true);
			if (Config.isAbc())
				purchasePrice.setVisible(true);
//			else {
				transactionNo.setVisible(true);
				transactionNo.setManaged(true);
//			}
		} else {
			controller.setReturMode(false);
			if (Config.isAbc())
				purchasePrice.setVisible(false);
//			else {
				transactionNo.setVisible(false);
				transactionNo.setManaged(false);
//			}
		}
		barcode.setText(null);
		if (Config.isAbc())
			barcode.requestFocus();
		else
			transactionNo.requestFocus();
		size.setText(null);
		qty.setText(null);
		availableQty.setText(null);
		productImage.setImage(new Image(Config.getDefaultProductImagePath()));
	}

	@FXML
	public void addVoucher() {
		VoucherDetail voucher = showVoucherPopup();
		if (voucher != null) {
			controller.addVoucher(voucher);
			switch (voucher.getVoucher().getType()) {
			case DiscountVoucher:
				voucherAmt.setText(CurrencyUtil.format(controller.getCurrent().getVoucherAmt(), false));
				break;
			case Cashback:
				voucherAmt.setText(CurrencyUtil.format(voucher.getRedeemAmt(), false));
				break;
			case ArVoucher:
				voucherAmt.setText(CurrencyUtil.format(controller.getCurrent().getVoucherAmt(), false));
				break;
			}
			totalPriceAfterVoucher.setText(CurrencyUtil.format(controller.getCurrent().getToBePaidAmt(), false));
		}
		cash.requestFocus();
	}

	@FXML
	public void payByCash() {
		if (this.cash.getText() == null || this.cash.getText().equals(""))
			return;
		BigDecimal cash = CurrencyUtil.parse(this.cash.getText());
		controller.payByCash(cash);
		updatePaymentInfo();
		printBtn.requestFocus();
	}

	@FXML
	public void payByDebitCard() {
		BankPayment payment;
		if (Config.isAbc())
			payment = showBankPayment(BankPayment.Type.DEBIT);
		else
			payment = showEdcPayment(BankPayment.Type.DEBIT);

		if (payment != null) {
			controller.payByCard(payment);
			updatePaymentInfo();
			if (controller.getCurrent().getToBePaidAmt().compareTo(BigDecimal.ZERO) == 0)
				printBtn.requestFocus();
			else
				cash.requestFocus();
		}
	}

	@FXML
	public void payByCreditCard() {
		BankPayment payment;
		if (Config.isAbc())
			payment = showBankPayment(BankPayment.Type.CREDIT);
		else
			payment = showEdcPayment(BankPayment.Type.CREDIT);

		if (payment.getPaymentAmt() != null) {
			controller.payByCard(payment);
			updatePaymentInfo();
			if (controller.getCurrent().getToBePaidAmt().compareTo(BigDecimal.ZERO) == 0)
				printBtn.requestFocus();
			else
				cash.requestFocus();
		}
	}

	@FXML
	public void payByTransfer() {
		BankPayment payment = showBankPayment(BankPayment.Type.TRANSFER);
		if (payment != null) {
			controller.payByCard(payment);
			updatePaymentInfo();
			if (controller.getCurrent().getToBePaidAmt().compareTo(BigDecimal.ZERO) == 0)
				printBtn.requestFocus();
			else
				cash.requestFocus();
		}
	}

	@FXML
	public void cashdrawBtnOnAction(){
		User user = MainApp.getInstance().showLoginPopup(false, false, Role.STORE_MANAGER);
		if (user != null)
			DeviceUtil.getCashDrawerDevice().open();
	}

	@FXML
	public void reprintSessionOnAction() throws SQLException{
		User user = MainApp.getInstance().showLoginPopupExt(false, false);
		if(user != null)
			if (user.getRole().getName().equals(Role.STORE_MANAGER) || user.getRole().getName().equals(Role.SUPER_ADMIN) ||
				user.getRole().getName().equals(Role.SUPER_USER) ){

				SodEod sodEod = showReprintSession(user.getRole().getName());
				SodEodCtrl ctrl = new SodEodCtrl();
				if(sodEod != null){
					ctrl.setReprintMode(true);
					ctrl.getSodEodDao().getDao().refresh(sodEod);

					if(sodEod.getType() ==  Type.DAY)
						ctrl.generateSodEodSummary(sodEod, true);
					else
						ctrl.generateSodEodSummary(sodEod, false);
				}
			}
	}

	@FXML
	public void pendingBtnOnAction() {
		if (controller.getCurrent().getStatus() == Transaction.Status.OPEN) {
			try {
				controller.getCurrent().setStatus(Transaction.Status.HOLD);
				controller.saveAll();
				resetAll();
//				controller.createNew();
			} catch (SQLException e) {
				logger.error(e);
				MainApp.showAlertError("common_error_db_content");
			}
		} else {
			Transaction transaction = showPendingTransaction();
			controller.setReprintMode(false);
			if (transaction != null) {
				try {
					resetAll();
					controller.load(transaction);
					controller.getCurrent().setToBePaidAmt(controller.getCurrent().getTotalPrice().subtract(controller.getCurrent().getTotalPaidAmt()));
					if (controller.getCurrent().getStatus() == Transaction.Status.HOLD) {
						setPaymentDisabled(false);
					} else if (controller.getCurrent().getStatus() == Transaction.Status.FINISHED) {
						setFormAddDisable(true);
						printBtn.setDisable(false);
						controller.setReprintMode(true);
						controller.getCurrent().setToBePaidAmt(BigDecimal.ZERO);
					} else {
						setFormAddDisable(false);
						setPaymentDisabled(false);
						printBtn.setDisable(false);
					}
					updateAll();
				} catch (SQLException e) {
					logger.error(e);
					MainApp.showAlertError("common_error_db_content");
				}
			}
		}
	}

	private void afterSelect() {
		if (controller.getSelectedItem().getImageUrl() != null) {
			if (!productImageService.isRunning()) {
				productImageService.reset();
				productImageService.start();
			}
		}

		if (controller.getSelectedItem().getSizeCategory() == null && controller.getSelectedItem().getProductSize() == null) { // product wihtout size
			try {
				size.setDisable(true);
				qty.setDisable(false);
				qty.requestFocus();
				controller.calculateAvailableQty();
				availableQty.setText(String.valueOf(controller.getAvailableQty()));
			} catch (Exception e) {
				logger.error(e.toString(), e);
			}
		} else { // product with size
			if (controller.getSelectedItem().getProductSize() != null) {
				size.setText(controller.getSelectedItem().getProductSize().getSize().getSizeNumber());
				size.setDisable(true);
				qty.setDisable(false);
				qty.requestFocus();
			} else {  // focus on size so user easily input the size
				size.setDisable(false);
				size.requestFocus();
			}
			if (controller.getSelectedStock() != null) {
				controller.calculateAvailableQty();
				availableQty.setText(String.valueOf(controller.getAvailableQty()));
			}
		}

		checkStockBtn.setDisable(false);
		if (controller.getSelectedItem().getShortName() != null) {
			article.setText(controller.getSelectedItem().getShortName());
		} else {
			article.setText(controller.getSelectedItem().getArticle());
		}

	}

	protected void startGetPromoTask() {
		Task<?> promoTask = new Task<Object>() {
			@Override
			protected  Object call() {
				startProgressBar();
				Object result = null;
				try {
					result = controller.checkLastItemForPromo();
				} catch (Exception e) {
					logger.error(e.toString(), e);
				}
				endProgressBar();
				return result;
			}
		};
		promoTask.setOnSucceeded(t -> {
			if (t.getSource().getValue() instanceof PromoItem) {
				showPromoInfo((PromoItem)t.getSource().getValue());
			} else if (t.getSource().getValue() instanceof PromoPrizeItem) {
				showPromoInfo((PromoPrizeItem)t.getSource().getValue());
				updateAll();
			}
		});

		Thread th = new Thread(promoTask);
		th.setDaemon(true);
		th.start();
	}

	@FXML
	public void checkoutAndPrint() {
		if(Config.isEnableLineDisplay()){
			showDefaultLDDMessage();
		}

		List<String> escLevels = controller.getEscalationLevel();
		String userRole = SessionCtrl.getSession().getUser().getRole().getName();
		if (escLevels.indexOf(userRole) < 0) {
			User user = MainApp.getInstance().showLoginPopup(false, false, escLevels.get(0));
			if (user == null) {
				return;
			}
		}

		setPaymentDisabled(true);
		setDisableBtn(true);

		// just incase user not fire the return key, Calculate cash.
		payByCash();

		if (!printService.isRunning()) {
			printService.reset();
			printService.start();
		}
	}

//	added from dean
	public void showDefaultLDDMessage(){
		LineDisplayDriver ldd = DeviceUtil.getLineDisplayDevice();
 		ldd.clear();
 		ldd.setText(0, Config.getPurchaseText());
 		ldd.setText(1, "Rp. " + CurrencyUtil.format(controller.getCurrent().getTotalPrice(),false));
 		ldd.display(0);
		ldd.display(1);
		synchronized (locker) {
 			ldd.setLocked(false);
		}
		Thread x1 = new Thread(new Runnable() {
 			@Override
 			public void run() {
 				try {
 					Thread.sleep(Config.getByeTimeout());

 					LineDisplayDriver ldd = DeviceUtil.getLineDisplayDevice();
 					synchronized (locker) {
 						if (ldd.isLocked())
 							return;

 						ldd.clear();
 						ldd.setText(0, Config.getByeTextTop());
 						ldd.setText(1, Config.getByeTextBottom());
 						if (Config.getByeTextTopMarquee())
 							ldd.startMarquee(0, LineDisplayConst.DISP_MT_RIGHT,
 									Config.getMarqueeSpeed(), Config.getMarqueeReturnSpeed());
 						else
 							ldd.display(0);

 						if (Config.getByeTextBottomMarquee())
 							ldd.startMarquee(1, LineDisplayConst.DISP_MT_RIGHT,
 									Config.getMarqueeSpeed(), Config.getMarqueeReturnSpeed());
 						else
 							ldd.display(1);
 					}

 					Thread.sleep(Config.getDefaultTimeout());

 					synchronized (locker) {
 						if (ldd.isLocked())
 							return;

 						ldd.clear();
 						ldd.setText(0, Config.getDefaultTextTop());
 						ldd.setText(1, Config.getDefaultTextBottom());
 						if (Config.getDefaultTextTopMarquee())
 							ldd.startMarquee(0, LineDisplayConst.DISP_MT_RIGHT,
 									Config.getMarqueeSpeed(), Config.getMarqueeReturnSpeed());
 						else
 							ldd.display(0);

 						if (Config.getDefaultTextBottomMarquee())
 							ldd.startMarquee(1, LineDisplayConst.DISP_MT_RIGHT,
 									Config.getMarqueeSpeed(), Config.getMarqueeReturnSpeed());
 						else
 							ldd.display(1);
 					}
 				} catch (InterruptedException e) {
 					e.printStackTrace();
 				}
 			}
 		});
		x1.start();
	}

	public Product showProductBrowser() {
		ProductBrowser controller = new ProductBrowser();
		if (!StringUtil.isEmpty(transactionNo.getText()))
			controller.setTransactionNo(transactionNo.getText());
		MainApp.getInstance().createModalWindow(controller,true,false,false).showAndWait();

		return controller.getSelected();
	}

	public void showPromoInfo(PromoItem item) {
		PromoInfo controller = new PromoInfo();
		controller.setModel(item);
		MainApp.getInstance().createModalWindow(controller).showAndWait();
	}

	public void showPromoInfo(PromoPrizeItem prize) {
		PromoInfo controller = new PromoInfo();
		controller.setModel(prize);
		MainApp.getInstance().createModalWindow(controller).showAndWait();
	}

	public Transaction showPendingTransaction() {
		TransactionHistory controller = new TransactionHistory();
		MainApp.getInstance().createModalWindow(controller).showAndWait();

		return controller.getSelected();
	}

	public SodEod showReprintSession(String role){
		SodEodHistory Ctrl = new SodEodHistory();
		Ctrl.setUserRole(role);
		MainApp.getInstance().createModalWindow(Ctrl).showAndWait();
		return Ctrl.getSelected();
	}

	public VoucherDetail showVoucherPopup() {
		UseVoucherForm controller = new UseVoucherForm();
		MainApp.getInstance().createModalWindow(controller).showAndWait();

		return controller.getModel();
	}

	public BankPayment showBankPayment(BankPayment.Type type) {
		BankPaymentForm controller = new BankPaymentForm();
		controller.setType(type);
		controller.setToBePaidAmt(this.controller.getCurrent().getToBePaidAmt());
		switch (type) {
		case DEBIT:
			if (this.controller.getCurrent().getDebit() != null)
				controller.setModel(this.controller.getCurrent().getDebit());
			break;
		case CREDIT:
			if ((this.controller.getCurrent().getCredit() != null))
				controller.setModel(this.controller.getCurrent().getCredit());
			break;
		case TRANSFER:
			if (this.controller.getCurrent().getTransfer() != null)
				controller.setModel(this.controller.getCurrent().getTransfer());
			break;
		}
		MainApp.getInstance().createModalWindow(controller).showAndWait();

		return controller.getModel();
	}

	public BankPayment showEdcPayment(BankPayment.Type type) {
		EdcPaymentForm controller = new EdcPaymentForm();
		controller.setType(type);
		controller.setToBePaidAmt(this.controller.getCurrent().getToBePaidAmt());
		switch (type) {
		case DEBIT:
			if (this.controller.getCurrent().getDebit() != null)
				controller.setModel((EdcPayment) this.controller.getCurrent().getDebit());
			break;
		case CREDIT:
			if ((this.controller.getCurrent().getCredit() != null))
				controller.setModel((EdcPayment) this.controller.getCurrent().getCredit());
			break;
		case TRANSFER:
			if (this.controller.getCurrent().getTransfer() != null)
				controller.setModel((EdcPayment) this.controller.getCurrent().getTransfer());
			break;
		}
		MainApp.getInstance().createModalWindow(controller).showAndWait();

		return controller.getModel();
	}

	public void doSpecialDisc(TransactionItem item) {
		if (controller.getCurrent().getStatus() == Transaction.Status.FINISHED)
			return;

		if (authorizedUser == null) {
			authorizedUser = MainApp.getInstance().showLoginPopup(false, false, Role.STORE_MANAGER);
		}

		if (authorizedUser != null) {
			BigDecimal finalPrice = showSpecialDiscPopup(item);
			controller.specialDisc(item, finalPrice);
			controller.processPayment();
			updateAll();
		}
	}

	public BigDecimal showSpecialDiscPopup(TransactionItem item) {
		SpecialDiscForm controller = new SpecialDiscForm();
		controller.setItem(item);
		MainApp.getInstance().createModalWindow(controller, false, false, true).showAndWait();

		return controller.getFinalPriceAmt();
	}

	private void updateCustomerInfo() {
		Member customer = controller.getCurrent().getMember();
		if (customer == null)
			return;

		StringBuilder strBuilder = new StringBuilder();

		strBuilder.append(customer.getMemberNo());
		strBuilder.append(" | ");
		strBuilder.append(customer.getName());
		if (customer.getAddress() != null) {
			strBuilder.append(" | ");
			strBuilder.append(customer.getAddress());
			if (customer.getCity() != null) {
				strBuilder.append(", ");
				strBuilder.append(customer.getCity());
			}
			if (customer.getPostcode() != null) {
				strBuilder.append(", ");
				strBuilder.append(customer.getPostcode());
			}
		}
		customerInfo.setText(strBuilder.toString());
		if (customer.getLevel() != null)
			customerLevel.setText(customer.getLevel().getDesc());
		if (customer.getPoint() != null)
			customerPoint.setText(customer.getPoint().toString());
	}

	public void resetCustomerInfo() {
		customerInfo.setText(null);
		customerLevel.setText(null);
		customerPoint.setText(null);
	}

	public void setFormAddDisable(boolean disabled) {
		barcode.setDisable(disabled);
		article.setDisable(disabled);
		size.setDisable(disabled);
		qty.setDisable(disabled);
		checkStockBtn.setDisable(disabled);
		productImage.setImage(new Image(Config.getDefaultProductImagePath()));
		returBtn.setDisable(disabled);
		browseProductBtn.setDisable(disabled);
	}

	public void resetFormAdd() {
		transactionNo.setVisible(false);
		transactionNo.setManaged(false);
		transactionNo.setText(null);

		barcode.setText(null);
		barcode.requestFocus();
		article.setText(null);
		size.setText(null);
		size.setDisable(true);
		qty.setText(null);
		qty.setDisable(true);
		availableQty.setText(null);
		checkStockBtn.setDisable(true);
		productImage.setImage(new Image(Config.getDefaultProductImagePath()));
		returBtn.setSelected(false);
		controller.setReturMode(false);

		returBtn.setDisable(false);
		barcode.setDisable(false);
		browseProductBtn.setDisable(false);
		qty.setDisable(false);
	}

	public void updateItem(TransactionItem item) {
		item.qtyProperty();
		item.discAmtProperty();
		item.discPctProperty();
		item.totalProperty();
	}

	public void updateSummaryInfo() {
		Transaction transaction = controller.getCurrent();
		subtotal.setText(CurrencyUtil.format(transaction.getSubTotalAmt(), false));
		totalDisc.setText(CurrencyUtil.format(transaction.getTotalDisc(), false));
		totalPrice.setText(CurrencyUtil.format(transaction.getTotalPrice(), false));
	}

	public void resetSummaryInfo() {
		subtotal.setText(null);
		totalDisc.setText(null);
		totalPrice.setText(null);
	}

	public void updateAll() {
		updateCustomerInfo();
		transactionRows.clear();
		transactionRows.addAll(controller.getCurrent().getItems());
		updateSummaryInfo();
		updatePaymentInfo();
	}

	public void updatePaymentInfo() {
		Transaction transaction = controller.getCurrent();
		if (transaction.getVoucherAmt() != null)
			voucherAmt.setText(CurrencyUtil.format(transaction.getVoucherAmt(), false));

		if (transaction.getStatus() == Transaction.Status.HOLD){
			// karena ketika dipending, cust belum menyerahkan uang
			totalPriceAfterVoucher.setText(CurrencyUtil.format(transaction.getTotalPrice(), false));
			transaction.setStatus(Transaction.Status.OPEN);
		} else {
			totalPriceAfterVoucher.setText(CurrencyUtil.format(transaction.getToBePaidAmt(), false));
		}
		if (transaction.getDebitAmt() != null)
			debitAmt.setText(CurrencyUtil.format(transaction.getDebitAmt(), false));
		if (transaction.getCreditAmt() != null)
			creditCardAmt.setText(CurrencyUtil.format(transaction.getCreditAmt(), false));
		if (transaction.getTransferAmt() != null)
			transferAmt.setText(CurrencyUtil.format(transaction.getTransferAmt(), false));
		if (transaction.getCashAmt() != null && transaction.getStatus() != Transaction.Status.HOLD)
			cash.setText(CurrencyUtil.format(transaction.getCashAmt(), false));
		if (transaction.getCashChange() != null){
			if(transaction.getMember() != null && transaction.getMember().getLevel().isCredit())
				change.setText(CurrencyUtil.format(BigDecimal.ZERO, false));
			else
				change.setText(CurrencyUtil.format(transaction.getCashChange(), false));
		}
	}

	public void resetPaymentInfo() {
		voucherAmt.setText(null);
		totalPriceAfterVoucher.setText(null);
		debitAmt.setText(null);
		creditCardAmt.setText(null);
		transferAmt.setText(null);
		cash.setText(null);
		change.setText(null);
	}

	public void setPaymentDisabled(boolean isDisabled) {
		voucherBtn.setDisable(isDisabled);
		debitBtn.setDisable(isDisabled);
		creditBtn.setDisable(isDisabled);
		transferBtn.setDisable(isDisabled);
		printBtn.setDisable(isDisabled);
		cash.setDisable(isDisabled);
		change.setDisable(isDisabled);
	}

	public void resetAll() {
		authorizedUser = null;
		controller.reset();
		resetCustomerInfo();
		resetFormAdd();
		transactionRows.clear();
		resetPaymentInfo();
		resetSummaryInfo();
		pendingBtn.setText("History");
		try {
			controller.createNew();
		} catch (SQLException e) {
			logger.error(e);
			MainApp.showAlertError("common_error_db_content");
		}
	}

	protected void displayTransactionInfo() {
		Screen primaryScreen = Screen.getPrimary();

		Screen.getScreens().stream()
        .filter(s->!s.equals(primaryScreen))
        .findFirst().ifPresent(s->secondaryScreen = s);

		if(secondaryScreen!=null){
		    secondaryStage = new Stage();
		    Parent root2;
			try {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("TransactionInfo.fxml"));
				TransactionInfo controller = new TransactionInfo();
				loader.setController(controller);
				root2 = loader.load();
				Scene scene2 = new Scene(root2);
			    secondaryStage.setScene(scene2);
			    Rectangle2D bounds2 = secondaryScreen.getBounds();
			    secondaryStage.setX(bounds2.getMinX());
			    secondaryStage.setY(bounds2.getMinY());
			    secondaryStage.setWidth(bounds2.getWidth());
			    secondaryStage.setHeight(bounds2.getHeight());
			    secondaryStage.initStyle(StageStyle.UNDECORATED);


			    controller.startImageSlider();
			    controller.startVideoSlider();

			    controller.getCustomerInfo().textProperty().bind(customerInfo.textProperty());
			    controller.getCustomerLevel().textProperty().bind(customerLevel.textProperty());
			    controller.getCustomerPoint().textProperty().bind(customerPoint.textProperty());
			    controller.getSubtotal().textProperty().bind(subtotal.textProperty());
			    controller.getTotalDisc().textProperty().bind(totalDisc.textProperty());
			    controller.getTotalPrice().textProperty().bind(totalPrice.textProperty());
			    controller.getVoucherAmt().textProperty().bind(voucherAmt.textProperty());
			    controller.getDebitAmt().textProperty().bind(debitAmt.textProperty());
			    controller.getCreditCardAmt().textProperty().bind(creditCardAmt.textProperty());
			    controller.getCash().textProperty().bind(cash.textProperty());
			    controller.getChange().textProperty().bind(change.textProperty());

			    controller.getTable().setItems(transactionRows);
			    secondaryStage.show();


			} catch (IOException e) {
				logger.error(e.toString(), e);
			}
		}
	}

	public void displayHotKeyInfo(){
		if (Config.isAbc()){
			hotKey1.setText("[ "+ Config.getCustomerHotKey() + " ]");
			hotKey2.setText("[ "+ Config.getBrowseProductHotKey()+ " ]");
			hotKey3.setText("[ "+ Config.getCheckAllStockHotKey()+ " ]");
			hotKey4.setText("[ "+ Config.getReturnHotKey()+ " ]");
			hotKey5.setText("[ "+ Config.getPendingHotKey()+ " ]");
			hotKey6.setText("[ "+ Config.getDebitHotKey()+ " ]");
			hotKey7.setText("[ "+ Config.getCreditHotKey()+ " ]");
			hotKey8.setText("[ "+ Config.getVoucherHotKey()+ " ]");
			hotKey9.setText("[ "+ Config.getTransferHotKey()+ " ]");
			hotKey10.setText("[ "+ Config.getCashHotKey()+ " ]");
			hotKey11.setText("[ "+ Config.getBarcodeHotKey()+ " ]");
			hotKey12.setText("[ "+ Config.getSaveHotKey()+ " ]");
			hotKey13.setText("[ "+ Config.getDeleteHotKey()+ " ]");
			hotKey14.setText("[ "+ Config.getDiscountHotKey()+ " ]");
			hotKey15.setText("[ "+ Config.getSelectItemHotKey()+ " ]");
			hotKey16.setText("[ "+ Config.getVoidHotKey()+ " ]");

			descrHotKey1.setText("CUSTOMER");
			descrHotKey2.setText("BROWSE PRODUCT");
			descrHotKey3.setText("CHECK ALL STOCK");
			descrHotKey4.setText("RETURN");
			descrHotKey5.setText("PENDING");
			descrHotKey6.setText("DEBIT");
			descrHotKey7.setText("CREDIT");
			descrHotKey8.setText("VOUCHER");
			descrHotKey9.setText("TRANSFER");
			descrHotKey10.setText("CASH");
			descrHotKey11.setText("BARCODE");
			descrHotKey12.setText("SAVE & PRINT");
			descrHotKey13.setText("DELETE ITEM");
			descrHotKey14.setText("DISCOUNT");
			descrHotKey15.setText("SELECT ITEMS");
			descrHotKey16.setText("VOID TRANS.");
		} else {
			hotKey1.setText("[ "+ Config.getCustomerHotKey() + " ]");
			hotKey2.setText("[ "+ Config.getBrowseProductHotKey()+ " ]");
			hotKey3.setText("[ "+ Config.getReturnHotKey()+ " ]");
			hotKey4.setText("[ "+ Config.getPendingHotKey()+ " ]");
			hotKey5.setText("[ "+ Config.getDebitHotKey()+ " ]");
			hotKey6.setText("[ "+ Config.getCreditHotKey()+ " ]");
			hotKey7.setText("[ "+ Config.getVoucherHotKey()+ " ]");
			hotKey8.setText("[ "+ Config.getTransferHotKey()+ " ]");
			hotKey9.setText("[ "+ Config.getCashHotKey()+ " ]");
			hotKey10.setText("[ "+ Config.getBarcodeHotKey()+ " ]");
			hotKey11.setText("[ "+ Config.getSaveHotKey()+ " ]");
			hotKey12.setText("[ "+ Config.getDeleteHotKey()+ " ]");
			hotKey13.setText("[ "+ Config.getDiscountHotKey()+ " ]");
			hotKey14.setText("[ "+ Config.getSelectItemHotKey()+ " ]");
			hotKey15.setText("[ "+ Config.getVoidHotKey()+ " ]");

			descrHotKey1.setText("Customer");
			descrHotKey2.setText("Browse Product");
			descrHotKey3.setText("Return");
			descrHotKey4.setText("Pending");
			descrHotKey5.setText("Debit");
			descrHotKey6.setText("Credit");
			descrHotKey7.setText("Voucher");
			descrHotKey8.setText("Transfer");
			descrHotKey9.setText("Cash");
			descrHotKey10.setText("Barcode");
			descrHotKey11.setText("Save & Print");
			descrHotKey12.setText("Delete Item");
			descrHotKey13.setText("Discount");
			descrHotKey14.setText("Select Item");
			descrHotKey15.setText("Void");

			hotKey16.setVisible(false);
			descrHotKey16.setVisible(false);
		}
	}
}
