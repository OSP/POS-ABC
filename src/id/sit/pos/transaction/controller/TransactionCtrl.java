package id.sit.pos.transaction.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.stmt.DeleteBuilder;

import id.r.engine.controller.RDao;
import id.r.engine.model.RModel;
import id.r.engine.model.SearchParam;
import id.r.engine.model.SearchParam.Operator;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.customer.model.Member;
import id.sit.pos.customer.controller.CustomerCtrl;
import id.sit.pos.customer.model.CustomerCashback;
import id.sit.pos.customer.model.MemberLevel;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.db.DbConnector;
import id.sit.pos.master.model.ProductSize;
import id.sit.pos.master.model.ProductStock;
import id.sit.pos.master.model.Promo;
import id.sit.pos.master.model.PromoItem;
import id.sit.pos.master.model.PromoPrizeItem;
import id.sit.pos.master.model.Size;
import id.sit.pos.master.model.Sku;
import id.sit.pos.master.model.Voucher;
import id.sit.pos.master.model.Voucher.Type;
import id.sit.pos.master.model.VoucherDetail;
import id.sit.pos.master.controller.ProductCtrl;
import id.sit.pos.master.controller.PromoCtrl;
import id.sit.pos.master.model.Ads;
import id.sit.pos.master.model.Product;
import id.sit.pos.transaction.model.AccountReceivable;
import id.sit.pos.transaction.model.BankPayment;
import id.sit.pos.transaction.model.CancelledItem;
import id.sit.pos.transaction.model.MutationHistory;
import id.sit.pos.transaction.model.PaymentType;
import id.sit.pos.transaction.model.Transaction;
import id.sit.pos.transaction.model.Transaction.Status;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.DateUtil;
import id.sit.pos.util.StringUtil;
import id.sit.pos.transaction.model.TransactionItem;
import id.sit.pos.user.controller.SessionCtrl;
import id.sit.pos.user.model.Role;
import id.sit.pos.user.model.User;

/**
 * @author PiNGUiN
 *
 */
/**
 * @author PiNGUiN
 *
 */
public class TransactionCtrl {
	private static Logger logger = Logger.getLogger(TransactionCtrl.class.getName());
	private String transactionNo;
	private Product selectedItem;
	private TransactionItem selectedReturnItem;
	private ProductStock selectedStock;
	private int availableQty = 0;
	private int selectedQty = 1;
	private Transaction current;
	private boolean returMode;
	private boolean promoApplied;
	private boolean reprintMode;
	private BigDecimal purchasePrice;
	private BigDecimal previousMemberBalance;

	private RDao<ProductSize> productSizeDao = new RDao<>(ProductSize.class);
	private RDao<ProductStock> productStockDao = new RDao<>(ProductStock.class);
	private RDao<Transaction> transactionDao;
	private RDao<TransactionItem> transactionItemDao = new RDao<TransactionItem>(TransactionItem.class);
	private RDao<MutationHistory> mutationHistoryDao = new RDao<>(MutationHistory.class);
	private RDao<BankPayment> bankPaymentDao = new RDao<>(BankPayment.class);
	private RDao<CancelledItem> cancelledItemDao = new RDao<>(CancelledItem.class);
	private RDao<Member> customerDao = new RDao<Member>(Member.class);
	private RDao<MemberLevel> customerLevelDao = new RDao<MemberLevel>(MemberLevel.class);
	private RDao<CustomerCashback> customerCashbackDao = new RDao<CustomerCashback>(CustomerCashback.class);
	private RDao<VoucherDetail> voucherDetailDao = new RDao<VoucherDetail>(VoucherDetail.class);
	private RDao<AccountReceivable> accountReceivableDao = new RDao<>(AccountReceivable.class);
	private RDao<Sku> skuDao = new RDao<>(Sku.class);
	private RDao<Ads> adsDao= new RDao<>(Ads.class);

	private ProductCtrl productCtrl = new ProductCtrl();
	private PromoCtrl promoCtrl = new PromoCtrl();
	private ReceiptPrinterCtrl receiptCtrl = new ReceiptPrinterCtrl();
	private CustomerCtrl customerCtrl = new CustomerCtrl();

	private TransactionItem lastItem;

	public TransactionCtrl() {
		transactionDao = new RDao<Transaction>(Transaction.class) {
			@Override
			public void beforeSave(RModel model) {
				super.beforeSave(model);
				if (((Transaction) model).getTransactionNo() == null) {
					Date date = new Date();
					Calendar cal = Calendar.getInstance();
					cal.setTime(date);
					Integer year = cal.get(Calendar.YEAR);
					Integer month = cal.get(Calendar.MONTH);
					Integer day = cal.get(Calendar.DAY_OF_MONTH);

					String transactionNo = String.valueOf(year).substring(2, 3)
							+ String.format("%02d", month)
							+ String.format("%02d", day)
							+ String.format(String.valueOf(System.currentTimeMillis()).substring(5, 13));
					((Transaction) model).setTransactionNo(transactionNo);
				}
			}
		};

		mutationHistoryDao = new RDao<MutationHistory>(MutationHistory.class) {
			@Override
			public void beforeSave(RModel model) {
				super.beforeSave(model);
				try {
					GenericRawResults<String[]> result = getDao().queryRaw("SELECT nextval('product_mutation_histories_id_seq')");
					Integer id = new Integer(result.getFirstResult()[0]);
					result.close();
					model.setId(id);
				} catch (SQLException e) {
					logger.error(e.toString(), e);
				}
			}
		};
	}

//	Key Combination 31/05/2016 - fzr
	public void deleteAllTransaction() throws SQLException{
		DeleteBuilder<Transaction, Integer> delBuildSales = transactionDao.getDao().deleteBuilder();
		delBuildSales.delete();

		DeleteBuilder<TransactionItem, Integer> delBuildSalesDetails = transactionItemDao.getDao().deleteBuilder();
		delBuildSalesDetails.delete();

		DeleteBuilder<BankPayment, Integer> delBuildSalesPaymentBank = bankPaymentDao.getDao().deleteBuilder();
		delBuildSalesPaymentBank.delete();
	}

	public void createNew() throws SQLException {
		reset();
		current = new Transaction();
		current.setSession(SessionCtrl.getSession());
		current.setPaymentType(PaymentType.CASH);
		current.setStatus(Status.NEW);
		current.setBranch(SettingCtrl.getSetting().getStore());
		current.setMachineId(SettingCtrl.getSetting().getMachineId());
		current.setCashier(SessionCtrl.getSession().getUser());
		current.setItems(new ArrayList<TransactionItem>());
		this.setReprintMode(false);
	}

	public void load (Transaction savedTransaction) throws SQLException {
		reset();
		current = savedTransaction;
		DaoManagerImpl.getUserDao().refresh(current.getCashier());
		Collection<TransactionItem> transactionItems = new ArrayList<>();
		int i = 1;
		for (TransactionItem item : current.getItems()) {
			try {
				item.setItemNo(i++);
				productCtrl.refreshFull(item.getProduct());
				skuDao.getDao().refresh(item.getSku());
				transactionItems.add(item);
			} catch (Exception e) {
				logger.error(e.toString(), e);
			}
		}
		current.setItems(transactionItems);
	}

	public void delete() throws SQLException {
		if (current.getDebit() != null)
			bankPaymentDao.delete(current.getDebit());

		if (current.getCredit() != null)
			bankPaymentDao.delete(current.getCredit());

		if (current.getTransfer() != null)
			bankPaymentDao.delete(current.getTransfer());

		for (TransactionItem item : current.getItems()) {
			saveCanceledItem(item);
			transactionItemDao.delete(item);
		}

		transactionDao.delete(current);
	}

	public boolean selectProduct(String barcode) throws SQLException {
		if (!isReturMode()){
			Product product = productCtrl.getProduct(barcode);
			if (product == null)
				return false;

			return selectProduct(product);
		} else {
			CloseableIterator<TransactionItem> iterator = transactionItemDao.search(new SearchParam[] {
					new SearchParam(Sku.class, Sku._BARCODE, barcode, Operator.E, new Class<?>[]{ Product.class }),
					new SearchParam(Transaction.class, Transaction._TRANSACTION_NO,transactionNo)
					});
			if (iterator.hasNext()){
				TransactionItem item = iterator.next();

				return selectReturnItem(item);
			}
			iterator.closeQuietly();
		}
		return false;
	}

	public boolean selectProduct(Product product) throws SQLException {
		selectedItem = product;
		CloseableIterator<TransactionItem> iterator = null;

		if (!isReturMode()){
			if (selectedItem != null) {
				if (selectedItem.getProductStock() != null) {
					selectedStock = selectedItem.getProductStock();
					if (!product.isComposite())
						availableQty = selectedStock.getAvailableQty();
					else
						availableQty = productCtrl.getAvailableQty(product);
					return true;
				}
			}
			return false;
		} else {
			if (Config.isAbc()){
				iterator = transactionItemDao.search(new SearchParam[] {
					new SearchParam(TransactionItem._PRODUCT_SIZE_ID, product.getProductSize().getId()),
					new SearchParam(TransactionItem._QTY, 0, Operator.GT),
					new SearchParam(Transaction.class, Transaction._TRANSACTION_NO, transactionNo)
				});
			} else {
				iterator = transactionItemDao.search(new SearchParam[] {
						new SearchParam(TransactionItem._SKU_ID, product.getSku().getId()),
						new SearchParam(TransactionItem._QTY, 0, Operator.GT),
						new SearchParam(Transaction.class, Transaction._TRANSACTION_NO, transactionNo)
					});
			}


			if (iterator.hasNext()){
				TransactionItem item = iterator.next();
				return selectReturnItem(item);
			}
			iterator.closeQuietly();
		}
		return false;
	}

	public boolean selectReturnItem(TransactionItem item) throws SQLException {
		if (item == null)
			return false;

//		productCtrl.getProductRDao().getDao().refresh(item.getProduct());
//		productCtrl.getSkuRDao().getDao().refresh(item.getSku());
//		productCtrl.getProductSizeRDao().getDao().refresh(item.getProductSize());

		selectedReturnItem = item;
		if(Config.isAbc()){
			selectedItem = selectedReturnItem.getProductSize().getProduct();
		} else {
			selectedItem = selectedReturnItem.getProduct();
		}
		selectedStock = item.getProductStock();
		productCtrl.refreshForAdd(selectedReturnItem.getProduct(),true);

		selectedItem.setSku(item.getSku());
		return true;
	}

	public void selectStock(ProductStock productSize) {
		selectedStock = productSize;
	}

	public void calculateAvailableQty() {
		int qty = 0;
		if (!returMode) {
			qty = selectedStock.getAvailableQty();

			for (TransactionItem item: current.getItems()) {
				if (Config.isAbc()){
					if(item.getQty() > 0 && item.getProduct().getId().equals(selectedStock.getProduct().getId())){
						qty -= item.getQty();
						break;
					}
				} else {
//					if (item.getQty()>0 && item.getProduct().getSku().getId().equals(selectedStock.getProduct().getSku().getId())) {
					if (item.getQty()>0 && item.getSku().getId().equals(selectedStock.getProduct().getSku().getId())) {

						qty -= item.getQty();
						break;
					}
				}
			}
		} else {
			qty = selectedReturnItem.getQty();
			if (qty < 0)
				qty = 0;
			else
				for (TransactionItem item: current.getItems()) {
					if (item.getQty()<0 && item.getProduct().getId().equals(selectedReturnItem.getProduct().getId())) {
						qty += item.getQty();
						break;
					}
				}
		}
		availableQty = qty;
	}

	public int getCurrentItemCount(Integer transId)
	{
		long totalItems = 0;
		try {
			totalItems = transactionItemDao.getDao().queryBuilder().where().eq("sale_id", transId).countOf();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return (int)totalItems ;
	}

	public boolean selectSize(String size) throws SQLException {
		selectedStock = productStockDao.queryFirst(new SearchParam[] {
				new SearchParam(ProductStock._WAREHOUSE_ID, SettingCtrl.getBranchId()),
				new SearchParam(Size.class, Size._NAME, size, new Class<?>[] {
					ProductSize.class
				})
		});

		if (selectedStock  != null) {
			return true;
		} else {
			return false;
		}
	}


	public void setSelectedQty(int selectedQty) throws UnsufficientStockException {
		if (!Config.isEnableMinusStock() || returMode) {
			if (selectedQty > getAvailableQty()) {
				throw new UnsufficientStockException();
			}
		}
		this.selectedQty = selectedQty;
	}

	public TransactionItem addSelectedItem() throws UnselectedProduct, UnselectedSize, SQLException, UndefinedPriceException, UndefinedCostPriceException {
		if (selectedItem == null)
			throw new UnselectedProduct();

		if (selectedItem.getPriceModal() == null)
			throw new UndefinedCostPriceException();

		if (selectedStock == null && !returMode)
			throw new UnselectedSize();

		if (current.getTransactionDate() == null)
			current.setTransactionDate(new Date());

		if (!returMode)
			for (TransactionItem item: current.getItems()) {
				/*
				 * cek apakah item yang sama?
				 * jika iya.. update qty
				*/

				if(!Config.isAbc()){
					if (item.getQty()>0 && item.getSku().getId().equals(selectedItem.getSku().getId())) {
						item.setQty(item.getQty()+selectedQty);
						processItem(item);
						summarize();
						return item;
					}
				}else{
					if (item.getQty()>0 && item.getProductSize().getId().equals(selectedItem.getProductSize().getId())) {

						item.setQty(item.getQty()+selectedQty);
						processItem(item);
						summarize();
						return item;
					}
				}
			}
		else
			for (TransactionItem item: current.getItems()) {
				if (item.getQty()<0 && item.getSku().getId().equals(selectedReturnItem.getSku().getId())) {
					item.setQty((item.getQty()*-1)+selectedQty);
					processReturnItem(item);
					summarize();
					return item;
				}
			}

		TransactionItem item;
		item = new TransactionItem();
		item.setItemNo(current.getItems().size()+1);
		item.setTransaction(current);
		item.setSku(selectedItem.getSku());
		item.setProduct(selectedItem);
		if (Config.isAbc()) {
			item.setProductSize(selectedItem.getProductSize());
			item.sizeProperty();
			item.barcodeProperty();
		}
		item.setProductStock(selectedStock);
		item.setQty(selectedQty);
		item.setCapitalPrice(selectedItem.getPriceModal());
		if (!returMode) {
			BigDecimal productPrice = selectedItem.getPriceTag();
			if (productPrice == null)
				throw new UndefinedPriceException();
			else {
				item.setInitPrice(productPrice);
			}

			processItem(item);
		} else {
			processReturnItem(item);
		}
		lastItem = item;
		current.getItems().add(item);
		summarize();
		current.setStatus(Status.OPEN);

		return item;
	}

	public void specialDisc(TransactionItem item, BigDecimal finalPrice) {
		item.setDiscPrice(finalPrice);
		item.setDiscAmt(item.getInitPrice().subtract(item.getDiscPrice()));
		item.setDiscPct(item.getDiscAmt().divide(item.getInitPrice(),  2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).floatValue());
		item.setTotalDisc(item.getDiscAmt().multiply(new BigDecimal(item.getQty())));
		item.setSubtotalPrice(item.getInitPrice().multiply(new BigDecimal(item.getQty())));
		item.setTotalPrice(item.getSubtotalPrice().subtract(item.getTotalDisc()));
		item.setSpecialDisc(true);
		summarize();
	}

 	protected TransactionItem processItem(TransactionItem item) {
		Product product = item.getProduct();


		BigDecimal initialPrice = item.getInitPrice();
		BigDecimal qty = new BigDecimal(item.getQty());
		item.setSubtotalPrice(initialPrice.multiply(new BigDecimal(item.getQty())));
		Member customer = current.getMember();
		if (customer == null || customer.getLevel() == null) {
			if (Config.isAbc()) {
				item.setDiscAmt(initialPrice.subtract(product.getPriceRetail()));
				item.setDiscPct(
						item.getDiscAmt().divide(initialPrice, 2, RoundingMode.HALF_UP)
								.multiply(new BigDecimal(100)).floatValue());
				item.setDiscPrice(item.getInitPrice().subtract(item.getDiscAmt()));
				item.setTotalDisc(item.getDiscAmt().multiply(qty));
				item.setTotalPrice(product.getPriceRetail().multiply(qty));
			} else {
				try {
					productCtrl.getProductRDao().getDao().refresh(product);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				item.setDiscPct(0f);
				item.setDiscAmt(BigDecimal.ZERO);
				item.setTotalDisc(BigDecimal.ZERO);
				item.setDiscPrice(item.getInitPrice().subtract(item.getDiscAmt()));
				item.setTotalPrice(item.getSubtotalPrice());
				if (Config.isEnablePpn() && item.getProduct().getTaxFlag() == Product.TaxFlag.BKP)
					item.setTotalPpn(item.getTotalPrice().divide(new BigDecimal(1.1),  2, RoundingMode.HALF_UP).divide(BigDecimal.TEN));
			}
		} else {
			if (Config.isAbc()) {
				if (customer.getLevel() != null) {
					if (customer.getLevel().getLevel() == 1)
						item.setDiscAmt(product.getPriceTag().subtract(product.getPriceMemberRetail()));
					if (customer.getLevel().getLevel() == 2)
						item.setDiscAmt(product.getPriceTag().subtract(product.getPriceMemberCredit()));
				} else {
					item.setDiscAmt(product.getPriceTag().subtract(product.getPriceRetail()));
				}

				item.setDiscPct(item.getDiscAmt().divide(initialPrice, 2, RoundingMode.HALF_UP)
						.multiply(new BigDecimal(100)).floatValue());
				item.setDiscPrice(item.getInitPrice().subtract(item.getDiscAmt()));
				item.setTotalDisc(item.getDiscAmt().multiply(qty));
				item.setTotalPrice(item.getSubtotalPrice().subtract(item.getTotalDisc()));
			} else {
				if (customer.getLevel() != null) {
					item.setDiscPct(customer.getLevel().getDiscPct());
					item.setDiscAmt(initialPrice.multiply(new BigDecimal(item.getDiscPct()).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP)));
					item.setDiscPrice(item.getInitPrice().subtract(item.getDiscAmt()));
					item.setTotalDisc(item.getDiscAmt().multiply(qty));
					item.setTotalPrice(item.getSubtotalPrice().subtract(item.getTotalDisc()));
				}
			}
		}

		return item;
	}

 	protected TransactionItem processReturnItem(TransactionItem item) {
 		if (Config.isAbc()) {
			if (purchasePrice != null && purchasePrice.compareTo(BigDecimal.ZERO)>0) {
				item.setDiscPrice(purchasePrice);
				item.setSubtotalPrice(item.getInitPrice().multiply(new BigDecimal(item.getQty())));
				item.setDiscAmt(item.getInitPrice().subtract(item.getDiscPrice()));
				item.setDiscPct(item.getDiscAmt().divide(item.getInitPrice(), 2, RoundingMode.HALF_UP).floatValue());
				item.setTotalPrice(item.getDiscPrice().multiply(new BigDecimal(item.getQty())));
			} else {
				processItem(item);
			}
		} else {
			item.setCapitalPrice(selectedReturnItem.getCapitalPrice());
			item.setInitPrice(selectedReturnItem.getInitPrice());
			item.setDiscPrice(selectedReturnItem.getDiscPrice());
			item.setSubtotalPrice(item.getInitPrice().multiply(new BigDecimal(item.getQty())));
			item.setDiscAmt(selectedReturnItem.getDiscAmt());
			item.setDiscPct(selectedReturnItem.getDiscPct());
			item.setTotalPrice(item.getDiscPrice().multiply(new BigDecimal(item.getQty())));
			item.setTotalPpn((selectedReturnItem.getTotalPpn().divide(new BigDecimal(selectedReturnItem.getQty()), 2, RoundingMode.HALF_UP)).multiply(new BigDecimal(item.getQty())));
		}
 		item.setCapitalPrice(item.getCapitalPrice().negate());
		item.setInitPrice(item.getInitPrice().negate());
		item.setDiscPrice(item.getDiscPrice().negate());
		item.setQty(-item.getQty());
		item.setSubtotalPrice(item.getSubtotalPrice().negate());
		item.setTotalDisc(item.getTotalDisc().negate());
		item.setTotalPrice(item.getTotalPrice().negate());
		item.setTotalPpn(item.getTotalPpn().negate());

		return item;
 	}

	public void processAllItem(){
		for (TransactionItem item : current.getItems()) {
			processItem(item);
		}
	}

	public void summarize(){
		current.setTotalQty(0);
		current.setTotalCapitalPrice(BigDecimal.ZERO);
		current.setSubTotalAmt(BigDecimal.ZERO);
		current.setTotalDisc(BigDecimal.ZERO);
		current.setTotalPrice(BigDecimal.ZERO);
		current.setToBePaidAmt(BigDecimal.ZERO);
		current.setTotalPpn(BigDecimal.ZERO);

		for (TransactionItem item : current.getItems()) {
			current.setTotalCapitalPrice(current.getTotalCapitalPrice().add(item.getCapitalPrice()));
			current.setTotalQty(current.getTotalQty()+item.getQty());
			current.setSubTotalAmt(current.getSubTotalAmt().add(item.getSubtotalPrice()));
			current.setTotalDisc(current.getTotalDisc().add(item.getTotalDisc()));
			current.setTotalPrice(current.getTotalPrice().add(item.getTotalPrice()));
			current.setTotalPpn(current.getTotalPpn().add(item.getTotalPpn()));
		}
		current.setToBePaidAmt(current.getTotalPrice().subtract(current.getTotalPaidAmt()));
	}

	public void processPayment() {
		BigDecimal toBePaid = current.getTotalPrice();
		BigDecimal totalPaid = BigDecimal.ZERO;
		VoucherDetail vd = current.getVoucher();

		if (vd != null && vd.getVoucher() != null) {
			if (vd.getVoucher().getType() == Voucher.Type.DiscountVoucher) {
				BigDecimal voucherAmt = null;
				if (vd.getVoucher().getDiscountAmt() != null)
					voucherAmt = vd.getVoucher().getDiscountAmt();
				else if (vd.getVoucher().getDiscountPct() != null)
					voucherAmt = current.getTotalPrice().multiply(new BigDecimal(vd.getVoucher().getDiscountPct()/100));

				if (voucherAmt != null)
					current.setVoucherAmt(voucherAmt);
					totalPaid = totalPaid.add(voucherAmt);
			} else if (vd.getVoucher().getType() == Voucher.Type.Cashback){
				totalPaid = totalPaid.add(vd.getRedeemAmt());
				current.setVoucherAmt(vd.getRedeemAmt());
			} if (vd.getVoucher().getType() == Voucher.Type.ArVoucher) {
				BigDecimal debt = vd.getVoucher().getMaxVoucherAmt();
				current.setTotalOutstanding(debt);
				current.setVoucherAmt(debt);
				toBePaid = current.getTotalPrice().subtract(debt);
			}
		}
		if (current.getDebitAmt() != null && current.getDebitAmt().compareTo(BigDecimal.ZERO)>0)
			totalPaid = totalPaid.add(current.getDebitAmt());
		if (current.getCreditAmt() != null && current.getCreditAmt().compareTo(BigDecimal.ZERO)>0)
			totalPaid = totalPaid.add(current.getCreditAmt());
		if (current.getTransferAmt() != null && current.getTransferAmt().compareTo(BigDecimal.ZERO)>0)
			totalPaid = totalPaid.add(current.getTransferAmt());
		if (current.getCashAmt() != null && current.getCashAmt().compareTo(BigDecimal.ZERO)>0)
			totalPaid = totalPaid.add(current.getCashAmt());

		toBePaid = toBePaid.subtract(totalPaid);

		// calculate change
		BigDecimal change = BigDecimal.ZERO;
		if ((vd != null && vd.getVoucher() != null) && vd.getVoucher().getType() == Voucher.Type.ArVoucher) {
			if (toBePaid.compareTo(BigDecimal.ZERO) < 0) {
				change = BigDecimal.ZERO;
			} else {
				change = totalPaid.subtract(current.getToBePaidAmt());
			}

		} else  {
			if (current.getTotalPrice().compareTo(BigDecimal.ZERO)<0) {

			} else  if (toBePaid.compareTo(BigDecimal.ZERO) < 0) {
				if(current.getCreditAmt().compareTo(BigDecimal.ZERO)>0){
					change = BigDecimal.ZERO;
				} else {
					change = toBePaid.abs();
				}
				toBePaid = BigDecimal.ZERO;
			}
		}
		current.setTotalPaidAmt(totalPaid);
		current.setToBePaidAmt(toBePaid);
		current.setCashChange(change);

		//member credit
		// TODO
		if (current.getMember() != null && current.getMember().getLevel() != null && current.getMember().getLevel().isCredit()) {
			BigDecimal memberBalance = current.getMember().getBalance();
			BigDecimal memberOutstanding = BigDecimal.ZERO;

			if(memberBalance.compareTo(BigDecimal.ZERO) >= 0){
				if (memberBalance.compareTo(toBePaid)> 0) {
					memberBalance = memberBalance.subtract(toBePaid).abs();
				} else {
					memberOutstanding = toBePaid.subtract(memberBalance);
				}
			} else {
				memberOutstanding = current.getToBePaidAmt();
			}
//			current.setTotalOutstanding(current.getToBePaidAmt());
			current.setTotalOutstanding(memberOutstanding);

		}
	}

	public void processAll() {
		processAllItem();
		summarize();
		processPayment();
	}

	public void saveAll() throws SQLException {
		TransactionManager.callInTransaction(DbConnector.getRemoteConnection(),()->{
			try {
				transactionDao.save(current);
				for (TransactionItem item: current.getItems()) {
					transactionItemDao.save(item);
				}
			} catch (SQLException e) {
				logger.error(e.toString(), e);
			}
			return null;
		});
	}

	public void deleteItem(TransactionItem item) throws SQLException {
		saveCanceledItem(item);

		current.getItems().remove(item);
		int i = 1;
		for (TransactionItem ti: current.getItems()) {
			ti.setItemNo(i);
			i++;
		}

		if (i==1) {
			current.setStatus(Status.NEW);
		}

		updateP2PPromoProducts();
		summarize();
		transactionItemDao.delete(item);
		promoCtrl.getPromoItems().remove(item.getProduct());
	}



	/**
	 * Check p2p promo product.<br/>
	 * If reference product not
	 * exist, remove the discount
	 */
	private void updateP2PPromoProducts(){
		for (TransactionItem item: current.getItems()) {
			try {
				PromoItem promoItem = promoCtrl.searchPromoItem(item.getProduct());
				if(promoItem != null){
					//check for p2p promo product
					if(promoItem.getReferenceProduct() != null){
						DaoManagerImpl.getPromoDao().refresh(promoItem.getPromo());

						boolean isValidP2p = false;
						for (TransactionItem other: current.getItems()) {
							if (other.getProduct().getId().equals(promoItem.getReferenceProduct().getId())) {
								isValidP2p = true;
								break;
							}
						}

						if(!isValidP2p){
							//remove discount
							BigDecimal p2pAmount = current.getP2pAmount();
							if(p2pAmount != null){
								current.setP2pAmount(
										p2pAmount.subtract(
												item.getDiscAmt().multiply(new BigDecimal(item.getQty()))));
							}
							item.setDiscPct(0f);
							item.setDiscAmt(item.getInitPrice().multiply(new BigDecimal(0)));
							item.setDiscPrice(item.getInitPrice().subtract(item.getDiscAmt()));
							item.setSubtotalPrice(item.getInitPrice().multiply(new BigDecimal(item.getQty())));
							item.setTotalDisc(item.getDiscAmt().multiply(new BigDecimal(item.getQty())));
							item.setTotalPrice(item.getSubtotalPrice().subtract(item.getTotalDisc()));
						}
					}

					promoCtrl.getPromoItems().remove(promoItem);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void saveCanceledItem(TransactionItem item) throws SQLException {
		CancelledItem cancelledItem = new CancelledItem(item);
		cancelledItem.setUser(SessionCtrl.getSession().getUser());
		cancelledItem.setCancelDate(new Date());
		cancelledItem.setMachineId(SettingCtrl.getSetting().getMachineId());
		cancelledItem.setStore(SettingCtrl.getSetting().getStore());
		cancelledItemDao.save(cancelledItem);
	}

	public void addVoucher(VoucherDetail voucherDetail) {
		current.setVoucher(voucherDetail);
		processPayment();
	}

	public BigDecimal payByCash(BigDecimal cash) {
		current.setCashAmt(cash);
		processPayment();
		return current.getCashChange();
	}

	public void payByCard(BankPayment payment) {
		if (payment.getType() == null)
			return;
		switch (payment.getType()) {
		case DEBIT:
			current.setDebit(payment);
			current.setDebitAmt(payment.getPaymentAmt().subtract(payment.getAdditionalCharge()));
			break;
		case CREDIT:
			current.setCredit(payment);
			current.setCreditAmt(payment.getPaymentAmt());
			break;
		case TRANSFER:
			current.setTransfer(payment);
			current.setTransferAmt(payment.getPaymentAmt().subtract(payment.getAdditionalCharge()));
			break;
		}
		processPayment();
	}

	public boolean isSettled () throws NoCustomerException {
		if (current.getVoucher() != null && current.getVoucher().getVoucher().getType() == Voucher.Type.ArVoucher) {
			if (current.getMember() ==null)
				throw new NoCustomerException();

			if (current.getToBePaidAmt().compareTo(BigDecimal.ZERO)>0) {
				return false;
			} else {
				return true;
			}
		} else if (current.getMember() != null && current.getMember().getLevel() != null ) {
			if (current.getMember().getLevel().isCredit()) {
				return true;
			}
		}

		if (current.getToBePaidAmt().compareTo(BigDecimal.ZERO) == 0 && current.getTotalPrice().compareTo(BigDecimal.ZERO)>=0) {
			return true;
		} else if (current.getToBePaidAmt().compareTo(BigDecimal.ZERO) <= 0 && Config.isEnableReturnCash()) {
			return true;
		}
		return false;
	}

	public List<String> getEscalationLevel() {
		List<String> escalationLevel = new ArrayList<String>();
		escalationLevel.add(Role.CASHIER);
		escalationLevel.add(Role.STORE_MANAGER);
		for (TransactionItem item : current.getItems()) {
			if (item.getQty()<0) {
				escalationLevel.remove(Role.CASHIER);
			}
		}

		return escalationLevel;
	}

	// flag untuk reprint 30/05/2016
	public void setPrintedCount()throws SQLException{
		BigDecimal printCount = new BigDecimal(1);

		if(current.getReceiptPrintedCount()== null){
			current.setReceiptPrintedCount(printCount);
		} else {
			Integer a = new Integer(current.getReceiptPrintedCount().intValue());
			a += 1;
			current.setReceiptPrintedCount(new BigDecimal(String.valueOf(a)));
		}
		transactionDao.save(current);
	}

	public void checkout() throws SQLException, UnpaidException, EmptyCartException, NoCustomerException, OverCreditLimitException {
		if (current.getItems().isEmpty()) {
			throw new EmptyCartException();
		} else if (!isSettled()) {
			throw new UnpaidException();
		}

		current.setTotalPrice(current.getTotalPrice());

		Member customer = current.getMember() ;
		BigDecimal currentCredit = BigDecimal.ZERO;
		if (customer != null) {
			if (customer.getBalance() == null) {
				customer.setBalance(new BigDecimal(0));
			}
			// TODO
			BigDecimal balance = customer.getBalance();
			setPreviousMemberBalance(balance);

// TODO ..... balance jadi double karena dihitung dua kali
			if (customer.getLevel() != null && customer.getLevel().isCredit()){

				if (balance.compareTo(BigDecimal.ZERO)> 0) {
					balance = balance.subtract(current.getTotalPrice());
					if (balance.compareTo(BigDecimal.ZERO) < 0) {
						// current.setToBePaidAmt(balance.abs());
						currentCredit = balance;
					} else {
						currentCredit = balance.add(current.getTotalOutstanding().negate());
					}
				} else {
					currentCredit = balance.add(current.getTotalOutstanding().negate());
				}



				if (customer.getCreditLimit()!=null && currentCredit.abs().compareTo(customer.getCreditLimit())>0) {
					throw new OverCreditLimitException();
				}

				// jika cust kelebihan bayar, maka sisa kembaliannya dijadikan pengurang hutang
				if (current.getCashChange().compareTo(BigDecimal.ZERO)>0){
					currentCredit = balance.add(current.getCashChange());

					BigDecimal currentOutstanding = BigDecimal.ZERO;
					BigDecimal totalPaid = BigDecimal.ZERO;
					BigDecimal currentCashChangeLeft = current.getCashChange();

					for (AccountReceivable currAr: customerCtrl.getCurrentAr(current.getMember())){
						if(currAr.getOutstanding().compareTo(BigDecimal.ZERO) > 0){
							currentOutstanding = currAr.getOutstanding();
							// outstanding di account receivable > uang kembalian
							if(currentOutstanding.compareTo(currentCashChangeLeft)> 0){
								currentOutstanding = currentOutstanding.subtract(currentCashChangeLeft);
								currAr.setOutstanding(currentOutstanding);
								currAr.setTotalPaid(currentCashChangeLeft);
								currentCashChangeLeft = BigDecimal.ZERO;
							} else {
								currentOutstanding = currentCashChangeLeft.subtract(currentOutstanding);
								if(currAr.getTotalPaid() != null)
									totalPaid = currAr.getTotalPaid();

								currAr.setTotalPaid(totalPaid.add(currAr.getOutstanding()));
								currAr.setOutstanding(BigDecimal.ZERO);
								currentCashChangeLeft = currentOutstanding;
								// sisa kembalian mau dikemanain cuy??? kalo hutang udah lunas??
							}
						}
						accountReceivableDao.save(currAr);
					}
				}
				balance = currentCredit;
			}

			current.getMember().setBalance(balance);

		}

		current.setStatus(Transaction.Status.FINISHED);

		TransactionManager.callInTransaction(DbConnector.getRemoteConnection(), new Callable<Void>() {
			public Void call() throws Exception {

				//generate voucher
				Date currentDate = new Date();
				CloseableIterator<VoucherDetail> vi = voucherDetailDao.search(new SearchParam[] {
					new SearchParam(VoucherDetail._AVAILABLE, true),
					new SearchParam(Voucher.class, Voucher._MIN_TRANSSACTION_AMT, current.getTotalPrice(), Operator.LT),
					new SearchParam(Voucher.class, Voucher._VALID_FROM, currentDate, Operator.LT),
					new SearchParam(Voucher.class, Voucher._VALID_UNTIL, currentDate, Operator.GT)
					});

				if (vi.hasNext()) {
					VoucherDetail earnedVoucher = vi.next();
					earnedVoucher.setIsAvailable(false);
					current.setEarnedVoucher(earnedVoucher);
					voucherDetailDao.save(earnedVoucher);
				}
				vi.closeQuietly();


				if (customer != null) {


					MemberLevel level = current.getMember().getLevel();

					MemberLevel nextLevel = null;
					CloseableIterator<MemberLevel> cli = customerLevelDao.search(new SearchParam[] {
							new SearchParam(MemberLevel._MIN_AMT, current.getTotalPrice(), Operator.LT),
							new SearchParam(MemberLevel._MIN_AMT, SearchParam.Order.DESC)
							});
					if (cli.hasNext()) {
						nextLevel = cli.next();
					}
					cli.closeQuietly();

					if (nextLevel != null && (level == null
							|| (level != null && level.getLevel() < nextLevel.getLevel())))
						level = nextLevel;

					// calculate cashback
					if (level != null) {
						current.getMember().setLevel(level);
						CloseableIterator<CustomerCashback> ci = customerCashbackDao
								.search(new SearchParam[] {
										new SearchParam(CustomerCashback._MEMBER_LEVEL, level.getLevel()),
										new SearchParam(CustomerCashback._TRANSACTION_AMT, current.getTotalPrice(),
												SearchParam.Operator.LT),
										new SearchParam(CustomerCashback._TRANSACTION_AMT,
												SearchParam.Order.DESC) });
						if (ci.hasNext()) {
							CustomerCashback cashback = ci.next();
							VoucherDetail voucherDetail = new VoucherDetail();
							voucherDetail.setGeneratedDate(new Date());
							voucherDetail.setCode(String.valueOf(System.currentTimeMillis()));
							voucherDetail.setIsAvailable(true);

							voucherDetailDao.save(voucherDetail);
							current.setCashback(voucherDetail);
							current.setCashbackAmt(cashback.getCashbackAmt());
						}
						ci.closeQuietly();
					}

					Integer point = current.getMember().getPoint();
					if (point == null)
						point = 0;

					point += current.getTotalPrice().divide(SettingCtrl.getSetting().getCustomerPointToAmount(), 2, RoundingMode.HALF_UP).intValue();
					current.getMember().setPoint(point);
					customerDao.save(current.getMember());
				}

				// check if credit transaction
				if  (current.getVoucher() != null) {
					current.getVoucher().setIsAvailable(false);
					current.getVoucher().setTransactionNo(current.getTransactionNo());
					voucherDetailDao.save(current.getVoucher());
					Voucher voucher = current.getVoucher().getVoucher();
					if (voucher.getType() == Type.ArVoucher) {
						saveAr();
					}
				} else if (current.getMember() != null && current.getMember().getLevel() != null && current.getMember().getLevel().isCredit()) {
					saveAr();
				}

				transactionDao.save(current);
				int printCount = 0;

				if(current.getReceiptPrintedCount() != null){
					printCount = current.getReceiptPrintedCount().intValue();
				}

				if(printCount == 0){
					for (TransactionItem item : current.getItems()) {
						if (Config.isAbc()){
							productCtrl.deductStock(null,item.getProduct(),item.getQty());
						} else {
							productCtrl.deductStock(item.getSku(),null,item.getQty());
						}

						productCtrl.deductStockPromo(item.getProduct(), item.getQty());
						transactionItemDao.save(item);
					}

					saveToMutationHistory();
				}
				return null;
			}
		});
	}

	private void saveAr() throws SQLException {
		AccountReceivable ar = new AccountReceivable();
		ar.setArNumber(String.valueOf(System.currentTimeMillis()));
		ar.setTransaction(current);
		ar.setTransactionNo(current.getTransactionNo());
		ar.setMember(current.getMember());
		ar.setBranch(SettingCtrl.getSetting().getStore());
		if (current.getMember() != null) {
			ar.setBillToName(current.getMember().getName());
			ar.setBillToEmail(current.getMember().getEmail());
			ar.setBillToAddress(current.getMember().getAddress());
			ar.setBillToPhone(current.getMember().getPhoneNo());
		}
		ar.setOutstanding(current.getTotalOutstanding());
		ar.setTotalAmt(current.getTotalOutstanding());
		accountReceivableDao.save(ar);
	}

	public void saveToMutationHistory() throws SQLException {
		MutationHistory mutationHistory = new MutationHistory();

		for (TransactionItem item : current.getItems()) {
			mutationHistory.setTransactionItem(item);

			QueryBuilder<ProductSize, Integer> productSizeQb = productSizeDao.getDao().queryBuilder();
			productSizeQb.where().eq(ProductSize._PRODUCT_ID, item.getProduct().getId());

			QueryBuilder<ProductStock, Integer> queryBuilder = productStockDao.getDao().queryBuilder();

			Where<ProductStock, Integer> w = queryBuilder.where();
			if (item.getProductSize()!=null)
				w.eq(ProductStock._PRODUCT_SIZE_ID, item.getProductSize().getId())								;
			else
				w.eq(ProductStock._WAREHOUSE_ID, SettingCtrl.getBranchId());

			queryBuilder.join(productSizeQb);

			PreparedQuery<ProductStock> pq = queryBuilder.prepare();

			List<ProductStock> list = productStockDao.getDao().query(pq);
			ProductStock stock ;
			if (!list.isEmpty()) {
				stock = list.get(0);
				mutationHistory.setProductStock(stock);
				mutationHistory.setOldQty(stock.getAvailableQty());
				mutationHistory.setMovedQty(item.getQty());
				mutationHistory.setNewQty(stock.getAvailableQty() - item.getQty());
				mutationHistory.setType(MutationHistory.Type.Sales);
				mutationHistory.setPrice(item.getProduct().getPriceModal().multiply(new BigDecimal(item.getQty())));
				mutationHistory.setDiscAmt(item.getTotalDisc());
				mutationHistoryDao.save(mutationHistory);
			}
		}
	}

	public void reset() {
		transactionNo = null;
		selectedItem = null;
		selectedReturnItem = null;
		promoCtrl.getPromoItems().clear();
		selectedStock = null;
		availableQty = 0;
		selectedQty = 1;
		current = null;
		returMode = false;
		lastItem = null;
		purchasePrice = null;
	}

	public String generateReceipt() {
		StringBuilder receipt = new StringBuilder();
		String space = ReceiptPrinterCtrl.getAdditionalSpace();

		if (current.getItems().stream().anyMatch(predicate->{
			if (predicate.getQty()<0)
				return true;
			return false;
		})) {
			receipt.append(StringUtil.center("RETUR", Config.getPrinterMaxCharPerLine()));
			receipt.append(String.format("%n%n"));
		}
		//penambahan label reprint struk --- 30/05/2016 fzr
		if (current.getReceiptPrintedCount() != null){
			String rPrint = current.getReceiptPrintedCount().toString();
			if (current.getReceiptPrintedCount().compareTo(BigDecimal.ZERO) == 1){
				receipt.append(StringUtil.center("[REPRINT STRUK #"+rPrint+"]", Config.getPrinterMaxCharPerLine()));
				receipt.append(String.format("%n%n"));
			}
		}

		receipt.append(String.format("Kasir       : %-15s%n", current.getCashier().getFirstName()));
		receipt.append(String.format("Trans. No.  : %-15s%n", current.getTransactionNo()));
		Date date = current.getTransactionDate();
		receipt.append(String.format("Trans. Date : %te %tB %tY %s%n", date, date, date, DateUtil.formatTime(date)));

		if (current.getMember() != null && current.getMember().getLevel() != null)
			receipt.append(String.format("Trans. Type : %-15s%n", "Credit"));

		receipt.append(String.format("%n"));

		if (current.getMember()!=null) {
			receipt.append(String.format("Member : %-30s%n", current.getMember().getName()));
//			if (current.getCustomer().getLevel()!=null)
//			receipt.append(String.format("Level  : %-30s%n", current.getCustomer().getLevel().getDesc()));
		}
		receipt.append(String.format(receiptCtrl.generateLine()));

		for (TransactionItem item : current.getItems()) {
			StringBuilder itemName = new StringBuilder();
			if (Config.isAbc())
			if (item.getProduct().getBarcode() != null)
				itemName.append(item.getProduct().getBarcode() + " ");

			if (Config.isAbc())
			if (item.getProduct().getBrand() != null && item.getProduct().getBrand().getName() != null)
				itemName.append(item.getProduct().getBrand().getName() + " ");

			if (item.getProduct().getShortName() != null)
				itemName.append(item.getProduct().getShortName() + " ");
			else
				if (item.getProduct().getArticle() != null)
				itemName.append(item.getProduct().getArticle() + " ");

			if (item.getProduct().getColour1() != null && item.getProduct().getColour1().getName() != null)
				itemName.append(item.getProduct().getColour1().getName() + " ");

			if (item.getProductStock().getProductSize() != null)
				receipt.append(String.format("%-29s SIZE %5s%n", itemName.toString(), item.getProductStock().getProductSize().getSize().getSizeNumber()));
			else
				receipt.append(String.format("%-40s%n", itemName.toString()));


			if (Config.isEnableDisc())
				receipt.append(String.format(space+"   Rp. %-11s   x  %3d %12s%n", CurrencyUtil.format(item.getInitPrice(), false), item.getQty(), CurrencyUtil.format(item.getSubtotalPrice(), false)));
			else
				receipt.append(String.format(space+"   Rp. %-11s   x  %3d %12s%n", CurrencyUtil.format(item.getDiscPrice(), false), item.getQty(), CurrencyUtil.format(item.getTotalPrice(), false)));

			if (Config.isEnableDisc() && item.getDiscPct().compareTo(0f)!=0)
				receipt.append(String.format(space+"   %-14s      %3.0f%% %12s%n", "DISC. %", item.getDiscPct(), CurrencyUtil.format(item.getTotalDisc().negate(), false)));
		}

		receipt.append(String.format(receiptCtrl.generateSmallLine()));

		if (Config.isEnableDisc() && current.getTotalDisc().compareTo(BigDecimal.ZERO) > 0)
			receipt.append(String.format(space+"%26s  %12s%n", "Subtotal", CurrencyUtil.format(current.getSubTotalAmt(), false)));

		if (!Config.isAbc() && current.getTotalPpn().compareTo(BigDecimal.ZERO)>0)
			receipt.append(String.format(space+"%26s  %12s%n", "DPP", CurrencyUtil.format(current.getTotalPrice().subtract(current.getTotalPpn()), false)));
		if (Config.isEnablePpn())
			receipt.append(String.format(space+"%26s  %12s%n", "PPN", CurrencyUtil.format(current.getTotalPpn(), false)));
		if (Config.isEnableDisc() && current.getTotalDisc() != null && current.getTotalDisc().compareTo(BigDecimal.ZERO) > 0) {
			receipt.append(String.format(space+"%26s  %12s%n", "Total Disc.", CurrencyUtil.format(current.getTotalDisc().negate(), false)));
		}

		receipt.append(String.format(space+"%26s  %12s%n", "TOTAL", CurrencyUtil.format(current.getTotalPrice(), false)));

		if (current.getVoucher() != null) {
			Voucher voucher = current.getVoucher().getVoucher();
			if (voucher != null) {
				switch(voucher.getType()) {
				case DiscountVoucher:
					receipt.append(String.format(space+"%26s  %12s%n", "Voucher", CurrencyUtil.format(current.getVoucherAmt(), false)));
					break;
				case ArVoucher:
					receipt.append(String.format(space+"%26s  %12s%n", "Credit Voucher", CurrencyUtil.format(current.getVoucherAmt(), false)));
					break;
				default:
					break;
				}
			}
		}

		if (current.getDebit() != null && current.getDebitAmt().compareTo(BigDecimal.ZERO)>0)
			receipt.append(String.format(space+"%26s  %12s%n", "Debit", CurrencyUtil.format(current.getDebitAmt(), false)));


		if (current.getCredit() != null && current.getCreditAmt().compareTo(BigDecimal.ZERO)>0){
			receipt.append(String.format(space+"%26s  %12s%n", "Credit Charge", CurrencyUtil.format(current.getCreditAmt().subtract(current.getTotalPrice()), false)));
			receipt.append(String.format(space+"%26s  %12s%n", "Credit", CurrencyUtil.format(current.getCreditAmt(), false)));
		}
		if (current.getTransfer() != null && current.getTransferAmt().compareTo(BigDecimal.ZERO)>0)
			receipt.append(String.format(space+"%26s  %12s%n", "Transfer", CurrencyUtil.format(current.getTransferAmt(), false)));

		if (current.getCashAmt().compareTo(BigDecimal.ZERO) > 0)
			receipt.append(String.format(space+"%26s  %12s%n", "Cash", CurrencyUtil.format(current.getCashAmt(), false)));

		if (current.getCashChange() != null && current.getCashChange().compareTo(BigDecimal.ZERO)>0 &&
				current.getMember() == null)
			receipt.append(String.format(space+"%26s  %12s%n", "Change", CurrencyUtil.format(current.getCashChange(), false)));

		receipt.append(String.format(receiptCtrl.generateSmallLine()));
		if (current.getToBePaidAmt().compareTo(BigDecimal.ZERO) > 0 )
			receipt.append(String.format(space+"%26s  %12s%n", "Outstanding", CurrencyUtil.format(current.getTotalOutstanding(), false)));

//		receipt.append(String.format("%n"));
//		receipt.append(String.format(space+"%26s%n", "Credit Limit Balance:"));
		if (current.getMember() != null && current.getMember().getLevel() != null) {
//			receipt.append(String.format(space+"%26s  %12s%n", "Previous Balance", CurrencyUtil.format(current.getMember().getBalance().subtract(current.getTotalOutstanding().negate()), false)));
			receipt.append(String.format(space+"%26s  %12s%n", "Previous Balance", CurrencyUtil.format(getPreviousMemberBalance(), false)));
			receipt.append(String.format(space+"%26s  %12s%n", "Current Balance", CurrencyUtil.format(current.getMember().getBalance(), false)));
		}

		receipt.append(String.format(receiptCtrl.generateSmallLine()));
		receipt.append(String.format(space+"%26s  %12d%n", "Total Qty", current.getTotalQty()));
		receipt.append(String.format(space+"%26s  %12d%n", "Total Item", this.getCurrentItemCount(current.getId())));
		receipt.append(String.format(receiptCtrl.generateLine()));


		receipt.append(String.format(Config.getReceiptFooter()));

		if (current.getEarnedVoucher() != null) {
			receipt.append(String.format("%n%n%n"));
			receipt.append(String.format(space+"%-18s %-20s%n", "Voucher Disc     : ", current.getEarnedVoucher().getCode()));
			Voucher voucher = current.getEarnedVoucher().getVoucher();
			receipt.append(String.format(space+"%-18s %-20s%n", "Valid From/To    :", DateUtil.format(voucher.getValidFrom())+"/"+DateUtil.format(voucher.getValidUntil())));
			if (voucher.getDiscountPct() != null)
				receipt.append(String.format(space+"%-18s %-20s%n", "Disc             : ", voucher.getDiscountPct()+"%"));
			else if (voucher.getDiscountAmt() != null)
				receipt.append(String.format(space+"%-18s %-20s%n", "Disc             : ", CurrencyUtil.format(voucher.getDiscountAmt())));
			receipt.append(String.format(space+"%-18s %-20s%n", "Term & Condition : ", voucher.getTerm()));
		}

		if (current.getCashback() != null) {
			receipt.append(String.format("%n%n%n"));
			receipt.append(String.format(space+"%-18s %-20s%n", "Voucher Cashback : ", current.getCashback().getCode()));
			Voucher voucher = current.getCashback().getVoucher();
			receipt.append(String.format(space+"%-18s %-20s%n", "Valid            : ", DateUtil.format(voucher.getValidFrom())+" to "+DateUtil.format(voucher.getValidUntil())));
			receipt.append(String.format(space+"%-18s %-20s%n", "Amount           : ", CurrencyUtil.format(current.getCashback().getRedeemAmt())));
			receipt.append(String.format(space+"%-18s %-20s%n", "Term & Condition : ", voucher.getTerm()));
		}

		if (!Config.isEnableAdvancedPrinting())
			receipt.append(String.format("%n%n%n%n%n%n%n%n."));
		else
			receipt.append(String.format("%n."));
		if (Config.isEnablePrinterTypeLX310())
			receipt.append(String.format("%n%n%n%n%n"));

		System.out.println(receipt.toString());
		return receipt.toString();
	}

	/**
	 * Fashion Only
	 * @return
	 * @throws SQLException
	 */
	public String generateEoDReportDetail() throws SQLException {
		StringBuilder receipt = new StringBuilder();
		receipt.append(receiptCtrl.generateHeader());

		receipt.append(String.format("Date       : %s%n", DateUtil.format(SessionCtrl.getSession().getStart())));
		receipt.append(String.format("Branch     : %s%n", current.getTransactionNo()));
		receipt.append(String.format("Machine Id : %s%n", current.getTransactionNo()));
		User userLogin = SessionCtrl.getSession().getUser();
		StringBuilder name = new StringBuilder();
		if (userLogin.getFirstName() != null)
			name.append(userLogin.getFirstName());
		if (userLogin.getLastName() != null)
			name.append(userLogin.getLastName());
		receipt.append(String.format("Cashier    : %s%n", name.toString()));
		receipt.append(String.format("%n"));

		StringBuilder query = new StringBuilder();
		query.append("SELECT p.article, p.colour_code, sz.size_number, SUM(sd.quantity) ")
			 .append("FROM sales_details sd ")
			 .append("INNER JOIN sales s ON s.id = sd.sale_id ")
			 .append("INNER JOIN products p ON p.id = sd.product_id ")
			 .append("INNER JOIN product_sizes ps ON ps.id = sd.product_size_id ")
			 .append("INNER JOIN size_details sz ON s.id = ps.size_detail_id ")
			 .append(String.format("WHERE s.transaction_date BETWEEN timestamp '%s' AND timestamp '%s'",
					 DateUtil.format(SessionCtrl.getSession().getStart(), "yyyy-M-dd HH:mm"),
					 DateUtil.format(SessionCtrl.getSession().getEnd(), "yyyy-M-dd HH:mm")))
			 .append("GROUP BY p.article, p.colour_code, sz.size_number ");

		Dao<TransactionItem, Integer> dao = transactionItemDao.getDao();
		GenericRawResults<Object[]> rawResults = dao.queryRaw(query.toString(),
			    new DataType[] { DataType.STRING, DataType.STRING, DataType.STRING, DataType.INTEGER});

		long total = 0;
		for (Object[] resultArray : rawResults) {
			receipt.append(String.format("%-15s %-15s %2s  %4d%n%n",
				  resultArray[0],  resultArray[1], resultArray[2], resultArray[3]));
			total += (int)resultArray[3];
		}
		rawResults.close();

		receipt.append(String.format("%n%n"));

		receipt.append(String.format("%28s  %10s%n%n", "Total Qty", total));

		receipt.append(Config.getEodFooter());

		receipt.append(String.format("%n%n%n%n%n%n%n."));

		System.out.println(receipt.toString());
		return receipt.toString();
	}

	public int countProduct (Product product) {
		IntSummaryStatistics  summary = current.getItems().stream()
							.filter(s->
								{return s.getProduct().getId() == product.getId();})
							.collect(Collectors.summarizingInt(v->v.getQty()));
		return (int) summary.getSum();
	}

	public <T> T checkLastItemForPromo() throws SQLException {
		BigDecimal minAmtPromo, qtyItem, minQtyPromo;

		PromoPrizeItem prizeItem = promoCtrl.getMatchingPrize(lastItem.getProduct());
		if (prizeItem != null) {
			applyPromo(prizeItem);
			return (T) prizeItem;
		}

//		PromoItem promoItem = promoCtrl.searchPromoItem(lastItem.getProduct(), countProduct(lastItem.getProduct()), current.getTotalPrice());
		PromoItem promoItem = promoCtrl.searchPromoItem(lastItem.getProduct());
		if (promoItem != null) {
			DaoManagerImpl.getPromoDao().refresh(promoItem.getPromo());
			qtyItem = new BigDecimal(countProduct(lastItem.getProduct()));
			minQtyPromo = promoItem.getPromo().getMinQty();
			minAmtPromo = promoItem.getPromo().getDiscAmt();

			if(promoItem.getReferenceProduct() != null){
				//p2p promo, check for reference product in product list
				for (TransactionItem item: current.getItems()) {
					if (item.getProduct().getId().equals(promoItem.getReferenceProduct().getId())) {
						System.out.println("p2p");
						applyPromo(promoItem);
						return (T) promoItem;
					}
				}
			} else{
				//non p2p
				if(qtyItem.compareTo(minQtyPromo) > -1 ){
					System.out.println("Current Promo: "+ promoItem.getPromo().getName());
					applyPromo(promoItem);
					return (T) promoItem;
				} else {
					if (current.getTotalPrice().compareTo(promoItem.getPromo().getMinTransactionAmt()) > -1){
						System.out.println("Current Promo: "+ promoItem.getPromo().getName());
					}else {
						System.out.println("No Promo");
					}
				}
			}

//			applyPromo(prizeItem);
//			return (T) promoItem;
		} else{
			//check p2p promo by reference_product_id
			promoItem = promoCtrl.searchPromoItemByReferenceProduct(lastItem.getProduct());
			if(promoItem != null){
				DaoManagerImpl.getPromoDao().refresh(promoItem.getPromo());
				qtyItem = new BigDecimal(countProduct(lastItem.getProduct()));
				minQtyPromo = promoItem.getPromo().getMinQty();
				minAmtPromo = promoItem.getPromo().getDiscAmt();

				//check is promo item is exist on product list
				for (TransactionItem item: current.getItems()) {
					if (item.getProduct().getId().equals(promoItem.getProduct().getId())) {
						System.out.println("p2p");
						applyPromo(promoItem);
						return (T) promoItem;
					}
				}
			}
		}
		return null;
	}

	public <T> void applyPromo(T promoItemOrPrize) {
		if (promoItemOrPrize instanceof PromoPrizeItem) {
			TransactionItem prizeItem = null;
			if (lastItem.getQty() > 1) {
				prizeItem = new TransactionItem();
				prizeItem.setItemNo(current.getItems().size()+1);
				prizeItem.setProduct(lastItem.getProduct());
				prizeItem.setProductSize(lastItem.getProductSize());
				prizeItem.setProductStock(lastItem.getProductStock());
				prizeItem.setInitPrice(lastItem.getInitPrice());
				prizeItem.setQty(1);
				current.getItems().add(prizeItem);
				lastItem.setQty(lastItem.getQty()-1);
			} else {
				prizeItem = lastItem;
			}
			setAsPrize(prizeItem);
			processPayment();
			promoCtrl.getPromoItems().remove(((PromoPrizeItem) promoItemOrPrize).getPromoItem());
		} else if (promoItemOrPrize instanceof PromoItem){
			Promo promo = ((PromoItem)promoItemOrPrize).getPromo();
			Float discPct = promo.getDiscPct();
			BigDecimal discAmt = promo.getDiscAmt();
			if(((PromoItem)promoItemOrPrize).getReferenceProduct() == null){
				if (discPct != null) {
					lastItem.setDiscPct(discPct);
					lastItem.setDiscAmt(lastItem.getInitPrice().multiply(new BigDecimal(discPct)));
					lastItem.setDiscPrice(lastItem.getInitPrice().subtract(lastItem.getDiscAmt()));
					lastItem.setSubtotalPrice(lastItem.getInitPrice().multiply(new BigDecimal(lastItem.getQty())));
					lastItem.setTotalDisc(lastItem.getDiscAmt().multiply(new BigDecimal(lastItem.getQty())));
					lastItem.setTotalPrice(lastItem.getSubtotalPrice().subtract(lastItem.getTotalDisc()));
				} else if (discAmt != null) {
					lastItem.setDiscAmt(discAmt);
					lastItem.setDiscPct(lastItem.getDiscAmt().divide(lastItem.getInitPrice(), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).floatValue());
					lastItem.setDiscPrice(lastItem.getInitPrice().subtract(lastItem.getDiscAmt()));
					lastItem.setSubtotalPrice(lastItem.getInitPrice().multiply(new BigDecimal(lastItem.getQty())));
					lastItem.setTotalDisc(lastItem.getDiscAmt().multiply(new BigDecimal(lastItem.getQty())));
					lastItem.setTotalPrice(lastItem.getSubtotalPrice().subtract(lastItem.getTotalDisc()));
				}
			} else{
				//p2p
				for (TransactionItem item: current.getItems()) {
					if (item.getProduct().getId().equals(((PromoItem)promoItemOrPrize).getProduct().getId())) {
						if (discPct != null) {
							item.setDiscPct(discPct);
							item.setDiscAmt(item.getInitPrice().multiply(new BigDecimal(discPct)));
							item.setDiscPrice(item.getInitPrice().subtract(item.getDiscAmt()));
							item.setSubtotalPrice(item.getInitPrice().multiply(new BigDecimal(item.getQty())));
							item.setTotalDisc(item.getDiscAmt().multiply(new BigDecimal(item.getQty())));
							item.setTotalPrice(item.getSubtotalPrice().subtract(item.getTotalDisc()));
						} else if (discAmt != null) {
							item.setDiscAmt(discAmt);
							item.setDiscPct(item.getDiscAmt().divide(item.getInitPrice(), 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).floatValue());
							item.setDiscPrice(item.getInitPrice().subtract(item.getDiscAmt()));
							item.setSubtotalPrice(item.getInitPrice().multiply(new BigDecimal(item.getQty())));
							item.setTotalDisc(item.getDiscAmt().multiply(new BigDecimal(item.getQty())));
							item.setTotalPrice(item.getSubtotalPrice().subtract(item.getTotalDisc()));
						}

						//store total p2p amount on current transaction
						BigDecimal p2pAmount = current.getP2pAmount();
						if(p2pAmount == null){
							p2pAmount = new BigDecimal(0);
						}
						p2pAmount = p2pAmount.add(
								item.getDiscAmt().multiply(new BigDecimal(item.getQty())));
						current.setP2pAmount(p2pAmount);
						break;
					}
				}
			}

			promo.getPrizeItems().forEach(prizeItem->{
				current.getItems().forEach(transactionItem->{
					if (transactionItem.getProduct().getId() == prizeItem.getProduct().getId()) {
						setAsPrize(transactionItem);
					}
				});
			});
			processPayment();
			promoCtrl.getPromoItems().remove((PromoItem) promoItemOrPrize);
		}
		summarize();
	}

	public void setAsPrize(TransactionItem item) {
		item.setDiscAmt(new BigDecimal(100));
		item.setDiscPct(100f);
		item.setDiscPrice(lastItem.getInitPrice());
		item.setSubtotalPrice(lastItem.getSubtotalPrice());
		item.setTotalDisc(lastItem.getTotalDisc());
		item.setTotalPrice(BigDecimal.ZERO);
	}

	public boolean isLastItemIsPrize() {
		if (promoCtrl.getMatchingPrize(lastItem.getProduct()) != null)
			return true;
		return false;
	}

	public Transaction getCurrent() {
		return current;
	}

	public boolean isOnProgress() {
		if (current == null)
			return false;
		return (current.getStatus() == Transaction.Status.OPEN) ? true : false;
	}

	public List<Ads> getListIklan() throws SQLException{
		Date currentDate = new Date();
		QueryBuilder<Ads, Integer> qb = adsDao.getDao().queryBuilder();
		qb.where()
		.le(Ads._VALID_FROM, DateUtil.format(currentDate, "yyyy-M-dd"))
		.and()
		.ge(Ads._VALID_UNTIL, DateUtil.format(currentDate, "yyyy-M-dd"))
		.and()
		.eq(Ads._OFFICE_ID,SettingCtrl.getSetting().getStoreId());
		return adsDao.getDao().query(qb.prepare());
	}

	// Generated Code
	public boolean isReturMode() {
		return returMode;
	}

	public void setReturMode(boolean returMode) {
		this.returMode = returMode;
	}

	public Product getSelectedItem() {
		return selectedItem;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public void setSelectedItem(Product selectedProduct) {
		this.selectedItem = selectedProduct;
	}

	public ProductStock getSelectedStock() {
		return selectedStock;
	}

	public int getSelectedQty() {
		return selectedQty;
	}

	public PromoCtrl getPromoCtrl() {
		return promoCtrl;
	}

	public void setPromoCtrl(PromoCtrl promoCtrl) {
		this.promoCtrl = promoCtrl;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public BigDecimal getPreviousMemberBalance() {
		return previousMemberBalance;
	}

	public void setPreviousMemberBalance(BigDecimal previousMemberBalance) {
		this.previousMemberBalance = previousMemberBalance;
	}

	public ProductCtrl getProductCtrl() {
		return productCtrl;
	}

	public void setProductCtrl(ProductCtrl productCtrl) {
		this.productCtrl = productCtrl;
	}

	public ReceiptPrinterCtrl getReceiptCtrl() {
		return receiptCtrl;
	}

	public void setReceiptCtrl(ReceiptPrinterCtrl receiptCtrl) {
		this.receiptCtrl = receiptCtrl;
	}

	public TransactionItem getSelectedReturnItem() {
		return selectedReturnItem;
	}

	public void setSelectedReturnItem(TransactionItem selectedReturnItem) {
		this.selectedReturnItem = selectedReturnItem;
	}

	public int getAvailableQty() {
		return availableQty;
	}


	public RDao<Sku> getSkuDao() {
		return skuDao;
	}

	public void setSkuDao(RDao<Sku> skuDao) {
		this.skuDao = skuDao;
	}

	public RDao<Ads> getAdsDao() {
		return adsDao;
	}

	public void setAdsDao(RDao<Ads> adsDao) {
		this.adsDao = adsDao;
	}

	public boolean isPromoApplied() {
		return promoApplied;
	}

	public void setPromoApplied(boolean promoApplied) {
		this.promoApplied = promoApplied;
	}

	public boolean isReprintMode() {
		return reprintMode;
	}

	public void setReprintMode(boolean reprintMode) {
		this.reprintMode = reprintMode;
	}

}
