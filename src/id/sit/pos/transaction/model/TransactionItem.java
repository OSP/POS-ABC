package id.sit.pos.transaction.model;

import java.math.BigDecimal;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.config.Config;
import id.sit.pos.master.model.Product;
import id.sit.pos.master.model.ProductSize;
import id.sit.pos.master.model.ProductStock;
import id.sit.pos.master.model.Sku;
import id.sit.pos.util.CurrencyUtil;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@DatabaseTable(tableName = "sales_details")
public class TransactionItem extends RModel {

	public static final String _TRANSACTION_ID = "sale_id";
	public static final String _PRODUCT_ID = "product_id";
	public static final String _SKU_ID = "sku_id";
	public static final String _PRODUCT_SIZE_ID = "product_size_id";
	public static final String _PRODUCT_STOCK_ID = "product_detail_id";
	public static final String _CAPITAL_PRICE = "capital_price";
	public static final String _INITIAL_PRICE = "price";
	public static final String _QTY = "quantity";
	public static final String _SUBTOTAL_PRICE = "subtotal_price";
	public static final String _DISC_PCT = "discount";
	public static final String _DISC_AMT = "discount_amt";
	public static final String _DISC_PRICE = "price_after_discount";
	public static final String _TOTAL_DISC_AMT = "total_discount_amt";
	public static final String _TOTAL_PRICE = "total_price";
	public static final String _IS_SPECIAL_DISC = "is_special_discount";
	public static final String _PPN = "ppn";

	@DatabaseField(columnName = _TRANSACTION_ID, foreign = true)
	private Transaction transaction;

	@DatabaseField(columnName = _PRODUCT_ID, foreign = true)
	private Product product;

	@DatabaseField(columnName = _SKU_ID, foreign = true)
	private Sku sku;

	@DatabaseField(columnName = _PRODUCT_SIZE_ID, foreign = true)
	private ProductSize productSize;

	@DatabaseField(columnName = _PRODUCT_STOCK_ID, foreign = true)
	private ProductStock productStock;

	@DatabaseField(columnName = _CAPITAL_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal capitalPrice = new BigDecimal(0);

	@DatabaseField(columnName = _INITIAL_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal initialPrice = new BigDecimal(0);

	@DatabaseField(columnName = _QTY)
	private int qty;

	@DatabaseField(columnName = _SUBTOTAL_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal subtotalPrice = new BigDecimal(0);

	@DatabaseField(columnName = _DISC_PCT)
	private Float discountPct;

	@DatabaseField(columnName = _DISC_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal discountAmt = new BigDecimal(0);

	@DatabaseField(columnName = _DISC_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal discountedPrice = new BigDecimal(0);

	@DatabaseField(columnName = _TOTAL_DISC_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalDisc = new BigDecimal(0);

	@DatabaseField(columnName = _TOTAL_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalPrice = new BigDecimal(0);

	@DatabaseField(columnName = _IS_SPECIAL_DISC)
	private boolean isSpecialDisc = false;

	@DatabaseField(columnName = _PPN, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalPpn = new BigDecimal(0);


	private IntegerProperty itemNoProperty;

	private StringProperty barcodeProperty;

	private StringProperty articleProperty;

	private StringProperty productNameProperty;

	private StringProperty sizeProperty;

	private StringProperty brandProperty;

	private StringProperty priceProperty;

	private IntegerProperty qtyProperty;

	private FloatProperty discPctProperty;

	private StringProperty discAmtProperty;

	private StringProperty discPriceProperty;

	private StringProperty totalProperty;



	public TransactionItem() {
		itemNoProperty = new SimpleIntegerProperty();
		barcodeProperty = new SimpleStringProperty();
		articleProperty = new SimpleStringProperty();
		productNameProperty = new SimpleStringProperty();
		sizeProperty = new SimpleStringProperty();
		brandProperty = new SimpleStringProperty();
		priceProperty = new SimpleStringProperty();
		qtyProperty = new SimpleIntegerProperty();
		discPctProperty = new SimpleFloatProperty();
		discAmtProperty = new SimpleStringProperty();
		discPriceProperty = new SimpleStringProperty();
		totalProperty = new SimpleStringProperty();
	}

	public Integer getItemNo() {
		return itemNoProperty.get();
	}

	public void setItemNo(Integer itemNo) {
		itemNoProperty.set(itemNo);
	}

	public IntegerProperty itemNoProperty() {
		return itemNoProperty;
	}

	public StringProperty barcodeProperty() {
		if (Config.isAbc()){
			barcodeProperty.set(getProduct().getBarcode());
		} else {
			if (getSku() != null)
				barcodeProperty.set(getSku().getBarcode());
			else
				barcodeProperty.set("");
		}
		return barcodeProperty;
	}

	public StringProperty articleProperty() {
		articleProperty.set(getProduct().getArticle());
		return articleProperty;
	}

	public StringProperty productNameProperty() {
		productNameProperty.set(getProduct().getShortName());
		return productNameProperty;
	}

	public StringProperty sizeProperty() {
		if(getProductSize()!=null){
			sizeProperty.set(getProductSize().getSize().getSizeNumber());
		}else{
			sizeProperty.set("0");
		}
		return sizeProperty;
	}

	public StringProperty brandProperty() {
		brandProperty.set(getProduct().getBrand().getName());
		return brandProperty;
	}

	public StringProperty priceProperty() {
		priceProperty.set(CurrencyUtil.format(getInitPrice(), false));
		return priceProperty;
	}

	public IntegerProperty qtyProperty() {
		qtyProperty.set(getQty());
		return qtyProperty;
	}

	public FloatProperty discPctProperty() {
		if (getDiscPct() != null)
			discPctProperty.set(getDiscPct());
		else
			discPctProperty.set(0);
		return discPctProperty;
	}

	public StringProperty discAmtProperty() {
		discAmtProperty.set(CurrencyUtil.format(getDiscAmt(), false));
		return discAmtProperty;
	}

	public StringProperty discPriceProperty() {
		discPriceProperty.set(CurrencyUtil.format(getDiscPrice(), false));
		return discPriceProperty;
	}

	public StringProperty totalProperty() {
		totalProperty.set(CurrencyUtil.format(getTotalPrice(), false));
		return totalProperty;
	}


	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Sku getSku() {
		return sku;
	}

	public void setSku(Sku sku) {
		this.sku = sku;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ProductSize getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSize productStock) {
		this.productSize = productStock;
	}

	public ProductStock getProductStock() {
		return productStock;
	}

	public void setProductStock(ProductStock productStock) {
		this.productStock = productStock;
	}

	public BigDecimal getCapitalPrice() {
		return capitalPrice;
	}

	public void setCapitalPrice(BigDecimal capitalPrice) {
		this.capitalPrice = capitalPrice;
	}

	public BigDecimal getInitPrice() {
		return initialPrice;
	}

	public void setInitPrice(BigDecimal initialPrice) {
		this.initialPrice = initialPrice;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int quantity) {
		qtyProperty();
		this.qty = quantity;
	}

	public BigDecimal getSubtotalPrice() {
		return subtotalPrice;
	}

	public void setSubtotalPrice(BigDecimal subtotalPrice) {
		this.subtotalPrice = subtotalPrice;
	}

	public Float getDiscPct() {
		return discountPct;
	}

	public void setDiscPct(Float discountPct) {
		discPctProperty();
		this.discountPct = discountPct;
	}

	public BigDecimal getDiscAmt() {
		return discountAmt;
	}

	public void setDiscAmt(BigDecimal discountAmt) {
		discAmtProperty();
		this.discountAmt = discountAmt;
	}

	public BigDecimal getTotalDisc() {
		return totalDisc;
	}

	public BigDecimal getDiscPrice() {
		return discountedPrice;
	}

	public void setDiscPrice(BigDecimal discountedPrice) {
		this.discountedPrice = discountedPrice;
	}

	public void setTotalDisc(BigDecimal totalDiscountAmt) {
		this.totalDisc = totalDiscountAmt;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		totalProperty();
		this.totalPrice = totalPrice;
	}

	public boolean isSpecialDisc() {
		return isSpecialDisc;
	}

	public void setSpecialDisc(boolean isSpecialDisc) {
		this.isSpecialDisc = isSpecialDisc;
	}

	public BigDecimal getTotalPpn() {
		return totalPpn;
	}

	public void setTotalPpn(BigDecimal totalPpn) {
		this.totalPpn = totalPpn;
	}
}
