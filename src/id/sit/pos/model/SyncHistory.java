package id.sit.pos.model;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.db.DaoManagerImpl;

@DatabaseTable(tableName = "SyncHistory")
public class SyncHistory extends RModel {
	public static final String _SYNC_DATE = "syncDate";

	public enum Status {
		SUCCESS_ALL, FAIL_PARTIAL, FAIL_ALL
	}

	@DatabaseField
	private Date syncDate;

	@DatabaseField
	private Status status;

	public SyncHistory(){

	}

	public static SyncHistory getLatest() throws SQLException{
		QueryBuilder<SyncHistory, Integer> queryBuilder = DaoManagerImpl.getSyncHistoryDao().queryBuilder();
		queryBuilder.limit(1L)
					.orderBy(_SYNC_DATE, false);
		List<SyncHistory> syncHistories = DaoManagerImpl.getSyncHistoryDao().query(queryBuilder.prepare());
		if (syncHistories!=null && !syncHistories.isEmpty()) {
			return syncHistories.get(0);
		}
		return null;
	}

	public Date getSyncDate() {
		return syncDate;
	}

	public void setSyncDate(Date syncDate) {
		this.syncDate = syncDate;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status successAll) {
		this.status = successAll;
	}

}
