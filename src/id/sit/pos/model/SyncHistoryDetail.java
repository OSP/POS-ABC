package id.sit.pos.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName="SyncHistoryDetail")
public class SyncHistoryDetail extends RModel {
	public enum Status {
		SUCCESS, FAIL
	}
	@DatabaseField(columnName = "syncHistoryId", foreign = true)
	private SyncHistory syncHistory;

	@DatabaseField
	private String modelName;

	private Status status;

	public SyncHistoryDetail(){

	}

	public SyncHistory getSyncHistory() {
		return syncHistory;
	}

	public void setSyncHistory(SyncHistory syncHistory) {
		this.syncHistory = syncHistory;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
