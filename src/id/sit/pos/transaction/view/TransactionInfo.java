package id.sit.pos.transaction.view;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.Date;


import com.j256.ormlite.stmt.QueryBuilder;

import id.r.engine.view.RUi;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.transaction.model.TransactionItem;
import id.sit.pos.util.DateUtil;
import id.sit.pos.util.EmbeddedMediaPlayer;
import id.sit.pos.util.MediaControl;
import javafx.animation.FadeTransition;
import javafx.animation.PauseTransition;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import id.sit.pos.master.model.Ads;

public class TransactionInfo extends RUi {

	private static final String _ADS_TYPE_IMAGE = "image";
	private static final String _ADS_TYPE_VIDEO = "video";

	@FXML
	private Label customerInfo;

	@FXML
	private Label customerLevel;

	@FXML
	private Label customerPoint;

	@FXML
	private ImageView productImage;

	@FXML
	private TableView<TransactionItem> table;

	@FXML
	private TableColumn<TransactionItem, Integer> itemNoClm;

	@FXML
	private TableColumn<TransactionItem, String> barcodeClm;

	@FXML
	private TableColumn<TransactionItem, String> articleClm;

	@FXML
	private TableColumn<TransactionItem, String> productName;

	@FXML
	private TableColumn<TransactionItem, String> sizeClm;

	@FXML
	private TableColumn<TransactionItem, BigDecimal> priceClm;

	@FXML
	private TableColumn<TransactionItem, Integer> qtyClm;

	@FXML
	private TableColumn<TransactionItem, Float> discPctClm;

	@FXML
	private TableColumn<TransactionItem, BigDecimal> discAmtClm;

	@FXML
	private TableColumn<TransactionItem, BigDecimal> totalClm;

	@FXML
	private Label subtotal;

	@FXML
	private Label totalDisc;

	@FXML
	private Label labelTotalDisc;

	@FXML
	private Label totalPrice;

	@FXML
	private Label voucherAmt;

	@FXML
	private Label totalPriceAfterVoucher;

	@FXML
	private Label cash;

	@FXML
	private Label change;

	@FXML
	private Label debitAmt;

	@FXML
	private Label creditCardAmt;

	@FXML
	private Label transferLbl;

	@FXML
	private Label transferAmt;

	@FXML
	private ProgressBar progressBar;

	@FXML
	private StackPane slideShowStackPane;

	@FXML
	private StackPane videoStackPane;

	private ObservableList<TransactionItem> tableData;

	public Service<Void> videoSliderService;
	public Service<Void> imageSliderService;

//	Node[] slides;
	Node[] videoSlides;
	Node[] imageSlides;
	MediaControl mc;

	String adsTypeImage = "image";

	public TransactionInfo() {
		imageSliderService = new Service<Void>(){
			@Override
			protected Task<Void> createTask(){
				return new Task<Void>(){
					@Override
					protected Void call() {
						try {
							showImageSlide();
						} catch (SQLException e) {
							e.printStackTrace();
						}
						return null;
					}
				};
			}
		};

		imageSliderService.setOnSucceeded(t -> {
			startImageAds();
		});

		videoSliderService = new Service<Void>() {
			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() {
						/*
						final DoubleProperty width = mc.fitWidthProperty();
						final DoubleProperty height = mc.fitHeightProperty();

						width.bind(Bindings.selectDouble(slideShowStackPane.parentProperty(),"width"));
						height.bind(Bindings.selectDouble(slideShowStackPane.parentProperty(),"height"));
						*/
						try {
							showVideoSlide();
							mc.setFitHeight(300);
							mc.setFitWidth(400);
							mc.setPreserveRatio(false);
						} catch (SQLException e) {
							e.printStackTrace();
						}
						return null;
					}
				};
			}
		};

		videoSliderService.setOnSucceeded(t -> {
			startVideoAds();
		});
	}

	@Deprecated
	public void start() {
		FadeTransition fadeIn[] = new FadeTransition[4];
		PauseTransition stayOn[] = new PauseTransition[4];
		FadeTransition fadeOut[] = new FadeTransition[4];
		int i = 0;
		for (Node slide : imageSlides) {
			final int idx = i;

			fadeIn[idx] = getFadeTransition(slide, 0.0, 1.0, 2000);

			Duration duration = Duration.millis(2000);
			if (slide == mc) { // harus diganti dengan isVideo
				duration = Duration.INDEFINITE;
			}

			stayOn[idx] = new PauseTransition(duration);
			fadeIn[idx].setOnFinished(value -> {
				stayOn[idx].play();
				if (slide == mc) { // harus diganti dengan isVideo
					mc.getMediaPlayer().setStartTime(Duration.ZERO);
					mc.getMediaPlayer().seek(Duration.ZERO);
					mc.getMediaPlayer().play();
				}
			});

			fadeOut[idx] = getFadeTransition(slide, 1.0, 0.0, 2000);
			if (slide == mc) { // harus diganti dengan isVideo
				mc.getMediaPlayer().setOnEndOfMedia(new Runnable() {

					@Override
					public void run() {
						stayOn[idx].stop();
						fadeOut[idx].play();
					}
				});
			} else {
				stayOn[idx].setOnFinished(value -> {
					fadeOut[idx].play();
				});
			}

			if (idx+1 < imageSlides.length)
				fadeOut[idx].setOnFinished(value->{
					fadeIn[idx+1].play();
				});
			else if (idx == imageSlides.length-1) {
				i = 0;
				fadeOut[idx].setOnFinished(value->{
					fadeIn[0].play();
				});
			}
			slide.setOpacity(0);
			if (slide == mc){
				this.videoStackPane.getChildren().add(slide);
			} else {
				this.slideShowStackPane.getChildren().add(slide);
			}
			i++;
		}
		fadeIn[0].play();
	}

	private void startVideoAds(){
		FadeTransition fadeIn[] = new FadeTransition[4];
		PauseTransition stayOn[] = new PauseTransition[4];
		FadeTransition fadeOut[] = new FadeTransition[4];
		int i = 0;
		for (Node slide : videoSlides) {
			final int idx = i;

			fadeIn[idx] = getFadeTransition(slide, 0.0, 1.0, 2000);

			Duration duration = Duration.millis(2000);
			if (slide == mc) { // harus diganti dengan isVideo
				duration = Duration.INDEFINITE;
			}

			stayOn[idx] = new PauseTransition(duration);
			fadeIn[idx].setOnFinished(value -> {
				stayOn[idx].play();
				mc.getMediaPlayer().setStartTime(Duration.ZERO);
				mc.getMediaPlayer().seek(Duration.ZERO);
				mc.getMediaPlayer().play();
			});

			fadeOut[idx] = getFadeTransition(slide, 1.0, 0.0, 2000);
			mc.getMediaPlayer().setOnEndOfMedia(new Runnable() {
				@Override
				public void run() {
					stayOn[idx].stop();
					fadeOut[idx].play();
				}
			});

			if (idx+1 < videoSlides.length)
				fadeOut[idx].setOnFinished(value->{
					fadeIn[idx+1].play();
				});
			else if (idx == videoSlides.length-1) {
				i = 0;
				fadeOut[idx].setOnFinished(value->{
					fadeIn[0].play();
				});
			}
			slide.setOpacity(0);
			this.videoStackPane.getChildren().add(slide);

			i++;
		}
		fadeIn[0].play();
	}

	private void startImageAds(){
		FadeTransition fadeIn[] = new FadeTransition[4];
		PauseTransition stayOn[] = new PauseTransition[4];
		FadeTransition fadeOut[] = new FadeTransition[4];
		int i = 0;
		for (Node slide : imageSlides) {
			final int idx = i;
			fadeIn[idx] 		= getFadeTransition(slide, 0.0, 1.0, 3000);
			Duration duration   = Duration.millis(3000);

			stayOn[idx] = new PauseTransition(duration);
			fadeIn[idx].setOnFinished(value -> {
				stayOn[idx].play();
			});

			fadeOut[idx] = getFadeTransition(slide, 1.0, 0.0, 2000);
			stayOn[idx].setOnFinished(value -> {
				fadeOut[idx].play();
			});

			if (idx+1 < imageSlides.length)
				fadeOut[idx].setOnFinished(value->{
					fadeIn[idx+1].play();
				});
			else if (idx == imageSlides.length-1) {
				i = 0;
				fadeOut[idx].setOnFinished(value->{
					fadeIn[0].play();
				});
			}
			slide.setOpacity(0);
			this.slideShowStackPane.getChildren().add(slide);

			i++;
		}
		fadeIn[0].play();
	}
	// the method in the Transition helper class:

	public FadeTransition getFadeTransition(Node imageView, double fromValue, double toValue,
			int durationInMilliseconds) {

		FadeTransition ft = new FadeTransition(Duration.millis(durationInMilliseconds), imageView);
		ft.setFromValue(fromValue);
		ft.setToValue(toValue);

		return ft;

	}

	@FXML
	public void initialize() {

		itemNoClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, Integer>("itemNo"));

		if (Config.isEnableProductName())
			productName.setCellValueFactory(new PropertyValueFactory<TransactionItem, String>("productName"));
		else
			productName.setVisible(false);

		if (Config.isEnableSize())
			sizeClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, String>("size"));
		else
			sizeClm.setVisible(false);

		priceClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, BigDecimal>("price"));

		qtyClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, Integer>("qty"));

		if (Config.isAbc()){
			discPctClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, Float>("discPct"));
			discAmtClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, BigDecimal>("discAmt"));
		} else {
			discPctClm.setVisible(false);
			discAmtClm.setVisible(false);
		}

		totalClm.setCellValueFactory(new PropertyValueFactory<TransactionItem, BigDecimal>("total"));

		table.setItems(tableData);

		if (!Config.isEnableTransfer()) {
			transferLbl.setVisible(false);
			transferAmt.setVisible(false);
		}

		if (!Config.isAbc()){
			labelTotalDisc.setVisible(false);
			totalDisc.setVisible(false);
		}
	}

	public List<Ads> getListVideoAds() throws SQLException{
		Date currentDate = new Date();
		QueryBuilder<Ads, Integer> qb = DaoManagerImpl.getAdsDao().queryBuilder();
		qb.where()
		.le(Ads._VALID_FROM, DateUtil.format(currentDate, "yyyy-M-dd"))
		.and()
		.ge(Ads._VALID_UNTIL, DateUtil.format(currentDate, "yyyy-M-dd"))
		.and()
		.eq(Ads._OFFICE_ID,SettingCtrl.getSetting().getStoreId())
		.and()
		.eq(Ads._ADS_TYPE,_ADS_TYPE_VIDEO);
		return DaoManagerImpl.getAdsDao().query(qb.prepare());
	}

	public List<Ads> getListImageAds() throws SQLException{
		Date currentDate = new Date();
		QueryBuilder<Ads, Integer> qb = DaoManagerImpl.getAdsDao().queryBuilder();
		qb.where()
		.le(Ads._VALID_FROM, DateUtil.format(currentDate, "yyyy-M-dd"))
		.and()
		.ge(Ads._VALID_UNTIL, DateUtil.format(currentDate, "yyyy-M-dd"))
		.and()
		.eq(Ads._OFFICE_ID,SettingCtrl.getSetting().getStoreId())
		.and()
		.eq(Ads._ADS_TYPE, _ADS_TYPE_IMAGE);
		return DaoManagerImpl.getAdsDao().query(qb.prepare());
	}

	public void showImageSlide() throws SQLException{
		List<Ads> imageAds = this.getListImageAds();
		imageSlides = new Node[imageAds.size()];
		Image[] image = new Image[imageAds.size()];
		System.out.println("Slides count: "+ imageAds.size());

		for (int i = 0; i < imageSlides.length; i++){
			final int j = i;
			image[j]	= new Image(imageAds.get(j).getUrl());
			imageSlides[j]	= new ImageView(image[j]);
			System.out.println(imageAds.get(j).getUrl());
		}
	}

	public void showVideoSlide() throws SQLException{
		List<Ads> videoAds = this.getListVideoAds();
		videoSlides = new Node[videoAds.size()];
		System.out.println("Slides count: "+ videoAds.size());

		for (int i = 0; i < videoSlides.length; i++){
			final int j = i;
			mc = new MediaControl(EmbeddedMediaPlayer.createMediaPlayer(videoAds.get(j).getUrl()));
			videoSlides[j] = mc;
			System.out.println(videoAds.get(j).getUrl());
		}
	}

	public void startImageSlider(){
		if(!imageSliderService.isRunning()){
			imageSliderService.reset();
			imageSliderService.start();
		}
	}

	public void startVideoSlider(){
		if(!videoSliderService.isRunning()){
			videoSliderService.reset();
			videoSliderService.start();
		}
	}

	public Label getCustomerInfo() {
		return customerInfo;
	}

	public Label getCustomerLevel() {
		return customerLevel;
	}

	public Label getCustomerPoint() {
		return customerPoint;
	}

	public ImageView getProductImage() {
		return productImage;
	}

	public TableView<TransactionItem> getTable() {
		return table;
	}

	public TableColumn<TransactionItem, String> getProductName() {
		return productName;
	}

	public Label getSubtotal() {
		return subtotal;
	}

	public Label getTotalDisc() {
		return totalDisc;
	}

	public Label getTotalPrice() {
		return totalPrice;
	}

	public Label getVoucherAmt() {
		return voucherAmt;
	}

	public Label getTotalPriceAfterVoucher() {
		return totalPriceAfterVoucher;
	}

	public Label getCash() {
		return cash;
	}

	public Label getChange() {
		return change;
	}

	public Label getDebitAmt() {
		return debitAmt;
	}

	public Label getCreditCardAmt() {
		return creditCardAmt;
	}

	public Label getTransferLbl() {
		return transferLbl;
	}

	public Label getTransferAmt() {
		return transferAmt;
	}

	public StackPane getSlideShowStackPane() {
		return slideShowStackPane;
	}

	public ObservableList<TransactionItem> getTableData() {
		return tableData;
	}
}

