package id.sit.pos.transaction.view;

import java.sql.SQLException;

import id.r.engine.controller.RDao;
import id.r.engine.view.RForm;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.master.model.Voucher;
import id.sit.pos.master.model.VoucherDetail;
import id.sit.pos.master.model.Warehouse;
import id.sit.pos.transaction.controller.VoucherController;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.DateUtil;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class UseVoucherForm extends RForm<VoucherDetail>{
	@FXML
	private TextField voucherCode;

	@FXML
	private Label status;

	@FXML
	private Label voucherType;

	@FXML
	private Label voucherName;

	@FXML
	private Label validFrom;

	@FXML
	private Label validTo;

	@FXML
	private Label availableBranch;

	@FXML
	private Label discountPct;

	@FXML
	private Label discountAmt;

	@FXML
	private Label multiple;

	@FXML
	private Label maxAmt;

	@FXML
	private Label term;

	private VoucherController controller = new VoucherController();

	private Service<VoucherDetail> getVoucherDetailService;

	@FXML
	public void initialize() {
		getVoucherDetailService = new Service<VoucherDetail>() {

			@Override
			protected Task<VoucherDetail> createTask() {
				return new Task<VoucherDetail>() {
					@Override
					protected VoucherDetail call() throws InterruptedException {
						startProgressBar();
						VoucherDetail result = null;

						try {
							result = controller.getVoucherDetail(voucherCode.getText());
						} catch (SQLException e) {
							logger.error(e.toString(), e);
							MainApp.showAlertError("common_error_db_content");
						} finally {
							endProgressBar();
						}
						return result;
					}
				};
			}
		};

		getVoucherDetailService.setOnSucceeded(t -> {
			VoucherDetail voucher = (VoucherDetail) t.getSource().getValue();
			if (voucher != null) {
				updateForm(voucher);
				saveBtn.requestFocus();
			} else {
				MainApp.showAlertError("voucher_not_found");
			}
		});

		setAsFormFields(voucherType, voucherName, status, validFrom, validTo, availableBranch, discountPct, discountAmt, multiple, maxAmt, term);
	}

	@FXML
	public void voucherCodeOnAction() {
		if (!getVoucherDetailService.isRunning()) {
			getVoucherDetailService.reset();
			getVoucherDetailService.start();
		}
	}

	@Override
	public RDao<VoucherDetail> initRDao() {
		return new RDao<VoucherDetail>(VoucherDetail.class);
	}

	@Override
	public VoucherDetail initModel() {
		return new VoucherDetail();
	}

	@Override
	public void updateForm(VoucherDetail model) {
		if (model != null && model.getVoucher() != null && model.getVoucher().getType() != null) {
			this.model = model;
			Voucher voucher = model.getVoucher();
			voucherType.setText(voucher.getType().toString());
			voucherName.setText(voucher.getName());

			if (model.getIsAvailable())
				status.setText("Available");
			else
				status.setText("Used In: " + model.getTransactionNo());

			if (model.getVoucher().getValidFrom() != null)
				validFrom.setText(DateUtil.format(voucher.getValidFrom()));

			if (voucher.getValidUntil() != null)
				validTo.setText(DateUtil.format(voucher.getValidUntil()));

			if (voucher.getValidBranch() != null)
				if (!voucher.getValidBranch().getBranches().isEmpty()) {
					StringBuilder sb = new StringBuilder();
					voucher.getValidBranch().getBranches().forEach(v->{
						Warehouse warehouse = (Warehouse) v;
						sb.append(warehouse.getName());
						sb.append(", ");
					});
					sb.delete(sb.length() - 3, sb.length() - 1);
					availableBranch.setText(sb.toString());
				} else {
					availableBranch.setText(voucher.getValidBranch().getName());
				}

			if (voucher.getDiscountPct() != null)
				discountPct.setText(voucher.getDiscountPct().toString());

			if (voucher.getDiscountAmt() != null)
				discountAmt.setText(CurrencyUtil.format(voucher.getDiscountAmt()));

			multiple.setText(voucher.isMultiple() ? "Yes" : "No");

			if (voucher.getMaxVoucherAmt() != null)
				maxAmt.setText(CurrencyUtil.format(voucher.getMaxVoucherAmt()));

			term.setText(voucher.getTerm());
		} else {
			showAlertError("trans_voucher_invalid");
			resetForm();
		}
	}

	@Override
	public VoucherDetail fetchFormData() {
		return model;
	}

	@Override
	public void beforeSave() {
		super.beforeSave();
		try {
			if (!controller.isValid(model.getVoucher())) {
				showMessage(Config.getMessage("trans_voucher_invalid"), MessageType.ERROR);
			}
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			MainApp.showAlertError("common_error_db_content");
		}
	}

	@Override
	public void save() {
		super.save();
		getStage().close();
	}

	@Override
	public void cancel() {
		model = null;
		super.cancel();
	}
}
