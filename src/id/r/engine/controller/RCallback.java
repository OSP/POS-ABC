package id.r.engine.controller;

public interface RCallback {
	public static final String STARTED = "STARTED";
	public static final String ON_PROGRESS = "ON_PROGRESS";
	public static final String FINISHED = "FINISHED";
	public static final String FAILED = "FAILED";

	public abstract <T> void onEvent(String event, T... value);
}
