package id.sit.pos.db;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import id.sit.pos.config.Config;

public class DbConnector {
	private static Logger logger = Logger.getLogger(DbConnector.class.getName());
	private static ConnectionSource localConnection;
	private static ConnectionSource remoteConnection;
	public static ConnectionSource getLocalConnection() throws SQLException {
		if (localConnection == null || !localConnection.isOpen()) {
			logger.info("Making connection using: "+Config.getLocalDbUrl());
			localConnection = new JdbcPooledConnectionSource(Config.getLocalDbUrl());
		}

		return localConnection;
	}

	public static ConnectionSource getRemoteConnection() throws SQLException {
		if (remoteConnection == null || !remoteConnection.isOpen()) {
			logger.info("Making connection using: "+Config.getRemoteDbUrl());
			remoteConnection = new JdbcPooledConnectionSource(Config.getRemoteDbUrl(), Config.getRemoteDbUsername(), Config.getRemoteDbPassword());
		}

		return remoteConnection;
	}
}
