package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "skus")

public class Sku extends RModel {

	public static final String _PRODUCT_ID = "product_id";
	public static final String _UNIT_ID = "unit_id";
	public static final String _BARCODE = "barcode";
	public static final String _COVERTION = "convertion_unit";

	@DatabaseField(columnName = _PRODUCT_ID, foreign = true)
	private Product product;

	@DatabaseField(columnName = _UNIT_ID, foreign = true)
	private Unit unit;

	@DatabaseField(columnName = _BARCODE)
	private String barcode;

	@DatabaseField(columnName = _COVERTION)
	private Integer convertion;

	public Sku() {

	}

	public Sku(Integer id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Integer getConvertion() {
		return convertion;
	}

	public void setConvertion(Integer convertion) {
		this.convertion = convertion;
	}

}
