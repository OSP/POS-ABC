package id.r.engine.view;

import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;
import id.sit.pos.config.Config;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.user.controller.SessionCtrl;
import id.sit.pos.user.model.AuditTrail;
import id.sit.pos.util.CurrencyUtil;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 *
 * @author Rindi J Ismail Enriched javafx GUI
 */
public class RUi {
	protected static Logger logger;

	@FXML
	protected Label message;

	@FXML
	private ProgressBar progressBar;

	private String title;

	private ObservableList<Image> icons = FXCollections.observableArrayList();

	protected Stage stage;

	private Service<Void> progressUpdater;

	private EventHandler<KeyEvent> numberEh;

	private EventHandler<KeyEvent> alphanumericEh;

	private EventHandler<KeyEvent> nameEh;

	private EventHandler<KeyEvent> addressEh;

	protected AuditTrail auditTrail;

	public RUi() {
		logger = Logger.getLogger(getClass().getName());

		progressUpdater = new Service<Void>() {

			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() throws InterruptedException {
						final int max = 1000;
						for (int i = 1; i <= 900;) {
							if (isDone()) {
								break;
							}

							updateProgress(i, max);
						}

						wait();

						updateProgress(max, max);
						Thread.sleep(1000);
						updateProgress(0, max);
						return null;
					}
				};
			}
		};
		getIcons().add(new Image(Config.getLogoPath()));

		alphanumericEh = new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				if (e.getCharacter().matches("[a-z0-9]")) {
				} else {
					e.consume();
				}
			}
		};

		nameEh = new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				if (e.getCharacter().matches("[a-z0-9\\s]") || (e.isShiftDown() && e.getCharacter().matches("[a-zA-Z0-9\\s]"))) {
				} else {
					e.consume();
				}
			}
		};

		addressEh = new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				if (e.getCharacter().matches("[\\w\\s,-\\.]") || (e.isShiftDown() && e.getCharacter().matches("[a-zA-Z0-9\\s]"))) {
				} else {
					e.consume();
				}
			}
		};

		numberEh = new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				if (e.getCharacter().matches("[0-9]")) {
				} else {
					e.consume();
				}
			}
		};

	}

	@FXML
	protected void initialize() {
		if (progressBar != null)
			progressBar.progressProperty().bind(progressUpdater.progressProperty());
	}

	protected void startProgressBar() {
		Platform.runLater(() -> {
			if (!progressUpdater.isRunning()) {
				progressUpdater.reset();
				progressUpdater.start();
			} else {
				if (progressUpdater.cancel()) {
					progressUpdater.reset();
					progressUpdater.start();
				}
			}
		});
	}

	protected void endProgressBar() {
		Platform.runLater(() -> {
			progressUpdater.cancel();
		});
	}

	public void setAsMandatoryFields (TextInputControl... fields) {
		for (TextInputControl field : fields) {
			field.addEventFilter(KeyEvent.KEY_TYPED , alphanumericEh);
		}
	}

	public void setAsAlphanumericFields (TextInputControl... fields) {
		for (TextInputControl field : fields) {
			field.addEventFilter(KeyEvent.KEY_TYPED , alphanumericEh);
		}
	}

	public void setAsNameFields (TextInputControl... fields) {
		for (TextInputControl field : fields) {
			field.addEventFilter(KeyEvent.KEY_TYPED , nameEh);
		}
	}

	public void setAsAddressFields (TextInputControl... fields) {
		for (TextInputControl field : fields) {
			field.addEventFilter(KeyEvent.KEY_TYPED , addressEh);
		}
	}

	public void setAsNumericFields (TextInputControl... fields) {
		for (TextInputControl field : fields) {
			field.addEventFilter(KeyEvent.KEY_TYPED , numberEh);
			field.focusedProperty().addListener((ov, t, t1) -> {
				Platform.runLater(() -> {
					if (field.getText() != null && field.isFocused() && !field.getText().isEmpty()) {
						field.selectAll();
					}
				});
			});
		}
	}

	public void setAsCurrencyFields (TextInputControl... fields) {
		for (TextInputControl field : fields) {
			field.addEventFilter(KeyEvent.KEY_TYPED , numberEh);
			field.textProperty().addListener((observable, oldValue, newValue) -> {
				field.setText(CurrencyUtil.format(newValue));
			});

			field.focusedProperty().addListener((ov, t, t1) -> {
				Platform.runLater(() -> {
					if (field.isFocused() && field.getText() != null && !field.getText().isEmpty()) {
						field.selectAll();
					}
				});
			});
		}
	}

	public void setAsUppercaseFields(TextInputControl... fields) {
		for (TextInputControl field : fields) {
			field.textProperty().addListener((ov, oldValue, newValue) -> {
				if (newValue != null)
					field.setText(newValue.toUpperCase());
			});
		}
	}

	public void showAlertError(String messageId) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Alert alert = new Alert(AlertType.ERROR);

				alert.initOwner(stage);
				alert.setTitle(Config.getMessage("common_error_title"));
				alert.setContentText(Config.getMessage(messageId));

				alert.showAndWait();
			}
		});
	}

	protected void logAccess() {
		if (auditTrail == null) {
			auditTrail = new AuditTrail();
			auditTrail.setUser(SessionCtrl.getSession().getUser());
			auditTrail.setScreenName(getScreenName());
			auditTrail.setCreatedAt(new Date());
		}else {
			auditTrail.setUpdatedAt(new Date());
		}
		try {
			DaoManagerImpl.getAuditTrailDao().create(auditTrail);
		} catch (SQLException e) {
			logger.error(e.toString(), e);
		}
	}

	public String getScreenName() {
		return "";
	};

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ObservableList<Image> getIcons() {
		return icons;
	}
}
