package id.sit.pos.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import id.sit.pos.config.Config;

public class DateUtil {
	public static String format(Date date) {
		if (date == null)
			return null;
		SimpleDateFormat formatter = new SimpleDateFormat(Config.getDateFormat());
		return formatter.format(date);
	}

	public static String formatTime(Date date) {
		if (date == null)
			return null;
		SimpleDateFormat formatter = new SimpleDateFormat(Config.getTimeFormat());
		return formatter.format(date);
	}

	public static String format4Db(Date date) {
		if (date == null)
			return null;
		SimpleDateFormat formatter = new SimpleDateFormat(Config.getDbDateFormat());
		return formatter.format(date);
	}

	public static String formatDateTime4Db(Date date) {
		if (date == null)
			return null;
		SimpleDateFormat formatter = new SimpleDateFormat(Config.getDbDateTimeFormat());
		return formatter.format(date);
	}

	public static String formatDateTime(Date date) {
		if (date == null)
			return null;
		SimpleDateFormat formatter = new SimpleDateFormat(Config.getDateFormat()+ " " + Config.getTimeFormat());
		return formatter.format(date);
	}

	public static String format(Date date, String format) {
		if (date == null || format == null)
			return null;
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(date);
	}

	public static Date parse(String date, String format) throws ParseException {
		if (date == null || format == null)
			return null;
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.parse(date);
	}

	public static Date parseDbDate(String date) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(Config.getDateFormat());
		return formatter.parse(date);
	}

	public static Date getStartOfDay(){
		return getStartOfDay(new Date());
	}

	public static Date getEndOfDay(){
		return getEndOfDay(new Date());
	}

	public static Date getStartOfDay(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
	    int year = cal.get(Calendar.YEAR);
	    int month = cal.get(Calendar.MONTH);
	    int day = cal.get(Calendar.DAY_OF_MONTH);
	    cal.set(year, month, day, 0, 0, 0);

	    return cal.getTime();
	}

	public static Date getEndOfDay(Date date){
		Calendar cal = Calendar.getInstance();
	    int year = cal.get(Calendar.YEAR);
	    int month = cal.get(Calendar.MONTH);
	    int day = cal.get(Calendar.DAY_OF_MONTH);
	    cal.set(year, month, day, 23, 59, 59);

	    return cal.getTime();
	}

	public static Date getPreviousDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, -1);

		return cal.getTime();
	}
}
