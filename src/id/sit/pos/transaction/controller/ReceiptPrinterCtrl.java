package id.sit.pos.transaction.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import javax.print.PrintException;
import org.apache.log4j.Logger;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.master.model.Entities;
import id.sit.pos.master.model.Warehouse;
import id.sit.pos.util.DeviceUtil;
import id.sit.pos.util.PrintUtil;
import id.sit.pos.util.StringUtil;

public class ReceiptPrinterCtrl {

	private static Logger logger = Logger.getLogger(ReceiptPrinterCtrl.class.getName());
	public String generateHeader() {
		Integer maxChar = new Integer(Config.getPrinterMaxCharPerLine());
		StringBuilder receipt = new StringBuilder();
		String aNPWP = null;

		if(Config.isEnablePpn()){
			try {
				aNPWP = getNPWP();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		Warehouse store = SettingCtrl.getSetting().getStore();
		receipt.append(StringUtil.center("<<< "+store.getName()+" >>>", maxChar));
		receipt.append(String.format("%n"));
		String address = store.getAddress();

		if (address != null) {
			if (address.length() > maxChar) {
				address = address.substring(0, maxChar-1);
			} else {
				address = StringUtil.center(address, maxChar);
			}
			receipt.append(StringUtil.center(address, maxChar));
			receipt.append(String.format("%n"));
		}
		if (store.getCity() != null ){
			receipt.append(StringUtil.center(store.getCity(), maxChar));
		}
		receipt.append(String.format("%n"));
		receipt.append(StringUtil.center("TELP. "+SettingCtrl.getSetting().getStore().getPhoneNumber(), maxChar));
		receipt.append(String.format("%n"));
		if (Config.isEnablePpn()&& aNPWP != null && aNPWP != ""){
			receipt.append(StringUtil.center("NPWP:" + aNPWP, maxChar));
			receipt.append(String.format("%n"));
		}

		receipt.append(String.format(generateLine()));
		return receipt.toString();
	}

	public String generateLine(){
		String line = null;
			if (Config.isEnablePrinterType48()){
				line = "================================================%n";
			} else {
				line = "========================================%n";
			}
		return line;
	}

	public String generateSmallLine(){
		String line = null;
			if (Config.isEnablePrinterType48()){
				line = "------------------------------------------------%n";
			} else {
				line = "----------------------------------------%n";
			}
		return line;
	}

	public static String getAdditionalSpace(){
		String space = "";
		if (Config.isEnablePrinterType48()){
			space = "       ";
		}
		return space;
	}

	public void print(String fileName, String receiptContent) throws PrintException, IOException {
		StringBuilder receipt = new StringBuilder(generateHeader());
		receipt.append(receiptContent);
		printToFile(fileName+".txt", receipt.toString());
//		InputStream is = new ByteArrayInputStream(
//				Charset.forName("UTF-8").encode(receipt.toString()).array());
		if (Config.isEnableAdvancedPrinting())
			DeviceUtil.getPosPrintDevice().print(receipt.toString());
		else
			PrintUtil.printText(receipt.toString());
	}

	public void printToFile(String fileName, String receiptContent) throws FileNotFoundException {
		PrintWriter writer;
		try {
			File dir = new File("receipts\\");
			if (!dir.exists()) {
				dir.mkdir();
			}
			writer = new PrintWriter(dir.getPath()+"\\"+fileName, "UTF-8");
			writer.println(receiptContent);
			writer.close();
		} catch (UnsupportedEncodingException e) {
			logger.error(e.toString(),e);
		}
	}

	public String getNPWP() throws SQLException{
		String aResult = null;
		Dao<Entities, Integer> entDao;
		entDao = DaoManagerImpl.getEntitiesDao();

		GenericRawResults<String[]> rawResults = entDao.queryRaw("SELECT npwp FROM entities WHERE id = 1");
		for (String[] results : rawResults){
			aResult = results[0];
		}
		rawResults.close();
		return aResult;
	}
}
