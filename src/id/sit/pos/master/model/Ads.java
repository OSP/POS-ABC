package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "ads")
public class Ads extends RModel{
	public static final String _URL = "url";
	public static final String _ADS_TYPE = "ad_type";
	public static final String _VALID_FROM = "valid_from";
	public static final String _VALID_UNTIL = "valid_until";
	public static final String _OFFICE_ID = "store_id";

	@DatabaseField(columnName = _URL)
	private String url;

	@DatabaseField(columnName = _ADS_TYPE)
	private String adsType;

	@DatabaseField(columnName = _VALID_FROM)
	private String validFrom;

	@DatabaseField(columnName = _VALID_UNTIL)
	private String validUntil;

	@DatabaseField(columnName = _OFFICE_ID, foreign = true)
	private Warehouse officeId;

	public Ads(){

	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAdsType() {
		return adsType;
	}

	public void setAdsType(String adsType) {
		this.adsType = adsType;
	}

	public String getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	public String getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(String validUntil) {
		this.validUntil = validUntil;
	}

	public Warehouse getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Warehouse officeId) {
		this.officeId = officeId;
	}


}
