package id.sit.pos.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;

import id.sit.pos.config.Config;

public class PrintUtil {
	private final static char temp1B[] = { 0x1B };
	private final static char temp67[] = { 0x67 };
	private static Logger logger = Logger.getLogger(PrintUtil.class.getName());;
	private static volatile boolean jobRunning = true;
	private static long timeout = 0;

//	public static void printText(InputStream is) throws PrintException, IOException {

	public static void printText(String s) throws PrintException, IOException {
		if (Config.isEnablePrinterTypeLX310())
			s = String.valueOf(temp1B) + String.valueOf(temp67) + s;
			InputStream is = new ByteArrayInputStream(Charset.forName("UTF-8").encode(s).array());

		DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;

		// Locate the default print service for this environment.
		PrintService service = PrintServiceLookup.lookupDefaultPrintService();

		// Create and return a PrintJob capable of handling data from
		// any of the supported document flavors.
		DocPrintJob printJob = service.createPrintJob();

		// register a listener to get notified when the job is complete
		printJob.addPrintJobListener(new JobCompleteMonitor());

		// Construct a SimpleDoc with the specified
		// print data, doc flavor and doc attribute set.
		Doc doc = new SimpleDoc(is, flavor, null);

		// Print a document with the specified job attributes.
		printJob.print(doc, null);
	}

	private static class JobCompleteMonitor extends PrintJobAdapter {

		@Override
		public void printJobCanceled(PrintJobEvent pje) {
			// TODO Auto-generated method stub
			super.printJobCanceled(pje);
		}

		@Override
		public void printJobFailed(PrintJobEvent pje) {
			// TODO Auto-generated method stub
			super.printJobFailed(pje);
		}

		@Override
		public void printJobCompleted(PrintJobEvent jobEvent) {
			jobRunning = false;
			logger.log(Level.INFO, "Finished printing");
		}
	}
}
