package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "size_details")
public class Size extends RModel{
	public static final String _SIZE_CATEGORY_ID = "size_id";
	public static final String _NAME = "size_number";

	@DatabaseField(columnName=_SIZE_CATEGORY_ID, foreign = true)
	private SizeCategory sizeCategory;

	@DatabaseField(columnName = _NAME)
	private String sizeNumber;

	public Size(){

	}

	public Size(Integer id){
		this.id = id;
	}

	public String getSizeNumber() {
		return sizeNumber;
	}

	public void setSizeNumber(String sizeNumber) {
		this.sizeNumber = sizeNumber;
	}

	public SizeCategory getSizeCategory() {
		return sizeCategory;
	}

	public void setSizeCategory(SizeCategory sizeCategory) {
		this.sizeCategory = sizeCategory;
	}

}
