package id.sit.pos.transaction.view;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;


import id.r.engine.controller.RDao;
import id.r.engine.model.SearchParam;
import id.r.engine.model.SearchParam.Operator;
import id.r.engine.view.RGridChooser;
import id.sit.pos.config.Config;
import id.sit.pos.customer.model.Member;
import id.sit.pos.transaction.model.Transaction;
import id.sit.pos.util.DateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;

public class TransactionHistory extends RGridChooser<Transaction> {
	@FXML
	private TextField transactionNoSc;

	@FXML
	private DatePicker transactionDateSc;

	@FXML
	private ComboBox<Transaction.Status> transactionStatusSc;

	@FXML
	private TextField customerNameSc;

	@FXML
	private TableColumn<Transaction, Integer> transactionNoClm;

	@FXML
	private TableColumn<Transaction, Integer> timeClm;

	@FXML
	private TableColumn<Transaction, Integer> customerNameClm;

	@FXML
	private TableColumn<Transaction, Integer> totalItemClm;

	@FXML
	private TableColumn<Transaction, Integer> totalPriceClm;

	public enum Mode {
		STATIC, DYNAMIC
	}

	public TransactionHistory() {
		super();
		getIcons().add(new Image(Config.getLogoPath()));
		setTitle(Config.getMessage("trans_pending_title"));
	}
	@FXML
	public void initialize() {
		super.initialize();
		transactionNoClm.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("transactionNo"));
		timeClm.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("time"));
		customerNameClm.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("customerName"));
		totalItemClm.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("totalItem"));
		totalPriceClm.setCellValueFactory(new PropertyValueFactory<Transaction, Integer>("totalPrice"));

		setAsSearchCriteriaFields(transactionNoSc);

		transactionStatusSc.getItems().addAll(Transaction.Status.values());
		transactionDateSc.setValue((new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		transactionStatusSc.setValue(Transaction.Status.OPEN);
		search();
	}

	@Override
	public SearchParam[] getSearchParams() {
		LocalDate localDateSc = transactionDateSc.getValue();
		Date dateSc = null;
		if (localDateSc != null) {
			Instant instant = Instant.from(localDateSc.atStartOfDay(ZoneId.systemDefault()));
			dateSc = Date.from(instant);
		}

		SearchParam[] searchParams = new SearchParam[] {
				new SearchParam(Transaction._TRANSACTION_NO, transactionNoSc.getText()),
				new SearchParam(Transaction._TRANSACTION_DATE, DateUtil.getStartOfDay(dateSc), Operator.GTE),
				new SearchParam(Transaction._TRANSACTION_DATE, DateUtil.getEndOfDay(dateSc), Operator.LTE),
				new SearchParam(Transaction._STATUS, transactionStatusSc.getSelectionModel().getSelectedItem()),
				new SearchParam(Member.class, Member._NAME, customerNameSc.getText())
		};
		return searchParams;
	}

	@Override
	protected RDao<Transaction> initDao() {
		return new RDao<>(Transaction.class);
	}
}
