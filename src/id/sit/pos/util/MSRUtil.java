package id.sit.pos.util;

import java.util.function.Consumer;

import org.apache.log4j.Logger;

import jpos.JposConst;
import jpos.JposException;
import jpos.MSR;
import jpos.events.DataEvent;
import jpos.events.DataListener;
import jpos.events.DirectIOEvent;
import jpos.events.DirectIOListener;
import jpos.events.ErrorEvent;
import jpos.events.ErrorListener;
import jpos.events.StatusUpdateEvent;
import jpos.events.StatusUpdateListener;

/**
 *
 * @author dean
 */
public class MSRUtil implements DataListener, ErrorListener, StatusUpdateListener, DirectIOListener, JposConst {
	private static Logger logger = Logger.getLogger(MSRUtil.class.getName());
    private MSR msr = null;
    private Consumer<Data> callback = null;

    public void readOnce(Consumer<Data> callback) throws JposException {
    	this.callback = callback;
    }

    public String convertByteToString(byte[] dataIn){
        String dataOut = "";
        if(dataIn == null)return "";

        for(int i = 0; i < dataIn.length ; i++){
            dataOut = dataOut + (char)dataIn[i];
        }

        return dataOut;
    }

    public void startDevice() {
        // MSR
    	try {
	        msr = new MSR();
	        msr.open("MCRST-A10");
	        msr.addDataListener(this);
	        msr.addDirectIOListener(this);
	        msr.addErrorListener(this);
	        msr.addStatusUpdateListener(this);
	        msr.claim(100);
	        msr.setDeviceEnabled(true);
	        msr.setDataEventEnabled(true);

	        //logger.info("MSRUtil: Device Started");
	        System.out.println("MSRUtil: Device Started");
    	} catch(JposException e){
        	logger.error(e.toString(), e);
        	System.out.println("MSRUtil: Device Stop" + e.toString());
        	stopDevice();
        }
    }

    public void stopDevice() {
        // MSR
    	try {
	        msr.removeDataListener(this);
	        msr.removeDirectIOListener(this);
	        msr.removeErrorListener(this);
	        msr.removeStatusUpdateListener(this);
	        msr.close();

	        logger.info("MSRUtil: Device Stop");
    	} catch (JposException e) {
    		logger.error(e.toString(), e);
    	}
    }

    public void dataOccurred(DataEvent de) {
    	logger.info("GESEKKK");
        try {
            // Data kartu setelah digesek
            System.out.println("DecodeData      : " + msr.getDecodeData());
            System.out.println("ParseDecodeData : " + msr.getParseDecodeData());

            System.out.println("Data 1: " + convertByteToString(msr.getTrack1Data()));
            System.out.println("Data 2: " + convertByteToString(msr.getTrack2Data()));
            System.out.println("Data 3: " + convertByteToString(msr.getTrack3Data()));
            System.out.println("Data 4: " + convertByteToString(msr.getTrack4Data()));

            Data data = new Data();
            data.setAccountNumber(msr.getAccountNumber());
            System.out.println("Account Number  : " + msr.getAccountNumber());
            System.out.println("Expiration Date : " + msr.getExpirationDate());
            System.out.println("Title           : " + msr.getTitle());
            System.out.println("First Name      : " + msr.getFirstName());
            System.out.println("Middle Initial  : " + msr.getMiddleInitial());
            System.out.println("Surname         : " + msr.getSurname());
            System.out.println("Suffix          : " + msr.getSuffix());
            System.out.println("Service Code    : " + msr.getServiceCode());
            System.out.println(convertByteToString(msr.getTrack1DiscretionaryData()));

            if (this.callback != null)
            	callback.accept(data);

        } catch(JposException e){
        	logger.error(e.toString(), e);
        	logger.info("GESEK SALAHHH");
	        stopDevice();
        }
    }

    public void errorOccurred(ErrorEvent ee) {
    }

    public void statusUpdateOccurred(StatusUpdateEvent sue) {
    }

    public void directIOOccurred(DirectIOEvent dioe) {
    }

	public class Data {
		private String accountNumber;

		public String getAccountNumber() {
			return accountNumber;
		}

		public void setAccountNumber(String accountNumber) {
			this.accountNumber = accountNumber;
		}
	}
}
