package id.r.engine.view;

public interface RChooser {
	public abstract void choose();

	public abstract void cancel();
}
