package id.sit.pos.customer.model;

import java.math.BigDecimal;
import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.common.Gender;
import id.sit.pos.util.DateUtil;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@DatabaseTable(tableName = "members")
public class Member extends RModel {
	public static final String _MEMBER_NO = "card_number";
	public static final String _NAME = "name";
	public static final String _BIRTH_DATE = "birthday";
	public static final String _GENDER = "gender";
	public static final String _EMAIL = "email";
	public static final String _PHONE_NO = "hp";
	public static final String _ADDRESS = "address";
	public static final String _CITY = "city";
	public static final String _POSTCODE = "zip";
	public static final String _BALANCE = "balance";
	public static final String _LEVEL_ID = "member_level_id";
	public static final String _POINT = "point";
	public static final String _CREDIT_LIMIT = "credit_limit";

	@DatabaseField(columnName = _MEMBER_NO)
	private String memberNo;

	@DatabaseField(columnName = _NAME)
	private String name;

	@DatabaseField(columnName = _BIRTH_DATE)
	private Date birthDate;

	@DatabaseField(columnName = _GENDER)
	private Gender gender;

	@DatabaseField(columnName = _EMAIL)
	private String email;

	@DatabaseField(columnName = _PHONE_NO)
	private String phoneNo;

	@DatabaseField(columnName = _ADDRESS)
	private String address;

	@DatabaseField(columnName = _CITY)
	private String city;

	@DatabaseField(columnName = _POSTCODE)
	private String postcode;

	@DatabaseField(columnName = _LEVEL_ID, foreign = true, foreignAutoRefresh = true)
	private MemberLevel level;

	@DatabaseField(columnName = _BALANCE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal balance;

	@DatabaseField(columnName = _CREDIT_LIMIT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal creditLimit;

	@DatabaseField(columnName = _POINT)
	private Integer point;

	/* FX Stuff */
	private IntegerProperty noProperty;
	private StringProperty memberIdProperty;
	private StringProperty nameProperty;
	private StringProperty birthDateProperty;
	private StringProperty genderProperty;
	private StringProperty emailProperty;
	private StringProperty phoneNoProperty;
	private StringProperty addressProperty;
	private StringProperty levelProperty;

	public Member() {
		noProperty = new SimpleIntegerProperty();
		memberIdProperty = new SimpleStringProperty();
		nameProperty = new SimpleStringProperty();
		birthDateProperty = new SimpleStringProperty();
		genderProperty = new SimpleStringProperty();
		emailProperty = new SimpleStringProperty();
		phoneNoProperty = new SimpleStringProperty();
		addressProperty = new SimpleStringProperty();
		levelProperty = new SimpleStringProperty();
	}

	public IntegerProperty noProperty() {
		return noProperty;
	}

	public Integer getNo() {
		return noProperty.get();
	}

	public void setNo(Integer no) {
		this.noProperty.set(no);
	}

	public StringProperty idProperty() {
		memberIdProperty.set(getMemberNo());
		return memberIdProperty;
	}

	public StringProperty nameProperty() {
		nameProperty.set(getName());
		return nameProperty;
	}

	public StringProperty birthDateProperty() {
		birthDateProperty.set(DateUtil.format(getBirthDate()));
		return birthDateProperty;
	}

	public StringProperty genderProperty() {
		if (getGender() != null)
			genderProperty.set(getGender().toString());
		else
			genderProperty.set("");
		return genderProperty;
	}

	public StringProperty emailProperty() {
		emailProperty.set(getEmail());
		return emailProperty;
	}

	public StringProperty phoneNoProperty() {
		phoneNoProperty.set(getPhoneNo());
		return phoneNoProperty;
	}

	public StringProperty addressProperty() {
		addressProperty.set(getAddress());
		return addressProperty;
	}

	public StringProperty levelProperty() {
		if (getLevel() != null)
			levelProperty.set(getLevel().getDesc());
		return levelProperty;
	}
	/* End of FX Stuff */


	public String getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public MemberLevel getLevel() {
		return level;
	}

	public void setLevel(MemberLevel level) {
		this.level = level;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}
}
