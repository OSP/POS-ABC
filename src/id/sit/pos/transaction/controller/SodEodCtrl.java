package id.sit.pos.transaction.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import javax.print.PrintException;

import org.apache.log4j.Logger;
import org.apache.log4j.lf5.util.DateFormatManager;

import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.controller.RDao;
import id.r.engine.model.SearchParam;
import id.r.engine.model.SearchParam.Operator;
import id.r.engine.model.SearchParam.Order;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.transaction.controller.ReceiptPrinterCtrl;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.master.model.Product;
import id.sit.pos.transaction.model.SodEod;
import id.sit.pos.transaction.model.Transaction;
import id.sit.pos.transaction.model.TransactionItem;
import id.sit.pos.transaction.model.SodEod.Type;
import id.sit.pos.user.controller.SessionCtrl;
import id.sit.pos.user.model.Role;
import id.sit.pos.user.model.Session;
import id.sit.pos.user.model.User;
import id.sit.pos.util.DateUtil;
import id.sit.pos.util.PrintUtil;
import id.sit.pos.util.StringUtil;
import id.sit.pos.util.CurrencyUtil;


public class SodEodCtrl {
	private static Logger logger = Logger.getLogger(SodEodCtrl.class.getName());

	private RDao<SodEod> rDao;
	private RDao<TransactionItem> transactionItemDao = new RDao<TransactionItem>(TransactionItem.class);
	private ReceiptPrinterCtrl receiptPrinterCtrl = new ReceiptPrinterCtrl();
	private Transaction current;
	private boolean reprintMode;
	public static boolean isCloseDay;

	public SodEodCtrl () {
		rDao = new RDao<>(SodEod.class);
	}

	public SodEodCtrl(RDao<SodEod> sodEodDao) {
		super();
		this.rDao = sodEodDao;
	}

	public SodEod getOpenShift() throws SQLException {
		QueryBuilder<SodEod, Integer> qb = DaoManagerImpl.getSodEodDao().queryBuilder();
		qb.where().isNull(SodEod._END);

		QueryBuilder<Session, Integer> sessionQb = DaoManagerImpl.getSessionDao().queryBuilder();
		sessionQb.where().eq(Session._MACHINE_ID, SettingCtrl.getSetting().getMachineId())
		.and()
		.eq(Session._STORE_ID, SettingCtrl.getSetting().getStoreId());
		qb.join(sessionQb);
		return qb.queryForFirst();
	}

	public SodEod getOpenShift(Integer userId, Integer shiftNo) throws SQLException {
	    Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -1);
	    Date yesterday = cal.getTime();

		String query = "SELECT * "
			+ "FROM   "+SodEod.class.getAnnotation(DatabaseTable.class).tableName()+" sod "
			+ "WHERE  sod."+SodEod._USER_ID+" = "+userId
			+   " AND \""+SodEod._SHIFT_NO+"\" = "+shiftNo
			+ 	" AND "+SodEod._START+" >= '"+DateUtil.formatDateTime4Db(yesterday)+"'"
			+ 	" AND "+SodEod._END+" is null";

		GenericRawResults<SodEod> rawResults;
		SodEod existing = null;
		rawResults = rDao.getDao().queryRaw(query, rDao.getDao().getRawRowMapper());
		existing = rawResults.getFirstResult();

		return existing;
	}

	public boolean isShiftClosed(Integer userId, Integer shiftNo) throws SQLException {
	    Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -1);

		String query = "SELECT 1 "
			+ "FROM   "+SodEod.class.getAnnotation(DatabaseTable.class).tableName()+" sod "
			+ "WHERE  sod."+SodEod._USER_ID+" = "+userId
			+   " AND \""+SodEod._SHIFT_NO+"\" = "+shiftNo
			+ 	" AND "+SodEod._END+" <= '"+DateUtil.formatDateTime4Db(new Date())+"'";

		GenericRawResults<String[]> rawResults;
		rawResults = rDao.getDao().queryRaw(query);
		if (rawResults.getFirstResult() != null)
			return true;
		else
			return false;
	}

	public boolean isDayClosed() throws SQLException {
	    Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -1);

		String query = "SELECT 1 "
			+ "FROM   "+SodEod.class.getAnnotation(DatabaseTable.class).tableName()+" sod "
			+ "WHERE  sod."+SodEod._STORE_ID+" = "+SettingCtrl.getSetting().getStoreId()
			+   " AND sod."+SodEod._MACHINE_ID+" = '"+SettingCtrl.getSetting().getMachineId()+"'"
			+ 	" AND sod."+SodEod._DATE+" = '"+DateUtil.format4Db(new Date())+"'"
			+ 	" AND sod."+SodEod._TYPE+" = '"+Type.DAY.toString()+"'";

		GenericRawResults<String[]> rawResults;
		rawResults = rDao.getDao().queryRaw(query);
		if (rawResults.getFirstResult() != null)
			return true;
		else
			return false;
	}

	public int getLastShiftNo() throws SQLException {
		SodEod existing = getLastShiftOfDay();

		if (existing != null && existing.getShiftNo() != null)
			return existing.getShiftNo();
		else
			return 0;
	}

	public SodEod getLastShiftOfDay() throws SQLException {
		SodEod existing = rDao.queryFirst(new SearchParam[]{
			new SearchParam(SodEod._TYPE, Type.SHIFT),
			new SearchParam(SodEod._DATE, new Date()),
			new SearchParam(SodEod._STORE_ID, SettingCtrl.getSetting().getStoreId()),
			new SearchParam(SodEod._MACHINE_ID, SettingCtrl.getSetting().getMachineId(), Operator.E),
			new SearchParam(SodEod._SHIFT_NO, Order.DESC)
		});

		if (existing != null)
			return existing;

		return null;
	}

	public SodEod getFirtShiftOfDay() throws SQLException {
		SodEod existing = rDao.queryFirst(new SearchParam[]{
			new SearchParam(SodEod._TYPE, Type.SHIFT),
			new SearchParam(SodEod._DATE, new Date()),
			new SearchParam(SodEod._STORE_ID, SettingCtrl.getSetting().getStoreId()),
			new SearchParam(SodEod._MACHINE_ID, SettingCtrl.getSetting().getMachineId(), Operator.E),
			new SearchParam(SodEod._SHIFT_NO, 1)
		});

		if (existing != null)
			return existing;

		return null;
	}

	public void endShift(SodEod model) throws SQLException {
		Dao<Transaction, Integer> transactionDao;

		transactionDao = DaoManagerImpl.getTransactionDao();
		String query = "SELECT sum(s."+Transaction._CASH_AMT+") cash, " 	//0
			+ " sum(s."+Transaction._DEBIT_AMT+") debit, " 					//1
			+ " sum(s."+Transaction._CREDIT_AMT+") credit, " 					//2
			+ " sum(s."+Transaction._TRANSFER_AMT+") transfer, " 				//3
			+ " sum(s."+Transaction._VOUCHER_AMT+") voucher, " 				//4
			+ " sum(s."+Transaction._TOTAL_PRICE+") sales, " 					//5
			+ " sum(s."+Transaction._TOTAL_DISC+") discount, " 			//6
			+ " sum(s."+Transaction._TOTAL_SPECIAL_DISC+") special_discount, "//7
			+ " sum(s."+Transaction._TOTAL_PPN+") ppn, "						//8
			+ " sum(s."+Transaction._CASH_CHANGE+") change "	//9
			+ " FROM "+Transaction.class.getAnnotation(DatabaseTable.class).tableName()+" s "
			+ " INNER JOIN "+Session.class.getAnnotation(DatabaseTable.class).tableName()+" ss ON ss.id = s."+Transaction._SESSION_ID
			+ " WHERE s."+Transaction._STATUS+" = '"+Transaction.Status.FINISHED+"'"
			+ " AND ss."+Session._SODEOD_ID+"="+SessionCtrl.getSession().getSodEod().getId();

		GenericRawResults<String[]> rawResults = transactionDao.queryRaw(query);
		String[] results = rawResults.getFirstResult();
		if (results[0] != null && results[9] != null)
			model.setCashSales(new BigDecimal(results[0]).subtract(new BigDecimal(results[9])));
		if (results[1] != null)
			model.setDebit(new BigDecimal(results[1]));
		if (results[2] != null)
			model.setCredit(new BigDecimal(results[2]));
		if (results[3] != null)
			model.setTransfer(new BigDecimal(results[3]));
		if (results[4] != null)
			model.setVoucher(new BigDecimal(results[4]));
		if (results[8] != null)
			model.setPpn(new BigDecimal(results[8]));
		if (results[5] != null)
			model.setNetSales(new BigDecimal(results[5]).subtract(model.getPpn()));
		if (results[6] != null)
			model.setDisc(new BigDecimal(results[6]));
		if (results[7] != null)
			model.setSpecialDisc(new BigDecimal(results[7]));

		query = "SELECT sum(sd.total_price) retur "
				+ " FROM  "+TransactionItem.class.getAnnotation(DatabaseTable.class).tableName()+" sd "
				+ " INNER JOIN "+Transaction.class.getAnnotation(DatabaseTable.class).tableName()+" s ON s.id = sd.sale_id "
				+ " INNER JOIN "+Session.class.getAnnotation(DatabaseTable.class).tableName()+" ss ON ss.id = s.session_id "
				+ " WHERE  sd.quantity < 0 "
				+ "   AND ss.sodeod_id="+SessionCtrl.getSession().getSodEod().getId();

		rawResults = transactionDao.queryRaw(query);
		results = rawResults.getFirstResult();
		if (results[0] != null)
			model.setNetReturn(new BigDecimal(results[0]));
		else
			model.setNetReturn(BigDecimal.ZERO);

		model.setSales(model.getNetSales().subtract(model.getNetReturn()).add(model.getPpn()));

		model.setActual(model.getCashIncome().add(model.getDebit()).add(model.getCredit()).add(model.getTransfer()).add(model.getDisc()).add(model.getSpecialDisc()));

		model.setDifference(model.getActual().subtract(model.getSales()));

		query = "SELECT count("+Transaction._TRANSACTION_NO+") receiptCount "
				+ " FROM   sales s "
				+ " INNER JOIN sessions ss ON ss.id = s.session_id "
				+ " WHERE  s.status = '"+Transaction.Status.FINISHED+"'"
				+ "   AND ss.sodeod_id="+SessionCtrl.getSession().getSodEod().getId();

		rawResults = transactionDao.queryRaw(query);
		model.setReceiptCount(new Integer(rawResults.getFirstResult()[0]));
	}

	public void endDay(SodEod model) throws SQLException {
		Dao<Transaction, Integer> transactionDao;

		transactionDao = DaoManagerImpl.getTransactionDao();
		SodEod startShift = getFirtShiftOfDay();
		if (startShift != null)
			model.setStart(startShift.getStart());
		SodEod endShift = getLastShiftOfDay();
		if (endShift != null)
			model.setEnd(endShift.getEnd());

		String start = DateUtil.formatDateTime4Db(model.getStart());
		String end = DateUtil.formatDateTime4Db(model.getEnd());

		String query = "SELECT "
				+ " sum(s."+SodEod._NET_SALES+"), " 		//0
				+ " sum(s."+SodEod._NET_RETURN+"), " 	//1
				+ " sum(s."+SodEod._PPN+"), " 			//2
				+ " sum(s."+SodEod._SALES+"), " 			//3
				+ " sum(s."+SodEod._CASH_SALES+"), " 	//4
				+ " sum(s."+SodEod._CASH_INCOME+"), " 	//5
				+ " sum(s."+SodEod._DEBIT+"), "			//6
				+ " sum(s."+SodEod._CREDIT+"), "			//7
				+ " sum(s."+SodEod._TRANSFER+"), "		//8
				+ " sum(s."+SodEod._VOUCHER+"), "		//9
				+ " sum(s."+SodEod._DISC+"), " 			//10
				+ " sum(s."+SodEod._SPECIAL_DISC+"), "	//11
				+ " sum(s."+SodEod._ACTUAL+"), "			//12
				+ " sum(s."+SodEod._DIFFERENCE+"), "		//13
				+ " sum(s."+SodEod._PAID_DIFFERENCE+"), "//14
				+ " sum(s."+SodEod._RECEIPT_COUNT+"), " 	//15
				+ " min(s."+SodEod._START+"), " 			//16
				+ " max(s."+SodEod._END+") " 			//17
				+ " FROM sod_eods s "
				+ " WHERE s.start_time between '"+start+"' :: timestamp and '"+end+"' :: timestamp";
		GenericRawResults<String[]> rawResults = transactionDao.queryRaw(query);
		String[] results = rawResults.getFirstResult();
		if (results[0] != null)
			model.setNetSales(new BigDecimal(results[0]));
		if (results[1] != null)
			model.setNetReturn(new BigDecimal(results[1]));
		if (results[2] != null)
			model.setPpn(new BigDecimal(results[2]));
		if (results[3] != null)
			model.setSales(new BigDecimal(results[3]));
		if (results[4] != null)
			model.setCashSales(new BigDecimal(results[4]));
		if (results[5] != null)
			model.setCashIncome(new BigDecimal(results[5]));
		if (results[6] != null)
			model.setDebit(new BigDecimal(results[6]));
		if (results[7] != null)
			model.setCredit(new BigDecimal(results[7]));
		if (results[8] != null)
			model.setTransfer(new BigDecimal(results[8]));
		if (results[9] != null)
			model.setVoucher(new BigDecimal(results[9]));
		if (results[10] != null)
			model.setDisc(new BigDecimal(results[10]));
		if (results[11] != null)
			model.setSpecialDisc(new BigDecimal(results[11]));
		if (results[12] != null)
			model.setActual(new BigDecimal(results[12]));
		if (results[13] != null)
			model.setDifference(new BigDecimal(results[13]));
		if (results[14] != null)
			model.setPaidDifference(new BigDecimal(results[14]));
		if (results[15] != null)
			model.setReceiptCount(new Integer(results[15]));
//		try {
//			if (results[16] != null)
//				model.setStart(DateUtil.parseDbDate(results[16]));
//			if (results[17] != null)
//				model.setEnd(DateUtil.parseDbDate(results[17]));
//		} catch (ParseException e) {
//			logger.error(e.toString(), e);
//		}
	}

	public RDao<SodEod> getSodEodDao() {
		return rDao;
	}

	public void setSodEodDao(RDao<SodEod> sodEodDao) {
		this.rDao = sodEodDao;
	}

	public String generateEoDReportDetail() throws SQLException {
		StringBuilder receipt = new StringBuilder();
		receipt.append(receiptPrinterCtrl.generateHeader());

		receipt.append(String.format("Date       : %s%n", DateUtil.format(SessionCtrl.getSession().getStart())));
		receipt.append(String.format("Branch     : %s%n", current.getTransactionNo()));
		receipt.append(String.format("Machine Id : %s%n", current.getTransactionNo()));
		User userLogin = SessionCtrl.getSession().getUser();
		StringBuilder name = new StringBuilder();
		if (userLogin.getFirstName() != null)
			name.append(userLogin.getFirstName());
		if (userLogin.getLastName() != null)
			name.append(userLogin.getLastName());
		receipt.append(String.format("Cashier    : %s%n", name.toString()));
		receipt.append(String.format("%n"));

		StringBuilder query = new StringBuilder();
		query.append("SELECT p.article, p.colour_code, sz.size_number, SUM(sd.quantity) ")
			 .append("FROM sales_details sd ")
			 .append("INNER JOIN sales s ON s.id = sd.sale_id ")
			 .append("INNER JOIN products p ON p.id = sd.product_id ")
			 .append("INNER JOIN product_sizes ps ON ps.id = sd.product_size_id ")
			 .append("INNER JOIN size_details sz ON s.id = ps.size_detail_id ")
			 .append(String.format("WHERE s.transaction_date BETWEEN timestamp '%s' AND timestamp '%s'",
					 DateUtil.format(SessionCtrl.getSession().getStart(), "yyyy-M-dd HH:mm"),
					 DateUtil.format(SessionCtrl.getSession().getEnd(), "yyyy-M-dd HH:mm")))
			 .append("GROUP BY p.article, p.colour_code, sz.size_number ");

		Dao<TransactionItem, Integer> dao = transactionItemDao.getDao();
		GenericRawResults<Object[]> rawResults = dao.queryRaw(query.toString(),
			    new DataType[] { DataType.STRING, DataType.STRING, DataType.STRING, DataType.INTEGER});

		long total = 0;
		for (Object[] resultArray : rawResults) {
			receipt.append(String.format("%-15s %-15s %2s  %4d%n%n",
				  resultArray[0],  resultArray[1], resultArray[2], resultArray[3]));
			total += (int)resultArray[3];
		}
		rawResults.close();

		receipt.append(String.format("%n%n"));

		receipt.append(String.format("%28s  %10s%n%n", "Total Qty", total));

		receipt.append(Config.getEodFooter());

		receipt.append(String.format("%n%n%n%n%n%n%n."));

		System.out.println(receipt.toString());
		return receipt.toString();
	}

	public String generateInisialReport() throws Exception{
		StringBuilder receipt = new StringBuilder();
		receipt.append(receiptPrinterCtrl.generateHeader());
		receipt.append(StringUtil.center("INISIAL", Config.getPrinterMaxCharPerLine()));
		receipt.append(String.format("%n%n%n"));
		receipt.append(String.format("Kasir       : %-15s%n", SettingCtrl.getSetting().getMachineName()));
		receipt.append(String.format("Date        : %-15s%n", DateUtil.format(SessionCtrl.getSession().getStart())));
		receipt.append(String.format("Name        : %-15s%n", SessionCtrl.getSession().getUser().getFirstName()));
		receipt.append(String.format("Shift       : %-15d%n", SessionCtrl.getSession().getSodEod().getShiftNo()));
		receipt.append(String.format("Modal Awal  : %-15s%n", CurrencyUtil.format(SessionCtrl.getSession().getSodEod().getModal(), true)));
		receipt.append(String.format(receiptPrinterCtrl.generateSmallLine()));
		receipt.append(String.format("%n"));
		receipt.append(StringUtil.center("Kasir", Config.getPrinterMaxCharPerLine()/2) + StringUtil.center("Kepala Toko", Config.getPrinterMaxCharPerLine()/2));
		receipt.append(String.format("%n%n%n%n%n"));
		receipt.append(StringUtil.center("(--------------)", Config.getPrinterMaxCharPerLine()/2) + StringUtil.center("(--------------)", Config.getPrinterMaxCharPerLine()/2));
		receipt.append(String.format("%n%n"));
		receipt.append(String.format(receiptPrinterCtrl.generateLine()));
		receipt.append(String.format("%n"));
		receipt.append(String.format("Print Date  : %-15s%n", DateUtil.formatDateTime(new Date())));
		receipt.append(String.format("%n%n%n%n%n"));
		receipt.append(String.format("%n%n%n."));
		if (Config.isEnablePrinterTypeLX310()){
			receipt.append(String.format("%n%n%n%n%n"));
		}
		System.out.println(receipt.toString());

		return receipt.toString();
	}

	public CloseableIterator<SodEod> getListSodEod(Date searchDate, String sessionType, String userRoles) throws SQLException{
		int officeId 		= SettingCtrl.getSetting().getStoreId();
		String startDate 	= null;
		String endDate 		= null;

		if (userRoles.equals(Role.STORE_MANAGER)){
			startDate 	= DateUtil.format4Db(DateUtil.getPreviousDate(new Date()));
			endDate 	= DateUtil.format4Db(new Date());
		} else {
			startDate 	= DateUtil.format4Db(DateUtil.getPreviousDate(searchDate));
			endDate 	= DateUtil.format4Db(searchDate);
		}

		StringBuilder qry 	= new StringBuilder();

		qry.append("SELECT id, sod_eod_date, session_type, session, user_id, ");
		qry.append("total_sales, actual_end_amount, ");
		qry.append("difference FROM sod_eods ");
		qry.append("WHERE sod_eod_date BETWEEN '"+ startDate+ "' AND '"+ endDate + "' " );
		qry.append("AND end_time IS NOT NULL ");
		qry.append("AND session_type = '"+ sessionType +"' ");
		qry.append("AND office_id = "+officeId+" ");
		qry.append("ORDER BY id, session");

		GenericRawResults<SodEod> rawResults = rDao.getDao().queryRaw(qry.toString(),
				new RawRowMapper<SodEod>() {

					@Override
					public SodEod mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
						SodEod sodEod = new SodEod();
						Date sodEodDate = null;
						int i = 0;
						while (i < columnNames.length){
							String value = resultColumns[i];
							if(value != null){
								switch (columnNames[i]){
									case SodEod._ID:
										sodEod.setId(new Integer(value));
										break;
									case SodEod._DATE:
										try {
											sodEodDate = new SimpleDateFormat(Config.getDbDateFormat()).parse(value);
										} catch (ParseException e) {
											e.printStackTrace();
										}
										sodEod.setDate(sodEodDate);
										break;
									case SodEod._TYPE:
										sodEod.setType(Type.valueOf(value));
										break;
									case SodEod._SHIFT_NO:
										sodEod.setShiftNo(new Integer(value));
										break;
									case SodEod._USER_ID:
										sodEod.setUser(new User(new Integer(value)));
									case SodEod._SALES:
										sodEod.setSales(new BigDecimal(value));
										break;
									case SodEod._ACTUAL:
										sodEod.setActual(new BigDecimal(value));
										break;
									case SodEod._DIFFERENCE:
										sodEod.setDifference(new BigDecimal(value));
										break;
									default :
										break;
								}
							}
							i++;
						}
						return sodEod;
					}
		});

		return rawResults.closeableIterator();
	}


	public boolean isReprintMode() {
		return reprintMode;
	}


	public void generateSodEodSummary(SodEod model, boolean isEod ){
		StringBuilder receipt = new StringBuilder();
		String spaces = ReceiptPrinterCtrl.getAdditionalSpace();

		if(model != null){
			if (isReprintMode())
				receipt.append(StringUtil.center("<< REPRINT >>", Config.getPrinterMaxCharPerLine()));

			receipt.append(String.format("%n"));

			if (isEod)
				receipt.append(StringUtil.center("TUTUP HARIAN", Config.getPrinterMaxCharPerLine()));
			else
				receipt.append(StringUtil.center("TUTUP SHIFT", Config.getPrinterMaxCharPerLine()));

			receipt.append(String.format("%n"));

			if (!isEod){
				receipt.append(String.format("Shift       : %-15d%n", model.getShiftNo()));
				receipt.append(String.format("Kasir       : %-15s%n", model.getUser().getUsername()));
			}

			receipt.append(String.format("Date        : %-15s%n%n", DateUtil.format(model.getStart())));

			receipt.append(String.format(receiptPrinterCtrl.generateLine()));
			receipt.append(String.format("%s%n", "SALES"));
			receipt.append(String.format(receiptPrinterCtrl.generateSmallLine()));

			receipt.append(String.format(spaces+"%26s  %12s%n", "Cash Income", CurrencyUtil.format(model.getCashIncome(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Net Sales", CurrencyUtil.format(model.getNetSales(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Net Return", CurrencyUtil.format(model.getNetReturn(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "PPN", CurrencyUtil.format(model.getPpn(), false)));

			receipt.append(String.format(receiptPrinterCtrl.generateSmallLine()));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Total Sales", CurrencyUtil.format(model.getSales(), false)));

			receipt.append(String.format(receiptPrinterCtrl.generateLine()));
			receipt.append(String.format("%n"));
			receipt.append(String.format(receiptPrinterCtrl.generateLine()));
			receipt.append(String.format("%s%n", "ACTUAL"));
			receipt.append(String.format(receiptPrinterCtrl.generateSmallLine()));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Cash", CurrencyUtil.format(model.getCashSales(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Cash Income", CurrencyUtil.format(model.getCashIncome(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Debit", CurrencyUtil.format(model.getDebit(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Credit", CurrencyUtil.format(model.getCredit(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Voucher", CurrencyUtil.format(model.getVoucher(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Discount", CurrencyUtil.format(model.getDisc(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Special Discount", CurrencyUtil.format(model.getSpecialDisc(), false)));
			receipt.append(String.format(receiptPrinterCtrl.generateSmallLine()));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Total Actual", CurrencyUtil.format(model.getActual(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Variance", CurrencyUtil.format(model.getDifference(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Variance Paid", CurrencyUtil.format(model.getPaidDifference(), false)));
			receipt.append(String.format(spaces+"%26s  %12s%n", "Receipt Count", model.getReceiptCount()));
			receipt.append(String.format(receiptPrinterCtrl.generateLine()));
			receipt.append(Config.getEodFooter());
			receipt.append(String.format("%n"));
			receipt.append(String.format(receiptPrinterCtrl.generateSmallLine()));
			receipt.append(String.format("       Print Date : %20s%n", DateUtil.formatDateTime(new Date())));
			receipt.append(String.format("%n%n%n%n%n"));
			receipt.append(String.format("%n%n%n."));
			if (Config.isEnablePrinterTypeLX310()){
				receipt.append(String.format("%n%n%n%n%n"));
			}

			System.out.println(receipt.toString());
			try {
				if (isEod)
					receiptPrinterCtrl.print("TUTUP HARIAN "+DateUtil.format(model.getEnd()), receipt.toString());
				else
					receiptPrinterCtrl.print("TUTUP SHIFT "+model.getShiftNo()+" "+DateUtil.format(model.getEnd()), receipt.toString());

			} catch (PrintException | IOException e) {
				logger.error(e.toString(), e);
				MainApp.showAlertDbError();
			}

//			try {
//				PrintUtil.printText(receipt.toString());
//			} catch (PrintException | IOException e) {
//				e.printStackTrace();
//			}


		}
	}


	public void setReprintMode(boolean reprintMode) {
		this.reprintMode = reprintMode;
	}

	public boolean isCloseDay() {
		return isCloseDay;
	}

	public static void setCloseDay(boolean isCloseDay) {
		SodEodCtrl.isCloseDay = isCloseDay;
	}



}
