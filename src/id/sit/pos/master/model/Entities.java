package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "entities")
public class Entities extends RModel {
	public static final String _NAME = "name";
	public static final String _ADDRESS = "address";
	public static final String _NPWP = "npwp";
	public static final String _NPWP_ADDRESS = "npwp_address";

	@DatabaseField(columnName = _NAME)
	private String name;

	@DatabaseField(columnName = _ADDRESS)
	private String address;

	@DatabaseField(columnName = _NPWP)
	private String npwp;

	@DatabaseField(columnName = _NPWP_ADDRESS)
	private String npwp_address;


	public Entities(){

	}

	public Entities(Integer id){
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getNpwp() {
		return npwp;
	}


	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}


	public String getNpwp_address() {
		return npwp_address;
	}


	public void setNpwp_address(String npwp_address) {
		this.npwp_address = npwp_address;
	}


}
