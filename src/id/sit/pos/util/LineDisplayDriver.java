/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.sit.pos.util;

import jpos.events.DirectIOEvent;
import jpos.events.StatusUpdateEvent;

import org.apache.log4j.Logger;

import jpos.*;

/**
 *
 * @author dean
 */
public class LineDisplayDriver implements jpos.LineDisplayConst,
                                          jpos.events.StatusUpdateListener,
                                          jpos.events.DirectIOListener {

	private static Logger logger = Logger.getLogger(LineDisplayDriver.class.getName());
	private LineDisplay ld;
    private String[] text = new String[2];
    private boolean[] isMarqueeOn = new boolean[2];
    private int[] marqueeWindow = new int[2];
    private boolean isLocked = false;

    public void initDevice() {
        try {
            ld = new LineDisplay();
            ld.open("HP_POLE_DISPLAY");
            ld.addDirectIOListener(this);
            ld.addStatusUpdateListener(this);
            ld.claim(1000);
            ld.setDeviceEnabled(true);
        } catch (JposException ex) {
            ex.printStackTrace();
            logger.info(ex);

        }
    }

    public void stopDevice() {
        try {
            ld.removeDirectIOListener(this);
            ld.removeStatusUpdateListener(this);
            ld.close();
//        } catch (JposException ex) {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setText(int row, String text) {
        this.text[row] = text;
    }

    public void clear() {
        try {
            if (isMarqueeOn[0]) stopMarquee(0);
            if (isMarqueeOn[1]) stopMarquee(1);

            ld.clearText();
//        } catch (JposException ex) {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void startMarquee(int row, int direction, int unitWait, int repeatWait) {
        if (isMarqueeOn[row])
            return;

        try {
            ld.createWindow(row, 0, 1, 20, 1, Math.max(20, text[row].length() + 1));
            ld.setMarqueeType(DISP_MT_INIT);
            ld.setMarqueeFormat(DISP_MF_WALK);
            ld.setMarqueeUnitWait(unitWait);
            ld.setMarqueeRepeatWait(repeatWait);
            ld.displayTextAt(0, 0, text[row], DISP_DT_NORMAL);
            ld.setMarqueeType(direction);
            marqueeWindow[row] = ld.getCurrentWindow();

            isMarqueeOn[row] = true;
//        } catch (JposException ex) {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void stopMarquee(int row) {
        if (!isMarqueeOn[row])
            return;

        try {
            isMarqueeOn[row] = false;
            ld.setCurrentWindow(marqueeWindow[row]);
            ld.setMarqueeType(DISP_MT_NONE);
            ld.destroyWindow();
            ld.refreshWindow(0);
//        } catch (JposException ex) {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setBrigthness(int perc) {
        try {
            ld.setDeviceBrightness(perc);
//        } catch (JposException ex) {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void display(int row) {
        try {
            ld.displayTextAt(row, 0, text[row], DISP_MT_NONE);
//        } catch (JposException ex) {
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void statusUpdateOccurred(StatusUpdateEvent sue) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void directIOOccurred(DirectIOEvent dioe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	public boolean isLocked() {
		return isLocked;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}


}
