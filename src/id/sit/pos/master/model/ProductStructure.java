package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "product_structures")
public class ProductStructure extends RModel {
	public static final String _SKU_ID = "sku_id";
	public static final String _PARENT_SKU_ID = "parent_id";
	public static final String _CONVERTION = "quantity";

	@DatabaseField(columnName = _SKU_ID, foreign = true, foreignAutoRefresh = true)
	private Sku sku;

	@DatabaseField(columnName = _PARENT_SKU_ID, foreign = true)
	private Sku parentSku;

	@DatabaseField(columnName = _CONVERTION)
	private int convertion;

	public Sku getSku() {
		return sku;
	}

	public void setSku(Sku sku) {
		this.sku = sku;
	}

	public Sku getParentSku() {
		return parentSku;
	}

	public void setParentSku(Sku parentSku) {
		this.parentSku = parentSku;
	}

	public int getConvertion() {
		return convertion;
	}

	public void setConvertion(int convertion) {
		this.convertion = convertion;
	}
}
