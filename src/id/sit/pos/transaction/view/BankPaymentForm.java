package id.sit.pos.transaction.view;

import java.math.BigDecimal;

import id.r.engine.controller.RDao;
import id.r.engine.view.RForm;
import id.sit.pos.config.Config;
import id.sit.pos.transaction.model.BankPayment;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.MSRUtil;
import id.sit.pos.util.StringUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class BankPaymentForm extends RForm<BankPayment> {
	@FXML
	private Label message;

	@FXML
	private TextField bankName;

	@FXML
	private TextField accountNumber;

	@FXML
	private TextField accountName;

	@FXML
	private TextField cardNumber;

	@FXML
	private Label remainingToBePaid;

	@FXML
	private TextField additionalCharge;

	@FXML
	private TextField paymentAmt;

	private BankPayment.Type type;

	private BigDecimal toBePaidAmt;

	public BankPaymentForm() {
		super();
		if (type == BankPayment.Type.CREDIT)
			setTitle(Config.getMessage("trans_credit_title"));
		else if (type == BankPayment.Type.DEBIT)
			setTitle(Config.getMessage("trans_debit_title"));
		else if (type == BankPayment.Type.TRANSFER)
			setTitle(Config.getMessage("trans_transfer_title"));
	}

	@FXML
	public void initialize() {
		super.initialize();
		bankName.requestFocus();
		setAsNameFields(bankName, accountName);
		setAsNumericFields(accountNumber, cardNumber);
		setAsCurrencyFields(additionalCharge, paymentAmt);
		setAsFormFields(bankName, accountName, accountNumber, cardNumber, additionalCharge, paymentAmt);
		remainingToBePaid.setText(CurrencyUtil.format(toBePaidAmt));

		additionalCharge.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
		        if (!newPropertyValue) { //onfocus
		        	if (StringUtil.isEmpty(additionalCharge.getText()))
		        		additionalCharge.setText("0");
		        	remainingToBePaid.setText(CurrencyUtil.format(toBePaidAmt.add(CurrencyUtil.parse(additionalCharge.getText()))));
		        }
			});
	}

	@Override
	public RDao<BankPayment> initRDao() {
		return new RDao<BankPayment>(BankPayment.class);
	}

	@Override
	public BankPayment initModel() {
		return new BankPayment();
	}

	@Override
	public void updateForm(BankPayment model) {
		bankName.setText(model.getBankName());
		accountNumber.setText(model.getAccountNo());
		accountName.setText(model.getAccountName());
		cardNumber.setText(model.getCardNumber());
		if (model.getAdditionalCharge() != null)
			additionalCharge.setText(model.getAdditionalCharge().toString());

		if (model.getToBePaidAmt() != null)
			remainingToBePaid.setText(CurrencyUtil.format(model.getToBePaidAmt()));

		if (model.getPaymentAmt() != null)
			paymentAmt.setText(model.getPaymentAmt().toString());
	}

	@Override
	public BankPayment fetchFormData() {
		model.setType(type);
		model.setBankName(bankName.getText());
		model.setAccountNo(accountNumber.getText());
		model.setAccountName(accountName.getText());
		model.setCardNumber(cardNumber.getText());
		model.setAdditionalCharge(CurrencyUtil.parse(additionalCharge.getText()));
		model.setToBePaidAmt(toBePaidAmt);
		if (paymentAmt.getText() == null || paymentAmt.getText().isEmpty())
			message.setText(Config.getMessage("trans_payment_error_amount"));
		else
			model.setPaymentAmt(CurrencyUtil.parse(paymentAmt.getText()));

		return model;
	}

	@Override
	public void afterSave() {
		super.afterSave();
		getStage().close();
	}

	// Generated code
	public BankPayment.Type getType() {
		return type;
	}

	public void setType(BankPayment.Type type) {
		this.type = type;
	}

	public BigDecimal getToBePaidAmt() {
		return toBePaidAmt;
	}

	public void setToBePaidAmt(BigDecimal toBePaidAmt) {
		this.toBePaidAmt = toBePaidAmt;
	}
}
