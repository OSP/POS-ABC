package id.sit.pos.master.view;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.CloseableIterator;

import id.r.engine.controller.RDao;
import id.r.engine.model.SearchParam;
import id.r.engine.view.RGridChooser;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.master.controller.ProductCtrl;
import id.sit.pos.master.model.Product;
import id.sit.pos.master.model.ProductSize;
import id.sit.pos.master.model.ProductStock;
import id.sit.pos.master.model.Size;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import org.comtel2000.keyboard.control.VkProperties;
import id.sit.pos.master.model.MClass;

public class ProductBrowser extends RGridChooser<Product> implements VkProperties {

	@FXML
	private TextField barcodeSc;

	@FXML
	private TextField articleSc;

	@FXML
	private TextField nameSc;

	@FXML
	private TextField colorCodeSc;

	@FXML
	private TextField sizeSc;

	@FXML
	private TableColumn<Product, String> barcodeClm;

	@FXML
	private TableColumn<Product, String> articleClm;

	@FXML
	private TableColumn<Product, String> nameClm;

	@FXML
	private TableColumn<Product, String> colorCodeClm;

	@FXML
	private TableColumn<Product, String> sizeClm;

	@FXML
	private TableColumn<Product, Integer> stockClm;

	@FXML
	private TableColumn<Product, String> unitClm;

	@FXML
	private TableColumn<Product, Integer> convertionClm;

	@FXML
	private TableColumn<Product, String> sellingPriceClm;

	ProductCtrl productCtrl = new ProductCtrl();
	MClass	mclass = new MClass();

	private String transactionNo;


	// TODO
	public ProductBrowser() {
		super();
		getIcons().add(new Image(Config.getLogoPath()));
		setTitle(Config.getMessage("product_b_title"));

		searchService = new Service<ObservableList<Product>>() {

			@Override
			protected Task<ObservableList<Product>> createTask() {
				return new Task<ObservableList<Product>>() {
					@Override
					protected ObservableList<Product> call() throws InterruptedException, SQLException {
						updateMessage(Config.getMessage("common_searching"));
						startProgressBar();
						ObservableList<Product> result = FXCollections.observableArrayList();;

						CloseableIterator<Product> iterator = productCtrl.getProducts(nameSc.getText(), barcodeSc.getText(), articleSc.getText(), sizeSc.getText(), colorCodeSc.getText(), transactionNo);
						iterator.forEachRemaining(product->{
							result.add(product);
						});

						iterator.closeQuietly();
						if (result.isEmpty())
							updateMessage("No data match with search criteria.");
						else
							updateMessage("");

						endProgressBar();
						return result;
					}
				};
			}

			@Override
			public boolean cancel() {
				if (super.cancel()) {
					endProgressBar();
					return true;
				}
				return false;
			}
		};

		searchService.setOnFailed(value -> {
			MainApp.showAlertDbError();
			logger.error(value.getSource().getException().toString(), value.getSource().getException());
		});
	}

	@FXML
	@Override
	public void initialize() {
		super.initialize();

		nameSc.textProperty().addListener((ov, oldValue, newValue)->{
			nameSc.setText(newValue.toUpperCase());
		});

		if (Config.isEnableProductName())
			setAsSearchCriteriaFields(nameSc);
		else {
			nameSc.setVisible(false);
			nameSc.setManaged(false);
		}

		setAsSearchCriteriaFields(barcodeSc, articleSc);
		if (Config.isEnableColour())
			setAsSearchCriteriaFields(colorCodeSc);
		else {
			colorCodeSc.setVisible(false);
			colorCodeSc.setManaged(false);
		}

		if (Config.isEnableSize())
			setAsSearchCriteriaFields(sizeSc);
		else {
			sizeSc.setVisible(false);
			sizeSc.setManaged(false);
		}

		// Setup column
		if(Config.isAbc()){
			barcodeClm.setCellValueFactory(new PropertyValueFactory<Product, String>("barcode"));

			unitClm.setVisible(false);
			convertionClm.setVisible(false);
			sellingPriceClm.setVisible(false);

		} else {
			barcodeClm.setCellValueFactory(new PropertyValueFactory<Product, String>("barcodeSku"));
			unitClm.setCellValueFactory(new PropertyValueFactory<Product, String>("unit"));
			convertionClm.setCellValueFactory(new PropertyValueFactory<Product, Integer>("convertion"));
			sellingPriceClm.setCellValueFactory(new PropertyValueFactory<Product, String>("sellingPrice"));
		}

		articleClm.setCellValueFactory(new PropertyValueFactory<Product, String>("article"));

		if (Config.isEnableProductName())
			nameClm.setCellValueFactory(new PropertyValueFactory<Product, String>("shortName"));
		else {
			nameClm.setVisible(false);
		}

		if (Config.isEnableColour())
			colorCodeClm.setCellValueFactory(new PropertyValueFactory<Product, String>("colourCode"));
//			colorCodeClm.setCellValueFactory(new PropertyValueFactory<Product, String>("colourName"));
		else
			colorCodeClm.setVisible(false);

		if (Config.isEnableSize())
			sizeClm.setCellValueFactory(new PropertyValueFactory<Product, String>("size"));
		else
			sizeClm.setVisible(false);

		stockClm.setCellValueFactory(new PropertyValueFactory<Product, Integer>("stock"));

		setCellAlignment();

		if (Config.isEnableOnScreenKeyboard()){
			articleSc.getProperties().put(VK_TYPE, VK_TYPE_NUMERIC);
			barcodeSc.getProperties().put(VK_TYPE, VK_TYPE_NUMERIC);
		}

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				nameSc.requestFocus();
			}
		});

		search();
	}

	private void setCellAlignment(){
		sellingPriceClm.setStyle("-fx-alignment: top-right; -fx-padding: 0,10,0,0");
		convertionClm.setStyle("-fx-alignment: top-right");
		unitClm.setStyle("-fx-alignment: top-center");
		sizeClm.setStyle("-fx-alignment: top-right; -fx-padding: 0,20,0,0");
	}

	/**
	 * Unused
	 */
	@Deprecated @Override
	public SearchParam[] getSearchParams() {
		List<SearchParam> searchParams = new ArrayList<SearchParam>();

		searchParams.add(new SearchParam(Product._BARCODE, barcodeSc.getText()));
		searchParams.add(new SearchParam(Product._ARTICLE, articleSc.getText()));

		if (Config.isEnableProductName())
			searchParams.add(new SearchParam(Product._SHORT_NAME, nameSc.getText()));
		if (Config.isEnableColour())
			searchParams.add(new SearchParam(Product._COLOUR_CODE, colorCodeSc.getText()));

		if (Config.isAbc())
			searchParams.add(new SearchParam(ProductStock.class, ProductStock._WAREHOUSE_ID, SettingCtrl.getBranchId(),
					new Class<?>[] { ProductSize.class }));
		else
			searchParams
					.add(new SearchParam(ProductStock.class, ProductStock._WAREHOUSE_ID, SettingCtrl.getBranchId()));

		if (Config.isHideEmptyStock()) {
			if (Config.isAbc())
				searchParams.add(new SearchParam(ProductStock.class, ProductStock._AVAILABLE_QTY, 0,
						SearchParam.Operator.GT, new Class<?>[] { ProductSize.class }));
			else
				searchParams.add(
						new SearchParam(ProductStock.class, ProductStock._AVAILABLE_QTY, 0, SearchParam.Operator.GT));
		}

		if (Config.isEnableSize()) {
			searchParams.add(
					new SearchParam(Size.class, Size._NAME, sizeSc.getText(), new Class<?>[] { ProductSize.class }));
		}
		return searchParams.toArray(new SearchParam[searchParams.size()]);
	}

	@Override
	protected RDao<Product> initDao() {
		return new RDao<>(Product.class);
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}
}
