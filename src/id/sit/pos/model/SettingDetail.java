package id.sit.pos.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "global_settings")
public class SettingDetail extends RModel {

	public static final String _CATEGORY = "category";
	public static final String _NAME = "name";
	public static final String _DESCRIPTION = "description";
	public static final String _ORDER = "order";
	public static final String _IS_ACTIVE = "is_active";

	@DatabaseField(columnName = _CATEGORY)
	private String category;

	@DatabaseField
	private String name;

	@DatabaseField
	private String description;

	@DatabaseField
	private Integer order;

	@DatabaseField(columnName = _IS_ACTIVE)
	private Boolean isActive;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getValue() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
}
