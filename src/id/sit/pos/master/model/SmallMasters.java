package id.sit.pos.master.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SmallMasters {
//	@Expose(serialize = true, deserialize = true)
//	private Member[] members;

	@Expose(serialize = true, deserialize = true)
	private Brand[] brands;

	@Expose(serialize = true, deserialize = true)
	private Colour[] colours;

	@Expose(serialize = true, deserialize = true)
	@SerializedName("m_classes")
	private MClass[] mClasses;

	@Expose(serialize = true, deserialize = true)
	private Warehouse[] warehouses;

	@Expose(serialize = true, deserialize = true)
	@SerializedName("sizes")
	private SizeCategory[] sizeCategories;

	@Expose(serialize = true, deserialize = true)
	@SerializedName("size_details")
	private Size[] sizes;

//	public Member[] getMembers() {
//		return members;
//	}

	public Brand[] getBrands() {
		return brands;
	}

	public Colour[] getColours() {
		return colours;
	}

	public MClass[] getmClasses() {
		return mClasses;
	}

	public Warehouse[] getWarehouses() {
		return warehouses;
	}

	public SizeCategory[] getSizeCategories() {
		return sizeCategories;
	}

	public Size[] getSizes() {
		return sizes;
	}
}
