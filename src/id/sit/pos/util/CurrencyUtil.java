package id.sit.pos.util;

import java.math.BigDecimal;
import java.text.NumberFormat;

import id.sit.pos.config.Config;

/**
 * Collection of Currency tools
 * @author Rindi J Ismail
 *
 */
public class CurrencyUtil {
	/**
	 * format BigDecimal to string using currency symbols
	 * @param money
	 * @return
	 */
	public static String format(BigDecimal money) {
		if (money != null) {
			NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Config.getCurrencyLocale());
			return currencyFormat.format(money.toBigInteger());
		}
		return null;
	}

	/**
	 * Format BigDecimal to string with or without currency symbols
	 * @param money
	 * @param withSymbol
	 * @return
	 */
	public static String format(BigDecimal money, boolean withSymbol) {
		if (money == null)
			return null;

		if (withSymbol)
			return format(money);
		else {
			NumberFormat currencyFormat = NumberFormat.getNumberInstance(Config.getCurrencyLocale());
			return currencyFormat.format(money.toBigInteger());
		}
	}

	/**
	 * Add digit separator every 3 digit using . or , (depend on locale)
	 * @param money
	 * @return
	 */
	public static String format(String money) {
		if (money == null || money.length()<=3)
			return money;

		StringBuilder result = new StringBuilder("");
		money = clearFormat(money);
		int end = money.length();
		while (end > 3) {
			int start = end - 3;
			result.insert(0, getDigitSeparator().concat(money.substring(start, end)));
			money = money.substring(0, start);
			end = money.length();
		}

		result.insert(0, money);
		return result.toString();
	}

	/**
	 * Clear currency format to number only string without removing decimal
	 * @param money
	 * @return
	 */
	public static String clearFormat(String money) {
		if (Config.getCurrencyLocale().getCountry() == "ID")
			return money.replaceAll("[.]", "");
		else
			return money.replaceAll(getDigitSeparator(), "");
	}

	/**
	 * Parse string formatted currency to BigDecimal
	 * @param money
	 * @return
	 */
	public static BigDecimal parse(String money) {
		if (StringUtil.isEmpty(money))
			return null;
		money = clearFormat(money);
		return new BigDecimal(money);
	}

	/**
	 * get digit separator for currently used format
	 * @return
	 */
	public static String getDigitSeparator() {
		String separator = ",";

		if (Config.getCurrencyLocale().getCountry() == "ID")
			separator = ".";

		return separator;
	}
}
