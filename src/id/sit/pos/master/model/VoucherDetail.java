package id.sit.pos.master.model;

import java.math.BigDecimal;
import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "voucher_details")
public class VoucherDetail extends RModel{
	public static final String _VOUCHER_ID = "voucher_id";
	public static final String _CODE = "voucher_code";
	public static final String _GENERATED_DATE = "generated_date";
	public static final String _AVAILABLE = "available";
	public static final String _TRANSACTION_NO = "order_no";
	public static final String _REDEEM_AMT = "reedem_amount";

	@DatabaseField(columnName = _VOUCHER_ID, foreign = true, foreignAutoRefresh = true)
	private Voucher voucher;

	@DatabaseField(columnName = _CODE)
	private String code;

	@DatabaseField(columnName = _GENERATED_DATE)
	private Date generatedDate;

	@DatabaseField(columnName = _AVAILABLE)
	private boolean isAvailable;

	@DatabaseField(columnName = _TRANSACTION_NO)
	private String transactionNo;

	@DatabaseField(columnName = _REDEEM_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal redeemAmt;

	public Voucher getVoucher() {
		return voucher;
	}

	public void setVoucher(Voucher voucher) {
		this.voucher = voucher;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getGeneratedDate() {
		return generatedDate;
	}

	public void setGeneratedDate(Date generatedDate) {
		this.generatedDate = generatedDate;
	}

	public boolean getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public BigDecimal getRedeemAmt() {
		return redeemAmt;
	}

	public void setRedeemAmt(BigDecimal redeemAmt) {
		this.redeemAmt = redeemAmt;
	}
}
