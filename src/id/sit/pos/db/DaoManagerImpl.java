package id.sit.pos.db;

import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;

import id.sit.pos.customer.model.Member;
import id.sit.pos.customer.model.CustomerCashback;
import id.sit.pos.customer.model.MemberLevel;
import id.sit.pos.master.model.*;
import id.sit.pos.model.*;
import id.sit.pos.transaction.model.*;
import id.sit.pos.user.model.AuditTrail;
import id.sit.pos.user.model.Role;
import id.sit.pos.user.model.Session;
import id.sit.pos.user.model.User;

public class DaoManagerImpl {

	private static Dao<Setting, Integer> settingDao;
	private static Dao<SettingDetail, Integer> settingDetailDao;
	private static Dao<SyncHistory, Integer> syncHistoryDao;
	private static Dao<AuditTrail, Integer> auditTrailDao;

	private static Dao<User, Integer> userDao;
	private static Dao<Role, Integer> roleDao;
	private static Dao<Session, Integer> sessionDao;
	private static Dao<EDC, Integer> edcDao;
	private static Dao<Member, Integer> memberDao;
	private static Dao<MemberLevel, Integer> memberLevelDao;
	private static Dao<CustomerCashback, Integer> customerCashbackDao;
	private static Dao<Brand, Integer> brandDao;
	private static Dao<Colour, Integer> colourDao;
	private static Dao<MClass, Integer> mClassDao;
	private static Dao<Size, Integer> sizeDao;
	private static Dao<SizeCategory, Integer> sizeCategoryDao;
	private static Dao<Product, Integer> productDao;
	private static Dao<ProductSize, Integer> productSizeDao;
	private static Dao<ProductStock, Integer> productStockDao;
	private static Dao<ProductStructure, Integer> productStructureDao;
	private static Dao<ProductBranchSetting, Integer> productBranchSettingDao;
	private static Dao<ProductPrice, Integer> productPriceDao;
	private static Dao<ProductPriceDetail, Integer> productPriceDetailDao;
	private static Dao<Promo, Integer> promoDao;
	private static Dao<PromoItem, Integer> promoItemDao;
	private static Dao<PromoPrizeItem, Integer> promoPrizeItemDao;
	private static Dao<Warehouse, Integer> warehouseDao;
	private static Dao<Voucher, Integer> voucherDao;
	private static Dao<VoucherDetail, Integer> voucherDetailDao;
	private static Dao<Transaction, Integer> transactionDao;
	private static Dao<TransactionItem, Integer> transactionItemDao;
	private static Dao<CancelledItem, Integer> cancelledItemDao;
	private static Dao<BankPayment, Integer> bankPaymentDao;
	private static Dao<EdcPayment, Integer> edcPaymentDao;
	private static Dao<MutationHistory, Integer> mutationHistoryDao;
	private static Dao<AccountReceivable, Integer> accountReceivableDao;
	private static Dao<SodEod, Integer> sodEodDao;
	private static Dao<Sku, Integer> skuDao;
	private static Dao<Unit, Integer> unitDao;
	private static Dao<PosMachine, Integer> posMachineDao;
	private static Dao<Entities, Integer> entitiesDao;
	private static Dao<Ads, Integer> adsDao;

	public static Dao<Setting, Integer> getSettingDao() throws SQLException {
		if (settingDao == null) {
			settingDao = DaoManager.createDao(DbConnector.getLocalConnection(), Setting.class);
		}
		return settingDao;
	}

	public static Dao<SettingDetail, Integer> getSettingDetailDao() throws SQLException {
		if (settingDetailDao == null) {
			settingDetailDao = DaoManager.createDao(DbConnector.getRemoteConnection(), SettingDetail.class);
		}
		return settingDetailDao;
	}

	public static Dao<Session, Integer> getSessionDao() throws SQLException {
		if (sessionDao == null) {
			sessionDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Session.class);
		}
		return sessionDao;
	}

	public static Dao<EDC, Integer> getEdcDao() throws SQLException {
		if (edcDao == null) {
			edcDao = DaoManager.createDao(DbConnector.getRemoteConnection(), EDC.class);
		}
		return edcDao;
	}

	public static Dao<SyncHistory, Integer> getSyncHistoryDao() throws SQLException {
		if (syncHistoryDao == null) {
			syncHistoryDao = DaoManager.createDao(DbConnector.getLocalConnection(), SyncHistory.class);
		}
		return syncHistoryDao;
	}

	public static Dao<AuditTrail, Integer> getAuditTrailDao() throws SQLException {
		if (auditTrailDao == null) {
			auditTrailDao = DaoManager.createDao(DbConnector.getLocalConnection(), AuditTrail.class);
		}
		return auditTrailDao;
	}

	public static Dao<Member, Integer> getMemberDao() throws SQLException {
		if (memberDao == null) {
			memberDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Member.class);
		}
		return memberDao;
	}

	public static Dao<MemberLevel, Integer> getMemberLevelDao() throws SQLException {
		if (memberLevelDao == null)
			memberLevelDao = DaoManager.createDao(DbConnector.getRemoteConnection(), MemberLevel.class);

		return memberLevelDao;
	}

	public static Dao<CustomerCashback, Integer> getCustomerCashbackDao() throws SQLException {
		if (customerCashbackDao == null)
			customerCashbackDao = DaoManager.createDao(DbConnector.getRemoteConnection(), CustomerCashback.class);

		return customerCashbackDao;
	}

	public static Dao<User, Integer> getUserDao() throws SQLException {
		if (userDao == null) {
			userDao = DaoManager.createDao(DbConnector.getRemoteConnection(), User.class);
		}
		return userDao;
	}

	public static Dao<Role, Integer> getRoleDao() throws SQLException {
		if (roleDao == null) {
			roleDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Role.class);
		}
		return roleDao;
	}

	public static Dao<Brand, Integer> getBrandDao() throws SQLException {
		if (brandDao == null) {
			brandDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Brand.class);
		}
		return brandDao;
	}

	public static Dao<Colour, Integer> getColourDao() throws SQLException {
		if (colourDao == null) {
			colourDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Colour.class);
		}
		return colourDao;
	}

	public static Dao<MClass, Integer> getMClassDao() throws SQLException {
		if (mClassDao == null) {
			mClassDao = DaoManager.createDao(DbConnector.getRemoteConnection(), MClass.class);
		}
		return mClassDao;
	}

	public static Dao<Product, Integer> getProductDao() throws SQLException {
		if (productDao == null) {
			productDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Product.class);
		}
		return productDao;
	}

	public static Dao<ProductBranchSetting, Integer> getProductBranchSettingDao() throws SQLException {
		if (productBranchSettingDao == null) {
			productBranchSettingDao = DaoManager.createDao(DbConnector.getRemoteConnection(),
					ProductBranchSetting.class);
		}
		return productBranchSettingDao;
	}

	public static Dao<ProductSize, Integer> getProductSizeDao() throws SQLException {
		if (productSizeDao == null) {
			productSizeDao = DaoManager.createDao(DbConnector.getRemoteConnection(), ProductSize.class);
		}
		return productSizeDao;
	}

	public static Dao<ProductStock, Integer> getProductStockDao() throws SQLException {
		if (productStockDao == null) {
			productStockDao = DaoManager.createDao(DbConnector.getRemoteConnection(), ProductStock.class);
		}
		return productStockDao;
	}

	public static Dao<ProductStructure, Integer> getProductStructureDao() throws SQLException {
		if (productStructureDao == null) {
			productStructureDao = DaoManager.createDao(DbConnector.getRemoteConnection(), ProductStructure.class);
		}
		return productStructureDao;
	}

	public static Dao<ProductPrice, Integer> getProductPriceDao() throws SQLException {
		if (productPriceDao == null) {
			productPriceDao = DaoManager.createDao(DbConnector.getRemoteConnection(), ProductPrice.class);
		}
		return productPriceDao;
	}

	public static Dao<ProductPriceDetail, Integer> getProductPriceDetailDao() throws SQLException {
		if (productPriceDetailDao == null) {
			productPriceDetailDao = DaoManager.createDao(DbConnector.getRemoteConnection(), ProductPriceDetail.class);
		}
		return productPriceDetailDao;
	}

	public static Dao<Size, Integer> getSizeDao() throws SQLException {
		if (sizeDao == null) {
			sizeDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Size.class);
		}
		return sizeDao;
	}

	public static Dao<SizeCategory, Integer> getSizeCategoryDao() throws SQLException {
		if (sizeCategoryDao == null) {
			sizeCategoryDao = DaoManager.createDao(DbConnector.getRemoteConnection(), SizeCategory.class);
		}
		return sizeCategoryDao;
	}

	public static Dao<Warehouse, Integer> getWarehouseDao() throws SQLException {
		if (warehouseDao == null) {
			warehouseDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Warehouse.class);
		}
		return warehouseDao;
	}

	public static Dao<Voucher, Integer> getVoucherDao() throws SQLException {
		if (voucherDao == null) {
			voucherDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Voucher.class);
		}
		return voucherDao;
	}

	public static Dao<VoucherDetail, Integer> getVoucherDetailDao() throws SQLException {
		if (voucherDetailDao == null) {
			voucherDetailDao = DaoManager.createDao(DbConnector.getRemoteConnection(), VoucherDetail.class);
		}
		return voucherDetailDao;
	}

	public static Dao<Transaction, Integer> getTransactionDao() throws SQLException {
		if (transactionDao == null) {
			transactionDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Transaction.class);
		}
		return transactionDao;
	}

	public static Dao<TransactionItem, Integer> getTransactionItemDao() throws SQLException {
		if (transactionItemDao == null) {
			transactionItemDao = DaoManager.createDao(DbConnector.getRemoteConnection(), TransactionItem.class);
		}
		return transactionItemDao;
	}

	public static Dao<CancelledItem, Integer> getCancelledItemDao() throws SQLException {
		if (cancelledItemDao == null) {
			cancelledItemDao = DaoManager.createDao(DbConnector.getRemoteConnection(), CancelledItem.class);
		}
		return cancelledItemDao;
	}

	public static Dao<BankPayment, Integer> getBankPaymentDao() throws SQLException {
		if (bankPaymentDao == null) {
			bankPaymentDao = DaoManager.createDao(DbConnector.getRemoteConnection(), BankPayment.class);
		}
		return bankPaymentDao;
	}

	public static Dao<EdcPayment, Integer> getEdcPaymentDao() throws SQLException {
		if (edcPaymentDao == null) {
			edcPaymentDao = DaoManager.createDao(DbConnector.getRemoteConnection(), EdcPayment.class);
		}
		return edcPaymentDao;
	}

	public static Dao<MutationHistory, Integer> getMutationHistoryDao() throws SQLException {
		if (mutationHistoryDao == null) {
			mutationHistoryDao = DaoManager.createDao(DbConnector.getRemoteConnection(), MutationHistory.class);
		}
		return mutationHistoryDao;
	}

	public static Dao<AccountReceivable, Integer> getAccountReceivableDao() throws SQLException {
		if (accountReceivableDao == null) {
			accountReceivableDao = DaoManager.createDao(DbConnector.getRemoteConnection(), AccountReceivable.class);
		}
		return accountReceivableDao;
	}

	public static Dao<Promo, Integer> getPromoDao() throws SQLException {
		if (promoDao == null) {
			promoDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Promo.class);
		}
		return promoDao;
	}

	public static Dao<PromoItem, Integer> getPromoItemDao() throws SQLException {
		if (promoItemDao == null) {
			promoItemDao = DaoManager.createDao(DbConnector.getRemoteConnection(), PromoItem.class);
		}
		return promoItemDao;
	}

	public static Dao<PromoPrizeItem, Integer> getPromoPrizeItemDao() throws SQLException {
		if (promoPrizeItemDao == null) {
			promoPrizeItemDao = DaoManager.createDao(DbConnector.getRemoteConnection(), PromoPrizeItem.class);
		}
		return promoPrizeItemDao;
	}

	public static Dao<SodEod, Integer> getSodEodDao() throws SQLException {
		if (sodEodDao == null) {
			sodEodDao = DaoManager.createDao(DbConnector.getRemoteConnection(), SodEod.class);
		}
		return sodEodDao;
	}

	public static Dao<Sku, Integer> getSkuDao() throws SQLException {
		if (skuDao == null) {
			skuDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Sku.class);
		}
		return skuDao;
	}

	public static Dao<Unit, Integer> getUnitDao() throws SQLException {
		if (unitDao == null) {
			unitDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Unit.class);
		}
		return unitDao;
	}

	public static Dao<PosMachine, Integer> getPosMachineDao() throws SQLException {
		if (posMachineDao == null) {
			posMachineDao = DaoManager.createDao(DbConnector.getRemoteConnection(), PosMachine.class);
		}
		return posMachineDao;
	}

	public static Dao<Entities, Integer> getEntitiesDao() throws SQLException {
		if (entitiesDao == null) {
			entitiesDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Entities.class);
		}
		return entitiesDao;
	}

	public static Dao<Ads, Integer> getAdsDao() throws SQLException {
		if (adsDao == null)
			adsDao = DaoManager.createDao(DbConnector.getRemoteConnection(), Ads.class);
		return adsDao;
	}
}
