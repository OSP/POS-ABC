package id.sit.pos.master.model;

import java.math.BigDecimal;
import java.util.Date;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "promotions")
public class Promo extends RModel{
	public static final String _VALID_FROM = "from";
	public static final String _VALID_UNTIL = "to";
	public static final String _MIN_TRANSACTION_AMT = "min";

	@DatabaseField(columnName = "code")
	private String code;

	@DatabaseField(columnName = "name")
	private String name;

	@DatabaseField(columnName = "from")
	private Date validFrom;

	@DatabaseField(columnName = "to")
	private Date validUntil;

	@DatabaseField(columnName = "min_amount")
	private BigDecimal minTransactionAmt;

	@DatabaseField(columnName = "min_qty")
	private BigDecimal minQty;

	@DatabaseField(columnName = "office_id", foreign = true)
	private Warehouse branch;

	@DatabaseField(columnName = "multi")
	private Boolean multi;

	@DatabaseField(columnName = "discount_percent")
	private Float discPct;

	@DatabaseField(columnName = "discount_amount", dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal discAmt;

	@DatabaseField(columnName = "term_and_condition")
	private String termAndCondition;

	@ForeignCollectionField
	private ForeignCollection<PromoItem> promoItems;

	@ForeignCollectionField
	private ForeignCollection<PromoPrizeItem> prizeItems;

	public Promo() {

	}

	public Promo(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Date validTo) {
		this.validUntil = validTo;
	}

	public BigDecimal getMinTransactionAmt() {
		return minTransactionAmt;
	}

	public BigDecimal getMinQty() {
		return minQty;
	}

	public void setMinQty(BigDecimal minQty) {
		this.minQty = minQty;
	}

	public void setMinTransactionAmt(BigDecimal minTransactionAmt) {
		this.minTransactionAmt = minTransactionAmt;
	}

	public Warehouse getBranchId() {
		return branch;
	}

	public void setBranchId(Warehouse branchId) {
		this.branch = branchId;
	}

	public Boolean isMulti() {
		return multi;
	}

	public void setMulti(boolean multi) {
		this.multi = multi;
	}

	public Float getDiscPct() {
		return discPct;
	}

	public void setDiscPct(Float discount) {
		this.discPct = discount;
	}

	public BigDecimal getDiscAmt() {
		return discAmt;
	}

	public void setDiscAmt(BigDecimal discAmt) {
		this.discAmt = discAmt;
	}

	public String getTermAndCondition() {
		return termAndCondition;
	}

	public void setTermAndCondition(String termAndCondition) {
		this.termAndCondition = termAndCondition;
	}

	public ForeignCollection<PromoItem> getPromoItems() {
		return promoItems;
	}

	public void setPromoItems(ForeignCollection<PromoItem> promoItems) {
		this.promoItems = promoItems;
	}

	public ForeignCollection<PromoPrizeItem> getPrizeItems() {
		return prizeItems;
	}

	public void setPrizeItems(ForeignCollection<PromoPrizeItem> prizeItems) {
		this.prizeItems = prizeItems;
	}

}
