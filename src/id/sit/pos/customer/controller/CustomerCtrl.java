package id.sit.pos.customer.controller;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.stmt.QueryBuilder;

import id.r.engine.controller.RDao;
import id.r.engine.model.SearchParam;
import id.r.engine.model.SearchParam.Operator;
import id.sit.pos.customer.model.Member;
import id.sit.pos.customer.model.CustomerCashback;
import id.sit.pos.customer.model.MemberLevel;
import id.sit.pos.master.model.Voucher;
import id.sit.pos.transaction.model.AccountReceivable;

public class CustomerCtrl {
	private RDao<Member> customerDao;
	private RDao<MemberLevel> customerLevelDao;
	private RDao<CustomerCashback> customerCashbackDao;
	private RDao<AccountReceivable> accountReceivableDao = new RDao<>(AccountReceivable.class);

	public List<AccountReceivable> getCurrentAr(Member member) throws SQLException{
		QueryBuilder<AccountReceivable, Integer> qb = accountReceivableDao.getDao().queryBuilder();
		qb.where().eq(AccountReceivable._MEMBER_ID, member.getId()).and()
		.gt(AccountReceivable._OUTSTANDING, BigDecimal.ZERO);
		return accountReceivableDao.getDao().query(qb.prepare());
	}

	public BigDecimal getTotalOutstanding(Member member) throws SQLException{
		long totalOts = accountReceivableDao.getDao().queryRawValue("select sum(outstanding) from account_receivables where member_id = "+member.getId());
		BigDecimal result = new BigDecimal(totalOts);
		return result;
	}


}
