package id.sit.pos.db;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import id.sit.pos.MainApp;
import id.sit.pos.model.Setting;
import id.sit.pos.user.model.Session;

public class DbHelper {
	private static Logger logger = Logger.getLogger(MainApp.class.getName());

	public static void createOrUpdateAllTable() {
		try {
			ConnectionSource connectionSource = DbConnector.getLocalConnection();
			TableUtils.createTableIfNotExists(connectionSource, Session.class);
			TableUtils.createTableIfNotExists(connectionSource, Setting.class);
		} catch (SQLException e) {
			logger.error(e.toString(), e);
		}
	}

	public static void dropAllTable() {
		try {
			ConnectionSource connectionSource = DbConnector.getLocalConnection();
			TableUtils.dropTable(connectionSource, Session.class, false);
			TableUtils.dropTable(connectionSource, Setting.class, false);
		} catch (SQLException e) {
			logger.error(e.toString(), e);
		}
	}
}
