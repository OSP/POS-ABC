package id.sit.pos.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HttpUtil {
//	private final static String USER_AGENT = "Mozilla/5.0";
	private static Logger logger = Logger.getLogger(HttpUtil.class.getName());

	public static String sendGet(String url, String charset, Entry<String, Object>[] bodyParams) throws Exception {

		URL obj = new URL(url+"?"+encodeParams(bodyParams, charset));
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");

		// add request header
//		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Charset", charset);

		logger.log(Level.INFO, "Sending GET request to URL : {0} ", obj);
		int responseCode = con.getResponseCode();
		logger.log(Level.INFO, "Response Code : {0} ", responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		logger.log(Level.INFO, "Response : {0} ", response.toString());
		return response.toString();
	}

	public static String sendPost(String url, String charset, Entry<String, Object>[] bodyParams) throws IOException {

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("POST");
//		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("Accept-Charset", charset);

		String encodedParam = encodeParams(bodyParams, charset);

		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(encodedParam);
		wr.flush();
		wr.close();

		logger.log(Level.INFO, "Sending POST request to URL : {0} \nWith param: {1}", new Object[]{obj, encodedParam});
		int responseCode = con.getResponseCode();
		logger.log(Level.INFO, "Response Code : {0} ", responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		logger.log(Level.INFO, "Response : {0} ", response.toString());
		return response.toString();
	}

	public static String encodeParams (Entry <String, Object>[] bodyParams, String charset) {
		StringBuilder urlParameters = new StringBuilder();

		for(Entry<String, Object> entry : bodyParams) {
		    String key = entry.getKey();
		    String value = entry.getValue().toString();

		    try {
				urlParameters.append(key).append("=").append(URLEncoder.encode(value, charset)).append("&");
			} catch (UnsupportedEncodingException e) {
				logger.log(Level.SEVERE, e.toString(), e);
			}
		}
		urlParameters.deleteCharAt(urlParameters.length()-1);

		return urlParameters.toString();
	}
}
