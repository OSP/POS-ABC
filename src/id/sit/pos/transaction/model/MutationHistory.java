package id.sit.pos.transaction.model;

import java.math.BigDecimal;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.master.model.ProductStock;

@DatabaseTable(tableName = "product_mutation_histories")
public class MutationHistory extends RModel {

	public enum Type {
		Sales
	}

	@DatabaseField(columnName = "product_detail_id", foreign = true)
	private ProductStock productStock;

	@DatabaseField(columnName = "product_mutation_detail_id", foreign = true)
	private TransactionItem transactionItem;

	@DatabaseField(columnName = "old_quantity")
	private Integer oldQty;

	@DatabaseField(columnName = "moved_quantity")
	private Integer movedQty;

	@DatabaseField(columnName = "new_quantity")
	private Integer newQty;

	@DatabaseField(columnName = "mutation_type")
	private Type type;

	@DatabaseField(columnName = "price", dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal price;

	@DatabaseField(columnName = "discount", dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal discAmt;

	public MutationHistory() {

	}

	public ProductStock getProductStock() {
		return productStock;
	}

	public void setProductStock(ProductStock productStock) {
		this.productStock = productStock;
	}

	public TransactionItem getTransactionItem() {
		return transactionItem;
	}

	public void setTransactionItem(TransactionItem transactionItem) {
		this.transactionItem = transactionItem;
	}

	public Integer getOldQty() {
		return oldQty;
	}

	public void setOldQty(Integer oldQty) {
		this.oldQty = oldQty;
	}

	public Integer getMovedQty() {
		return movedQty;
	}

	public void setMovedQty(Integer movedQty) {
		this.movedQty = movedQty;
	}

	public Integer getNewQty() {
		return newQty;
	}

	public void setNewQty(Integer newQty) {
		this.newQty = newQty;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal priceBeforeDisc) {
		this.price = priceBeforeDisc;
	}

	public BigDecimal getDiscAmt() {
		return discAmt;
	}

	public void setDiscAmt(BigDecimal discAmt) {
		this.discAmt = discAmt;
	}
}
