package id.sit.pos.customer.model;

import java.math.BigDecimal;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "member_levels")
public class MemberLevel extends RModel{
	public static final String _LEVEL = "level";
	public static final String _DESCRIPTION = "description";
	public static final String _MIN_AMT = "minimum_amount";
	public static final String _DISC_PCT = "discount_percent";
	public static final String _IS_CREDIT = "is_credit";

	@DatabaseField(columnName = _LEVEL)
	private Integer level;

	@DatabaseField(columnName = _DESCRIPTION)
	private String desc;

	@DatabaseField(columnName = _MIN_AMT)
	private BigDecimal minAmt;

	@DatabaseField(columnName = _DISC_PCT)
	private Float discPct;

	@DatabaseField(columnName = _IS_CREDIT)
	private boolean isCredit;

	public MemberLevel() {

	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String name) {
		this.desc = name;
	}

	public BigDecimal getMinAmt() {
		return minAmt;
	}

	public void setMinAmt(BigDecimal minAmt) {
		this.minAmt = minAmt;
	}

	public Float getDiscPct() {
		return discPct;
	}

	public void setDiscPct(Float discPct) {
		this.discPct = discPct;
	}

	public boolean isCredit() {
		return isCredit;
	}

	public void setCredit(boolean isCredit) {
		this.isCredit = isCredit;
	}

}
