package id.r.engine.model;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

public abstract class RModel {
	public static final String _ID = "id";
	public static final String _CREATED_AT = "created_at";
	public static final String _UPDATED_AT = "updated_at";
	public static final String _VERSION = "version";

	@DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
	protected Integer id;

	@DatabaseField(columnName = _CREATED_AT)
	protected Date createdAt;

	@DatabaseField(columnName = _UPDATED_AT)
	protected Date updatedAt;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
