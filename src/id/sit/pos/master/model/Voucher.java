package id.sit.pos.master.model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "vouchers")
public class Voucher extends RModel{

	public enum Type {
		DiscountVoucher, ArVoucher, Cashback
	}

	public static final String _NAME = "name";
	public static final String _TYPE = "type";
	public static final String _VALID_FROM = "valid_from";
	public static final String _VALID_UNTIL = "valid_until";
	public static final String _MIN_TRANSSACTION_AMT = "min_amount";
	public static final String _DISC_PCT = "discount_percent";
	public static final String _DISC_AMT = "discount_amount";
	public static final String _MAX_VOUCHER_PCS = "max_voucher";
	public static final String _MAX_VOUCHER_AMT = "max_voucher_amt";
	public static final String _IS_MULTIPLE = "is_multiple";
	public static final String _VALID_BRANCH = "office_id";
	public static final String _TERM = "term";

	@DatabaseField
	private String name;

	@DatabaseField(columnName = _TYPE)
	private Type type;

	@DatabaseField(columnName = _VALID_FROM)
	private Date validFrom;

	@DatabaseField(columnName = _VALID_UNTIL)
	private Date validUntil;

	@DatabaseField(columnName = _MIN_TRANSSACTION_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal minTransactionAmt;

	@DatabaseField(columnName = _DISC_PCT)
	private Float discountPct;

	@DatabaseField(columnName = _DISC_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal discountAmt;

	@DatabaseField(columnName = _MAX_VOUCHER_PCS)
	private Integer maxVoucherPcs;

	@DatabaseField(columnName = _MAX_VOUCHER_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal maxVoucherAmt;

	@DatabaseField(columnName = _IS_MULTIPLE)
	private boolean isMultiple;

	@DatabaseField(columnName = _VALID_BRANCH, foreign = true)
	private Warehouse validBranch;

	@DatabaseField(columnName = _TERM)
	private String term;

	@ForeignCollectionField
	private Collection<VoucherDetail> details;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}

	public BigDecimal getMinTransactionAmt() {
		return minTransactionAmt;
	}

	public void setMinTransactionAmt(BigDecimal minTransactionAmt) {
		this.minTransactionAmt = minTransactionAmt;
	}

	public Float getDiscountPct() {
		return discountPct;
	}

	public void setDiscountPct(Float discountPct) {
		this.discountPct = discountPct;
	}

	public BigDecimal getDiscountAmt() {
		return discountAmt;
	}

	public void setDiscountAmt(BigDecimal discountAmt) {
		this.discountAmt = discountAmt;
	}

	public int getMaxVoucherPcs() {
		return maxVoucherPcs;
	}

	public void setMaxVoucherPcs(Integer maxVoucherPcs) {
		this.maxVoucherPcs = maxVoucherPcs;
	}

	public BigDecimal getMaxVoucherAmt() {
		return maxVoucherAmt;
	}

	public void setMaxVoucherAmt(BigDecimal maxVoucherAmt) {
		this.maxVoucherAmt = maxVoucherAmt;
	}

	public boolean isMultiple() {
		return isMultiple;
	}

	public void setMultiple(boolean isMultiple) {
		this.isMultiple = isMultiple;
	}

	public Warehouse getValidBranch() {
		return validBranch;
	}

	public void setValidBranch(Warehouse validBranch) {
		this.validBranch = validBranch;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public Collection<VoucherDetail> getDetails() {
		return details;
	}

	public void setDetails(Collection<VoucherDetail> details) {
		this.details = details;
	}
}
