package id.sit.pos.master.model;

import java.math.BigDecimal;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.util.CurrencyUtil;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@DatabaseTable(tableName = "products")
public class Product extends RModel {
	public enum TaxFlag{
		BKP, NBKP
	}

	public enum ActiveStatus{
		ACTIVE, DEACTIVE
	}
	public static final String _BARCODE = "barcode";
	public static final String _ARTICLE = "article";
	public static final String _SHORT_NAME = "short_name";
	public static final String _COLOUR_CODE = "colour_code";
	public static final String _MCLASS_ID = "m_class_id";
	public static final String _BRAND_ID = "brand_id";
	public static final String _DEPARTMENT_ID = "department_id";
	public static final String _UNIT_ID = "unit_id";
	public static final String _SIZE_ID = "size_id";
	public static final String _PRICE_TAG = "harga_bandrol";
	public static final String _PRICE_RETAIL = "harga_eceran";
	public static final String _PRICE_MEMBER_RETAIL = "harga_member_eceran";
	public static final String _PRICE_MEMBER_CREDIT = "harga_kredit";
	public static final String _TAX_FLAG = "flag_pajak";
	public static final String _IS_COMPOSITE = "is_composite";

	@DatabaseField(columnName = _BARCODE)
	protected String barcode;

	@DatabaseField(columnName = _ARTICLE)
	protected String article;

	@DatabaseField(columnName = _SHORT_NAME)
	protected String shortName;

	@DatabaseField(columnName = _COLOUR_CODE)
	protected String colourCode;

	@DatabaseField(columnName = "image")
	protected String imageUrl;

	@DatabaseField(columnName = "purchase_price")
	protected BigDecimal priceInvoice;

	@DatabaseField(columnName = "cost_of_products")
	protected BigDecimal priceModal;

	@DatabaseField(columnName = _PRICE_TAG)
	protected BigDecimal priceTag;

	@DatabaseField(columnName = _PRICE_RETAIL)
	protected BigDecimal priceRetail;

	@DatabaseField(columnName = _PRICE_MEMBER_RETAIL)
	protected BigDecimal priceMemberRetail;

	@DatabaseField(columnName = _PRICE_MEMBER_CREDIT)
	protected BigDecimal priceMemberCredit;

	@DatabaseField(columnName = _BRAND_ID, foreign = true)
	protected Brand brand;

	@DatabaseField(columnName = _DEPARTMENT_ID, foreign = true)
	protected MClass department;

	@DatabaseField(columnName = _MCLASS_ID, foreign = true)
	protected MClass mClass;

	@DatabaseField(columnName = _SIZE_ID, foreign = true)
	protected SizeCategory sizeCategory;

	@DatabaseField(columnName = _UNIT_ID, foreign = true)
	protected Unit unit;

	@DatabaseField(columnName = "colour_id", foreign = true)
	protected Colour colour1;

	@DatabaseField(columnName = "colour2_id", foreign = true)
	protected Colour colour2;

	@DatabaseField(columnName = "colour3_id", foreign = true)
	protected Colour colour3;

	@DatabaseField(columnName = "colour4_id", foreign = true)
	protected Colour colour4;

	@DatabaseField(columnName = _TAX_FLAG)
	protected TaxFlag taxFlag;

	@DatabaseField(columnName = _IS_COMPOSITE)
	protected boolean isComposite;

	protected ProductSize productSize;

	protected ProductStock productStock;

	protected Sku sku;

	@ForeignCollectionField
	protected ForeignCollection<ProductSize> sizes;

	@ForeignCollectionField
	protected ForeignCollection<ProductBranchSetting> branchSettings;

	private StringProperty barcodeProperty;
	private StringProperty articleProperty;
	private StringProperty nameProperty;
	private StringProperty colourCodeProperty;
	private StringProperty sizeProperty;
	private IntegerProperty stockProperty;
	private StringProperty brandProperty;
	private StringProperty mClassProperty;
	private StringProperty priceProperty;
	private StringProperty unitProperty;
	private IntegerProperty convertionProperty;
	private StringProperty barcodeSkuProperty;
	private StringProperty sellingPriceProperty;

	public Product(Integer id) {
		this();
		this.id = id;
	}

	public Product() {
		barcodeProperty = new SimpleStringProperty();
		articleProperty = new SimpleStringProperty();
		nameProperty = new SimpleStringProperty();
		colourCodeProperty = new SimpleStringProperty();
		sizeProperty =  new SimpleStringProperty();
		stockProperty = new SimpleIntegerProperty();
		brandProperty = new SimpleStringProperty();
		mClassProperty = new SimpleStringProperty();
		priceProperty = new SimpleStringProperty();
		unitProperty = new SimpleStringProperty();
		convertionProperty = new SimpleIntegerProperty();
		barcodeSkuProperty = new SimpleStringProperty();
		sellingPriceProperty = new SimpleStringProperty();
	}

	public StringProperty barcodeProperty() {
		barcodeProperty.set(barcode);
		return barcodeProperty;
	}

	public StringProperty articleProperty() {
		articleProperty.set(article);
		return articleProperty;
	}

	public StringProperty shortNameProperty() {
		nameProperty.set(shortName);
		return nameProperty;
	}

	public StringProperty sizeProperty() {
		sizeProperty.set(getProductSize().getSize().getSizeNumber());
		return sizeProperty;
	}

	public StringProperty colourCodeProperty() {
		colourCodeProperty.set(getColourCode());
		return colourCodeProperty;
	}

	public IntegerProperty stockProperty() {
		if (getProductStock() != null)
			stockProperty.set(getProductStock().getAvailableQty());
		else
			stockProperty.set(0);
		return stockProperty;
	}

	public StringProperty brandProperty() {
		if (brand != null)
			brandProperty.set(brand.getName());
		else
			brandProperty.set("");
		return brandProperty;
	}

	public StringProperty mClassProperty() {
		if (mClass != null)
			mClassProperty.set(mClass.getName());
		else
			mClassProperty.set(null);
		return mClassProperty;
	}

	public StringProperty priceProperty() {
		if (priceTag!=null)
			priceProperty.set(CurrencyUtil.format(priceTag));
		else
			priceProperty.set("0");
		return priceProperty;
	}

	public StringProperty unitProperty() {
		if (unitProperty != null)
			unitProperty.set(getSku().getUnit().getShortName());
		else
			unitProperty.set(null);

		return unitProperty;
	}

	public IntegerProperty convertionProperty(){
		if (getSku().getConvertion() != null)
			convertionProperty.set(getSku().getConvertion());
		else
			convertionProperty.set(0);

		return convertionProperty;
	}

	public StringProperty barcodeSkuProperty() {
		if (barcodeProperty != null)
			barcodeSkuProperty.set(getSku().getBarcode());
		else
			barcodeSkuProperty.set(null);

		return barcodeSkuProperty;
	}

	public StringProperty sellingPriceProperty(){
		if (getPriceTag() != null)
			sellingPriceProperty.set(CurrencyUtil.format(getPriceTag(),true));
		else
			sellingPriceProperty.set(null);
		return sellingPriceProperty;
	}


	public MClass getDepartment() {
		return department;
	}

	public void setDepartment(MClass department) {
		this.department = department;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public MClass getMClass() {
		return mClass;
	}

	public void setMClass(MClass mclass) {
		this.mClass = mclass;
	}

	public SizeCategory getSizeCategory() {
		return sizeCategory;
	}

	public void setSizeCategory(SizeCategory sizeCategory) {
		this.sizeCategory = sizeCategory;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String name) {
		this.shortName = name;
	}

	public Colour getColour1() {
		return colour1;
	}

	public void setColour1(Colour colour1) {
		this.colour1 = colour1;
	}

	public Colour getColour2() {
		return colour2;
	}

	public void setColour2(Colour colour2) {
		this.colour2 = colour2;
	}

	public Colour getColour3() {
		return colour3;
	}

	public void setColour3(Colour colour3) {
		this.colour3 = colour3;
	}

	public Colour getColour4() {
		return colour4;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public String getColourCode() {
		return colourCode;
	}

	public void setColourCode(String colourCode) {
		this.colourCode = colourCode;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public BigDecimal getPriceInvoice() {
		return priceInvoice;
	}

	public void setPriceInvoice(BigDecimal priceInvoice) {
		this.priceInvoice = priceInvoice;
	}

	public BigDecimal getPriceModal() {
		return priceModal;
	}

	public void setPriceModal(BigDecimal priceModal) {
		this.priceModal = priceModal;
	}

	public BigDecimal getPriceTag() {
		return priceTag;
	}

	public void setPriceTag(BigDecimal priceSell) {
		this.priceTag = priceSell;
	}

	public BigDecimal getPriceRetail() {
		return priceRetail;
	}

	public void setPriceRetail(BigDecimal priceRetail) {
		this.priceRetail = priceRetail;
	}

	public BigDecimal getPriceMemberRetail() {
		return priceMemberRetail;
	}

	public void setPriceMemberRetail(BigDecimal priceMemberRetail) {
		this.priceMemberRetail = priceMemberRetail;
	}

	public BigDecimal getPriceMemberCredit() {
		return priceMemberCredit;
	}

	public void setPriceMemberCredit(BigDecimal priceMemberCredit) {
		this.priceMemberCredit = priceMemberCredit;
	}

	public void setColour4(Colour colour4) {
		this.colour4 = colour4;
	}

	public ForeignCollection<ProductSize> getSizes() {
		return sizes;
	}

	public void setSizes(ForeignCollection<ProductSize> sizes) {
		this.sizes = sizes;
	}

	public ForeignCollection<ProductBranchSetting> getBranchSettings() {
		return branchSettings;
	}

	public void setBranchSettings(ForeignCollection<ProductBranchSetting> branch) {
		this.branchSettings = branch;
	}

	public ProductSize getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSize productSize) {
		this.productSize = productSize;
	}

	public ProductStock getProductStock() {
		return productStock;
	}

	public void setProductStock(ProductStock productStock) {
		this.productStock = productStock;
	}

	public TaxFlag getTaxFlag() {
		return taxFlag;
	}

	public void setTaxFlag(TaxFlag taxFlag) {
		this.taxFlag = taxFlag;
	}

	public Sku getSku() {
		return sku;
	}

	public void setSku(Sku sku) {
		this.sku = sku;
	}

	public boolean isComposite() {
		return isComposite;
	}

	public void setComposite(boolean isComposite) {
		this.isComposite = isComposite;
	}
}
