package id.sit.pos.restclient.controller;

import java.lang.reflect.Method;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.util.Date;
import java.util.List;

import id.r.engine.controller.RCallback;
import id.r.engine.model.RModel;
import id.sit.pos.config.Config;
import id.sit.pos.master.model.*;
import id.sit.pos.model.SyncHistory;
import id.sit.pos.user.model.User;

public class DataSynchronizer{
	private Warehouse branch;
	private List<Clock> schedules;
	private SyncHistory latestSync;
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public DataSynchronizer() {
		try {
			latestSync = SyncHistory.getLatest();
		} catch (SQLException e) {
			//TODO
			e.printStackTrace();
		}
	}


	public void syncMaster(RCallback callback) {
		callback.onEvent(RCallback.STARTED);
		SyncHistory history = new SyncHistory();
		history.setSyncDate(new Date());
		history.setStatus(SyncHistory.Status.SUCCESS_ALL);
		RCallback emptyCallback = new RCallback() {
			@Override
			public void onEvent(String event, Object... value) {
				if (event==RCallback.FAILED)
					history.setStatus(SyncHistory.Status.FAIL_PARTIAL);
			}
		};

		callback.onEvent(RCallback.ON_PROGRESS, 0.1);
		syncSmallMasters(emptyCallback);
		callback.onEvent(RCallback.ON_PROGRESS, 0.3);
		syncProduct(emptyCallback);
		callback.onEvent(RCallback.ON_PROGRESS, 0.4);
		syncProductStock(emptyCallback);
		callback.onEvent(RCallback.ON_PROGRESS, 0.5);
		syncProductBranchSetting(emptyCallback);
		callback.onEvent(RCallback.ON_PROGRESS, 0.7);
//		syncMember(emptyCallback);
		callback.onEvent(RCallback.ON_PROGRESS, 0.8);

		//			history.save();
		callback.onEvent(RCallback.ON_PROGRESS, 0.9);
	}

	public boolean syncUser(RCallback callback) {
		callback.onEvent(RCallback.STARTED);
		RestClient<User[]> restClient = new RestClient<User[]>(null, User[].class);

		try {
			if (latestSync!=null)
				restClient.putBodyParam("last_update", dateFormat.format(latestSync.getSyncDate()));
			restClient.putBodyParam("branch_id", String.valueOf(branch.getId()));
			callback.onEvent(RCallback.ON_PROGRESS, 0.1);
			User[] models = restClient.sendGet();
			callback.onEvent(RCallback.ON_PROGRESS, 0.5);
			System.out.println("Temporary log: Brands retrieved"); //TODO
			for (int i = 0; i < models.length; i++) {
//				models[i].save();
				if(callback!=null) callback.onEvent(RCallback.ON_PROGRESS,((float)i/(models.length-1))*0.5);
			}
			callback.onEvent(RCallback.FINISHED);
			System.out.println("Temporary log: All Brand stored to database"); //TODO
		} catch (Exception e) {
			//TODO
				e.printStackTrace();
				callback.onEvent(RCallback.FAILED, e);
		}
		return true;
	}

	public void syncSmallMasters(RCallback callback) {
		callback.onEvent(RCallback.STARTED);
		RestClient<SmallMasters> restClient = new RestClient<SmallMasters>(null, SmallMasters.class);
		try {
			if (latestSync!=null)
				restClient.putBodyParam("last_update", dateFormat.format(latestSync.getSyncDate()));
			restClient.putBodyParam("branch_id", String.valueOf(branch.getId()));

			callback.onEvent(RCallback.ON_PROGRESS, 0.1);
			SmallMasters masters = restClient.sendGet();
			callback.onEvent(RCallback.ON_PROGRESS, 0.5);
			System.out.println(
					"Temporary log: Brand, Colour, Department, MClass, Size Category, Size, Warehouse are retrieved");

			Method[] methods = masters.getClass().getMethods();
			for (int i = 0; i < methods.length; i++) {
				if (!methods[i].getName().startsWith("get"))
					break;
				RModel[] models = (RModel[]) methods[i].invoke(masters, null);
				for (int j = 0; j < models.length; j++) {
//					models[j].save();

				}
				callback.onEvent(RCallback.ON_PROGRESS, 0.5 + ((float)i/methods.length)*0.5);
			}

			System.out.println(
					"Temporary log: Brand, Colour, Department, MClass, Size Ctegory, Size, Warehouse are stored to database");
		} catch (Exception e) {
			//TODO
			e.printStackTrace();
			callback.onEvent(RCallback.FAILED, e);
		}

	}

//	public void syncMember(RCallback callback) {
//		callback.onEvent(RCallback.STARTED);
//		RestClient<Member[]> restClient = new RestClient<Member[]>();
//		restClient.setUrl(Config.getApiUrlListMember());
//		if (latestSync!=null)
//			restClient.putBodyParam("last_update", dateFormat.format(latestSync.getSyncDate()));
//		restClient.setResponseClass(Member[].class);
//		try {
//			Member[] models = restClient.sendGet();
//			callback.onEvent(RCallback.ON_PROGRESS, 0.5);
//			System.out.println("Temporary log: Member's retrieved");
//			for (int i = 0; i < models.length; i++) {
//				models[i].save();
//				callback.onEvent(RCallback.ON_PROGRESS, 0.5+((float)i/models.length)*0.5);
//			}
//			System.out.println("Temporary log: All Member stored to database");
//		} catch (Exception e) {
//			//TODO
//			e.printStackTrace();
//			callback.onEvent(RCallback.FAILED, e);
//		}
//	}

	public void syncProduct(RCallback callback) {
		callback.onEvent(RCallback.STARTED);
		RestClient<Product[]> restClient = new RestClient<Product[]>(null, Product[].class);
		try {
			if (latestSync!=null)
				restClient.putBodyParam("last_update", dateFormat.format(latestSync.getSyncDate()));
			restClient.putBodyParam("branch_id", String.valueOf(branch.getId()));

			Product[] models = restClient.sendGet();
			callback.onEvent(RCallback.ON_PROGRESS, 0.5);
			System.out.println("Temporary log: Product's retrieved");
			for (int i = 0; i < models.length; i++) {
//				models[i].save();
				callback.onEvent(RCallback.ON_PROGRESS, 0.5 + ((float)i/models.length)*0.5);
			}
			System.out.println("Temporary log: All Product stored to database");
		} catch (Exception e) {
			//TODO
			e.printStackTrace();
			callback.onEvent(RCallback.FAILED, e);
		}
	}

	public void syncProductStock(RCallback callback) {
		callback.onEvent(RCallback.STARTED);
		RestClient<ProductStock[]> restClient = new RestClient<ProductStock[]>(null, ProductStock[].class);
//		restClient.setUrl(Config.getApiUrlListProductStock());
		try {
			if (latestSync!=null)
				restClient.putBodyParam("last_update", dateFormat.format(latestSync.getSyncDate()));
			restClient.putBodyParam("branch_id", String.valueOf(branch.getId()));

			ProductStock[] models = restClient.sendGet();
			callback.onEvent(RCallback.ON_PROGRESS, 0.5);
			System.out.println("Temporary log: ProductStock's retrieved");
			for (int i = 0; i < models.length; i++) {
//				models[i].save();
				callback.onEvent(RCallback.ON_PROGRESS, 0.5 + ((float)i/(models.length-1))*0.5);
			}
			System.out.println("Temporary log: All ProductStock stored to database");
		} catch (Exception e) {
			//TODO
			e.printStackTrace();
			callback.onEvent(RCallback.FAILED, e);
		}
	}

	public boolean syncProductBranchSetting(RCallback callback) {
		callback.onEvent(RCallback.STARTED);
		RestClient<ProductBranchSetting[]> restClient = new RestClient<ProductBranchSetting[]>(null, ProductBranchSetting[].class);
//		restClient.setUrl(Config.getApiUrlListProductWarehouse());
		try {
			if (latestSync!=null)
				restClient.putBodyParam("last_update", dateFormat.format(latestSync.getSyncDate()));
			restClient.putBodyParam("branch_id", String.valueOf(branch.getId()));

			ProductBranchSetting[] models = restClient.sendGet();
			callback.onEvent(RCallback.ON_PROGRESS, 0.5);
			System.out.println("Temporary log: ProductBranchSetting retrieved");
			for (int i = 0; i < models.length; i++) {
//				models[i].save();
				callback.onEvent(RCallback.ON_PROGRESS, 0.5 + ((float)i/models.length)*0.5);
			}
			System.out.println("Temporary log: All ProductBranchSetting stored to database");
		} catch (Exception e) {
			//TODO
			e.printStackTrace();
			callback.onEvent(RCallback.FAILED, e);
		}
		return true;
	}

	public void syncWarehouse() throws Exception {
		RestClient<Warehouse[]> restClient = new RestClient<Warehouse[]>(null, Warehouse[].class);

		Warehouse[] models = restClient.sendGet();
		System.out.println("Temporary log: Warehouses retrieved");
		for (int i = 0; i < models.length; i++) {
//			models[i].save();
		}
		System.out.println("Temporary log: All Warehouse stored to database");
	}

	public Warehouse getBranch() {
		return branch;
	}

	public void setBranch(Warehouse branch) {
		this.branch = branch;
	}

	public List<Clock> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<Clock> schedules) {
		this.schedules = schedules;
	}
}
