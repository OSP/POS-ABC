package id.sit.pos;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.comtel2000.keyboard.control.KeyBoardPopup;
import org.comtel2000.keyboard.control.KeyBoardPopupBuilder;

import id.r.engine.controller.RCallback;
import id.r.engine.view.RBrowser;
import id.r.engine.view.RUi;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.customer.model.Member;
import id.sit.pos.customer.view.MemberMaintenance;
import id.sit.pos.db.DbHelper;
import id.sit.pos.transaction.controller.SodEodCtrl;
import id.sit.pos.transaction.model.SodEod;
import id.sit.pos.transaction.view.EndShiftForm;
import id.sit.pos.transaction.view.StartShiftForm;
import id.sit.pos.transaction.view.TransactionWindow;
import id.sit.pos.user.controller.SessionCtrl;
import id.sit.pos.user.model.User;
import id.sit.pos.user.view.LoginForm;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.DeviceUtil;
import id.sit.pos.util.LineDisplayDriver;
import id.sit.pos.view.SettingForm;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import jpos.LineDisplayConst;


public class MainApp extends Application {
	private static Logger logger = Logger.getLogger(MainApp.class.getName());
	private static MainApp instance;
	private Stage primaryStage;
	private BorderPane rootLayout;
	private static File lockFile;
	private static FileChannel channel;
	private static FileLock lock;

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;

		instance = this;

		initData();
		primaryStage.setTitle(Config.getMessage("app_title"));
		primaryStage.getIcons().add(new Image(Config.getLogoPath()));

		try {
			SettingCtrl.getInstance().load();

		} catch (SQLException e) {
			logger.error(e);
			showAlertDbError();
			System.exit(0);
		}

		if (SettingCtrl.getSetting() == null){
			showSettingPopup();
		} else {
			(new Config()).LoadSettingFromDB();
		}

		if (SettingCtrl.getSetting() == null) {
			System.exit(0);
		}

		showLoginPopup(true);

		if (SessionCtrl.getSession() != null) {
			SodEod sodEod;
			if (SessionCtrl.getSession().getSodEod() != null)
				sodEod = SessionCtrl.getSession().getSodEod();
			else
				sodEod = showSodPopup();

			if (sodEod != null && !SodEodCtrl.isCloseDay)
				showTransactionWindow();
			else
				MainApp.getInstance().logout();
		}
	}

	public void initRootLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			Screen primaryScreen = Screen.getPrimary();
			Rectangle2D bounds = primaryScreen.getBounds();
		    primaryStage.setX(bounds.getMinX());
		    primaryStage.setY(bounds.getMinY());
		    primaryStage.setWidth(bounds.getWidth());
		    primaryStage.setHeight(bounds.getHeight());
		    primaryStage.initStyle(StageStyle.UNDECORATED);
			primaryStage.show();
			if (Config.isEnableOnScreenKeyboard()){
				initKeyboard(scene);
			}

		} catch (IOException e) {
			logger.error(e.toString(), e);
		}
	}

	private void initData() {
		try {
			DbHelper.createOrUpdateAllTable();
//			(new Config()).loadSetting();
		} catch (Exception e) {
			logger.error(e.toString(), e);
			showAlertError("common_error_content");
		}
	}

	public void showSettingPopup() {
		SettingForm controller = new SettingForm();
		createModalWindow(controller).showAndWait();
	}

	public void logout() {
		try {
			SessionCtrl.getInstance().doLogout();
			getPrimaryStage().close();
			Platform.exit();
			try {
				stop();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			showAlertError("common_error_db_content");
		}
	}

	public SodEod showSodPopup() {
		StartShiftForm controller = new StartShiftForm();
		createModalWindow(controller, true, false, false).showAndWait();
		return controller.getModel();
	}

	public SodEod showEodPopup(boolean isEod) {
		EndShiftForm controller = new EndShiftForm();
		controller.setEod(isEod);
		createModalWindow(controller, true, false, false).showAndWait();
		return controller.getModel();
	}

	public void showSodEodWebview() {
		RBrowser browser = new RBrowser();
		browser.setUrl("http://192.168.1.17:3000/finances/sod_eods/new?locale=en&web_view=1");
		MainApp.getInstance().createModalWindow(browser).showAndWait();
	}

	public void showTransactionWindow() {
		try {
			initRootLayout();
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(TransactionWindow.class.getResource("TransactionWindow.fxml"));
			TransactionWindow controller = new TransactionWindow();
			controller.setStage(primaryStage);
			loader.setController(controller);
			AnchorPane anchorPane = (AnchorPane) loader.load();

			rootLayout.setCenter(anchorPane);

		} catch (IOException e) {
			logger.error(e.toString(), e);
		}
	}

	public User showLoginPopup(boolean storeToSession) {
		return showLoginPopup(storeToSession, true, null);
	}

	public User showLoginPopup(boolean storeToSession, boolean isStart) {
		return showLoginPopup(storeToSession, isStart, null);
	}

	public User showLoginPopup(boolean storeToSession, boolean isStart, String roleName) {
		LoginForm controller = new LoginForm();
		controller.setStoreToSession(storeToSession);
		controller.setStrat(isStart);
		controller.setRoleName(roleName);
		createModalWindow(controller).showAndWait();
		return controller.getUser();
	}

	public User showLoginPopupExt(boolean storeToSession, boolean isStart) {
		LoginForm controller = new LoginForm();
		controller.setStoreToSession(storeToSession);
		controller.setStrat(isStart);
		controller.setLogType(controller.LOG_TYPE_EXT);
		createModalWindow(controller).showAndWait();
		return controller.getUser();
	}

	public Member showCustomerWindow() {
		MemberMaintenance controller = new MemberMaintenance();
		createModalWindow(controller,true,true,false).showAndWait();

		return controller.getSelected();
	}

	public <T extends RUi> Stage createModalWindow(T controller) {
		return createModalWindow(controller, true, false, false);
	}

	public <T extends RUi> Stage createModalWindow(T controller, boolean closeable, boolean maximized, boolean borderless) {
		try {
			Stage stage = new Stage();
			stage.getIcons().addAll(controller.getIcons());
			stage.setTitle(controller.getTitle());
			stage.initModality(Modality.WINDOW_MODAL);
			if (borderless)
				stage.initStyle(StageStyle.UNDECORATED);
			stage.initOwner(primaryStage);
			controller.setStage(stage);
			FXMLLoader loader = new FXMLLoader(
					controller.getClass().getResource(controller.getClass().getSimpleName() + ".fxml"));
			loader.setController(controller);
			AnchorPane page = (AnchorPane) loader.load();
			Scene scene = new Scene(page);
			stage.setScene(scene);
			if (maximized == true){
				stage.setMaximized(true);
			}

			/* on-screen keyboard/virtual keyboard setting */
			if (Config.isEnableOnScreenKeyboard()){
				initKeyboard(scene);
			}

			if (!closeable)
				stage.setOnCloseRequest(event-> {
			        event.consume();
			        stage.requestFocus();
				});

			/* close stage/windows by pressing ESC Key */
			scene.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
			    if (e.getCode().equals(KeyCode.ESCAPE)) {
			    	stage.close();
			    }
			});
			return stage;
		} catch (IOException e) {
			logger.error(e.toString(), e);
		}
		return null;
	}

	private void initKeyboard(Scene scene) {
		KeyBoardPopup popup = KeyBoardPopupBuilder.create().initScale(1.5).initLocale(Locale.ENGLISH).build();
	    popup.addFocusListener(scene);
	}

	public <T extends RUi> void createWindow(T controller) {
		try {
			FXMLLoader loader = new FXMLLoader(
					controller.getClass().getResource(controller.getClass().getSimpleName()));
			loader.setController(controller);
			AnchorPane page = (AnchorPane) loader.load();
			Stage stage = new Stage();
			stage.getIcons().addAll(controller.getIcons());
			stage.setTitle(controller.getTitle());


			Scene scene = new Scene(page);
			stage.setScene(scene);

			controller.setStage(stage);

			initKeyboard(scene);
			stage.showAndWait();

		} catch (IOException e) {
			logger.error(e.toString(), e);
		}
	}

	public static void showAlertDbError() {
		showAlertError("common_error_db_content");
	}

	public static void showAlertDbError(Stage stage) {
		showAlertError("common_error_db_content", stage);
	}

	public static void showAlertCreditLimitError(Member member){
		StringBuilder sbMsg = new StringBuilder();
		sbMsg.append(Config.getMessage("trans_over_credit_limit_error"));
		sbMsg.append(String.format("%n"));
		sbMsg.append("Current member credit limit: "+ CurrencyUtil.format(member.getCreditLimit()));

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Alert alert = new Alert(AlertType.ERROR);
				alert.initOwner(getInstance().getPrimaryStage());
				alert.setTitle(Config.getMessage("common_info_title"));
				alert.setHeaderText(null);
				alert.setContentText(sbMsg.toString());
				alert.showAndWait();
			}
		});
	}

	public static void showAlertError(String messageId) {
		showAlertError(messageId, getInstance().getPrimaryStage());
	}

	public static void showAlertError(String messageId, Stage stage) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Alert alert = new Alert(AlertType.ERROR);

				alert.initOwner(stage);
				alert.setTitle(Config.getMessage("common_error_title"));
				alert.setContentText(Config.getMessage(messageId));

				alert.showAndWait();
			}
		});
	}

	public static void showAlertConfirm(String message, RCallback callback) {

		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Alert alert = new Alert(AlertType.CONFIRMATION);

				alert.setTitle(Config.getMessage("common_confirm_title"));
				alert.setContentText(message);
				alert.initOwner(MainApp.getInstance().primaryStage);

				Optional<ButtonType> result = alert.showAndWait();

				if (result.get() == ButtonType.OK) {
					callback.onEvent(RCallback.FINISHED, new Boolean(true));
				} else {
					callback.onEvent(RCallback.FINISHED, new Boolean(false));
				}
			}
		});
	}

	public static void showInformation(String message) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.initOwner(getInstance().getPrimaryStage());
				alert.setTitle(Config.getMessage("common_info_title"));
				alert.setHeaderText(null);
				alert.setContentText(Config.getMessage("trans_user_changes")+ " "+ message);
				alert.showAndWait();
			}
		});
	}

	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static MainApp getInstance() {
		return instance;
	}

	public static void showWelcomeMessageOnLDD(){
 		LineDisplayDriver ldd = DeviceUtil.getLineDisplayDevice();
 		ldd.clear();
 		ldd.setText(0, Config.getDefaultTextTop());
 		ldd.setText(1, Config.getDefaultTextBottom());
 		if (Config.getDefaultTextTopMarquee())
 			ldd.startMarquee(0, LineDisplayConst.DISP_MT_RIGHT,
 					Config.getMarqueeSpeed(), Config.getMarqueeReturnSpeed());
 		else
 			ldd.display(0);

 		if (Config.getDefaultTextBottomMarquee())
 			ldd.startMarquee(1, LineDisplayConst.DISP_MT_RIGHT,
 					Config.getMarqueeSpeed(), Config.getMarqueeReturnSpeed());
 		else
			ldd.display(1);
	}

	public static void main(String[] args) {
		Config appConfig = new Config();

		checkApplication();

		try {
			appConfig.loadSetting();
		} catch (Exception e) {
			e.printStackTrace();
			showAlertError("common_error_content");
		}

		DeviceUtil.initDevice();
		if (Config.isEnableLineDisplay()){
			showWelcomeMessageOnLDD();
		}

		launch(args);
	}

	private static class ShutdownApps extends Thread{
		public void run(){
			unlockFile();
		}
	}

	public static void unlockFile() {
		// release and delete file lock
        try {
            if(lock != null) {
                lock.release();
                channel.close();
                lockFile.delete();
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
	}

	private static void checkApplication(){
		try {
			lockFile = new File("pos.lock");
			// Check if the lock exist
			if(lockFile.exists()){
				// if exist try to delete it
				lockFile.delete();
			}

			channel = new RandomAccessFile(lockFile, "rw").getChannel();
			lock = channel.tryLock();

			if(lock == null){
				// File is lock by other application
				channel.close();
				System.out.println("POS Already Runnning");
				throw new RuntimeException("POS Already Runnning");
			}
			// Add shutdown hook to release lock when application shutdown
			ShutdownApps shutdownApps = new ShutdownApps();
			Runtime.getRuntime().addShutdownHook(shutdownApps);

			//Your application tasks here..
            System.out.println("Running POS");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                System.exit(0);
            }

		} catch (IOException e) {
			throw new RuntimeException("Could not start process.", e);
		}
	}
}
