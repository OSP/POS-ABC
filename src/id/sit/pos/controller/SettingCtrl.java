package id.sit.pos.controller;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.master.model.Warehouse;
import id.sit.pos.model.Setting;
import id.sit.pos.model.SettingDetail;

public class SettingCtrl {
	private static Logger logger = Logger.getLogger(SettingCtrl.class.getName());
	private static Setting setting;
	private static SettingCtrl instance;

	private SettingCtrl() {
	}

	public static SettingCtrl getInstance() {
		if (instance == null)
			instance = new SettingCtrl();
		return instance;
	}

	public Setting load() throws SQLException {
		Dao<Setting, Integer> dao;
		Dao<Warehouse, Integer> warehouseDao = null;
		dao = DaoManagerImpl.getSettingDao();
		QueryBuilder<Setting, Integer> queryBuilder = dao.queryBuilder();
		setting = dao.queryForFirst(queryBuilder.prepare());
		if (setting != null) {
			warehouseDao = DaoManagerImpl.getWarehouseDao();
			if (setting.getStoreId() != null) {
				Warehouse branch = warehouseDao.queryForId(setting.getStoreId());
				if (branch != null)
					setting.setBranch(branch);
				else {
					setting = null;
					DeleteBuilder<Setting, Integer> delBuilder = dao.deleteBuilder();
					delBuilder.delete();
				}
			}
		}
		return setting;
	}

	public static void setSetting(Setting newSetting) {
		setting = newSetting;
	}

	public static Setting getSetting() {
		return setting;
	}

	public static Setting getNewInstance() {
		setting = new Setting();
		setting.setMachineId(generateMachineId());
		return setting;
	}

	public static Integer getBranchId() {
		return SettingCtrl.getSetting().getStoreId();
	}

	public static String generateMachineId() {
		return UUID.randomUUID().toString();
	}

	public BigDecimal fetchSettings() throws SQLException {
		Dao<SettingDetail, Integer> detailDao = DaoManagerImpl.getSettingDetailDao();
		QueryBuilder<SettingDetail, Integer> detailQb = detailDao.queryBuilder();
		detailQb.where().eq(SettingDetail._IS_ACTIVE, true);
		detailQb.orderBy(SettingDetail._CATEGORY, true).orderBy(SettingDetail._ORDER, true);

		List<SettingDetail> allSetting = detailDao.query(detailQb.prepare());

		BigDecimal amount = new BigDecimal(100000);
		for (SettingDetail settingDetail : allSetting) {
			switch (settingDetail.getCategory()) {
			case "member_point_to_amount":
				setting.setCustomerPointToAmount(new BigDecimal(settingDetail.getValue()));
				break;
			case "sale_extra_charges":
				setting.getTransactionsExtraCharges()
						.add(new SimpleEntry<Integer, String>(settingDetail.getId(), settingDetail.getValue()));
				break;
			case "sale_rounding":
				setting.setTransactionRounding(new BigDecimal(settingDetail.getValue()));
				break;
			default:
				break;
			}
		}
		return amount;
	}
}
