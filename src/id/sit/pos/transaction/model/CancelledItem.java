package id.sit.pos.transaction.model;

import java.math.BigDecimal;
import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.master.model.Product;
import id.sit.pos.master.model.ProductStock;
import id.sit.pos.master.model.Warehouse;
import id.sit.pos.user.model.User;

@DatabaseTable(tableName = "cancel_items")
public class CancelledItem extends RModel {

	public static final String _PRODUCT_ID = "product_id";
	public static final String _PRODUCT_STOCK_ID = "product_detail_id";
	public static final String _INITIAL_PRICE = "price";
	public static final String _QTY = "quantity";
	public static final String _SUBTOTAL_PRICE = "subtotal_price";
	public static final String _DISC_PCT = "discount";
	public static final String _DISC_AMT = "discount_amt";
	public static final String _DISC_PRICE = "price_after_discount";
	public static final String _TOTAL_DISC_AMT = "total_discount_amt";
	public static final String _TOTAL_PRICE = "total_price";

	@DatabaseField(columnName = _PRODUCT_ID, foreign = true)
	private Product product;

	@DatabaseField(columnName = _PRODUCT_STOCK_ID, foreign = true)
	private ProductStock productStock;

	@DatabaseField(columnName = _INITIAL_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal initPrice = new BigDecimal(0);

	@DatabaseField(columnName = _QTY)
	private int qty;

	@DatabaseField(columnName = _SUBTOTAL_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal subtotalPrice = new BigDecimal(0);

	@DatabaseField(columnName = _DISC_PCT)
	private Float discPct;

	@DatabaseField(columnName = _DISC_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal discAmt = new BigDecimal(0);

	@DatabaseField(columnName = _DISC_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal discPrice = new BigDecimal(0);

	@DatabaseField(columnName = _TOTAL_DISC_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalDisc = new BigDecimal(0);

	@DatabaseField(columnName = _TOTAL_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalPrice = new BigDecimal(0);

	@DatabaseField(columnName = "user_id", foreign = true)
	private User user;

	@DatabaseField(columnName = "cancel_date")
	private Date cancelDate;

	@DatabaseField(columnName = "client_id")
	private String machineId;

	@DatabaseField(columnName = "store_id", foreign = true)
	private Warehouse store;

	public CancelledItem() {
	}

	public CancelledItem(TransactionItem item) {
		setProduct(getProduct());
		setProductStock(getProductStock());
		setInitPrice(getInitPrice());
		setQty(getQty());
		setSubtotalPrice(getSubtotalPrice());
		setDiscAmt(getDiscAmt());
		setDiscPct(getDiscPct());
		setDiscPrice(getDiscPrice());
		setTotalDisc(getTotalDisc());
		setTotalPrice(getTotalDisc());
		setCreatedAt(getCreatedAt());
		setUpdatedAt(getUpdatedAt());
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ProductStock getProductStock() {
		return productStock;
	}

	public void setProductStock(ProductStock productStock) {
		this.productStock = productStock;
	}

	public BigDecimal getInitPrice() {
		return initPrice;
	}

	public void setInitPrice(BigDecimal initialPrice) {
		this.initPrice = initialPrice;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public BigDecimal getSubtotalPrice() {
		return subtotalPrice;
	}

	public void setSubtotalPrice(BigDecimal subtotalPrice) {
		this.subtotalPrice = subtotalPrice;
	}

	public Float getDiscPct() {
		return discPct;
	}

	public void setDiscPct(Float discPct) {
		this.discPct = discPct;
	}

	public BigDecimal getDiscAmt() {
		return discAmt;
	}

	public void setDiscAmt(BigDecimal discAmt) {
		this.discAmt = discAmt;
	}

	public BigDecimal getDiscPrice() {
		return discPrice;
	}

	public void setDiscPrice(BigDecimal discPrice) {
		this.discPrice = discPrice;
	}

	public BigDecimal getTotalDisc() {
		return totalDisc;
	}

	public void setTotalDisc(BigDecimal totalDisc) {
		this.totalDisc = totalDisc;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public Warehouse getStore() {
		return store;
	}

	public void setStore(Warehouse store) {
		this.store = store;
	}
}
