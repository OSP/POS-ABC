package id.sit.pos.transaction.model;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.config.Config;
import id.sit.pos.customer.model.Member;
import id.sit.pos.master.model.VoucherDetail;
import id.sit.pos.master.model.Warehouse;
import id.sit.pos.user.model.Session;
import id.sit.pos.user.model.User;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.DateUtil;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@DatabaseTable(tableName = "sales")
public class Transaction extends RModel{
	public static final String _TRANSACTION_NO = "code";
	public static final String _TRANSACTION_DATE = "transaction_date";
	public static final String _STATUS = "status";
	public static final String _MEMBER_ID = "member_id";
	public static final String _VOUCHER_AMT = "voucher_amt";
	public static final String _DEBIT_ID = "debit_id";
	public static final String _DEBIT_AMT = "debit_amt";
	public static final String _CREDIT_ID = "credit_id";
	public static final String _CREDIT_AMT = "credit_amt";
	public static final String _TRANSFER_AMT = "transfer_amt";
	public static final String _CASH_AMT = "payment_cash";
	public static final String _CASH_CHANGE = "exchange";
	public static final String _TOTAL_QTY = "total_quantity";
	public static final String _SUBTOTAL_PRICE = "subtotal_price";
	public static final String _TOTAL_DISC = "total_discount";
	public static final String _TOTAL_SPECIAL_DISC = "total_special_discount";
	public static final String _TOTAL_CAPITAL_PRICE = "total_capital";
	public static final String _TOTAL_PRICE = "total_price";
	public static final String _TOTAL_PPN = "total_ppn";
	public static final String _SESSION_ID = "session_id";
	public static final String _ITEMS = "items";
	public static final String _RECEIPT_COUNT = "receipt_count";
	public static final String _P2P_AMOUNT = "p2p_amt";

	public enum Status {
	    NEW, OPEN, FINISHED, HOLD
	}

	@DatabaseField(columnName = _SESSION_ID, foreign = true)
	private Session session;

	@DatabaseField(columnName = _MEMBER_ID, foreign = true)
	private Member member;

	@DatabaseField(columnName = _TRANSACTION_NO)
	private String transactionNo;

	@DatabaseField(columnName = _TRANSACTION_DATE)
	private Date transactionDate;

	@DatabaseField(columnName = _TOTAL_QTY)
	private int totalQty;

	@DatabaseField(columnName = _SUBTOTAL_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal subTotalAmt = new BigDecimal(0);

	@DatabaseField(columnName = _TOTAL_DISC, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalDisc = new BigDecimal(0);

	@DatabaseField(columnName = _TOTAL_SPECIAL_DISC, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalSpecialDisc = new BigDecimal(0);

	@DatabaseField(columnName = _TOTAL_CAPITAL_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalCaptialPrice = new BigDecimal(0);

	@DatabaseField(columnName = _TOTAL_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalPrice = new BigDecimal(0);

	@DatabaseField(columnName = _CASH_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal cashAmt = new BigDecimal(0);

	@DatabaseField(columnName = _CASH_CHANGE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal cashChange = new BigDecimal(0);

	@DatabaseField(columnName = "rounding_amt", dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal roundingAmt;

	@DatabaseField(columnName = _STATUS)
	private Status status;

	@DatabaseField(columnName="voucher_code", foreign = true)
	private VoucherDetail voucher;

	@DatabaseField(columnName = "payment_type")
	private PaymentType paymentType;

	@DatabaseField(columnName=_VOUCHER_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal voucherAmt = BigDecimal.ZERO;

	@DatabaseField(columnName = _DEBIT_ID, foreign = true)
	private BankPayment debit;

	@DatabaseField(columnName = _DEBIT_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal debitAmt = BigDecimal.ZERO;

	@DatabaseField(columnName = _CREDIT_ID, foreign = true)
	private BankPayment credit;

	@DatabaseField(columnName = _CREDIT_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal creditAmt = BigDecimal.ZERO;

	@DatabaseField(columnName = "transfer_id", foreign = true)
	private BankPayment transfer;

	@DatabaseField(columnName = _TRANSFER_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal transferAmt = BigDecimal.ZERO;

	@DatabaseField(columnName = "total_paid", dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalPaidAmt = BigDecimal.ZERO;

	@DatabaseField(columnName = "total_outstanding", dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalOutstanding;

	private BigDecimal toBePaidAmt = BigDecimal.ZERO;

	@DatabaseField(columnName = "earned_voucher_id", foreign = true)
	private VoucherDetail earnedVoucher;

	@DatabaseField(columnName = "cashback_id", foreign = true)
	private VoucherDetail cashback;

	@DatabaseField(columnName = "cashback_amt", dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal cashbackAmt;

	@DatabaseField(columnName = "client_id")
	private String machineId;

	@DatabaseField(columnName = "store_id", foreign = true)
	private Warehouse branch;

	@DatabaseField(columnName = "user_id", foreign = true)
	private User cashier;

	@DatabaseField(columnName = _TOTAL_PPN, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal totalPpn;

	@ForeignCollectionField(columnName = _ITEMS)
	private Collection<TransactionItem> items;

	@DatabaseField(columnName = _RECEIPT_COUNT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal receiptPrintedCount;
	private BigDecimal creditChargeAmmount;

	@DatabaseField(columnName = _P2P_AMOUNT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal p2pAmount = new BigDecimal(0);

	private StringProperty transactionNoProperty;
	private StringProperty timeProperty;
	private StringProperty customerNameProperty;
	private IntegerProperty totalItemProperty;
	private StringProperty totalPriceProperty;

	public Transaction(){
		transactionNoProperty = new SimpleStringProperty();
		timeProperty = new SimpleStringProperty();
		customerNameProperty = new SimpleStringProperty();
		totalItemProperty = new SimpleIntegerProperty();
		totalPriceProperty = new SimpleStringProperty();
	}

	public StringProperty transactionNoProperty() {
		if (getTransactionNo() != null)
			transactionNoProperty.set(getTransactionNo());
		else
			transactionNoProperty.set("");
		return transactionNoProperty;
	}

	public StringProperty timeProperty() {
		timeProperty.set(DateUtil.format(getTransactionDate(), Config.getTimeFormat()));
		return timeProperty;
	}

	public StringProperty customerNameProperty() {
		if (getMember() != null)
			customerNameProperty.set(getMember().getName());
		else
			customerNameProperty.set("");
		return customerNameProperty;
	}

	public IntegerProperty totalItemProperty() {
		totalItemProperty.set(getTotalQty());
		return totalItemProperty;
	}

	public StringProperty totalPriceProperty() {
		totalPriceProperty.set(CurrencyUtil.format(getTotalPrice()));
		return totalPriceProperty;
	}


	//Generated Setter Getter

	public Collection<TransactionItem> getItems() {
		return items;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member customer) {
		this.member = customer;
		customerNameProperty();
	}

	public void setItems(Collection<TransactionItem> items) {
		this.items = items;
	}


	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
		transactionNoProperty();
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
		timeProperty();
	}

	public BigDecimal getSubTotalAmt() {
		return subTotalAmt;
	}

	public int getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(int totalQty) {
		this.totalQty = totalQty;
		totalItemProperty();
	}

	public void setSubTotalAmt(BigDecimal subTotalAmt) {
		this.subTotalAmt = subTotalAmt;
	}

	public BigDecimal getTotalDisc() {
		return totalDisc;
	}

	public void setTotalDisc(BigDecimal totalDiscAmt) {
		this.totalDisc = totalDiscAmt;
	}

	public BigDecimal getTotalCapitalPrice() {
		return totalCaptialPrice;
	}

	public void setTotalCapitalPrice(BigDecimal totalCaptialPrice) {
		this.totalCaptialPrice = totalCaptialPrice;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPriceAmt) {
		this.totalPrice = totalPriceAmt;
		totalPriceProperty();
	}

	public VoucherDetail getVoucher() {
		return voucher;
	}

	public void setVoucher(VoucherDetail voucher) {
		this.voucher = voucher;
	}

	public BigDecimal getVoucherAmt() {
		return voucherAmt;
	}

	public void setVoucherAmt(BigDecimal voucherAmt) {
		this.voucherAmt = voucherAmt;
	}

	public BigDecimal getCashAmt() {
		return cashAmt;
	}

	public void setCashAmt(BigDecimal cashAmt) {
		this.cashAmt = cashAmt;
	}

	public BigDecimal getCashChange() {
		return cashChange;
	}

	public void setCashChange(BigDecimal cashChange) {
		this.cashChange = cashChange;
	}

	public BigDecimal getRoundingAmt() {
		return roundingAmt;
	}

	public void setRoundingAmt(BigDecimal roundingAmt) {
		this.roundingAmt = roundingAmt;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public BankPayment getDebit() {
		return debit;
	}

	public void setDebit(BankPayment debit) {
		this.debit = debit;
	}

	public BankPayment getCredit() {
		return credit;
	}

	public void setCredit(BankPayment credit) {
		this.credit = credit;
	}

	public BigDecimal getDebitAmt() {
		return debitAmt;
	}

	public void setDebitAmt(BigDecimal debitAmt) {
		this.debitAmt = debitAmt;
	}

	public BigDecimal getCreditAmt() {
		return creditAmt;
	}

	public void setCreditAmt(BigDecimal creditAmt) {
		this.creditAmt = creditAmt;
	}

	public BankPayment getTransfer() {
		return transfer;
	}

	public void setTransfer(BankPayment transfer) {
		this.transfer = transfer;
	}

	public BigDecimal getTransferAmt() {
		return transferAmt;
	}

	public void setTransferAmt(BigDecimal transferAmt) {
		this.transferAmt = transferAmt;
	}

	public BigDecimal getTotalPaidAmt() {
		return totalPaidAmt;
	}

	public void setTotalPaidAmt(BigDecimal totalPaymentAmt) {
		this.totalPaidAmt = totalPaymentAmt;
	}

	public BigDecimal getTotalOutstanding() {
		return totalOutstanding;
	}

	public void setTotalOutstanding(BigDecimal totalOutstanding) {
		this.totalOutstanding = totalOutstanding;
	}

	public BigDecimal getToBePaidAmt() {
		return toBePaidAmt;
	}

	public void setToBePaidAmt(BigDecimal debtAmt) {
		this.toBePaidAmt = debtAmt;
	}

	public VoucherDetail getEarnedVoucher() {
		return earnedVoucher;
	}

	public void setEarnedVoucher(VoucherDetail earnedVoucher) {
		this.earnedVoucher = earnedVoucher;
	}

	public VoucherDetail getCashback() {
		return cashback;
	}

	public void setCashback(VoucherDetail cashback) {
		this.cashback = cashback;
	}

	public BigDecimal getCashbackAmt() {
		return cashbackAmt;
	}

	public void setCashbackAmt(BigDecimal cashbackAmt) {
		this.cashbackAmt = cashbackAmt;
	}

	public String getMachineId() {
		return machineId;
	}

	public void setMachineId(String machineId) {
		this.machineId = machineId;
	}

	public Warehouse getBranch() {
		return branch;
	}

	public void setBranch(Warehouse branch) {
		this.branch = branch;
	}

	public User getCashier() {
		return cashier;
	}

	public void setCashier(User cashier) {
		this.cashier = cashier;
	}

	public BigDecimal getTotalPpn() {
		return totalPpn;
	}

	public void setTotalPpn(BigDecimal totalPpn) {
		this.totalPpn = totalPpn;
	}

	public BigDecimal getReceiptPrintedCount() {
		return receiptPrintedCount;
	}

	public void setReceiptPrintedCount(BigDecimal receiptPrintedCount) {
		this.receiptPrintedCount = receiptPrintedCount;
	}

	public BigDecimal getCreditChargeAmmount() {
		return creditChargeAmmount;
	}

	public void setCreditChargeAmmount(BigDecimal creditChargeAmmount) {
		this.creditChargeAmmount = creditChargeAmmount;
	}

	public BigDecimal getP2pAmount() {
		return p2pAmount;
	}

	public void setP2pAmount(BigDecimal p2pAmount) {
		this.p2pAmount = p2pAmount;
	}

}
