package id.sit.pos.user.view;

import java.awt.AWTEvent;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.sql.SQLException;

import id.r.engine.view.RUi;
import id.sit.pos.config.Config;
import id.sit.pos.transaction.controller.SodEodCtrl;
import id.sit.pos.transaction.model.SodEod;
import id.sit.pos.user.controller.SessionCtrl;
import id.sit.pos.user.model.Role;
import id.sit.pos.user.model.User;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.stage.PopupWindow.AnchorLocation;


public class LoginForm extends RUi{
	private final String tooltips = "Warning, Capslock are Active.";
	@FXML
	private TextField username;

	@FXML
	private PasswordField password;

	@FXML
	private Label info;

	@FXML
	private Label message;

	@FXML
	private Button loginBtn;

	@FXML
	private Button cancelBtn;

	private boolean storeToSession;
	private boolean isStart;
	private String roleName;
	private String logType;
	private boolean authenticated;
	private User user;
	private SodEod openSodEod;
	private SodEodCtrl sodEodCtrl = new SodEodCtrl();

	public final String LOG_TYPE_EXT = "ext";

	public LoginForm(){
		setTitle("POS Login");
	}

	@FXML
	public void initialize(){
		super.initialize();
		StringBuilder msg = new StringBuilder("");
		try {
			if (isStart) {
				if (sodEodCtrl.isDayClosed()) {
					message.setText("This day has been closed! No more transaction allowed!");
					username.setDisable(true);
					password.setDisable(true);
					loginBtn.setDisable(true);
				}
				openSodEod = sodEodCtrl.getOpenShift();

				if (openSodEod != null) {
					msg.append(openSodEod.getUser().getUsername() + " has an unclosed shift. Only "+openSodEod.getUser().getUsername()+" can login until the shift is closed");
					msg.append(String.format("%n%n"));
				}
			}

			if (roleName != null) {
				msg.append(info.getText()+"Please login as "+roleName);
			}

			if (logType != null && logType == LOG_TYPE_EXT){
				msg.append(info.getText()+"Please login as: \n"+ Role.STORE_MANAGER + " or "+
					Role.SUPER_ADMIN +" ("+ Role.SUPER_USER+")"  );
			}

			info.setText(msg.toString());
		} catch (SQLException e) {
			logger.error(e.toString(), e);
		}

		loginBtn.defaultButtonProperty().bind(loginBtn.focusedProperty());
		cancelBtn.defaultButtonProperty().bind(cancelBtn.focusedProperty());
	}

	@FXML
	public void usernameOnAction(){
		if (username.getText()!=null && username.getText()!=""){
			passwordOnAction();
		}
	}

	@FXML
	public void passwordOnAction(){
		if (password.getText()!=null && password.getText()!=""){
			loginBtnOnAction();
		}
	}

	@FXML
	public void loginBtnOnAction() {
		try {
			if (roleName == null)
				user = SessionCtrl.getInstance().authenticate(username.getText(), password.getText(),null,logType);
			else
				user = SessionCtrl.getInstance().authenticate(username.getText(), password.getText(), roleName);

			if (user != null) {
				if (isStart && openSodEod != null && !user.getId().equals(openSodEod.getUser().getId())) {
					message.setText(Config.getMessage("invalid_username_or_password"));
					user = null;
					return;
				}

				if (storeToSession) {
					SessionCtrl.getInstance().doLogin(user);
					if (openSodEod != null) {
						SessionCtrl.getSession().setSodEod(openSodEod);
						SessionCtrl.getSession().setShiftNo(openSodEod.getShiftNo());
					}
					SessionCtrl.update();
				}
				getStage().close();
			} else {
				message.setText(Config.getMessage("invalid_username_or_password"));
			}

		} catch (SQLException e1) {
			logger.error(e1.toString(), e1);
			message.setText(Config.getMessage("common_error_db_content"));
		}
	}

	@FXML
	public void cancelBtnAction() {
		getStage().close();
	}

	public boolean isStoreToSession() {
		return storeToSession;
	}

	public void setStoreToSession(boolean storeToSession) {
		this.storeToSession = storeToSession;
	}

	public boolean isAuthenticated() {
		return authenticated;
	}

	public User getUser() {
		return user;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public boolean isStart() {
		return isStart;
	}

	public void setStrat(boolean isStart) {
		this.isStart = isStart;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}
}
