package id.r.engine.view;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.CloseableIterator;

import id.r.engine.controller.RDao;
import id.r.engine.model.RModel;
import id.r.engine.model.SearchParam;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Control;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public abstract class RGridSearch<T extends RModel> extends RGrid<T> {

	protected Service<ObservableList<T>> searchService;
	private EventHandler<KeyEvent> searchCriteriaEh;
	protected List<Control> searchCriterias = new ArrayList<>();
	private RDao<T> rDao;

	public RGridSearch() {
		rDao = initDao();

		searchService = new Service<ObservableList<T>>() {

			@Override
			protected Task<ObservableList<T>> createTask() {
				return new Task<ObservableList<T>>() {
					@Override
					protected ObservableList<T> call() throws InterruptedException {
						updateMessage(Config.getMessage("common_searching"));
						startProgressBar();
						ObservableList<T> result;

						result = search(getSearchParams());
						if (result.isEmpty())
							updateMessage("No data match with search criteria.");
						else
							updateMessage("");

						endProgressBar();
						return result;
					}
				};
			}

			@Override
			public boolean cancel() {
				if (super.cancel()) {
					endProgressBar();
					return true;
				}
				return false;
			}
		};

		searchService.setOnFailed(value->{
			MainApp.showAlertDbError();
		});

		searchCriteriaEh = new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent e) {
				if (e.getCode() == KeyCode.DOWN) {
					table.requestFocus();
			        table.getSelectionModel().select(0);
			        table.getFocusModel().focus(0);
				}
				if (e.getCode() == KeyCode.ESCAPE) {
					getStage().close();
				}
			}
		};
	}

	@FXML
	public void initialize() {
		super.initialize();
		if (message != null)
			message.textProperty().bind(searchService.messageProperty());

		table.setOnKeyPressed(value->{
			if (value.getCode() == KeyCode.ESCAPE)
				if (searchCriterias.size()>0)
					searchCriterias.get(0).requestFocus();
		});

		table.itemsProperty().bind(searchService.valueProperty());
	};

	protected abstract RDao<T> initDao();


	protected abstract SearchParam[] getSearchParams();

	public ObservableList<T> search(SearchParam[] params) {
		CloseableIterator<T> iterator = null;
		try {
			ObservableList<T> ol = FXCollections.observableArrayList();

			iterator = getRdao().search(params, true);
			while (iterator.hasNext()) {
				try {
					ol.add(iterator.next());
				} catch (RuntimeException e) {
					logger.error(e.toString(), e);
				}

			}
			return ol;
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			MainApp.showAlertDbError(getStage());
		} finally {
			try {
				if (iterator != null)
					iterator.close();
			} catch (SQLException e) {
				logger.error(e.toString(), e);
			}
		}
		return null;
	}

	@FXML
	public void search(){
		if (!searchService.isRunning()) {
			searchService.reset();
			searchService.start();
		} else {
			if (searchService.cancel()) {
				searchService.reset();
				searchService.start();
			}
		}
	}

	public void setAsSearchCriteriaFields (TextInputControl... fields) {
		for (TextInputControl field : fields) {
			searchCriterias.add(field);
			field.addEventFilter(KeyEvent.KEY_PRESSED, searchCriteriaEh);
		}
	}

	public RDao<T> getRdao() {
		return rDao;
	}

	public void setrDao(RDao<T> rDao) {
		this.rDao = rDao;
	}

}
