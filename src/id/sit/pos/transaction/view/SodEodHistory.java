package id.sit.pos.transaction.view;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import com.j256.ormlite.dao.CloseableIterator;

import id.r.engine.controller.RDao;
import id.r.engine.model.SearchParam;
import id.r.engine.model.SearchParam.Operator;
import id.r.engine.view.RGridChooser;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.transaction.controller.SodEodCtrl;
import id.sit.pos.transaction.model.SodEod;
import id.sit.pos.user.model.Role;
import id.sit.pos.util.DateUtil;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;

public class SodEodHistory extends RGridChooser<SodEod>{
	@FXML
	private DatePicker sodEodDatePicker;

	@FXML
	private ComboBox<SodEod.Type> sodEodTypeComboBox;

	@FXML
	private TableView<SodEod> table;

	@FXML
	private TableColumn<SodEod, String> sodEodDateClm;

	@FXML
	private TableColumn<SodEod, String> sodEodTypeClm;

	@FXML
	private TableColumn<SodEod, Integer> sodEodNumberClm;

	@FXML
	private TableColumn<SodEod, String> sodEodUserClm;

	@FXML
	private TableColumn<SodEod, BigDecimal> sodEodTotalSalesClm;

	@FXML
	private TableColumn<SodEod, BigDecimal> sodEodTotalActualClm;

	@FXML
	private TableColumn<SodEod, BigDecimal> sodEodDifferenceClm;


	public String userRole = null;
	SodEodCtrl sodEodCtrl = new SodEodCtrl();

	public SodEodHistory() {
		super();

		getIcons().add(new Image(Config.getLogoPath()));
		setTitle(Config.getMessage("sod_eod_history"));

		searchService = new Service<ObservableList<SodEod>>() {
			@Override
			protected Task<ObservableList<SodEod>> createTask() {
				return new Task<ObservableList<SodEod>>() {
					@Override
					protected ObservableList<SodEod> call() throws InterruptedException, SQLException {
						String sessionType = sodEodTypeComboBox.getSelectionModel().getSelectedItem().toString();
						LocalDate localDate = sodEodDatePicker.getValue();
						Date dateSearch = null;
						if (localDate != null) {
							Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
							dateSearch = Date.from(instant);
						}


						updateMessage(Config.getMessage("common_searching"));
						startProgressBar();
						ObservableList<SodEod> result = FXCollections.observableArrayList();;

						CloseableIterator<SodEod> iterator = sodEodCtrl.getListSodEod(dateSearch, sessionType, getUserRole() );
						iterator.forEachRemaining(sodEod->{
							result.add(sodEod);
						});

						iterator.closeQuietly();
						if (result.isEmpty())
							updateMessage("No data match with search criteria.");
						else
							updateMessage("");

						endProgressBar();
						return result;
					}
				};
			}

			@Override
			public boolean cancel() {
				if (super.cancel()) {
					endProgressBar();
					return true;
				}
				return false;
			}
		};

		searchService.setOnFailed(value -> {
			MainApp.showAlertDbError();
			logger.error(value.getSource().getException().toString(), value.getSource().getException());
		});

	}

	@FXML
	@Override
	public void initialize() {
		super.initialize();

		if(this.getUserRole().equals(Role.STORE_MANAGER)){
			sodEodDatePicker.setDisable(true);
		} else {
			sodEodDatePicker.setDisable(false);
		}

		sodEodDateClm.setCellValueFactory(new PropertyValueFactory<SodEod, String>("date"));
		sodEodTypeClm.setCellValueFactory(new PropertyValueFactory<SodEod, String>("type"));
		sodEodNumberClm.setCellValueFactory(new PropertyValueFactory<SodEod, Integer>("shift"));
		sodEodUserClm.setCellValueFactory(new PropertyValueFactory<SodEod, String>("userName"));
		sodEodTotalSalesClm.setCellValueFactory(new PropertyValueFactory<SodEod, BigDecimal>("salesAmount"));
		sodEodTotalActualClm.setCellValueFactory(new PropertyValueFactory<SodEod, BigDecimal>("actualSales"));
		sodEodDifferenceClm.setCellValueFactory(new PropertyValueFactory<SodEod, BigDecimal>("difference"));

		sodEodTypeComboBox.getItems().addAll(SodEod.Type.values());
		sodEodTypeComboBox.setValue(SodEod.Type.SHIFT);
		sodEodDatePicker.setValue((new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

		sodEodNumberClm.setStyle("-fx-alignment: center-right");
		sodEodTotalSalesClm.setStyle("-fx-alignment: center-right");
		sodEodTotalActualClm.setStyle("-fx-alignment: center-right");
		sodEodDifferenceClm.setStyle("-fx-alignment: center-right");

		sodEodTypeComboBox.setOnAction(e -> {
			if(sodEodTypeComboBox.getSelectionModel().getSelectedIndex() == SodEod.Type.DAY.ordinal()){
				sodEodTypeClm.setVisible(false);
				sodEodNumberClm.setVisible(false);
			} else {
				sodEodTypeClm.setVisible(true);
				sodEodNumberClm.setVisible(true);
			}

			search();
		});


		search();
	};

	@Override
	protected RDao<SodEod> initDao() {
		return new RDao<>(SodEod.class);
	}

	@Deprecated @Override
	protected SearchParam[] getSearchParams() {
		LocalDate localDate = sodEodDatePicker.getValue();
		Date startDate= null;
		Date endDate = null;

		if (localDate != null) {
			Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			endDate = Date.from(instant);
		}

		startDate = DateUtil.getPreviousDate(endDate);

		SearchParam[] searchParams = new SearchParam[] {
				new SearchParam(SodEod.class, SodEod._DATE, DateUtil.getStartOfDay(startDate), Operator.GTE),
				new SearchParam(SodEod.class, SodEod._DATE, DateUtil.getStartOfDay(endDate), Operator.LTE),
				new SearchParam(SodEod.class, SodEod._TYPE, sodEodTypeComboBox.getSelectionModel().getSelectedItem()),
				new SearchParam(SodEod.class, SodEod._STORE_ID, SettingCtrl.getSetting().getStoreId()
		)};
		return searchParams;
	}


	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}



}
