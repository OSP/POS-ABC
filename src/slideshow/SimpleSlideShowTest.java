package slideshow;

import java.sql.SQLException;

import id.sit.pos.MainApp;
import id.sit.pos.util.EmbeddedMediaPlayer;
import id.sit.pos.util.MediaControl;
import javafx.animation.FadeTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.application.Application;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;
import sun.security.util.Length;

public class SimpleSlideShowTest extends Application {

	class SimpleSlideShow {

		StackPane root = new StackPane();
		Node[] slides;
		MediaControl mc;

		public Service<Void> playerService;

		public SimpleSlideShow() {
			this.slides = new Node[4];
//			Image image1 = new Image(SimpleSlideShowTest.class.getResource("promo1.png").toExternalForm());
			Image image1 = new Image("http://appforhealth.com/wp-content/uploads/2014/09/coffee.jpg");
			Image image2 = new Image(SimpleSlideShowTest.class.getResource("promo2.png").toExternalForm());
			Image image3 = new Image(SimpleSlideShowTest.class.getResource("promo3.png").toExternalForm());

			System.out.println(SimpleSlideShowTest.class.getResource("promo2.png").toExternalForm());

			slides[0] = new ImageView(image1);
			slides[2] = new ImageView(image2);
			slides[3] = new ImageView(image3);

			playerService = new Service<Void>() {

				@Override
				protected Task<Void> createTask() {
					return new Task<Void>() {
						@Override
						protected Void call() {
							mc = new MediaControl(EmbeddedMediaPlayer.createMediaPlayer(EmbeddedMediaPlayer.MEDIA_URL));
							slides[1] = mc;
							return null;
						}
					};
				}
			};

			playerService.setOnSucceeded(t -> {
				start();
			});

		}

		public StackPane getRoot() {
			return root;
		}

		// The method I am running in my class

		public void start() {

			FadeTransition fadeIn[] = new FadeTransition[4];
			PauseTransition stayOn[] = new PauseTransition[4];
			FadeTransition fadeOut[] = new FadeTransition[4];
			int i = 0;
			for (Node slide : slides) {
				final int idx = i;

				fadeIn[idx] = getFadeTransition(slide, 0.0, 1.0, 2000);

				Duration duration = Duration.millis(2000);
				if (slide == mc) { // harus diganti dengan isVideo
					duration = Duration.INDEFINITE;
				}

				stayOn[idx] = new PauseTransition(duration);

				fadeIn[idx].setOnFinished(value -> {

					stayOn[idx].play();
					if (slide == mc) { // harus diganti dengan isVideo
						mc.getMediaPlayer().setStartTime(Duration.ZERO);
						mc.getMediaPlayer().seek(Duration.ZERO);
						mc.getMediaPlayer().play();
					}
				});

				fadeOut[idx] = getFadeTransition(slide, 1.0, 0.0, 2000);

				if (slide == mc) { // harus diganti dengan isVideo
					mc.getMediaPlayer().setOnEndOfMedia(new Runnable() {

						@Override
						public void run() {
							stayOn[idx].stop();
							fadeOut[idx].play();
						}
					});
				} else {
					stayOn[idx].setOnFinished(value -> {
						fadeOut[idx].play();
					});
				}

				if (idx+1 < slides.length)
					fadeOut[idx].setOnFinished(value->{
						fadeIn[idx+1].play();
					});
				else if (idx == slides.length-1) {
					i = 0;
					fadeOut[idx].setOnFinished(value->{
						fadeIn[0].play();
					});
				}

				slide.setOpacity(0);
				this.root.getChildren().add(slide);

				i++;
			}
			fadeIn[0].play();
		}

		// the method in the Transition helper class:

		public FadeTransition getFadeTransition(Node imageView, double fromValue, double toValue,
				int durationInMilliseconds) {

			FadeTransition ft = new FadeTransition(Duration.millis(durationInMilliseconds), imageView);
			ft.setFromValue(fromValue);
			ft.setToValue(toValue);

			return ft;

		}

	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		SimpleSlideShow simpleSlideShow = new SimpleSlideShow();
		Scene scene = new Scene(simpleSlideShow.getRoot());
		primaryStage.setScene(scene);

		primaryStage.show();
		simpleSlideShow.playerService.start();
	}
}