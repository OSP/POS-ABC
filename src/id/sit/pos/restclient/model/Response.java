package id.sit.pos.restclient.model;

import com.google.gson.annotations.Expose;

public class Response {
	@Expose(serialize = true, deserialize = true)
	private boolean status;

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
}
