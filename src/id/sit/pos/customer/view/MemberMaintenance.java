package id.sit.pos.customer.view;

import java.time.ZoneId;
import java.util.Date;

import id.r.engine.controller.RDao;
import id.r.engine.model.SearchParam;
import id.r.engine.view.RBrowser;
import id.r.engine.view.RCrud;
import id.r.engine.view.RCrudChooser;
import id.r.engine.view.RForm;
import id.sit.pos.MainApp;
import id.sit.pos.common.Gender;
import id.sit.pos.config.Config;
import id.sit.pos.customer.model.Member;
import id.sit.pos.util.CurrencyUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;

public class MemberMaintenance extends RCrudChooser<Member> {
	@FXML
	private TextField scMemberNo;

	@FXML
	private TextField scName;

	@FXML
	private ComboBox<String> scGender;

	@FXML
	private TextField scPhoneNo;

	@FXML
	private TextField scAddress;

	@FXML
	private TableColumn<Member, Integer> idClm;

	@FXML
	private TableColumn<Member, Integer> nameClm;

	@FXML
	private TableColumn<Member, Integer> birthDateClm;

	@FXML
	private TableColumn<Member, Integer> genderClm;

	@FXML
	private TableColumn<Member, Integer> emailClm;

	@FXML
	private TableColumn<Member, Integer> phoneNoClm;

	@FXML
	private TableColumn<Member, Integer> addressClm;

	@FXML
	private TableColumn<Member, String> levelClm;

	@FXML
	private TextField memberNo;

	@FXML
	private TextField name;

	@FXML
	private DatePicker birthDate;

	@FXML
	private ComboBox<Gender> gender;

	@FXML
	private TextField email;

	@FXML
	private TextField phoneNo;

	@FXML
	private TextArea address;

	@FXML
	private TextField city;

	@FXML
	private TextField postcode;

	@FXML
	private Label registerDate;

	@FXML
	private Label level;

	@FXML
	private Label balance;

	@FXML
	private Button saveChooseBtn;

	@FXML
	private Button transactionHistoryBtn;

	public MemberMaintenance() {
		getIcons().add(new Image(Config.getLogoPath()));
		setTitle(Config.getMessage("customer_title"));
	}

	@FXML
	public void initialize() {
		super.initialize();
		idClm.setCellValueFactory(new PropertyValueFactory<Member, Integer>("id"));
		nameClm.setCellValueFactory(new PropertyValueFactory<Member, Integer>("name"));
		birthDateClm.setCellValueFactory(new PropertyValueFactory<Member, Integer>("birthDate"));
		genderClm.setCellValueFactory(new PropertyValueFactory<Member, Integer>("gender"));
		emailClm.setCellValueFactory(new PropertyValueFactory<Member, Integer>("email"));
		phoneNoClm.setCellValueFactory(new PropertyValueFactory<Member, Integer>("phoneNo"));
		addressClm.setCellValueFactory(new PropertyValueFactory<Member, Integer>("address"));
		levelClm.setCellValueFactory(new PropertyValueFactory<Member, String>("level"));

		// fields initialization
		Gender genders[] = Gender.values();
		Gender male = genders[0];
		Gender female = genders[1];
		this.gender.getItems().addAll(male, female);

		setAsSearchCriteriaFields(scMemberNo, scName, scGender, scPhoneNo, scAddress);
		setAsFormFields(memberNo, name, birthDate, gender, email, phoneNo, address, city, postcode, balance, level);
		setAsNameFields(name, city);
		setAsNumericFields(phoneNo, postcode);
		setAsAddressFields(address);

		//transaction history
		if (Config.isAbc()) {
			transactionHistoryBtn.setDisable(true);
			transactionHistoryBtn.setManaged(false);
		}else{
			transactionHistoryBtn.setVisible(false);
		}

		// screen mode initialization
		setMode(RForm.Mode.VIEW);
		search();
	}

	@Override
	protected RDao<Member> initRDao() {
		return new RDao<Member>(Member.class);
	}

	@Override
	protected Member initModel() {
		return new Member();
	}

	@Override
	public SearchParam[] getSearchParams() {
		return new SearchParam[] { new SearchParam(Member.class, Member._MEMBER_NO, scMemberNo.getText()),
				new SearchParam(Member.class, Member._NAME, scName.getText()),
				new SearchParam(Member.class, Member._PHONE_NO, scPhoneNo.getText()),
				new SearchParam(Member.class, Member._ADDRESS, scAddress.getText()) };
	}

	@Override
	protected void updateForm(Member model) {
		memberNo.setText(model.getMemberNo());
		name.setText(model.getName());
		if (model.getBirthDate() != null)
			birthDate.setValue(model.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		gender.getSelectionModel().select(model.getGender());
		email
		.setText(model.getEmail());
		phoneNo.setText(model.getPhoneNo());
		if (model.getLevel() != null)
			level.setText(model.getLevel().getLevel().toString());
		address.setText(model.getAddress());
		city.setText(model.getCity());
		postcode.setText(model.getPostcode());
		if (model.getBalance() != null)
			balance.setText(CurrencyUtil.format(model.getBalance()));
		if (model.getLevel() != null)
			level.setText(model.getLevel().getDesc());
	}

	@Override
	public Member fetchFormData() {
		model.setMemberNo(memberNo.getText());
		model.setName(name.getText());
		if (birthDate.getValue() != null)
			model.setBirthDate(Date.from(birthDate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
		if (gender.getSelectionModel().getSelectedItem() == Gender.Male)
			model.setGender(Gender.Male);
		else
			model.setGender(Gender.Female);
		model.setEmail(email.getText());
		model.setPhoneNo(phoneNo.getText());
		model.setAddress(address.getText());
		model.setCity(city.getText());
		model.setPostcode(postcode.getText());
		return model;
	}

	@Override
	public void edit() {
		super.edit();
		if (Config.isAbc()) {
			if (transactionHistoryBtn != null)
				transactionHistoryBtn.setDisable(true);
		}
	}

	public void afterSave() {
		super.afterSave();
		if (Config.isAbc()) {
			if (transactionHistoryBtn != null)
				transactionHistoryBtn.setDisable(false);
		}
	}

	@FXML
	public void showTransactionHistory() {
		RBrowser browser = new RBrowser();
		browser.setUrl(Config.getWebviewUrlTransactionHistory() + "?member_id=" + getSelected().getId());
		MainApp.getInstance().createModalWindow(browser).showAndWait();
	}
}
