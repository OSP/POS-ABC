package id.sit.pos.view;

import java.sql.SQLException;

import com.j256.ormlite.dao.CloseableIterator;

import id.r.engine.controller.RDao;
import id.r.engine.model.SearchParam;
import id.r.engine.view.RForm;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.master.model.Warehouse;
import id.sit.pos.model.Setting;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;

public class SettingForm extends RForm<Setting>{
	@FXML
    private ComboBox<Warehouse> branch;

	@FXML
    private TextField machineName;

	@FXML
    private Label message;

	private RDao<Warehouse> warehouseDao = new RDao<>(Warehouse.class);

	public SettingForm() {
		if (SettingCtrl.getSetting() == null)
			setTitle(Config.getMessage("setting_first_title"));
		else
			setTitle(Config.getMessage("setting_edit_title"));

		getIcons().add(new Image(Config.getLogoPath()));
	}

	@FXML
	public void initialize(){
		super.initialize();
		setAsFormFields(branch, machineName);
		setAsAlphanumericFields(machineName);
		try {
			CloseableIterator<Warehouse> iterator = warehouseDao.search(new SearchParam[]{
					new SearchParam(Warehouse.class, Warehouse._TYPE, Warehouse.Type.Branch)
			});
			iterator.forEachRemaining(w->{
				branch.getItems().add(w);
			});

			if (branch.getItems().size() == 1) {
				branch.getSelectionModel().select(0);
			}
		} catch (SQLException e) {
			logger.error(e.toString(), e);
			MainApp.showAlertDbError();
		}
//		updateModel(SettingCtrl.getSetting());
		setMode(Mode.EDIT);

		getStage().setOnCloseRequest(r->{
			cancel();
		});
	}


	@Override
	public void cancel(){
		super.cancel();
		if (model.getId() == null) {
			model = null;
		}
		SettingCtrl.setSetting(model);
		getStage().close();
	}

	@Override
	public RDao<Setting> initRDao() {
		return new RDao<>(Setting.class);
	}

	@Override
	public Setting initModel() {
		Setting setting = SettingCtrl.getSetting();
		if (setting == null)
			setting = SettingCtrl.getNewInstance();
		return setting;
	}

	@Override
	public void updateForm(Setting model) {
		branch.getSelectionModel().select(model.getStore());
		if (model.getMachineId() == null)
			model.setMachineId(SettingCtrl.generateMachineId());
		machineName.setText(model.getMachineName());
	}

	@Override
	public Setting fetchFormData() {
		model.setBranch(branch.getSelectionModel().getSelectedItem());
		if (model.getMachineId()==null)
			model.setMachineId(SettingCtrl.generateMachineId());
		model.setMachineName(machineName.getText());
		return model;
	}

	@Override
	public void resetForm() {
		branch.getSelectionModel().clearSelection();
		machineName.setText(null);
	}

	@Override
	public void afterSave(){
		super.afterSave();
		SettingCtrl.setSetting(model);
		(new Config()).LoadSettingFromDB();
	}
}
