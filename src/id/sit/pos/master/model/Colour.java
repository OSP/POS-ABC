package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;


@DatabaseTable(tableName = "colours")
public class Colour extends RModel{
	@DatabaseField
	private String code;

	@DatabaseField
	private String name;

	public Colour() {
	}

	public Colour(Integer id){
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public Date getUpdatedAt() {
//		return updatedAt;
//	}
//
//	public void setUpdatedAt(Date updatedAt) {
//		this.updatedAt = updatedAt;
//	}

//	public Dao<Colour, Integer> getDao() throws SQLException {
//		if (dao==null) {
//			dao=DaoManagerImpl.getColourDao();
//		}
//		return dao;
//	}
}
