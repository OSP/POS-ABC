package id.sit.pos.master.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "m_classes")
public class MClass extends RModel {
	public static final String _NAME = "name";
	public static final String _PATH_ID = "path_id";
	public static final String _PATH_CODE = "path_code";

	@DatabaseField
	private String code;

	@DatabaseField
	private String name;

	@DatabaseField
	private int level;

	@DatabaseField (columnName = _PATH_ID)
	private String pathId;

	@DatabaseField (columnName = _PATH_CODE)
	private String pathCode;


	@ForeignCollectionField(foreignFieldName = "parent")
	private ForeignCollection<MClass> childs;

	@DatabaseField(columnName="parent_id", foreign = true)
	private MClass parent;


	public MClass(){
	}

	public ForeignCollection<MClass> getChilds() {
		return childs;
	}

	public void setChilds(ForeignCollection<MClass> childs) {
		this.childs = childs;
	}

	public MClass getParent() {
		return parent;
	}

	public void setParent(MClass parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getPathId() {
		return pathId;
	}

	public void setPathId(String pathId) {
		this.pathId = pathId;
	}

	public String getPathCode() {
		return pathCode;
	}

	public void setPathCode(String pathCode) {
		this.pathCode = pathCode;
	}


}
