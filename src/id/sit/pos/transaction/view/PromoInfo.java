package id.sit.pos.transaction.view;

import id.sit.pos.master.model.PromoPrizeItem;

import java.math.BigDecimal;

import com.j256.ormlite.dao.CloseableIterator;

import id.r.engine.view.RUi;
import id.sit.pos.config.Config;
import id.sit.pos.master.model.Promo;
import id.sit.pos.master.model.PromoItem;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.DateUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;

public class PromoInfo extends RUi {
	@FXML
	private Label promoDesc;

	@FXML
	private Label department;

	@FXML
	private Label mClass;

	@FXML
	private Label brand;

	@FXML
	private Label article;

	@FXML
	private Label barcode;


	@FXML
	private Label minTransactionAmt;

	@FXML
	private Label minQty;

	@FXML
	private Label validFrom;

	@FXML
	private Label validUntil;

	@FXML
	private Label isMultiple;

	@FXML
	private Label discountPct;

	@FXML
	private Label productName;

	@FXML
	private TableView<PromoPrizeItem> table;

	@FXML
	private TableColumn<PromoPrizeItem, String> departmentClm;

	@FXML
	private TableColumn<PromoPrizeItem, String> mClassClm;

	@FXML
	private TableColumn<PromoPrizeItem, String> brandClm;

	@FXML
	private TableColumn<PromoPrizeItem, String> articleClm;

	@FXML
	private TableColumn<PromoPrizeItem, BigDecimal> barcodeClm;

	@FXML
	private TableColumn<PromoPrizeItem, String> qtyPrizeClm;

	private ObservableList<PromoPrizeItem> tableData = FXCollections.observableArrayList();

	private Promo model;

	private PromoItem promoItem;

	private PromoPrizeItem prizeItem;

	public PromoInfo() {

	}

	@FXML
	public void initialize() {
		setTitle("Promotion Info");
		getIcons().add(new Image(Config.getLogoPath()));

		departmentClm.setCellValueFactory(new PropertyValueFactory<PromoPrizeItem, String>("department"));
		mClassClm.setCellValueFactory(new PropertyValueFactory<PromoPrizeItem, String>("mClass"));
		brandClm.setCellValueFactory(new PropertyValueFactory<PromoPrizeItem, String>("brand"));
		articleClm.setCellValueFactory(new PropertyValueFactory<PromoPrizeItem, String>("article"));
		barcodeClm.setCellValueFactory(new PropertyValueFactory<PromoPrizeItem, BigDecimal>("barcode"));

		table.setItems(tableData);
		table.setMouseTransparent(true);

		displayData();
	}

	@FXML
	public void ok() {
		getStage().close();
	}

	public void displayData() {
		if (model != null) {
			promoDesc.setText(model.getName());
			if (promoItem.getProduct().getDepartment() != null)
				department.setText(promoItem.getProduct().getDepartment().getName());
			if (promoItem.getProduct().getMClass() != null)
				mClass.setText(promoItem.getProduct().getMClass().getName());
			if (promoItem.getProduct().getBrand() != null)
				brand.setText(promoItem.getProduct().getBrand().getName());
			if (promoItem.getProduct().getShortName() != null)
				productName.setText(promoItem.getProduct().getShortName());
			article.setText(promoItem.getProduct().getArticle());
			barcode.setText(promoItem.getProduct().getBarcode());
			if (promoItem.getMinQty() != null)
				minQty.setText(promoItem.getMinQty().toString());


			if (model.getMinTransactionAmt() != null)
				minTransactionAmt.setText(CurrencyUtil.format(model.getMinTransactionAmt()));
			if (model.getMinQty() != null)
				minQty.setText(model.getMinQty().toString());
			if (model.getValidFrom() != null)
				validFrom.setText(DateUtil.format(model.getValidFrom()));
			if (model.getValidUntil() != null)
				validUntil.setText(DateUtil.format(model.getValidUntil()));
			if (model.isMulti() != null)
				isMultiple.setText(model.isMulti().toString());
			if (model.getDiscPct() != null)
				discountPct.setText(model.getDiscPct().toString());

			if (model.getPrizeItems() != null) {
				CloseableIterator<PromoPrizeItem> iterator = model.getPrizeItems().closeableIterator();
				while (iterator.hasNext()) {
					tableData.add(iterator.next());
				}
				iterator.closeQuietly();
			}
			if (prizeItem != null) {
				table.getSelectionModel().select(prizeItem);
			}
		}
	}

	public Promo getModel() {
		return model;
	}

	public void setModel(Promo model) {
		this.model = model;
	}

	public void setModel(PromoItem item) {
		promoItem = item;
		this.model = item.getPromo();
	}

	public void setModel(PromoPrizeItem prize) {
		prizeItem = prize;
		this.model = prize.getPromo();
	}

	public PromoItem getPromoItem() {
		return promoItem;
	}

	public void setPromoItem(PromoItem promoItem) {
		this.promoItem = promoItem;
	}
}
