package id.sit.pos.user.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "roles")
public class Role extends RModel {
	public static final String CASHIER = "Kasir";
	public static final String CASHIER_HEAD = "Kepala Kasir";
	public static final String STORE_MANAGER = "Kepala Toko";
	public static final String CEO = "CEO";
	public static final String SUPER_ADMIN = "Super Admin";
	public static final String SUPER_USER = "Super User";

	public static final String _NAME = "name";

	@DatabaseField
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
