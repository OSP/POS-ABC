package id.sit.pos.util;

import org.apache.log4j.Logger;

import id.sit.pos.config.Config;
import jpos.JposException;
import jpos.POSPrinter;
import jpos.POSPrinterConst;
import jpos.events.StatusUpdateEvent;
import jpos.events.StatusUpdateListener;

public class POSPrintUtil implements POSPrinterConst, StatusUpdateListener {
	private POSPrinter printer;
	private final char temp1B[] = { 0x1B };
	private static Logger logger = Logger.getLogger(POSPrintUtil.class.getName());

	public void startDevice() {
		try {
			printer = new POSPrinter();
			printer.open(Config.getAdvancedPrinterType());
			printer.addStatusUpdateListener(this);
			printer.claim(1000);
			printer.setDeviceEnabled(true);

			logger.info("POSPrinter: Device Started");
		} catch(JposException e){
        	logger.error(e.toString(), e);
        	stopDevice();
        }
	}

	public void stopDevice() {
		try {
			printer.removeStatusUpdateListener(this);
			printer.close();

			logger.info("POSPrinter: Device Stop");
		} catch(JposException e){
        	logger.error(e.toString(), e);
        }
    }

	public void print(String s) {
		try {
			printer.printNormal(PTR_S_RECEIPT, s);
			printer.printNormal(PTR_S_RECEIPT, String.valueOf(temp1B) + "|100fP");
		} catch(JposException e){
        	logger.error(e.toString(), e);
        	stopDevice();
        }

		logger.info("POSPrinter: Printing");
	};

	@Override
	public void statusUpdateOccurred(StatusUpdateEvent arg0) {
		// TODO Auto-generated method stub

	}
}

