package id.r.engine.view;

import id.r.engine.model.RModel;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableRow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public abstract class RCrudChooser<T extends RModel> extends RCrud<T> {

	@FXML
	private Button chooseBtn;

	@Override
	public void initialize() {
		super.initialize();
		chooseBtn.defaultButtonProperty().bind(chooseBtn.focusedProperty());
		chooseBtn.disableProperty().bind(saveBtn.disabledProperty());
	}

	@Override
	public void rowMouseEvent(MouseEvent event, TableRow<T> row) {
		super.rowMouseEvent(event, row);
		if (event.getClickCount() == 2 && (!row.isEmpty())) {
			select(row.getItem());
			choose();
		}
	}

	@Override
	public void rowKeyEvent(KeyEvent event, T item) {
		super.rowKeyEvent(event, item);
		if (event.getCode() == KeyCode.E) {
			edit();
		} else if (event.getCode() == KeyCode.ENTER) {
			choose();
		} else if (event.getCode() == KeyCode.DELETE) {
			delete();
		}
	}

	@Override
	public void setMode(Mode mode) {
		super.setMode(mode);
		if (mode == Mode.VIEW) {
			chooseBtn.setText("Select");
		} else if (mode == Mode.EDIT) {
			chooseBtn.setText("Save & Select");
		}
	}

	@FXML
	public void choose() {
		if (getMode() == RForm.Mode.EDIT) {
			save();
			select(model);
		} else {
			T item = table.getSelectionModel().getSelectedItem();
			select(item);
		}
		getStage().close();
	}
}
