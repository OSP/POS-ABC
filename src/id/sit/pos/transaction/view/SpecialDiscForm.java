package id.sit.pos.transaction.view;

import java.math.BigDecimal;
import java.math.RoundingMode;

import id.r.engine.view.RUi;
import id.sit.pos.MainApp;
import id.sit.pos.config.Config;
import id.sit.pos.transaction.model.TransactionItem;
import id.sit.pos.util.CurrencyUtil;
import id.sit.pos.util.StringUtil;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;

public class SpecialDiscForm extends RUi {
	@FXML
	private GridPane discountAbcGp;

	@FXML
	private ToggleGroup group1;

	@FXML
	private RadioButton disc1Rb;

	@FXML
	private Label disc1Amt;

	@FXML
	private RadioButton disc2Rb;

	@FXML
	private Label disc2Amt;

	@FXML
	private RadioButton disc3Rb;

	@FXML
	private Label disc3Amt;

	@FXML
	private RadioButton customDiscRb;

	@FXML
	private TextField customDiscPct;

	@FXML
	private TextField customDiscAmt;

	@FXML
	private RadioButton finalPriceRb;

	@FXML
	private TextField finalPrice;

	private TransactionItem item;

	private BigDecimal finalDiscAmt;
	private BigDecimal finalPriceAmt;

	@FXML
	private Button saveBtn;

	@FXML
	private Button cancelBtn;

	public SpecialDiscForm() {
		setTitle("Special Discount Per Item");
	}

	@FXML
	public void initialize() {
		super.initialize();

		if (!Config.isAbc()) {
			discountAbcGp.setVisible(false);
			discountAbcGp.setManaged(false);
		}

		disc1Rb.setUserData(1);
		disc2Rb.setUserData(2);
		customDiscRb.setUserData(3);
		setAsCurrencyFields(customDiscAmt, finalPrice);
		finalPriceRb.setUserData(4);

		customDiscPct.focusedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue,
					Boolean newPropertyValue) {
				if (newPropertyValue) { // on focus
					customDiscAmt.setText(null);
				} else { // out focus
					if (!StringUtil.isEmpty(customDiscPct.getText()))
						updateFinalPrice(new Float(customDiscPct.getText()));
				}
			}
		});

		customDiscAmt.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue)-> {
				if (newPropertyValue) { // on focus
					customDiscPct.setText(null);
				} else { // out focus
					if(!StringUtil.isEmpty(customDiscAmt.getText())){
						updateFinalPrice(new BigDecimal(customDiscAmt.getText()));
					}
				}
		});

		group1.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
			public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
				if (group1.getSelectedToggle() != null) {
					switch ((Integer) group1.getSelectedToggle().getUserData()) {
					case 1:
						updateFinalPrice(item.getProduct().getPriceMemberRetail());
						setCustomDiscDisabled(true);
						break;
					case 2:
						updateFinalPrice(item.getProduct().getPriceMemberCredit());
						setCustomDiscDisabled(true);
						break;
					case 3:
						setCustomDiscDisabled(false);
						customDiscPct.requestFocus();
						finalPrice.setDisable(true);
						break;
					case 4:
						setCustomDiscDisabled(true);
						finalPrice.setDisable(false);
						finalPrice.requestFocus();
						break;
					}
				}
			}
		});

		group1.selectToggle(finalPriceRb);
		saveBtn.defaultButtonProperty().bind(saveBtn.focusedProperty());
		cancelBtn.defaultButtonProperty().bind(cancelBtn.focusedProperty());
	}

	@FXML
	public void save() {
		if ((Integer) group1.getSelectedToggle().getUserData() != 4) {
			if (customDiscPct.isFocused())
				updateFinalPrice(new Float(customDiscPct.getText()));
			else
				updateFinalPrice(CurrencyUtil.parse(customDiscAmt.getText()));
		} else {
			finalPriceAmt = CurrencyUtil.parse(finalPrice.getText());
		}
		if (!Config.isEnableLossPrice()) {
			if (item.getSku().getConvertion() == 1) {
				if (finalPriceAmt.compareTo(item.getProduct().getPriceModal()) < 0) {
					MainApp.showAlertError("trans_spec_disc_error");
					return;
				}
			} else {
				if (finalPriceAmt.compareTo(item.getProduct().getPriceModal().multiply(new BigDecimal(item.getSku().getConvertion()))) < 0) {
					MainApp.showAlertError("trans_spec_disc_error");
					return;
				}
			}
		}
		getStage().close();
	}

	@FXML
	public void cancel() {
		finalDiscAmt = null;
		finalPriceAmt = null;
		getStage().close();
	}

	public void updateFinalPrice(Float discPct) {
		BigDecimal initPrice = item.getInitPrice();
		finalDiscAmt = initPrice.multiply(new BigDecimal(discPct)).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
		customDiscAmt.setText(CurrencyUtil.format(finalDiscAmt, false));
		finalPriceAmt = initPrice.subtract(finalDiscAmt);
		finalPrice.setText(CurrencyUtil.format(finalPriceAmt, false));
	}

	public void updateFinalPrice(BigDecimal discAmt) {
		BigDecimal initPrice = item.getInitPrice();
		finalDiscAmt = discAmt;
		BigDecimal discPct = discAmt.divide(initPrice, 2, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
		customDiscPct.setText(discPct.toString());
		finalPriceAmt = initPrice.subtract(finalDiscAmt);
		finalPrice.setText(CurrencyUtil.format(finalPriceAmt, false));
	}

	public void setCustomDiscDisabled(boolean isDisabled) {
		customDiscPct.setDisable(isDisabled);
		customDiscAmt.setDisable(isDisabled);
	}

	public TransactionItem getItem() {
		return item;
	}

	public void setItem(TransactionItem item) {
		this.item = item;
	}

	public BigDecimal getFinalPriceAmt() {
		return finalPriceAmt;
	}

	public void setFinalPriceAmt(BigDecimal finalPriceAmt) {
		this.finalPriceAmt = finalPriceAmt;
	}
}