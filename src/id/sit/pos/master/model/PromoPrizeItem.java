package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@DatabaseTable(tableName = "prize_lists")
public class PromoPrizeItem extends RModel{
	public static final String _PRODUCT_ID = "product_id";
	public static final String _PROMO_ID = "promotion_id";
	public static final String _MAX_QTY = "max_qty";

	@DatabaseField(columnName = _PROMO_ID, foreign = true)
	public Promo promo;

	@DatabaseField(columnName = _PRODUCT_ID, foreign = true, foreignAutoRefresh = true)
	public Product product;

	@DatabaseField(columnName = _MAX_QTY)
	private Integer maxQty;

	public PromoItem promoItem;

	private StringProperty departmentProperty = new SimpleStringProperty();

	private StringProperty mClassProperty = new SimpleStringProperty();

	private StringProperty brandProperty = new SimpleStringProperty();

	private StringProperty articleProperty = new SimpleStringProperty();

	private StringProperty barcodeProperty = new SimpleStringProperty();

	private IntegerProperty minQtyProperty = new SimpleIntegerProperty();

	public StringProperty departmentProperty() {
		if (getProduct().getDepartment() != null)
			departmentProperty.set(getProduct().getDepartment().getName());
		else
			departmentProperty.set("");
		return departmentProperty;
	}

	public StringProperty mClassProperty() {
		if (getProduct().getMClass() != null)
			mClassProperty.set(getProduct().getMClass().getName());
		else
			mClassProperty.set("");
		return mClassProperty;
	}

	public StringProperty brandProperty() {
		if (getProduct().getBrand()!= null)
			brandProperty.set(getProduct().getBrand().getName());
		else
			brandProperty.set("");
		return brandProperty;
	}

	public StringProperty articleProperty() {
		if (getProduct().getArticle() != null)
			articleProperty.set(getProduct().getArticle());
		else
			articleProperty.set("");
		return articleProperty;
	}

	public StringProperty barcodeProperty() {
		if (getProduct().getBarcode() != null)
			barcodeProperty.set(getProduct().getBarcode());
		else
			barcodeProperty.set("");
		return barcodeProperty;
	}

	public IntegerProperty minQtyProperty() {
		if(getMaxQty()!=null)
			minQtyProperty.set(getMaxQty());
		else
			minQtyProperty.set(0);
		return minQtyProperty;
	}

	public Promo getPromo() {
		return promo;
	}

	public void setPromo(Promo promo) {
		this.promo = promo;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
		departmentProperty();
		mClassProperty();
		brandProperty();
		articleProperty();
		barcodeProperty();
	}

	public Integer getMaxQty() {
		return maxQty;
	}

	public void setMaxQty(Integer minQty) {
		minQtyProperty();
		this.maxQty = minQty;
	}

	public PromoItem getPromoItem() {
		return promoItem;
	}

	public void setPromoItem(PromoItem promoItem) {
		this.promoItem = promoItem;
	}
}
