package id.sit.pos.master.controller;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.controller.RDao;
import id.r.engine.model.RModel;
import id.r.engine.model.SearchParam;
import id.r.engine.model.SearchParam.Operator;
import id.sit.pos.config.Config;
import id.sit.pos.controller.SettingCtrl;
import id.sit.pos.db.DaoManagerImpl;
import id.sit.pos.master.model.Brand;
import id.sit.pos.master.model.Colour;
import id.sit.pos.master.model.MClass;
import id.sit.pos.master.model.Product;
import id.sit.pos.master.model.ProductBranchSetting;
import id.sit.pos.master.model.ProductPrice;
import id.sit.pos.master.model.ProductPriceDetail;
import id.sit.pos.master.model.ProductSize;
import id.sit.pos.master.model.ProductStock;
import id.sit.pos.master.model.ProductStructure;
import id.sit.pos.master.model.PromoPrizeItem;
import id.sit.pos.master.model.Size;
import id.sit.pos.master.model.Sku;
import id.sit.pos.master.model.Unit;
import id.sit.pos.master.model.Warehouse;
import id.sit.pos.transaction.model.Transaction;
import id.sit.pos.transaction.model.TransactionItem;
import id.sit.pos.util.DateUtil;
import id.sit.pos.util.StringUtil;

public class ProductCtrl {
	private static Logger logger = Logger.getLogger(ProductCtrl.class.getName());

	private RDao<MClass> mClassRDao = new RDao<>(MClass.class);
	private RDao<Brand> brandRDao = new RDao<>(Brand.class);
	private RDao<Colour> colourRDao = new RDao<>(Colour.class);
	private RDao<Size> sizeRDao = new RDao<Size>(Size.class);
	private RDao<Product> productRDao = new RDao<Product>(Product.class);
	private RDao<ProductSize> productSizeRDao = new RDao<ProductSize>(ProductSize.class);
	private RDao<ProductStructure> productStructureRDao = new RDao<ProductStructure>(ProductStructure.class);
	private RDao<ProductStock> productStockRDao = new RDao<ProductStock>(ProductStock.class);
	private RDao<ProductBranchSetting> productBranchSettingRDao = new RDao<>(ProductBranchSetting.class);
	private RDao<ProductPrice> productPriceRDao = new RDao<>(ProductPrice.class);
	private RDao<ProductPriceDetail> productPriceDetailRDao = new RDao<>(ProductPriceDetail.class);
	private RDao<Sku> skuRDao = new RDao<>(Sku.class);
	private RDao<PromoPrizeItem> promoPrizeItemRDao = new RDao<>(PromoPrizeItem.class);

	@Deprecated
	public Product getProduct(String barcode, int branchId) throws SQLException {
		Product product = null;
		CloseableIterator<Product> iterator = null;
		CloseableIterator<Sku> iteratorSku = null;

		if (Config.isAbc()){
			iterator = productRDao.search(new SearchParam[] {
				new SearchParam(Product.class, Product._BARCODE, barcode),
				new SearchParam(ProductStock.class, ProductStock._WAREHOUSE_ID, branchId, new Class<?>[] { ProductSize.class }) });

			if (iterator.hasNext())
				product = iterator.next();

			List<ProductBranchSetting> branchSettings = productBranchSettingRDao.getDao().queryForMatching(new ProductBranchSetting(product, SettingCtrl.getSetting().getStore()));
			if (!branchSettings.isEmpty()) {
				ProductBranchSetting branchSetting = branchSettings.get(0);

				if (branchSetting.getMarginPct() != null) {
					BigDecimal marginPct = new BigDecimal(branchSetting.getMarginPct());
					product.setPriceTag(product.getPriceTag().add(product.getPriceTag().multiply(marginPct)));
					product.setPriceRetail(product.getPriceRetail().add(product.getPriceRetail().multiply(marginPct)));
					product.setPriceMemberRetail(product.getPriceMemberRetail().add(product.getPriceMemberRetail().multiply(marginPct)));
					product.setPriceMemberCredit(product.getPriceMemberCredit().add(product.getPriceMemberCredit().multiply(marginPct)));
				} else if (branchSetting.getMarginAmt() != null){
					BigDecimal marginAmt = branchSetting.getMarginAmt();
					product.setPriceTag(product.getPriceTag().add(marginAmt));
					product.setPriceRetail(product.getPriceRetail().add(marginAmt));
					product.setPriceMemberRetail(product.getPriceMemberRetail().add(marginAmt));
					product.setPriceMemberCredit(product.getPriceMemberCredit().add(marginAmt));
				}
			}
		} else {
			iteratorSku = skuRDao.search(new SearchParam[] {
					new SearchParam(Sku.class, Sku._BARCODE, barcode, Operator.E),
					new SearchParam(ProductStock.class, ProductStock._WAREHOUSE_ID, branchId, new Class<?>[] { Product.class }) });
			if (iteratorSku.hasNext()){
				Sku sku = iteratorSku.next();
				product = sku.getProduct();
				product.setSku(sku);
				ProductPriceDetail ppd = getPrice(product);
				if (ppd != null && ppd.getProductPrice() != null) {
					productPriceRDao.getDao().refresh(ppd.getProductPrice());
					product.setPriceModal(ppd.getProductPrice().getHpp());
					product.setPriceTag(ppd.getPrice());
				}
				refreshForAdd(product, false);
			}
		}
		if (iterator != null)
			iterator.closeQuietly();
		if (iteratorSku != null)
			iteratorSku.closeQuietly();
		return product;
	}

	/**
	 * Get list of product which match with parameter
	 * @param productName
	 * @param barcode
	 * @param article
	 * @param size
	 * @param colorCode
	 * @param transactionNo
	 * @return
	 * @throws SQLException
	 */
	public CloseableIterator<Product> getProducts(String productName, String barcode, String article, String size, String colorCode, String transactionNo) throws SQLException {
		int storeId = SettingCtrl.getSetting().getStoreId();
		StringBuilder query = new StringBuilder();
		if (Config.isAbc()) {
			query.append("SELECT p."+Product._ID+", p."+Product._ARTICLE+", p."+Product._BARCODE+", p."+Product._SHORT_NAME+", pd."+ProductStock._ID+" product_detail_id, ps."+ProductSize._ID+" product_size_id, ps."+ProductSize._SIZE_ID+", sd."+Size._NAME+", pd."+ProductStock._AVAILABLE_QTY);
			query.append(" FROM "+Product.class.getAnnotation(DatabaseTable.class).tableName()+" p");
			query.append(" INNER JOIN "+ProductSize.class.getAnnotation(DatabaseTable.class).tableName()+" ps ON ps."+ProductSize._PRODUCT_ID+" = p.id");
			query.append(" INNER JOIN "+ProductStock.class.getAnnotation(DatabaseTable.class).tableName()+" pd ON pd."+ProductStock._PRODUCT_SIZE_ID+" = ps.id");
			query.append(" INNER JOIN "+Size.class.getAnnotation(DatabaseTable.class).tableName()+" sd ON sd.id = ps."+ProductSize._SIZE_ID);
			query.append(" WHERE pd."+ProductStock._WAREHOUSE_ID+" = " + SettingCtrl.getSetting().getStoreId());
			query.append("SELECT p.id, p.article, p.barcode, pd.id product_detail_id, ");
			query.append("ps.id product_size_id, ps.size_detail_id, sd.size_number, pd.available_qty ");
			query.append("FROM products p ");
			query.append("INNER JOIN product_sizes ps ON ps.product_id = p.id ");
			query.append("INNER JOIN product_details pd ON pd.product_size_id = ps.id ");
			query.append("INNER JOIN size_details sd ON sd.id = ps.size_detail_id ");
			if (transactionNo != null) {
				query.append(" INNER JOIN sales_details sdt ON sdt."+TransactionItem._PRODUCT_SIZE_ID +" = ps.id");
				query.append(" INNER JOIN sales ss ON ss.id = sdt."+TransactionItem._TRANSACTION_ID);
			}
			query.append(" WHERE pd.warehouse_id = " + SettingCtrl.getSetting().getStoreId());
			if (!StringUtil.isEmpty(barcode))
			query.append("   AND p."+Product._BARCODE+" LIKE '"+ barcode + "'");
			query.append("   AND p.barcode LIKE '%"+ barcode + "%'");
			if (!StringUtil.isEmpty(article))
			query.append("   AND p."+Product._ARTICLE+" LIKE '" + article + "'");
			query.append("   AND p.article LIKE '%" + article + "%'");
			if (!StringUtil.isEmpty(colorCode))
			query.append("   AND p."+Product._COLOUR_CODE+" LIKE '" + colorCode + "'");
			if (!StringUtil.isEmpty(size))
			query.append("   AND sd."+Size._NAME+" LIKE '%" + size + "%'");
			query.append(" ORDER BY p."+Product._ARTICLE+" ASC, ps.id ASC");
			query.append("   AND sd.size_number LIKE '%" + size + "%'");
			if (transactionNo != null) {
				query.append("   AND ss."+Transaction._TRANSACTION_NO+" = '"+transactionNo+"'");
				query.append("   AND sdt."+TransactionItem._QTY+" > "+0);
			}
			query.append(" ORDER BY p.article ASC, ps.id ASC");
		} else {
			query.append(" SELECT p."+Product._ID+", p."+Product._ARTICLE+", s."+Sku._ID+" id_sku, s."+Sku._BARCODE+" barcode_sku, p."+Product._SHORT_NAME+", pd."+ProductStock._ID+" product_detail_id, pd."+ProductStock._AVAILABLE_QTY+", s."+Sku._COVERTION+", u."+Unit._SHORT_NAME+" unit_short_name, spd."+ProductPriceDetail._ID+" selling_price_details_id, sp."+ProductPrice._HPP+", spd."+ProductPriceDetail._PRICE);
			query.append(" FROM "+Product.class.getAnnotation(DatabaseTable.class).tableName()+" p");
			query.append(" INNER JOIN "+ProductStock.class.getAnnotation(DatabaseTable.class).tableName()+" pd ON pd."+ProductStock._PRODUCT_ID+" = p.id");
			query.append(" INNER JOIN "+Sku.class.getAnnotation(DatabaseTable.class).tableName()+" s on s."+Sku._PRODUCT_ID+" = p.id");
			query.append(" INNER JOIN "+Unit.class.getAnnotation(DatabaseTable.class).tableName()+" u on s."+Sku._UNIT_ID+" = u.id");
			query.append(" INNER JOIN "+ProductPrice.class.getAnnotation(DatabaseTable.class).tableName()+" sp on sp."+ProductPrice._PRODUCT_ID+" = p.id ");
			query.append(" INNER JOIN "+ProductPriceDetail.class.getAnnotation(DatabaseTable.class).tableName()+" spd on spd."+ProductPriceDetail._PRODUCT_PRICE_ID+" = sp.id");

			// prevent duplicate article in product browser
			query.append(" AND spd.sku_id = s.id");
			if (transactionNo != null) {
				query.append(" INNER JOIN "+TransactionItem.class.getAnnotation(DatabaseTable.class).tableName()+" sd ON sd."+TransactionItem._PRODUCT_ID+" = p.id");
				query.append(" INNER JOIN "+Transaction.class.getAnnotation(DatabaseTable.class).tableName()+" ss ON ss.id = sd."+TransactionItem._TRANSACTION_ID);
			}
			query.append(" WHERE true ");
			// filter by product status (active or deactive)
			// query.append(" AND p.status1 = true");
			query.append(" AND LOWER(p.status1) = LOWER('"+ Product.ActiveStatus.ACTIVE +"')");
//			query.append(" AND s."+Sku._COVERTION+" IS NOT NULL");
			if (!StringUtil.isEmpty(barcode))
				query.append("   AND s."+Sku._BARCODE+" LIKE '"+ barcode + "'");
			if (!StringUtil.isEmpty(article))
				query.append("   AND p."+Product._ARTICLE+" LIKE '" + article + "'");
			if (!StringUtil.isEmpty(productName))
				query.append("   AND lower(p."+Product._SHORT_NAME+") LIKE lower('%" + productName + "%')");
			query.append("   AND pd."+ProductStock._WAREHOUSE_ID+" = " + storeId);
			query.append("   AND sp."+ProductPrice._BRANCH_ID+" = " + storeId);
			query.append("   AND '" + DateUtil.format4Db(new Date()) + "'::date between sp."+ProductPrice._START_DATE+" AND sp."+ProductPrice._END_DATE);
			if (transactionNo != null) {
				query.append("   AND ss."+Transaction._TRANSACTION_NO+" = '"+transactionNo+"'");
				query.append("   AND sd."+TransactionItem._QTY+" > "+0);
			}
			query.append(" ORDER BY p."+Product._ARTICLE+" ASC");
		}

		GenericRawResults<Product> rawResults = productRDao.getDao().queryRaw(query.toString(),
				new RawRowMapper<Product>() {

			@Override
			public Product mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				Product product = new Product();
				int i = 0;
				while (i < columnNames.length) {
					String value = resultColumns[i];
					if (value != null) {
						switch (columnNames[i]) {
						case Product._ID:
							product.setId(new Integer(value));
							break;
						case Product._ARTICLE:
							product.setArticle(value);
							break;
						case Product._BARCODE:
							product.setBarcode(value);
							break;
						case "id_sku":
							product.setSku(new Sku(new Integer(value)));
							product.getSku().setBarcode(value);
							break;
						case "barcode_sku":
							product.getSku().setBarcode(value);
							break;
						case Product._SHORT_NAME:
							product.setShortName(value);
							break;
						case "cost_of_products":
							product.setPriceModal(new BigDecimal(value));
							break;
						case Product._COLOUR_CODE:
							product.setColourCode(value);
							break;
						case "product_size_id":
							product.setProductSize(new ProductSize(new Integer(value)));
							product.getProductSize().setProduct(product);
							break;
						case "product_detail_id":
							product.setProductStock(new ProductStock(new Integer(value)));
							product.getProductStock().setProduct(product);
							product.getProductStock().setProductSize(product.getProductSize());
							break;
						case ProductStock._AVAILABLE_QTY:
							product.getProductStock().setAvailableQty(new Integer(value));
							break;
						case "size_detail_id":
							product.getProductSize().setSize(new Size(new Integer(value)));
							break;
						case "size_number":
							product.getProductSize().getSize().setSizeNumber(value);
							break;
						case "unit_short_name":
							product.getSku().setUnit(new Unit());
							product.getSku().getUnit().setShortName(value);
							break;
						case Sku._COVERTION:
							product.getSku().setConvertion(new Integer(value));
							break;
						case ProductPrice._HPP:
							product.setPriceModal(new BigDecimal(value));
							break;
						case ProductPriceDetail._PRICE:
							product.setPriceTag(new BigDecimal(value));
							break;

						case RModel._CREATED_AT:
							try {
								product.setCreatedAt(DateUtil.parseDbDate(value));
							} catch (ParseException e) {
								logger.error(e.toString(), e);
							}
							break;
						case RModel._UPDATED_AT:
							try {
								product.setUpdatedAt(DateUtil.parseDbDate(value));
							} catch (ParseException e) {
								logger.error(e.toString(), e);
							}
							break;
						default:
							break;
						}
					}
					i++;
				}
				return product;
			}
		});

		return rawResults.closeableIterator();
	}

	/**
	 * Get product by barcode. First matching product will be returned
	 * @param barcode
	 * @return
	 * @throws SQLException
	 */
	public Product getProduct(String barcode) throws SQLException {
		Product product = null;
		CloseableIterator<Product> iterator = getProducts(null, barcode, null, null, null, null);
		if (iterator.hasNext()) {
			product = iterator.next();
			iterator.closeQuietly();
		}
		return product;
	}

	@Deprecated
	public Product getSoldProduct(String barcode, int branchId, String transNo) throws SQLException {
		CloseableIterator<Product> iterator = productRDao.search(new SearchParam[] {
				new SearchParam(Product.class, Product._BARCODE, barcode, Operator.E, new Class<?>[] { TransactionItem.class }),
				new SearchParam(Transaction.class, Transaction._TRANSACTION_NO,transNo)
				});
		Product product = null;
		if (iterator.hasNext()) {
			product = iterator.next();
			iterator.closeQuietly();
		}

		return product;
	}

	/**
	 * Get product from transaction record
	 * @param barcode
	 * @param transNo
	 * @return
	 * @throws SQLException
	 */
	public Product getSoldProduct(String barcode, String transNo) throws SQLException {
		Product product = null;
		CloseableIterator<Product> iterator = getProducts(null, barcode, null, null, null, transNo);
		if (iterator.hasNext()) {
			product = iterator.next();
			iterator.closeQuietly();
		}
		return product;
	}

	/**
	 * Get stock of Product
	 * Fashion Only
	 * @param productId
	 * @return
	 * @throws SQLException
	 */
	public List<ProductStock> getStocks(Integer productId) throws SQLException {
		return getStocks(productId, SettingCtrl.getSetting().getStoreId());
	}

	/**
	 * Get stock of Product
	 * Fashion Only
	 * @param productId
	 * @return
	 * @throws SQLException
	 */
	public List<ProductStock> getStocks(Integer productId, Integer storeId) throws SQLException {
		Dao<ProductStock, Integer> promoItemDao = DaoManagerImpl.getProductStockDao();
		String query = "SELECT pd.*, sd.size_number"
				+ " FROM product_details pd"
				+ " INNER JOIN product_sizes ps ON ps.id = pd.product_size_id"
				+ " INNER JOIN size_details sd ON sd.id = ps.size_detail_id"
				+ " WHERE ps.product_id = " + productId
				+ " AND pd.warehouse_id = " + storeId
				+ " ORDER BY sd.size_number ASC";


		GenericRawResults<ProductStock> rawResults = promoItemDao.queryRaw(query, new RawRowMapper<ProductStock>(){

			@Override
			public ProductStock mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				ProductStock prductStock = new ProductStock();
				int i = 0;
				while (i<columnNames.length) {
					String value = resultColumns[i];
					if (value != null) {
					switch (columnNames[i]) {
					case ProductStock._ID:
						prductStock.setId(new Integer(value));
						break;
					case ProductStock._PRODUCT_SIZE_ID:
						prductStock.setProductSize(new ProductSize(new Integer(value)));
						break;
					case ProductStock._WAREHOUSE_ID:
						prductStock.setWarehouse(new Warehouse(new Integer(value)));
						break;
					case ProductStock._ALLOCATED_QTY:
						prductStock.setAllocatedQty(new Integer(value));;
						break;
					case ProductStock._AVAILABLE_QTY:
						prductStock.setAvailableQty(new Integer(value));
						break;
					case ProductStock._DEFECT_QTY:
						prductStock.setDefectQty(new Integer(value));
						break;
					case ProductStock._FREEZE_QTY:
						prductStock.setFreezeQty(new Integer(value));
						break;
					case ProductStock._MIN_QTY:
						prductStock.setMinStock(new Integer(value));
						break;
					case ProductStock._ONLINE_QTY:
						prductStock.setOnlineQty(new Integer(value));
						break;
					case ProductStock._REJECT_QTY:
						prductStock.setRejectQty(new Integer(value));
						break;
					case RModel._CREATED_AT:
						try {
							prductStock.setCreatedAt(DateUtil.parseDbDate(value));
						} catch (ParseException e) {
							logger.error(e.toString(), e);
						}
						break;
					case RModel._UPDATED_AT:
						try {
							prductStock.setUpdatedAt(DateUtil.parseDbDate(value));
						} catch (ParseException e) {
							logger.error(e.toString(), e);
						}
						break;
					default:
						break;
					}
					}
					i++;
				}
				return prductStock;
			}

		});

		List<ProductStock> productStocks = null;
		try {
			productStocks = rawResults.getResults();
			rawResults.close();
		} catch (Exception e) {
			logger.error(e.toString(), e);
		}

		return productStocks;
	}

	/**
	 * Get stock of Product
	 * Retail Only
	 * @param productId
	 * @return
	 * @throws SQLException
	 */
	public ProductStock getStock(Sku sku) throws SQLException {
		Dao<ProductStock, Integer> promoItemDao = DaoManagerImpl.getProductStockDao();
		String query = "SELECT ps.*"
				+ " FROM "+ProductStock.class.getAnnotation(DatabaseTable.class).tableName()+" ps"
				+ " WHERE ps."+Product._ID+" = " + sku.getProduct().getId()
				+ "   AND ps."+ProductStock._SKU_ID+" = " + sku.getId()
				+ "   AND ps."+ProductStock._WAREHOUSE_ID+" = " + SettingCtrl.getSetting().getStoreId();


		GenericRawResults<ProductStock> rawResults = promoItemDao.queryRaw(query, new RawRowMapper<ProductStock>(){

			@Override
			public ProductStock mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				ProductStock prductStock = new ProductStock();
				int i = 0;
				while (i<columnNames.length) {
					String value = resultColumns[i];
					if (value != null) {
					switch (columnNames[i]) {
					case ProductStock._ID:
						prductStock.setId(new Integer(value));
						break;
					case ProductStock._WAREHOUSE_ID:
						prductStock.setWarehouse(new Warehouse(new Integer(value)));
						break;
					case ProductStock._MIN_QTY:
						prductStock.setMinStock(new Integer(value));
						break;
					case ProductStock._ROP_QTY:
						prductStock.setRopStock(new Integer(value));
						break;
					case ProductStock._MAX_QTY:
						prductStock.setMaxStock(new Integer(value));
						break;
					case ProductStock._AVAILABLE_QTY:
						prductStock.setAvailableQty(new Integer(value));
						break;
					case ProductStock._DEFECT_QTY:
						prductStock.setDefectQty(new Integer(value));
						break;
					case ProductStock._ONLINE_QTY:
						prductStock.setOnlineQty(new Integer(value));
						break;
					case ProductStock._REJECT_QTY:
						prductStock.setRejectQty(new Integer(value));
						break;
					case RModel._CREATED_AT:
						try {
							prductStock.setCreatedAt(DateUtil.parseDbDate(value));
						} catch (ParseException e) {
							logger.error(e.toString(), e);
						}
						break;
					case RModel._UPDATED_AT:
						try {
							prductStock.setUpdatedAt(DateUtil.parseDbDate(value));
						} catch (ParseException e) {
							logger.error(e.toString(), e);
						}
						break;
					default:
						break;
					}
					}
					i++;
				}
				return prductStock;
			}

		});

		return rawResults.getFirstResult();
	}

	/**
	 * Get available qty of product
	 * Retail Only
	 * @param product
	 * @return
	 * @throws SQLException
	 */
	public int getAvailableQty(Product product) throws SQLException {
		Sku sku = product.getSku();
		List<ProductStructure> productStructures = getProductStructures(sku);
		List<Integer> availableQtys = new ArrayList<Integer>();
		for (ProductStructure prodStructure: productStructures) {
			ProductStock stock = getStock(prodStructure.getSku());
			availableQtys.add((stock.getAvailableQty()/prodStructure.getSku().getConvertion())/prodStructure.getConvertion());
		}

		return availableQtys.stream()
			.min((p1, p2) -> Integer.compare(p1, p2))
			.get();
	}

	/**
	 * Deduct stock of promo product
	 * Retail Only
	 * @param product
	 * @param qty
	 * @throws SQLException
	 */
	public void deductStockPromo(Product product, int qty)throws SQLException{
	// deduct stock by promo start here
		for(PromoPrizeItem ppi: (new PromoCtrl()).getItemPrize(product)){
			ppi.getProduct().getId();
			ProductStock itemPrizeStock = productStockRDao.queryFirst(new SearchParam[]{
					new SearchParam(Product.class, Product._ID, ppi.getProduct().getId()),
					new SearchParam(ProductStock._WAREHOUSE_ID, SettingCtrl.getSetting().getStoreId())
			});

			DaoManagerImpl.getPromoDao().refresh(ppi.getPromo());
			// promo berlaku kelipatan
			if(ppi.getPromo().isMulti()){
				itemPrizeStock.setAvailableQty(itemPrizeStock.getAvailableQty() - ((qty / (new Integer(ppi.getPromo().getMinQty().intValue())) * ppi.getMaxQty())));
			} else {
				itemPrizeStock.setAvailableQty(itemPrizeStock.getAvailableQty() - ppi.getMaxQty());
			}
			productStockRDao.save(itemPrizeStock);
		}
	}

	/**
	 * Deduct stock of product. Only work for non promo product. for promo product please use
	 * {@link #deductStockPromo(Product, int)}
	 * Retail Only
	 * @param product
	 * @param qty
	 * @throws SQLException
	 */
//	public void deductStock(Sku sku, int qty) throws SQLException {
	public void deductStock(Sku sku, Product prod, int qty) throws SQLException {
		ProductStock stock = null;
		Product product = null;

		if (sku != null){
			skuRDao.getDao().refresh(sku);
			productRDao.getDao().refresh(sku.getProduct());
			product = sku.getProduct();
		}

		if (prod != null){
			product = prod;
			productRDao.getDao().refresh(product);
		}


		/* Cek Available Qty (Stock) */
		if(Config.isAbc()){
			stock = productStockRDao.getDao().queryForId(product.getProductStock().getId());
		} else {
			stock = productStockRDao.queryFirst(new SearchParam[]{
				new SearchParam(Product.class, Product._ID, product.getId()),
				new SearchParam(ProductStock._WAREHOUSE_ID, SettingCtrl.getSetting().getStoreId())
			});
		}

		if (Config.isAbc()) {
			stock.setAvailableQty(stock.getAvailableQty() - qty);
		} else {
			// default status is false
			if (!product.isComposite()) { // if false
				stock.setAvailableQty(stock.getAvailableQty() - (qty * sku.getConvertion()));
			} else {
				if (stock.isAssembled()) { //default is_assembled = true
					stock.setAvailableQty(stock.getAvailableQty() - (qty * sku.getConvertion()));
				} else { // if products has child, deduct child stock

					for(ProductStructure productStructure: getProductStructures(sku)) {
						productStructure.getSku().getConvertion();
						ProductStock componentStock = productStockRDao.queryFirst(new SearchParam[]{
							new SearchParam(Product.class, Product._ID, productStructure.getSku().getProduct().getId()),
							new SearchParam(ProductStock._WAREHOUSE_ID, SettingCtrl.getSetting().getStoreId())
						});
						componentStock.setAvailableQty(componentStock.getAvailableQty() - ((productStructure.getConvertion() * productStructure.getSku().getConvertion())* qty));
						productStockRDao.save(componentStock);
					}
				}
			}
		}
		productStockRDao.save(stock);
	}

	/**
	 * Get structure of product
	 * Retail Only
	 * @param sku
	 * @return
	 * @throws SQLException
	 */
	public List<ProductStructure> getProductStructures(Sku sku) throws SQLException {
		String query = "SELECT ps."+ProductStructure._CONVERTION+", s."+Sku._PRODUCT_ID+", s."+Sku._COVERTION
				+ " FROM "+Sku.class.getAnnotation(DatabaseTable.class).tableName()+" s"
				+ " INNER JOIN "+ProductStructure.class.getAnnotation(DatabaseTable.class).tableName()+" ps"
				+ " 		ON ps."+ProductStructure._SKU_ID+" = s."+Sku._ID
				+ " WHERE ps."+ProductStructure._PARENT_SKU_ID+" = " + sku.getId();

		GenericRawResults<ProductStructure> rawResults = productStructureRDao.getDao().queryRaw(query, new RawRowMapper<ProductStructure>(){

			@Override
			public ProductStructure mapRow(String[] columnNames, String[] resultColumns) throws SQLException {
				ProductStructure prductStructure = new ProductStructure();
				int i = 0;
				while (i<columnNames.length) {
					String value = resultColumns[i];
					if (value != null) {
						switch (columnNames[i]) {
						case ProductStructure._ID:
							prductStructure.setId(new Integer(value));
							break;
						case ProductStructure._SKU_ID:
							prductStructure.setSku(new Sku(new Integer(value)));
							break;
						case ProductStructure._CONVERTION:
							prductStructure.setConvertion(new Integer(value));
							break;
						case Sku._PRODUCT_ID:
							prductStructure.setSku(new Sku(new Integer(value)));
							prductStructure.getSku().setProduct(new Product(new Integer(value)));
							break;
						case Sku._COVERTION:
							prductStructure.getSku().setConvertion(new Integer(value));
							break;
						default:
							break;
						}
					}
					i++;
				}
				return prductStructure;
			}

		});

		return rawResults.getResults();
	}

	/**
	 * Get current price of product
	 * Retail Only
	 * @param product
	 * @return
	 * @throws SQLException
	 */
	public ProductPriceDetail getPrice(Product product) throws SQLException {
		return getPrice(product, SettingCtrl.getBranchId());
	}

	/**
	 * Get current price of product
	 * Retail Only
	 * @param product
	 * @param storeId
	 * @return
	 * @throws SQLException
	 */
	public ProductPriceDetail getPrice(Product product, Integer storeId) throws SQLException {
		return productPriceDetailRDao.queryFirst(new SearchParam[] {
				new SearchParam(ProductPriceDetail._SKU_ID, product.getSku().getId()),
				new SearchParam(ProductPrice.class, ProductPrice._OFFICE_ID, storeId),
				new SearchParam(ProductPrice.class, ProductPrice._START_DATE, new Date(), Operator.LTE),
				new SearchParam(ProductPrice.class, ProductPrice._END_DATE, new Date(), Operator.GTE)
		});
	}

	/**
	 * Refresh product's attribute with value from database
	 * @param product
	 * @throws SQLException
	 */
	public void refreshFull(Product product) throws SQLException {
		productRDao.getDao().refresh(product);

		if(product.getSku() != null)
			skuRDao.getDao().refresh(product.getSku());

		if (product.getBrand() != null)
			brandRDao.getDao().refresh(product.getBrand());
		if (product.getDepartment() != null)
			mClassRDao.getDao().refresh(product.getDepartment());
		if (product.getMClass() != null)
			mClassRDao.getDao().refresh(product.getMClass());
		if (product.getColour1() != null)
			colourRDao.getDao().refresh(product.getColour1());
		if (product.getColour2() != null)
			colourRDao.getDao().refresh(product.getColour2());
		if (product.getColour3() != null)
			colourRDao.getDao().refresh(product.getColour3());
		if (product.getColour4() != null)
			colourRDao.getDao().refresh(product.getColour4());
	}

	/**
	 * Similar as {@link #refreshForAdd(Product)} but this method is ligther because only mandatory attibutes are refreshed
	 * @param product
	 * @throws SQLException
	 */
//	public void refreshForAdd(Product product) throws SQLException {
	public void refreshForAdd(Product product, Boolean isReturMode) throws SQLException {
		BigDecimal modal = product.getPriceModal();
		BigDecimal price = product.getPriceTag();

		productRDao.getDao().refresh(product);
//		skuRDao.getDao().refresh(product.getSku());
		product.setPriceModal(modal);
		product.setPriceTag(price);

		if (Config.isAbc()){
			if(!isReturMode)
				productStockRDao.getDao().refresh(product.getProductSize().getProductStock());
			else
				productStockRDao.getDao().refresh(product.getProductSize().getProductStock());
		} else {
			productStockRDao.getDao().refresh(product.getProductStock());
		}

		if (product.getProductSize() != null)
			sizeRDao.getDao().refresh(product.getProductSize().getSize());
	}

	public RDao<Product> getProductRDao() {
		return productRDao;
	}
	public void setProductRDao(RDao<Product> productDao) {
		this.productRDao = productDao;
	}
	public RDao<ProductSize> getProductSizeRDao() {
		return productSizeRDao;
	}
	public void setProductSizeRDao(RDao<ProductSize> productSizeDao) {
		this.productSizeRDao = productSizeDao;
	}
	public RDao<ProductStock> getProductStockRDao() {
		return productStockRDao;
	}
	public void setProductStockRDao(RDao<ProductStock> productStockDao) {
		this.productStockRDao = productStockDao;
	}

	public RDao<Sku> getSkuRDao() {
		return skuRDao;
	}

	public RDao<MClass> getMClassRDao() {
		return mClassRDao;
	}

	public RDao<PromoPrizeItem> getPromoPrizeItemRDao() {
		return promoPrizeItemRDao;
	}

	public void setPromoPrizeItemRDao(RDao<PromoPrizeItem> promoPrizeItemRDao) {
		this.promoPrizeItemRDao = promoPrizeItemRDao;
	}


}
