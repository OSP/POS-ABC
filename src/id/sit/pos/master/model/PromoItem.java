package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "promo_item_lists")
public class PromoItem extends RModel{
	public static final String _PRODUCT_ID = "product_id";
	public static final String _PROMO_ID = "promotion_id";
	public static final String _MIN_QTY = "min_qty";
	public static final String _REFERENCE_PRODUCT_ID = "reference_product_id";

	@DatabaseField(columnName = _PROMO_ID, foreign = true)
	public Promo promo;

	@DatabaseField(columnName = _PRODUCT_ID, foreign = true)
	public Product product;

	@DatabaseField(columnName = _MIN_QTY)
	private Integer minQty;

	@DatabaseField(columnName = _REFERENCE_PRODUCT_ID, foreign = true)
	public Product referenceProduct;

	public PromoItem() {

	}

	public PromoItem(Integer id, Promo promo, Product product, Integer minQty) {
		super();
		this.id = id;
		this.promo = promo;
		this.product = product;
		this.minQty = minQty;
	}

	public Promo getPromo() {
		return promo;
	}

	public void setPromo(Promo promo) {
		this.promo = promo;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getMinQty() {
		return minQty;
	}

	public void setMinQty(Integer minQty) {
		this.minQty = minQty;
	}

	public Product getReferenceProduct() {
		return referenceProduct;
	}

	public void setReferenceProduct(Product referenceProduct) {
		this.referenceProduct = referenceProduct;
	}


}