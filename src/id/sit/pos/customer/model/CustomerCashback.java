package id.sit.pos.customer.model;

import java.math.BigDecimal;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "cashbacks")
public class CustomerCashback extends RModel {
	public static final String _MEMBER_LEVEL = "member_level_id";
	public static final String _TRANSACTION_AMT = "transaction_amount";
	public static final String _CASHBACK_AMT = "cashback_amount";

	@DatabaseField(columnName = _MEMBER_LEVEL, foreign = true)
	private MemberLevel memberLevel;

	@DatabaseField(columnName = _TRANSACTION_AMT)
	private BigDecimal transactionAmt;

	@DatabaseField(columnName = _CASHBACK_AMT)
	private BigDecimal cashbackAmt;

	public CustomerCashback(){
		super();
	}

	public BigDecimal getTransactionAmt() {
		return transactionAmt;
	}

	public void setTransactionAmt(BigDecimal transactionAmt) {
		this.transactionAmt = transactionAmt;
	}

	public BigDecimal getCashbackAmt() {
		return cashbackAmt;
	}

	public void setCashbackAmt(BigDecimal cashbackAmt) {
		this.cashbackAmt = cashbackAmt;
	}

	public MemberLevel getMemberLevel() {
		return memberLevel;
	}

	public void setMemberLevel(MemberLevel memberLevel) {
		this.memberLevel = memberLevel;
	}
}
