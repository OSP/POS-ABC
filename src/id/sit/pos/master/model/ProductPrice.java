package id.sit.pos.master.model;

import java.math.BigDecimal;
import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;;

@DatabaseTable(tableName = "selling_prices")
public class ProductPrice extends RModel {
	public static final String _PRODUCT_ID = "product_id";
	public static final String _BRANCH_ID = "branch_id";
	public static final String _OFFICE_ID = "office_id";
	public static final String _START_DATE = "start_date";
	public static final String _END_DATE = "end_date";
	public static final String _MARGIN_PCT = "margin";
	public static final String _MARGIN_AMT = "margin_amount";
	public static final String _HPP = "hpp_average";
	public static final String _DPP = "dpp";
	public static final String _PPN_IN = "ppn_in";
	public static final String _PPN_OUT = "ppn_out";
	public static final String _SELLING_PRICE = "selling_price";
	public static final String _IS_ACTIVE = "is_active";

	@DatabaseField(columnName = _PRODUCT_ID, foreign = true)
	private Product product;

	@DatabaseField(columnName = _OFFICE_ID, foreign = true)
	private Warehouse office;

	@DatabaseField(columnName = _START_DATE)
	private Date startDate;

	@DatabaseField(columnName = _END_DATE)
	private Date endDate;

	@DatabaseField(columnName = _MARGIN_PCT)
	private Float marginPct;

	@DatabaseField(columnName = _MARGIN_AMT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal marginAmt;

	@DatabaseField(columnName = _HPP, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal hpp;

	@DatabaseField(columnName = _DPP, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal dpp;

	@DatabaseField(columnName = _PPN_IN, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal ppnIn;

	@DatabaseField(columnName = _PPN_OUT, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal ppnOut;

	@DatabaseField(columnName = _SELLING_PRICE, dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal sellingPrice;


	@DatabaseField(columnName = _IS_ACTIVE)
	private boolean isActive;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Warehouse getOffice() {
		return office;
	}

	public void setOffice(Warehouse office) {
		this.office = office;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Float getMarginPct() {
		return marginPct;
	}

	public void setMarginPct(Float marginPct) {
		this.marginPct = marginPct;
	}

	public BigDecimal getMarginAmt() {
		return marginAmt;
	}

	public void setMarginAmt(BigDecimal marginAmt) {
		this.marginAmt = marginAmt;
	}

	public BigDecimal getHpp() {
		return hpp;
	}

	public void setHpp(BigDecimal hpp) {
		this.hpp = hpp;
	}

	public BigDecimal getDpp() {
		return dpp;
	}

	public void setDpp(BigDecimal dpp) {
		this.dpp = dpp;
	}

	public BigDecimal getPpnIn() {
		return ppnIn;
	}

	public void setPpnIn(BigDecimal ppnIn) {
		this.ppnIn = ppnIn;
	}

	public BigDecimal getPpnOut() {
		return ppnOut;
	}

	public void setPpnOut(BigDecimal ppnOut) {
		this.ppnOut = ppnOut;
	}

	public BigDecimal getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(BigDecimal sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
}
