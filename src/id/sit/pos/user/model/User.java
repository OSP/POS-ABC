package id.sit.pos.user.model;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;
import id.sit.pos.master.model.Warehouse;

@DatabaseTable(tableName = "users")
public class User extends RModel {
	public static final String _USERNAME = "username";
	public static final String _PASSWORD_HASH = "pos_password";
	public static final String _FIRST_NAME = "first_name";
	public static final String _LAST_NAME = "last_name";
	public static final String _ROLE_ID = "role_id";
	public static final String _STORE_ID = "branch_id";

	@DatabaseField
	private String username;

	private String password;

	@DatabaseField(columnName = _PASSWORD_HASH)
	private String passwordHash;

	@DatabaseField(columnName = _FIRST_NAME)
	private String firstName;

	@DatabaseField(columnName = _LAST_NAME)
	private String lastName;

	@DatabaseField(columnName = _ROLE_ID, foreign = true, foreignAutoRefresh = true)
	private Role role;

	@DatabaseField(columnName = _STORE_ID, foreign = true)
	private Warehouse store;

	@DatabaseField(columnName = "last_sign_in_at")
	private Date lastLogin;

	public User(){

	}

	public User(Integer id){
		this.id = id;
	}

	public User(String username, String password, Role role) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
	}

	//Generated code
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Warehouse getStore() {
		return store;
	}

	public void setStore(Warehouse branch) {
		this.store = branch;
	}
}
