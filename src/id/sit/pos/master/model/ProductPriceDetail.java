package id.sit.pos.master.model;

import java.math.BigDecimal;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "selling_price_details")
public class ProductPriceDetail extends RModel{
	public static final String _PRODUCT_PRICE_ID = "selling_price_id";
	public static final String _SKU_ID = "sku_id";
	public static final String _PRICE = "price";

	@DatabaseField(columnName = _PRODUCT_PRICE_ID, foreign = true)
	ProductPrice productPrice;

	@DatabaseField(columnName = _SKU_ID, foreign = true)
	Sku sku;

	@DatabaseField(columnName = _PRICE)
	BigDecimal price;

	public ProductPriceDetail(){

	}

	public ProductPriceDetail(Integer id){
		this.id = id;
	}

	public ProductPrice getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(ProductPrice productPrice) {
		this.productPrice = productPrice;
	}

	public Sku getSku() {
		return sku;
	}

	public void setSku(Sku sku) {
		this.sku = sku;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
