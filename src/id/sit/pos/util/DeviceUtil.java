package id.sit.pos.util;

import id.sit.pos.config.Config;
import id.sit.pos.util.LineDisplayDriver;
//import jpos.LineDisplayConst;


public class DeviceUtil {
	private static MSRUtil msr = new MSRUtil();
	private static POSPrintUtil print = new POSPrintUtil();
	private static CashDrawerUtil drawer = new CashDrawerUtil();
	private static LineDisplayDriver ldd = new LineDisplayDriver();

	public static void initDevice() {
		if (Config.isEnableMsr()) {
			System.out.println("MSR Driver Loaded");
			Thread x1 = new Thread(new Runnable() {
				@Override
				public void run() {
					msr.startDevice();
				}
			});
			x1.start();
		}

		if (Config.isEnableAdvancedPrinting()){
			System.out.println("Using Printer : "+Config.getAdvancedPrinterType());
			Thread x2 = new Thread(new Runnable() {
				@Override
				public void run() {
					print.startDevice();
				}
			});
			x2.start();
			System.out.println("Advanced Printing Driver Loaded");
		}

		if (Config.isEnableCashDrawer()) {
			System.out.println("Cashdrawer Driver Loaded");
			Thread x3 = new Thread(new Runnable() {
				@Override
				public void run() {
					drawer.startDevice();
				}
			});
			x3.start();
		}

		if (Config.isEnableLineDisplay()){
//			Thread x4 = new Thread(new Runnable() {
//
//				@Override
//				public void run() {
					ldd.initDevice();
					System.out.println("Line Display Driver Loaded");
//				}
//			});
//			x4.start();
		}
	}

	public static void killDevice() {
		msr.stopDevice();
		print.stopDevice();
		drawer.stopDevice();
		ldd.stopDevice();
	}

	public static MSRUtil getMsrDevice() {
		return msr;
	}

	public static POSPrintUtil getPosPrintDevice() {
		return print;
	}

	public static CashDrawerUtil getCashDrawerDevice() {
		return drawer;
	}

	public static LineDisplayDriver getLineDisplayDevice() {
		return ldd;
	}


}
