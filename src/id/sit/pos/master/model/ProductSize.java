package id.sit.pos.master.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "product_sizes")
public class ProductSize extends RModel{
	public static final String _PRODUCT_ID = "product_id";
	public static final String _SIZE_ID = "size_detail_id";

	@DatabaseField(columnName = _PRODUCT_ID, foreign = true)
	private Product product;

	@DatabaseField(columnName = _SIZE_ID, foreign = true)
	private Size size;

	@ForeignCollectionField
	private ForeignCollection<ProductStock> stocks;

	private ProductStock productStock;

	public ProductSize(){

	}

	public ProductSize(Integer id){
		this.id = id;
	}

	@Override
	public String toString() {
		if (size != null)
			return size.getSizeNumber();
		else
			return "Deleted Size";
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Size getSize() {
		return size;
	}

	public void setSize(Size size) {
		this.size = size;
	}

	public ForeignCollection<ProductStock> getStocks() {
		return stocks;
	}

	public void setStocks(ForeignCollection<ProductStock> stocks) {
		this.stocks = stocks;
	}

	public ProductStock getProductStock() {
		return productStock;
	}

	public void setProductStock(ProductStock productStock) {
		this.productStock = productStock;
	}


}
