package id.sit.pos.transaction.model;

import java.math.BigDecimal;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "sales_payment_bank")
public class BankPayment extends RModel{

	public enum Type {
		CREDIT, DEBIT, TRANSFER
	}

	@DatabaseField(columnName = "type")
	private Type type;

	@DatabaseField(columnName = "bank_name")
	private String bankName;

	@DatabaseField(columnName = "account_number")
	private String accountNo;

	@DatabaseField(columnName = "account_name")
	private String accountName;

	@DatabaseField(columnName = "card_number")
	private String cardNumber;

	@DatabaseField(columnName = "extra_charge", dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal additionalCharge = BigDecimal.ZERO;

	@DatabaseField(columnName = "outstanding_amount", dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal toBePaidAmt;

	@DatabaseField(columnName = "payment_amt", dataType = DataType.BIG_DECIMAL_NUMERIC)
	private BigDecimal paymentAmt;

	public BankPayment() {

	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public BigDecimal getAdditionalCharge() {
		return additionalCharge;
	}

	public void setAdditionalCharge(BigDecimal additionalCharge) {
		this.additionalCharge = additionalCharge;
	}

	public BigDecimal getToBePaidAmt() {
		return toBePaidAmt;
	}

	public void setToBePaidAmt(BigDecimal toBePaidAmt) {
		this.toBePaidAmt = toBePaidAmt;
	}

	public BigDecimal getPaymentAmt() {
		return paymentAmt;
	}

	public void setPaymentAmt(BigDecimal paymentAmt) {
		this.paymentAmt = paymentAmt;
	}
}
