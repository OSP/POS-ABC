package id.sit.pos.transaction.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.sit.pos.master.model.EDC;

@DatabaseTable(tableName = "sales_payment_bank")
public class EdcPayment extends BankPayment{

	@DatabaseField(columnName = "edc_machine_id", foreign = true)
	private EDC edc;

	public EdcPayment() {

	}

	public EDC getEdc() {
		return edc;
	}

	public void setEdc(EDC edc) {
		this.edc = edc;
	}
}
