package id.r.engine.model;


public class SearchParam {

	public enum Order {
		ASC, DESC
	};

	public enum Operator {
		/**
		 * Add a '<' clause so the column must be less-than or equal the value.
		 */
		LT,
		/**
		 * Add a '<=' clause so the column must be less-than or equal the value.
		 */
		LTE,
		/**
		 * Add a '=' clause so the column must be equal the value.
		 */
		E,
		/**
		 * Add a '>=' clause so the column must be greater-than or equal the
		 * value.
		 */
		GTE,
		/**
		 * Add a '>=' clause so the column must be greater-than the value.
		 */
		GT,
		/**
		 * Add a '<>' clause so the column must be not equal the value.
		 */
		N,
		/**
		 * Add a 'like value' clause so the column value must be string and contain '%'.
		 */
		L,
		/**
		 * Parse operator from value if value is instance of string.
		 */
		S,
		/**
		 * Add a 'in' clause so the column must not equal with one of the value.
		 */
		I
	};

	private Class<?> model;

	private Class<?>[] bridges;

	private String columnName;

	private Object value;

	private Operator operator;

	public SearchParam() {
	}

	public SearchParam(String columnName, Object value) {
		this(null, columnName, value);
	}

	public SearchParam(String columnName, Object value, Operator operator) {
		this(null, columnName, value, operator);
	}

	public SearchParam(Class<?> model, String columnName, Object value) {
		super();
		this.model = model;
		this.columnName = columnName;
		this.value = value;
	}

	public SearchParam(Class<?> model, String columnName, Object value, Operator operator) {
		this(model, columnName, value);
		this.operator = operator;
	}

	public SearchParam(Class<?> model, String columnName, Object value, Class<?>[] bridges) {
		this(model, columnName, value);
		this.bridges = bridges;
	}

	public SearchParam(Class<?> model, String columnName, Object value, Operator operator, Class<?>[] bridges) {
		this(model, columnName, value);
		this.operator = operator;
		this.bridges = bridges;
	}

	public Class<?> getModel() {
		return model;
	}

	public void setModel(Class<?> model) {
		this.model = model;
	}

	public Class<?>[] getBridges() {
		return bridges;
	}

	public void setBridges(Class<?>[] bridges) {
		this.bridges = bridges;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator usingOperator) {
		this.operator = usingOperator;
	}
}
