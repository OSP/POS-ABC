package id.sit.pos.master.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;


@DatabaseTable(tableName = "sizes")
public class SizeCategory extends RModel {
	public static final String _NAME = "description";

	@DatabaseField(columnName = _NAME)
	private String name;

	@ForeignCollectionField
	private ForeignCollection<Size> sizes;

	public SizeCategory(){

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ForeignCollection<Size> getSizes() {
		return sizes;
	}

	public void setSizes(ForeignCollection<Size> sizes) {
		this.sizes = sizes;
	}
}
