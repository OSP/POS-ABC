package id.sit.pos.master.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import id.r.engine.model.RModel;

@DatabaseTable(tableName = "product_details")
public class ProductStock extends RModel{
	public static final String _PRODUCT_ID = "product_id";
	public static final String _SKU_ID = "sku_id";
	public static final String _WAREHOUSE_ID = "warehouse_id";
	public static final String _PRODUCT_SIZE_ID = "product_size_id";
	public static final String _MIN_QTY = "min_stock";
	public static final String _ROP_QTY = "rop_stock";
	public static final String _MAX_QTY = "max_stock";
	public static final String _AVAILABLE_QTY = "available_qty";
	public static final String _FREEZE_QTY = "freezed_qty";
	public static final String _DEFECT_QTY = "defect_qty";
	public static final String _REJECT_QTY = "rejected_qty";
	public static final String _ONLINE_QTY = "online_qty";
	public static final String _ALLOCATED_QTY = "allocated_qty";
	public static final String _IS_ASSEMBLED = "is_assembled";

	@DatabaseField(columnName = _PRODUCT_ID, foreign = true)
	private Product product;

	@DatabaseField(columnName = _SKU_ID, foreign = true)
	private Sku sku;

	@DatabaseField(columnName = _PRODUCT_SIZE_ID, foreign = true)
	private ProductSize productSize;

	@DatabaseField(columnName = _WAREHOUSE_ID, foreign = true)
	private Warehouse warehouse;

	@DatabaseField(columnName = _MIN_QTY)
	private int minStock;

	@DatabaseField(columnName = _ROP_QTY)
	private int ropStock;

	@DatabaseField(columnName = _MAX_QTY)
	private int maxStock;

	@DatabaseField(columnName = _AVAILABLE_QTY)
	private int availableQty;

	@DatabaseField(columnName = _FREEZE_QTY)
	private int freezeQty;

	@DatabaseField(columnName = _DEFECT_QTY)
	private int defectQty;

	@DatabaseField(columnName = _REJECT_QTY)
	private int rejectQty;

	@DatabaseField(columnName = _ONLINE_QTY)
	private int onlineQty;

	@DatabaseField(columnName = _ALLOCATED_QTY)
	private int allocatedQty;

	@DatabaseField(columnName = _IS_ASSEMBLED)
	protected boolean isAssembled;

	public ProductStock() {
	}

	public ProductStock(Integer integer){
		this.id = integer;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Sku getSku() {
		return sku;
	}

	public void setSku(Sku sku) {
		this.sku = sku;
	}

	public int getMinStock() {
		return minStock;
	}

	public void setMinStock(int minStock) {
		this.minStock = minStock;
	}

	public int getRopStock() {
		return ropStock;
	}

	public void setRopStock(int ropStock) {
		this.ropStock = ropStock;
	}

	public int getMaxStock() {
		return maxStock;
	}

	public void setMaxStock(int maxStock) {
		this.maxStock = maxStock;
	}

	public int getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(int stockavailable) {
		this.availableQty = stockavailable;
	}

	public int getFreezeQty() {
		return freezeQty;
	}

	public void setFreezeQty(int freezeQty) {
		this.freezeQty = freezeQty;
	}

	public int getDefectQty() {
		return defectQty;
	}

	public void setDefectQty(int defectQty) {
		this.defectQty = defectQty;
	}

	public int getRejectQty() {
		return rejectQty;
	}

	public void setRejectQty(int rejectQty) {
		this.rejectQty = rejectQty;
	}

	public int getOnlineQty() {
		return onlineQty;
	}

	public void setOnlineQty(int onlineQty) {
		this.onlineQty = onlineQty;
	}

	public int getAllocatedQty() {
		return allocatedQty;
	}

	public void setAllocatedQty(int allocatedQty) {
		this.allocatedQty = allocatedQty;
	}

	public ProductSize getProductSize() {
		return productSize;
	}

	public void setProductSize(ProductSize productSize) {
		this.productSize = productSize;
	}

	public Warehouse getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}

	public boolean isAssembled() {
		return isAssembled;
	}

	public void setAssembled(boolean isAssembled) {
		this.isAssembled = isAssembled;
	}
}
